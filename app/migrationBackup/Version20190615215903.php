<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190615215903 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE advertising CHANGE date date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ari_article CHANGE date date DATETIME NOT NULL');
        $this->addSql('DROP INDEX title ON article');
        $this->addSql('ALTER TABLE article CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE banner CHANGE date_final date_final DATETIME DEFAULT NULL, CHANGE date_initial date_initial DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE culture CHANGE date date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE feature CHANGE scheduled_to scheduled_to DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE installation CHANGE atualizadoEm atualizadoEm DATETIME DEFAULT NULL, CHANGE criadoEm criadoEm DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE push CHANGE data data DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE schedule CHANGE scheduled_to scheduled_to DATETIME NOT NULL');
        $this->addSql('ALTER TABLE slide CHANGE date_final date_final DATETIME DEFAULT NULL, CHANGE date_initial date_initial DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE urgent CHANGE end_date end_date DATETIME NOT NULL, CHANGE start_date start_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE usuario CHANGE criado criado DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE subscription CHANGE next_due_date next_due_date DATETIME DEFAULT \'1970-01-01\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE advertising CHANGE date date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ari_article CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE article CHANGE date date DATETIME NOT NULL');
        $this->addSql('CREATE FULLTEXT INDEX title ON article (title)');
        $this->addSql('ALTER TABLE banner CHANGE date_initial date_initial DATETIME DEFAULT NULL, CHANGE date_final date_final DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE culture CHANGE date date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE feature CHANGE scheduled_to scheduled_to DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE installation CHANGE criadoEm criadoEm DATETIME DEFAULT NULL, CHANGE atualizadoEm atualizadoEm DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE push CHANGE data data DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE schedule CHANGE scheduled_to scheduled_to DATETIME NOT NULL');
        $this->addSql('ALTER TABLE slide CHANGE date_initial date_initial DATETIME DEFAULT NULL, CHANGE date_final date_final DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE subscription CHANGE next_due_date next_due_date DATETIME DEFAULT \'1970-01-01 00:00:00\'');
        $this->addSql('ALTER TABLE urgent CHANGE start_date start_date DATETIME NOT NULL, CHANGE end_date end_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE usuario CHANGE criado criado DATETIME DEFAULT NULL');
    }
}
