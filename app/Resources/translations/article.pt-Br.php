<?php
return array(
    "Sunday" => "Domingo",
    "Monday" => "Segunda",
    "Tuesday" => "Terça",
    "Wednesday" => "Quarta",
    "Thursday" => "Quinta",
    "Friday" => "Sexta",
    "Saturday" => "Sábado",
    "Jan" => "janeiro",
    "Feb" => "fevereiro",
    "Mar" => "março",
    "Apr" => "abril",
    "May" => "maio",
    "Jun" => "junho",
    "Jul" => "julho",
    "Ago" => "agosto",
    "Sep" => "setembro",
    "Oct" => "outubro",
    "Nov" => "novembro",
    "Dez" => "dezembro"
);  