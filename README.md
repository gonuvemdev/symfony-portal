# Portal AZ

Solução desenvolvida em PHP e Symfony para o Portal AZ. Este projeto gerencia toda a parte visual do portal e do Backoffice também.

## O que está incluso no docker:

* PHP-FPM 5.6
* Apache2
* Mysql 8
* PhpMyAdmin

## Como rodar

Para usar o projeto para desenvolvimento, basta usar o comando abaixo:

```
docker-compose up
```
Isso irá levantar todos os containers necessários, que estão mapeadas para as seguintes portas:

* **Porta 81**: A aplicação principal
* **Porta 8080**: Está rodando o *PhpMyAdmin*
* **Porta 3306**: O banco de dados MySql

Após isso, é necessário instalar as depêndencias do projeto para isso execute o comando abaixo

```
docker exec -i sf4_php bash -c "cd sf4/ && php composer.phar install"
```

Ao acessar o projeto pelo **localhost:81**, isso irá acessar a versão de produção, com acesso ao banco de produção. Para ter acesso ao ambiente de *desenvolvimento*, basta acessar usando **localhost:81/app_dev.php**.

## Atualizando o schema do Banco de Dados

Basta usar o comando abaixo (Dentro do container com o código)
```
php bin/console doctrine:schema:update --force
```

## Configurações

O projeto já está todo configurado, mas é possivel trocar as configurações tanto no arquivo *docker-compose*, quanto nos arquivos de config que estão na pasta *app/config/*.

As configurações estão divididos por ambiente, então para cada ambiente, basta achar os arquivos *config_* e *parameters_* de acordo com o seu ambiente *(prod, dev, test, ...)*
