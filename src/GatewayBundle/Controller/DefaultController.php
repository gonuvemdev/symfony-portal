<?php

namespace GatewayBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use BG\BarcodeBundle\Util\Base1DBarcode as barCode;
use BG\BarcodeBundle\Util\Base2DBarcode as matrixCode;

use BackBundle\Entity\Venda;
use BackBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use mPDF;

class DefaultController extends Controller
{

    /**
     * @Route("/user/gateway/cupom/draw/{id}/{destino}", name="draw_cupom")
     */
    public function drawCupomVenda(Request $request, $id, $destino)
    {
        
        return '';
    }

}
