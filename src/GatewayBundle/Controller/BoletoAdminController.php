<?php

namespace GatewayBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BackBundle\Entity\Venda;
use BackBundle\Entity\Item;
use BackBundle\Entity\Cupom;

use BG\BarcodeBundle\Util\Base1DBarcode as barCode;
use BG\BarcodeBundle\Util\Base2DBarcode as matrixCode;

use mPDF;

/**
 * Categoria controller.
 *
 * @Route("/admin/gateway/boleto")
 */
class BoletoAdminController extends Controller
{
    
    /**
     * Show Gateway entity.
     *
     * @Route("/update/{id}", name="gateway_slip_update_admin")
     */
    public function updateAction(Request $request, $id)
    {

        

        return '';
    }

    /**
     * @Route("/chargeslip/draw/{id}/{destino}", name="draw_slip_admin")
     */
    public function drawSlipVenda(Request $request, $id, $destino)
    {
        

            return '';
    }
}
