<?php

namespace GatewayBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use BackBundle\Entity\Venda;
use BackBundle\Entity\Item;
use BackBundle\Entity\Cupom;

use BG\BarcodeBundle\Util\Base1DBarcode as barCode;
use BG\BarcodeBundle\Util\Base2DBarcode as matrixCode;

use mPDF;

/**
 * Cartao controller.
 *
 * @Route("/user/gateway")
 */
class CartaoUserController extends Controller
{

   

    /**
     * @Route("/token", name="gateway_token")
     */
    public function tokenVenda(Request $request)
    {

        return '';

    }


    /**
     * @Route("/cardfatura/{parcelas}", name="gateway_card_fatura")
     */
    public function newFatura(Request $request, $parcelas)
    {
        return '';
    }


    /**
     * @Route("/cardfatura/{parcelas}/edit/{id}", name="gateway_card_fatura_edit")
     */
    public function editFatura(Request $request, $parcelas, $id)
    {

        return '';
    }


    /**
     * @Route("/chargecard/{id}", name="gateway_card")
     */
    public function chargeCardVenda(Request $request, $id)
    {
        
        return '';

    }
}
