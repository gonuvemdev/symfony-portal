<?php

namespace GatewayBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use BG\BarcodeBundle\Util\Base1DBarcode as barCode;
use BG\BarcodeBundle\Util\Base2DBarcode as matrixCode;

use BackBundle\Entity\Venda;
use BackBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use mPDF;


/**
 * Gateway controller.
 *
 * @Route("/admin/gateway")
 */
class AdminController extends Controller
{

    /**
     * Get Conta info.
     *
     * @Route("/info", name="gateway_info")
     * @Method({"GET","POST"})
     */
    public function infoAction(Request $request)
    {

        return '';
    }

    /**
     * Get Conta info.
     *
     * @Route("/saque", name="gateway_saque")
     * @Method({"GET","POST"})
     */
    public function saqueAction(Request $request)
    {

        return '';
    }

    /**
     * Get Conta info.
     *
     * @Route("/antecipacao", name="gateway_antecipacao")
     * @Method({"GET","POST"})
     */
    public function antecipacaoAction(Request $request)
    {

        return '';
    }

    /**
     * Get Conta info.
     *
     * @Route("/refund/{invoice}", name="gateway_refund")
     * @Method({"GET","POST"})
     */
    public function refundAction(Request $request, $invoice)
    {

        return '';
    
    }

    /**
     * Lists all Gateway entities.
     *
     * @Route("/index", name="gateway_index")
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $request)
    {

        return '';
    }

    /**
     * Show Gateway entity.
     *
     * @Route("/show/{id}", name="gateway_show")
     * @Method("GET")
     */
    public function showAction($id)
    {

        return '';
    }

    /**
     * Show Gateway entity.
     *
     * @Route("/cancel/{id}", name="gateway_cancel")
     * @Method("GET")
     */
    public function cancelAction($id)
    {

        return '';
    }

    public function relatorioAction($faturas, $estatisticas)
    {

        return '';
    }

    /**
     * @Route("/chargeslip/draw/{id}/{destino}", name="draw_slip")
     */
    public function drawSlipVenda(Request $request, $id, $destino)
    {
        
        return '';
    }

    /**
     * Pdf existing Matricula entities.
     *
     * @Route("/report", name="gateway_report")
     * @Method("POST")
     */
    public function reportAction(Request $request)
    {
        return '';
    }
}
