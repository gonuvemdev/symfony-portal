<?php

namespace GatewayBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use BG\BarcodeBundle\Util\Base1DBarcode as barCode;
use BG\BarcodeBundle\Util\Base2DBarcode as matrixCode;

use BackBundle\Entity\Venda;
use BackBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use mPDF;

/**
 * Categoria controller.
 *
 * @Route("/admin/gateway/cartao")
 */
class CartaoAdminController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return '';
    }
}
