<?php

namespace GatewayBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BackBundle\Entity\Venda;
use BackBundle\Entity\Item;
use BackBundle\Entity\Cupom;

use BG\BarcodeBundle\Util\Base1DBarcode as barCode;
use BG\BarcodeBundle\Util\Base2DBarcode as matrixCode;

use mPDF;

/**
 * Cartao controller.
 *
 * @Route("/user/gateway")
 */
class BoletoUserController extends Controller
{


    /**
     * @Route("/chargeslip", name="gateway_slip")
     */
    public function chargeSlipVenda(Request $request)
    {

        return '';
    }

    /**
     * @Route("/chargeslip/draw/{id}/{destino}", name="draw_slip")
     */
    public function drawSlipVenda(Request $request, $id, $destino)
    {
        return '';
    }
}
