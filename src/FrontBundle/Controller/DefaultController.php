<?php

namespace FrontBundle\Controller;

use BackBundle\Filter\NewsgroupFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FrontBundle\Form\SearchType;
use BackBundle\Entity\Article;
use BackBundle\Entity\Slide;
use BackBundle\Entity\City;
use BackBundle\Entity\NewsGroup;

use ClientBundle\Entity\Client;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{
    
    /**
     * @Route("/", name="home")
	* @Method({"GET"})
     */
    public function indexAction()
    {
        //return $this->redirect($this->generateUrl('manutencao'));
        //return $this->redirect($this->generateUrl('eleicao'));

        $em = $this->getDoctrine()->getManager();

        // Recuperando o stats Service para recuperar materias de acordo com critérios estatisticos
        $statsService = $this->get('stats_service');

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));
        $dayOfWeek = $today->format('l');
        if ($dayOfWeek == 'Sunday') { $dayOfWeek = 'Domingo'; }
        if ($dayOfWeek == 'Monday') { $dayOfWeek = 'Segunda'; }
        if ($dayOfWeek == 'Tuesday') { $dayOfWeek = 'Terça'; }
        if ($dayOfWeek == 'Wednesday') { $dayOfWeek = 'Quarta'; }
        if ($dayOfWeek == 'Thursday') { $dayOfWeek = 'Quinta'; }
        if ($dayOfWeek == 'Friday') { $dayOfWeek = 'Sexta'; }
        if ($dayOfWeek == 'Saturday') { $dayOfWeek = 'Sábado'; }

        $day = $today->format('d');
        $year = $today->format('Y');
        $month = $today->format('M');
        if ($month == 'Jan') { $month = 'jan'; }
        if ($month == 'Feb') { $month = 'fev'; }
        if ($month == 'Mar') { $month = 'mar'; }
        if ($month == 'Apr') { $month = 'abr'; }
        if ($month == 'May') { $month = 'mai'; }
        if ($month == 'Jun') { $month = 'jun'; }
        if ($month == 'Jul') { $month = 'jul'; }
        if ($month == 'Ago') { $month = 'ago'; }
        if ($month == 'Sep') { $month = 'set'; }
        if ($month == 'Oct') { $month = 'out'; }
        if ($month == 'Nov') { $month = 'nov'; }
        if ($month == 'Dez') { $month = 'dez'; }

        $time = $today->format('G:i');

        /*$features = $em->getRepository('BackBundle:Feature')->findBy(
            array('enabled' => true),array('id' => 'DESC'),null,null
        );*/

        $queryUrgent = $em->getRepository('BackBundle:Urgent')
            ->createQueryBuilder('u')
            ->where('u.startDate <= :startDate')
            ->setParameter(':startDate', $today)
            ->andWhere('u.endDate >= :endDate')
            ->setParameter(':endDate', $today)
            ->getQuery();
        $urgentes = $queryUrgent->getResult();

        $principal1 = null;
        $principal2 = null;
        $secundario1 = null;
        $secundario2 = null;
        $secundario3ComFoto = null;
        $featureComFoto1 = null;
        $featureComFoto2 = null;
        $featureSemFoto1 = null;
        $featureSemFoto2 = null;

        $politicaPrincipal = null;
        $politicaSecundario = null;
        $politicaTerciario1 = null;
        $politicaTerciario2 = null;
        $politicaFoto1 = null;
        $politicaFoto2 = null;
        $policitaFrase1 = null;
        $policitaFrase2 = null;

        $entretePrincipal = null;
        $entreteSlide = null;
        $entreteFotoPequena = null;
        $entreteFrase = null;
        $entreteFoto1 = null;
        $entreteFoto2 = null;
        $entreteFoto3 = null;

        $esportePrincipal = null;
        $esporteSecundarioFoto = null;
        $esporteFotoHorizontal1 = null;
        $esporteFrase = null;
        $esporteFotoHorizontal2 = null;
        $esporteFotoGrande = null;
        $esporteFotoPequena = null;

        $municipioPrincipal = null;
        $municipioFoto1 = null;
        $municipioFoto2 = null;
        $municipioFrase1 = null;
        $municipioFrase2 = null;
        $municipioFotoHorizontal = null;
        $municipioFotoGrande = null;
        $municipioFraseCurta = null;  
        
        $queryLocation = $em->getRepository('BackBundle:Location')
        ->createQueryBuilder('l')
        ->orderBy('l.description', 'ASC')
        ->getQuery();
        $locations = $queryLocation->getResult();

        foreach ($locations as $location) {
            $queryFeature = $em->getRepository('BackBundle:Feature')
            ->createQueryBuilder('f')
            ->where('f.enabled = :enabled')
            ->setParameter(':enabled', true)
            /*->andWhere('f.scheduledTo <= :data')
            ->setParameter(':data', $today)
            ->andWhere('f.scheduled = :scheduled')
            ->setParameter(':scheduled', true)*/
            ->andWhere('f.location = :location')
            ->setParameter(':location', $location)
            ->setMaxResults(2)
            ->orderBy('f.scheduledTo', 'ASC')
            ->addOrderBy('f.id', 'ASC')
            ->getQuery();
            $features = $queryFeature->getResult();  

            foreach ($features as $feature) {
                $location = $feature->getLocation();
                    
                if ($location->getDescription() == 'Principal 1') { $principal1 = $feature; }
                if ($location->getDescription() == 'Principal 2') { $principal2 = $feature; }
                if ($location->getDescription() == 'Secundário 1') { $secundario1 = $feature; }
                if ($location->getDescription() == 'Secundário 2') { $secundario2 = $feature; }
                if ($location->getDescription() == 'Secundário 3 Foto') { $secundario3ComFoto = $feature; }
                if ($location->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 == null) { $featureComFoto1 = $feature; } }
                if ($location->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 != null) { if ($feature != $featureComFoto1) { $featureComFoto2 = $featureComFoto1; $featureComFoto1 = $feature; } } }
                if ($location->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 == null) { $featureSemFoto1 = $feature; } }
                if ($location->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 != null) { if ($feature != $featureSemFoto1) { $featureSemFoto2 = $featureSemFoto1; $featureSemFoto1 = $feature; } } }

                if ($location->getDescription() == 'Política Principal') { $politicaPrincipal = $feature; }
                if ($location->getDescription() == 'Política Secundário') { $politicaSecundario = $feature; }
                if ($location->getDescription() == 'Política Terciário') { if ($politicaTerciario1 == null) { $politicaTerciario1 = $feature; } }
                if ($location->getDescription() == 'Política Terciário') { if ($politicaTerciario1 != null) { if ($feature != $politicaTerciario1) { $politicaTerciario2 = $politicaTerciario1; $politicaTerciario1 = $feature; } } }
                if ($location->getDescription() == 'Política Foto 1') { $politicaFoto1 = $feature; }
                if ($location->getDescription() == 'Política Foto 2') { $politicaFoto2 = $feature; }
                if ($location->getDescription() == 'Política Frase') { if ($policitaFrase1 == null) { $policitaFrase1 = $feature; } }
                if ($location->getDescription() == 'Política Frase') { if ($policitaFrase1 != null) { if ($feature != $policitaFrase1) { $policitaFrase2 = $policitaFrase1; $policitaFrase1 = $feature; } } }

                if ($location->getDescription() == 'Entretê Principal') { $entretePrincipal = $feature; }
                if ($location->getDescription() == 'Entretê Slide') { $entreteSlide = $feature; }
                if ($location->getDescription() == 'Entretê Foto Pequena') { $entreteFotoPequena = $feature; }
                if ($location->getDescription() == 'Entretê Frase') { $entreteFrase = $feature; }
                if ($location->getDescription() == 'Entretê Foto 1') { $entreteFoto1 = $feature; }
                if ($location->getDescription() == 'Entretê Foto 2') { $entreteFoto2 = $feature; }
                if ($location->getDescription() == 'Entretê Foto 3') { $entreteFoto3 = $feature; }
                
                if ($location->getDescription() == 'Esporte Principal') { $esportePrincipal = $feature; }
                if ($location->getDescription() == 'Esporte Secundário Foto') { $esporteSecundarioFoto = $feature; }
                if ($location->getDescription() == 'Esporte Foto Horizontal 1') { $esporteFotoHorizontal1 = $feature; }
                if ($location->getDescription() == 'Esporte Frase') { $esporteFrase = $feature; }
                if ($location->getDescription() == 'Esporte Foto Horizontal 2') { $esporteFotoHorizontal2 = $feature; }
                if ($location->getDescription() == 'Esporte Foto Grande') { $esporteFotoGrande = $feature; }
                if ($location->getDescription() == 'Esporte Foto Pequena') { $esporteFotoPequena = $feature; }

                if ($location->getDescription() == 'Municípios Principal') { $municipioPrincipal = $feature; }
                if ($location->getDescription() == 'Municípios Foto 1') { $municipioFoto1 = $feature; }
                if ($location->getDescription() == 'Municípios Foto 2') { $municipioFoto2 = $feature; }
                if ($location->getDescription() == 'Municípios Frase') { if ($municipioFrase1 == null) { $municipioFrase1 = $feature; } }
                if ($location->getDescription() == 'Municípios Frase') { if ($municipioFrase1 != null) { if ($feature != $municipioFrase1) { $municipioFrase2 = $municipioFrase1; $municipioFrase1 = $feature; } } }
                if ($location->getDescription() == 'Municípios Foto Horizontal') { $municipioFotoHorizontal = $feature; }
                if ($location->getDescription() == 'Municípios Foto Grande') { $municipioFotoGrande = $feature; }
                if ($location->getDescription() == 'Municípios Frase Curta') { $municipioFraseCurta = $feature; }

            }    
        }    

        /*$coverFeatures = $em->getRepository('BackBundle:Article')->findBy(
            array('coverFeature' => true),array('id' => 'DESC'),null,null
        );*/

        $queryCoverFeatures = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.coverFeature = :coverFeature')
            ->setParameter(':coverFeature', true)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
        $coverFeatures = $queryCoverFeatures->getResult();

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelPrincipal = :principal')
            ->setParameter(':todos', true)
            ->setParameter(':principal', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $querySlide = $em->getRepository('BackBundle:Slide')
            ->createQueryBuilder('s')
            ->orderBy('s.ordem', 'ASC')
            ->getQuery();
        $slides = $querySlide->getResult();

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        $groupsDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true),array('groupName' => 'ASC'),null,null
        );

        $cultures = $em->getRepository('BackBundle:Culture')->findBy(
            array('status' => true),array('date' => 'ASC'),null,null
        );         

        // Montando o objeto da notícia mais lida de politica nos ultimos 2 meses
        $date_begin = new \DateTime(date('Y-m-d H:i:s', strtotime('-120 day')));

        $maisLidaPolitica = $statsService->mostReadByEditorial('Política', $date_begin, $today);
        $relacionadasPolitica = $statsService->lastArticlesbyEditoria('Política', $date_begin, $today, 2);

        $maisLidaEntretenimento = $statsService->mostReadByEditorial('Entretenimento', $date_begin, $today);
        $relacionadasEntretenimento = $statsService->lastArticlesbyEditoria('Entretenimento', $date_begin, $today, 2);

        $maisLidaEsporte = $statsService->mostReadByEditorial('AZ Esporte', $date_begin, $today);
        $relacionadasEsporte = $statsService->lastArticlesbyEditoria('AZ Esporte', $date_begin, $today, 2);

        $maisLidaMunicipio = $statsService->mostReadByEditorial('Municípios', $date_begin, $today);
        $relacionadasMunicipios = $statsService->lastArticlesByEditoria('Municípios', $date_begin, $today, 2);

        // Montando o objeto das mais lidas em geral
        $yesterday = new \DateTime(date('Y-m-d H:i:s', strtotime('-1 day')));
        $maisLidas = $statsService->mostReadOfAll($yesterday, $today, 4);

        $partners = $em->getRepository('BackBundle:Editorial')->findBy(
            array('partner' => true),array('name' => 'ASC'),null,null
        );
        $queryPartners = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial IN (:editorial)')
            ->setParameter(':editorial', $partners)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
        $partnerArticles = $queryPartners->getResult();

        $metrica = $em->getRepository('BackBundle:Metric')->findOneBy(
            array(),array('id' => 'ASC')
        );
        $acessowebsite = $metrica->getAcessowebsite();
        $acessowebsite = $acessowebsite + 1;
        $metrica->setAcessowebsite($acessowebsite);
        $em->persist($metrica);
        $em->flush();

        return $this->render('FrontBundle:Default:index.html.twig', array(
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'dayOfWeek' => $dayOfWeek,
            'time' => $time,

            'slides' => $slides,
            'banners' => $banners,
            'coverFeatures' => $coverFeatures,
            'colunas' => $colunas,
            'blogs' => $blogs,
            'editorials' => $editorials,
            'groupsDestaque' => $groupsDestaque,
            'cultures' => $cultures,
            'partnerArticles' => $partnerArticles,

            'principal1' => $principal1,
            'principal2' => $principal2,
            'secundario1' => $secundario1,
            'secundario2' => $secundario2,
            'secundario3ComFoto' => $secundario3ComFoto,
            'featureComFoto1' => $featureComFoto1,
            'featureComFoto2' => $featureComFoto2,
            'featureSemFoto1' => $featureSemFoto1,
            'featureSemFoto2' => $featureSemFoto2,

            'politicaPrincipal' => $politicaPrincipal,
            'politicaSecundario' => $politicaSecundario,
            'politicaTerciario1' => $politicaTerciario1,
            'politicaTerciario2' => $politicaTerciario2,
            'politicaFoto1' => $politicaFoto1,
            'politicaFoto2' => $politicaFoto2,
            'policitaFrase1' => $policitaFrase1,
            'policitaFrase2' => $policitaFrase2,
            'maisLidaPolitica' => $maisLidaPolitica,
            'relacionadasPolitica' => $relacionadasPolitica,

            'entretePrincipal' => $entretePrincipal,
            'entreteSlide' => $entreteSlide,
            'entreteFotoPequena' => $entreteFotoPequena,
            'entreteFrase' => $entreteFrase,
            'entreteFoto1' => $entreteFoto1,
            'entreteFoto2' => $entreteFoto2,
            'entreteFoto3' => $entreteFoto3,
            'maisLidaEntretenimento' => $maisLidaEntretenimento,
            'relacionadasEntretenimento' => $relacionadasEntretenimento,

            'esportePrincipal' => $esportePrincipal,
            'esporteSecundarioFoto' => $esporteSecundarioFoto,
            'esporteFotoHorizontal1' => $esporteFotoHorizontal1,
            'esporteFrase' => $esporteFrase,
            'esporteFotoHorizontal2' => $esporteFotoHorizontal2,
            'esporteFotoGrande' => $esporteFotoGrande,
            'esporteFotoPequena' => $esporteFotoPequena,
            'maisLidaEsporte' => $maisLidaEsporte,
            'relacionadasEsporte' => $relacionadasEsporte,

            'municipioPrincipal' => $municipioPrincipal,
            'municipioFoto1' => $municipioFoto1,
            'municipioFoto2' => $municipioFoto2,
            'municipioFrase1' => $municipioFrase1,
            'municipioFrase2' => $municipioFrase2,
            'municipioFotoHorizontal' => $municipioFotoHorizontal,
            'municipioFotoGrande' => $municipioFotoGrande,
            'municipioFraseCurta' => $municipioFraseCurta,
            'maisLidaMunicipio' => $maisLidaMunicipio,
            'relacionadasMunicipios' => $relacionadasMunicipios,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5,

            'urgentes' => $urgentes,
            'maisLidas' => $maisLidas
        ));
    }


    /**
     * Search Bar Component
     */
    public function searchBarAction(){
        // Criando o formulário da barra de pesquisa
        $search_form = $this->createForm(SearchType::class, NULL, array(
            'action' => $this->generateUrl('handle_search'),
            'method' => 'POST'
        ));

        return $this->render('FrontBundle:Default:searchBar.html.twig', [
            'search_form' => $search_form->createView()
        ]);
    }

    /**
     * @Route("/search", name="handle_search")
     * @param Request $request
     */
    public function handleSearch(Request $request){
        $em = $this->getDoctrine()->getManager();
        $today = new \DateTime(date('Y-m-d H:i:s'));

        // Query para buscar os banners 
        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos')
            ->setParameter(':todos', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.editorial is not null');
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $lastOnes = $query->getResult();

        // Criando o formulário que contem a paginação
        $filterform = $this->createFormBuilder(null)
            ->setAction($this->generateUrl('handle_search'))
            ->add('page')
            ->add('search_query', HiddenType::class, [
                'data' => 'Termo'
            ])
            ->add('send', SubmitType::class, array(
                'label' => 'Buscar'
            ))
            ->getForm();
        
        $filterform->handleRequest($request);

        // Ação quando tiver paginação
        if ($filterform->isSubmitted() && $filterform->isValid()){
            $data = $filterform->getData();

            $searchQuery = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->addSelect("MATCH_AGAINST (a.title, :searchterm 'IN NATURAL MODE') as score")
            ->where('MATCH_AGAINST(a.title, :searchterm) > 0.8')
            ->andWhere('a.status = :artiStatus')
            ->setParameter('artiStatus', 'Aprovado')
            ->setParameter('searchterm', $data['search_query'])
            ->orderBy('a.date', 'desc');

            $query = $searchQuery->getQuery();
            $articles = $query->getResult();

            $hits = (count($articles) / 10);
            $count = count($articles);
            $page = 1;

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            if ($hits > 1) {
                $searchQuery->setFirstResult(10 * ($page - 1));
                $searchQuery->setMaxResults(10);
                $query = $searchQuery->getQuery();
                $articles = $query->getResult();
            }

            return $this->render('FrontBundle:Default:searchFeed.html.twig', array(
                'articles' => $articles,
                'search_term' => $data['search_query'],
                'hits' => $hits,
                'editorials' => $editorials,
                'blogs' => $blogs,
                'colunas' => $colunas,
                'lastOnes' => $lastOnes,
                'filter_form' => $filterform->createView(),
                'page' => $page,

                'bannerTopoPagina' => $bannerTopoPagina,
                'bannerTopoEditoria5' => $bannerTopoEditoria5,
            ));
        }


        // Criando o formulário da barra de pesquisa
        $search_form = $this->createForm(SearchType::class, NULL, array(
            'action' => $this->generateUrl('handle_search'),
            'method' => 'POST'
        ));

        $search_form->handleRequest($request);

        // Ação quando a função vier do search form
        if ($search_form->isSubmitted() && $search_form->isValid()){
            $data = $search_form->getData();

            $searchQuery = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->addSelect("MATCH_AGAINST (a.title, :searchterm 'IN NATURAL MODE') as score")
            ->where('MATCH_AGAINST(a.title, :searchterm) > 0.8')
            ->andWhere('a.status = :artiStatus')
            ->setParameter('artiStatus', 'Aprovado')
            ->setParameter('searchterm', $data['search_query'])
            ->orderBy('a.date', 'desc');

            $query = $searchQuery->getQuery();
            $articles = $query->getResult();

            $hits = (count($articles) / 10);
            $count = count($articles);
            $page = 1;

            if ($hits > 1) {
                $searchQuery->setFirstResult(10 * ($page - 1));
                $searchQuery->setMaxResults(10);
                $query = $searchQuery->getQuery();
                $articles = $query->getResult();
            }

            return $this->render('FrontBundle:Default:searchFeed.html.twig', array(
                'articles' => $articles,
                'search_term' => $data['search_query'],
                'hits' => ceil($hits),
                'editorials' => $editorials,
                'blogs' => $blogs,
                'colunas' => $colunas,
                'lastOnes' => $lastOnes,
                'filter_form' => $filterform->createView(),
                'page' => $page,

                'bannerTopoPagina' => $bannerTopoPagina,
                'bannerTopoEditoria5' => $bannerTopoEditoria5,
            ));
        }
    }

    /**
     * @Route("/index", name="portalaz_index")
    * @Method({"GET"})
     */
    public function portalazIndexAction()
    {
        //return $this->redirect($this->generateUrl('manutencao'));
        //return $this->redirect($this->generateUrl('eleicao'));

        $em = $this->getDoctrine()->getManager();

        // Recuperando o service para pegar as materias via estatisticas
        $statsService = $this->get('stats_service');

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));
        $dayOfWeek = $today->format('l');
        if ($dayOfWeek == 'Sunday') { $dayOfWeek = 'Domingo'; }
        if ($dayOfWeek == 'Monday') { $dayOfWeek = 'Segunda'; }
        if ($dayOfWeek == 'Tuesday') { $dayOfWeek = 'Terça'; }
        if ($dayOfWeek == 'Wednesday') { $dayOfWeek = 'Quarta'; }
        if ($dayOfWeek == 'Thursday') { $dayOfWeek = 'Quinta'; }
        if ($dayOfWeek == 'Friday') { $dayOfWeek = 'Sexta'; }
        if ($dayOfWeek == 'Saturday') { $dayOfWeek = 'Sábado'; }

        $day = $today->format('d');
        $year = $today->format('Y');
        $month = $today->format('M');
        if ($month == 'Jan') { $month = 'jan'; }
        if ($month == 'Feb') { $month = 'fev'; }
        if ($month == 'Mar') { $month = 'mar'; }
        if ($month == 'Apr') { $month = 'abr'; }
        if ($month == 'May') { $month = 'mai'; }
        if ($month == 'Jun') { $month = 'jun'; }
        if ($month == 'Jul') { $month = 'jul'; }
        if ($month == 'Ago') { $month = 'ago'; }
        if ($month == 'Sep') { $month = 'set'; }
        if ($month == 'Oct') { $month = 'out'; }
        if ($month == 'Nov') { $month = 'nov'; }
        if ($month == 'Dez') { $month = 'dez'; }

        $time = $today->format('G:i');

        /*$features = $em->getRepository('BackBundle:Feature')->findBy(
            array('enabled' => true),array('id' => 'DESC'),null,null
        );*/

        $queryUrgent = $em->getRepository('BackBundle:Urgent')
            ->createQueryBuilder('u')
            ->where('u.startDate <= :startDate')
            ->setParameter(':startDate', $today)
            ->andWhere('u.endDate >= :endDate')
            ->setParameter(':endDate', $today)
            ->getQuery();
        $urgentes = $queryUrgent->getResult();



        $principal1 = null;
        $principal2 = null;
        $secundario1 = null;
        $secundario2 = null;
        $secundario3ComFoto = null;
        $featureComFoto1 = null;
        $featureComFoto2 = null;
        $featureSemFoto1 = null;
        $featureSemFoto2 = null;

        $politicaPrincipal = null;
        $politicaSecundario = null;
        $politicaTerciario1 = null;
        $politicaTerciario2 = null;
        $politicaFoto1 = null;
        $politicaFoto2 = null;
        $policitaFrase1 = null;
        $policitaFrase2 = null;

        $entretePrincipal = null;
        $entreteSlide = null;
        $entreteFotoPequena = null;
        $entreteFrase = null;
        $entreteFoto1 = null;
        $entreteFoto2 = null;
        $entreteFoto3 = null;

        $esportePrincipal = null;
        $esporteSecundarioFoto = null;
        $esporteFotoHorizontal1 = null;
        $esporteFrase = null;
        $esporteFotoHorizontal2 = null;
        $esporteFotoGrande = null;
        $esporteFotoPequena = null;

        $municipioPrincipal = null;
        $municipioFoto1 = null;
        $municipioFoto2 = null;
        $municipioFrase1 = null;
        $municipioFrase2 = null;
        $municipioFotoHorizontal = null;
        $municipioFotoGrande = null;
        $municipioFraseCurta = null;  
        
        $queryLocation = $em->getRepository('BackBundle:Location')
        ->createQueryBuilder('l')
        ->orderBy('l.description', 'ASC')
        ->getQuery();
        $locations = $queryLocation->getResult();

        foreach ($locations as $location) {
            $queryFeature = $em->getRepository('BackBundle:Feature')
            ->createQueryBuilder('f')
            ->where('f.enabled = :enabled')
            ->setParameter(':enabled', true)
            //->andWhere('f.scheduledTo <= :data')
            //->setParameter(':data', $today)
            //->andWhere('f.scheduled = :scheduled')
            //->setParameter(':scheduled', true)
            ->andWhere('f.location = :location')
            ->setParameter(':location', $location)
            ->setMaxResults(2)
            ->orderBy('f.scheduledTo', 'ASC')
            ->addOrderBy('f.id', 'ASC')
            ->getQuery();
            $features = $queryFeature->getResult();  

            foreach ($features as $feature) {
                $location = $feature->getLocation();
                    
                if ($location->getDescription() == 'Principal 1') { $principal1 = $feature; }
                if ($location->getDescription() == 'Principal 2') { $principal2 = $feature; }
                if ($location->getDescription() == 'Secundário 1') { $secundario1 = $feature; }
                if ($location->getDescription() == 'Secundário 2') { $secundario2 = $feature; }
                if ($location->getDescription() == 'Secundário 3 Foto') { $secundario3ComFoto = $feature; }
                if ($location->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 == null) { $featureComFoto1 = $feature; } }
                if ($location->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 != null) { if ($feature != $featureComFoto1) { $featureComFoto2 = $featureComFoto1; $featureComFoto1 = $feature; } } }
                if ($location->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 == null) { $featureSemFoto1 = $feature; } }
                if ($location->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 != null) { if ($feature != $featureSemFoto1) { $featureSemFoto2 = $featureSemFoto1; $featureSemFoto1 = $feature; } } }

                if ($location->getDescription() == 'Política Principal') { $politicaPrincipal = $feature; }
                if ($location->getDescription() == 'Política Secundário') { $politicaSecundario = $feature; }
                if ($location->getDescription() == 'Política Terciário') { if ($politicaTerciario1 == null) { $politicaTerciario1 = $feature; } }
                if ($location->getDescription() == 'Política Terciário') { if ($politicaTerciario1 != null) { if ($feature != $politicaTerciario1) { $politicaTerciario2 = $politicaTerciario1; $politicaTerciario1 = $feature; } } }
                if ($location->getDescription() == 'Política Foto 1') { $politicaFoto1 = $feature; }
                if ($location->getDescription() == 'Política Foto 2') { $politicaFoto2 = $feature; }
                if ($location->getDescription() == 'Política Frase') { if ($policitaFrase1 == null) { $policitaFrase1 = $feature; } }
                if ($location->getDescription() == 'Política Frase') { if ($policitaFrase1 != null) { if ($feature != $policitaFrase1) { $policitaFrase2 = $policitaFrase1; $policitaFrase1 = $feature; } } }

                if ($location->getDescription() == 'Entretê Principal') { $entretePrincipal = $feature; }
                if ($location->getDescription() == 'Entretê Slide') { $entreteSlide = $feature; }
                if ($location->getDescription() == 'Entretê Foto Pequena') { $entreteFotoPequena = $feature; }
                if ($location->getDescription() == 'Entretê Frase') { $entreteFrase = $feature; }
                if ($location->getDescription() == 'Entretê Foto 1') { $entreteFoto1 = $feature; }
                if ($location->getDescription() == 'Entretê Foto 2') { $entreteFoto2 = $feature; }
                if ($location->getDescription() == 'Entretê Foto 3') { $entreteFoto3 = $feature; }
                
                if ($location->getDescription() == 'Esporte Principal') { $esportePrincipal = $feature; }
                if ($location->getDescription() == 'Esporte Secundário Foto') { $esporteSecundarioFoto = $feature; }
                if ($location->getDescription() == 'Esporte Foto Horizontal 1') { $esporteFotoHorizontal1 = $feature; }
                if ($location->getDescription() == 'Esporte Frase') { $esporteFrase = $feature; }
                if ($location->getDescription() == 'Esporte Foto Horizontal 2') { $esporteFotoHorizontal2 = $feature; }
                if ($location->getDescription() == 'Esporte Foto Grande') { $esporteFotoGrande = $feature; }
                if ($location->getDescription() == 'Esporte Foto Pequena') { $esporteFotoPequena = $feature; }

                if ($location->getDescription() == 'Municípios Principal') { $municipioPrincipal = $feature; }
                if ($location->getDescription() == 'Municípios Foto 1') { $municipioFoto1 = $feature; }
                if ($location->getDescription() == 'Municípios Foto 2') { $municipioFoto2 = $feature; }
                if ($location->getDescription() == 'Municípios Frase') { if ($municipioFrase1 == null) { $municipioFrase1 = $feature; } }
                if ($location->getDescription() == 'Municípios Frase') { if ($municipioFrase1 != null) { if ($feature != $municipioFrase1) { $municipioFrase2 = $municipioFrase1; $municipioFrase1 = $feature; } } }
                if ($location->getDescription() == 'Municípios Foto Horizontal') { $municipioFotoHorizontal = $feature; }
                if ($location->getDescription() == 'Municípios Foto Grande') { $municipioFotoGrande = $feature; }
                if ($location->getDescription() == 'Municípios Frase Curta') { $municipioFraseCurta = $feature; }

            }    
        }    

        /*$coverFeatures = $em->getRepository('BackBundle:Article')->findBy(
            array('coverFeature' => true),array('id' => 'DESC'),null,null
        );*/

        $queryCoverFeatures = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.coverFeature = :coverFeature')
            ->setParameter(':coverFeature', true)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
        $coverFeatures = $queryCoverFeatures->getResult();

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelPrincipal = :principal')
            ->setParameter(':todos', true)
            ->setParameter(':principal', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $querySlide = $em->getRepository('BackBundle:Slide')
            ->createQueryBuilder('s')
            ->orderBy('s.ordem', 'ASC')
            ->getQuery();
        $slides = $querySlide->getResult();

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $groupsDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true),array('groupName' => 'ASC'),null,null
        );

        $cultures = $em->getRepository('BackBundle:Culture')->findBy(
            array('status' => true),array('date' => 'ASC'),null,null
        );         

        // Montando o objeto da notícia mais lida de politica nos ultimos 2 meses
        $date_begin = new \DateTime(date('Y-m-d H:i:s', strtotime('now')));
        $date_end = new \DateTime(date('Y-m-d H:i:s', strtotime('-60 day')));
        $maisLidaPolitica = $statsService->mostReadByEditorial('Politica', $date_begin, $date_end);
        dump($maisLidaPolitica);

        $editoriaPolitica = $em->getRepository('BackBundle:Editorial')->findOneByName('Política');
        $queryMaisLidaPolitica = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaPolitica)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasPolitica = $queryMaisLidaPolitica->getResult();
        $relacionadasPolitica = [];
        if (count($maisLidasPolitica) > 0) {
            if (count($maisLidasPolitica) > 1) {
                array_push($relacionadasPolitica, $maisLidasPolitica[1]);
            }
            if (count($maisLidasPolitica) > 2) {
                array_push($relacionadasPolitica, $maisLidasPolitica[2]);
            }
        }

        $editoriaEntretenimento = $em->getRepository('BackBundle:Editorial')->findOneByName('Entretenimento');
        $queryMaisLidaEntretenimento = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaEntretenimento)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasEntretenimento = $queryMaisLidaEntretenimento->getResult();
        $maisLidaEntretenimento = null;
        $relacionadasEntretenimento = [];
        if (count($maisLidasEntretenimento) > 0) {
            $maisLidaEntretenimento = $maisLidasEntretenimento[0];

            if (count($maisLidasEntretenimento) > 1) {
                array_push($relacionadasEntretenimento, $maisLidasEntretenimento[1]);
            }
            if (count($maisLidasEntretenimento) > 2) {
                array_push($relacionadasEntretenimento, $maisLidasEntretenimento[2]);
            }
        }

        $editoriaEsporte = $em->getRepository('BackBundle:Editorial')->findOneByName('Esportes');
        $queryMaisLidaEsporte = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaEsporte)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasEsporte = $queryMaisLidaEsporte->getResult();
        $maisLidaEsporte = null;
        $relacionadasEsporte = [];
        if (count($maisLidasEsporte) > 0) {
            $maisLidaEsporte = $maisLidasEsporte[0];

            if (count($maisLidasEsporte) > 1) {
                array_push($relacionadasEsporte, $maisLidasEsporte[1]);
            }
            if (count($maisLidasEsporte) > 2) {
                array_push($relacionadasEsporte, $maisLidasEsporte[2]);
            }
        }

        $partners = $em->getRepository('BackBundle:Editorial')->findBy(
            array('partner' => true),array('name' => 'ASC'),null,null
        );
        $queryPartners = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial IN (:editorial)')
            ->setParameter(':editorial', $partners)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
        $partnerArticles = $queryPartners->getResult();

        $metrica = $em->getRepository('BackBundle:Metric')->findOneBy(
            array(),array('id' => 'ASC')
        );
        $acessowebsite = $metrica->getAcessowebsite();
        $acessowebsite = $acessowebsite + 1;
        $metrica->setAcessowebsite($acessowebsite);
        $em->persist($metrica);
        $em->flush();


        return $this->render('FrontBundle:Default:index.html.twig', array(
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'dayOfWeek' => $dayOfWeek,
            'time' => $time,

            'slides' => $slides,
            'banners' => $banners,
            'coverFeatures' => $coverFeatures,
            'colunas' => $colunas,
            'blogs' => $blogs,
            'groupsDestaque' => $groupsDestaque,
            'cultures' => $cultures,
            'partnerArticles' => $partnerArticles,

            'principal1' => $principal1,
            'principal2' => $principal2,
            'secundario1' => $secundario1,
            'secundario2' => $secundario2,
            'secundario3ComFoto' => $secundario3ComFoto,
            'featureComFoto1' => $featureComFoto1,
            'featureComFoto2' => $featureComFoto2,
            'featureSemFoto1' => $featureSemFoto1,
            'featureSemFoto2' => $featureSemFoto2,

            'politicaPrincipal' => $politicaPrincipal,
            'politicaSecundario' => $politicaSecundario,
            'politicaTerciario1' => $politicaTerciario1,
            'politicaTerciario2' => $politicaTerciario2,
            'politicaFoto1' => $politicaFoto1,
            'politicaFoto2' => $politicaFoto2,
            'policitaFrase1' => $policitaFrase1,
            'policitaFrase2' => $policitaFrase2,
            'maisLidaPolitica' => $maisLidaPolitica,
            'relacionadasPolitica' => $relacionadasPolitica,

            'entretePrincipal' => $entretePrincipal,
            'entreteSlide' => $entreteSlide,
            'entreteFotoPequena' => $entreteFotoPequena,
            'entreteFrase' => $entreteFrase,
            'entreteFoto1' => $entreteFoto1,
            'entreteFoto2' => $entreteFoto2,
            'entreteFoto3' => $entreteFoto3,
            'maisLidaEntretenimento' => $maisLidaEntretenimento,
            'relacionadasEntretenimento' => $relacionadasEntretenimento,

            'esportePrincipal' => $esportePrincipal,
            'esporteSecundarioFoto' => $esporteSecundarioFoto,
            'esporteFotoHorizontal1' => $esporteFotoHorizontal1,
            'esporteFrase' => $esporteFrase,
            'esporteFotoHorizontal2' => $esporteFotoHorizontal2,
            'esporteFotoGrande' => $esporteFotoGrande,
            'esporteFotoPequena' => $esporteFotoPequena,
            'maisLidaEsporte' => $maisLidaEsporte,
            'relacionadasEsporte' => $relacionadasEsporte,

            'municipioPrincipal' => $municipioPrincipal,
            'municipioFoto1' => $municipioFoto1,
            'municipioFoto2' => $municipioFoto2,
            'municipioFrase1' => $municipioFrase1,
            'municipioFrase2' => $municipioFrase2,
            'municipioFotoHorizontal' => $municipioFotoHorizontal,
            'municipioFotoGrande' => $municipioFotoGrande,
            'municipioFraseCurta' => $municipioFraseCurta,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5,

            'urgentes' => $urgentes
        ));
    }

    /**
     * @Route("/preview", name="preview")
     * @Method({"GET"})
     */
    public function previewAction()
    {
        $em = $this->getDoctrine()->getManager();

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));
        $dayOfWeek = $today->format('l');
        if ($dayOfWeek == 'Sunday') { $dayOfWeek = 'Domingo'; }
        if ($dayOfWeek == 'Monday') { $dayOfWeek = 'Segunda'; }
        if ($dayOfWeek == 'Tuesday') { $dayOfWeek = 'Terça'; }
        if ($dayOfWeek == 'Wednesday') { $dayOfWeek = 'Quarta'; }
        if ($dayOfWeek == 'Thursday') { $dayOfWeek = 'Quinta'; }
        if ($dayOfWeek == 'Friday') { $dayOfWeek = 'Sexta'; }
        if ($dayOfWeek == 'Saturday') { $dayOfWeek = 'Sábado'; }

        $day = $today->format('d');
        $year = $today->format('Y');
        $month = $today->format('M');
        if ($month == 'Jan') { $month = 'jan'; }
        if ($month == 'Feb') { $month = 'fev'; }
        if ($month == 'Mar') { $month = 'mar'; }
        if ($month == 'Apr') { $month = 'abr'; }
        if ($month == 'May') { $month = 'mai'; }
        if ($month == 'Jun') { $month = 'jun'; }
        if ($month == 'Jul') { $month = 'jul'; }
        if ($month == 'Ago') { $month = 'ago'; }
        if ($month == 'Sep') { $month = 'set'; }
        if ($month == 'Oct') { $month = 'out'; }
        if ($month == 'Nov') { $month = 'nov'; }
        if ($month == 'Dez') { $month = 'dez'; }

        $time = $today->format('G:i');

        /*$features = $em->getRepository('BackBundle:Feature')->findBy(
            array('enabled' => true),array('id' => 'DESC'),null,null
        );*/

        $queryUrgent = $em->getRepository('BackBundle:Urgent')
            ->createQueryBuilder('u')
            ->where('u.startDate <= :startDate')
            ->setParameter(':startDate', $today)
            ->andWhere('u.endDate >= :endDate')
            ->setParameter(':endDate', $today)
            ->getQuery();
        $urgentes = $queryUrgent->getResult();



        $principal1 = null;
        $principal2 = null;
        $secundario1 = null;
        $secundario2 = null;
        $secundario3ComFoto = null;
        $featureComFoto1 = null;
        $featureComFoto2 = null;
        $featureSemFoto1 = null;
        $featureSemFoto2 = null;

        $politicaPrincipal = null;
        $politicaSecundario = null;
        $politicaTerciario1 = null;
        $politicaTerciario2 = null;
        $politicaFoto1 = null;
        $politicaFoto2 = null;
        $policitaFrase1 = null;
        $policitaFrase2 = null;

        $entretePrincipal = null;
        $entreteSlide = null;
        $entreteFotoPequena = null;
        $entreteFrase = null;
        $entreteFoto1 = null;
        $entreteFoto2 = null;
        $entreteFoto3 = null;

        $esportePrincipal = null;
        $esporteSecundarioFoto = null;
        $esporteFotoHorizontal1 = null;
        $esporteFrase = null;
        $esporteFotoHorizontal2 = null;
        $esporteFotoGrande = null;
        $esporteFotoPequena = null;

        $municipioPrincipal = null;
        $municipioFoto1 = null;
        $municipioFoto2 = null;
        $municipioFrase1 = null;
        $municipioFrase2 = null;
        $municipioFotoHorizontal = null;
        $municipioFotoGrande = null;
        $municipioFraseCurta = null;  
        
        $queryLocation = $em->getRepository('BackBundle:Location')
        ->createQueryBuilder('l')
        ->orderBy('l.description', 'ASC')
        ->getQuery();
        $locations = $queryLocation->getResult();

        foreach ($locations as $location) {
            $queryFeature = $em->getRepository('BackBundle:Feature')
            ->createQueryBuilder('f')
            ->where('f.enabled = :enabled')
            ->setParameter(':enabled', true)
            //->andWhere('f.scheduledTo <= :data')
            //->setParameter(':data', $today)
            //->andWhere('f.scheduled = :scheduled')
            //->setParameter(':scheduled', true)
            ->andWhere('f.location = :location')
            ->setParameter(':location', $location)
            ->setMaxResults(2)
            ->orderBy('f.scheduledTo', 'ASC')
            ->addOrderBy('f.id', 'ASC')
            ->getQuery();
            $features = $queryFeature->getResult();  

            foreach ($features as $feature) {
                $location = $feature->getLocation();
                    
                if ($location->getDescription() == 'Principal 1') { $principal1 = $feature; }
                if ($location->getDescription() == 'Principal 2') { $principal2 = $feature; }
                if ($location->getDescription() == 'Secundário 1') { $secundario1 = $feature; }
                if ($location->getDescription() == 'Secundário 2') { $secundario2 = $feature; }
                if ($location->getDescription() == 'Secundário 3 Foto') { $secundario3ComFoto = $feature; }
                if ($location->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 == null) { $featureComFoto1 = $feature; } }
                if ($location->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 != null) { if ($feature != $featureComFoto1) { $featureComFoto2 = $featureComFoto1; $featureComFoto1 = $feature; } } }
                if ($location->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 == null) { $featureSemFoto1 = $feature; } }
                if ($location->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 != null) { if ($feature != $featureSemFoto1) { $featureSemFoto2 = $featureSemFoto1; $featureSemFoto1 = $feature; } } }

                if ($location->getDescription() == 'Política Principal') { $politicaPrincipal = $feature; }
                if ($location->getDescription() == 'Política Secundário') { $politicaSecundario = $feature; }
                if ($location->getDescription() == 'Política Terciário') { if ($politicaTerciario1 == null) { $politicaTerciario1 = $feature; } }
                if ($location->getDescription() == 'Política Terciário') { if ($politicaTerciario1 != null) { if ($feature != $politicaTerciario1) { $politicaTerciario2 = $politicaTerciario1; $politicaTerciario1 = $feature; } } }
                if ($location->getDescription() == 'Política Foto 1') { $politicaFoto1 = $feature; }
                if ($location->getDescription() == 'Política Foto 2') { $politicaFoto2 = $feature; }
                if ($location->getDescription() == 'Política Frase') { if ($policitaFrase1 == null) { $policitaFrase1 = $feature; } }
                if ($location->getDescription() == 'Política Frase') { if ($policitaFrase1 != null) { if ($feature != $policitaFrase1) { $policitaFrase2 = $policitaFrase1; $policitaFrase1 = $feature; } } }

                if ($location->getDescription() == 'Entretê Principal') { $entretePrincipal = $feature; }
                if ($location->getDescription() == 'Entretê Slide') { $entreteSlide = $feature; }
                if ($location->getDescription() == 'Entretê Foto Pequena') { $entreteFotoPequena = $feature; }
                if ($location->getDescription() == 'Entretê Frase') { $entreteFrase = $feature; }
                if ($location->getDescription() == 'Entretê Foto 1') { $entreteFoto1 = $feature; }
                if ($location->getDescription() == 'Entretê Foto 2') { $entreteFoto2 = $feature; }
                if ($location->getDescription() == 'Entretê Foto 3') { $entreteFoto3 = $feature; }
                
                if ($location->getDescription() == 'Esporte Principal') { $esportePrincipal = $feature; }
                if ($location->getDescription() == 'Esporte Secundário Foto') { $esporteSecundarioFoto = $feature; }
                if ($location->getDescription() == 'Esporte Foto Horizontal 1') { $esporteFotoHorizontal1 = $feature; }
                if ($location->getDescription() == 'Esporte Frase') { $esporteFrase = $feature; }
                if ($location->getDescription() == 'Esporte Foto Horizontal 2') { $esporteFotoHorizontal2 = $feature; }
                if ($location->getDescription() == 'Esporte Foto Grande') { $esporteFotoGrande = $feature; }
                if ($location->getDescription() == 'Esporte Foto Pequena') { $esporteFotoPequena = $feature; }

                if ($location->getDescription() == 'Municípios Principal') { $municipioPrincipal = $feature; }
                if ($location->getDescription() == 'Municípios Foto 1') { $municipioFoto1 = $feature; }
                if ($location->getDescription() == 'Municípios Foto 2') { $municipioFoto2 = $feature; }
                if ($location->getDescription() == 'Municípios Frase') { if ($municipioFrase1 == null) { $municipioFrase1 = $feature; } }
                if ($location->getDescription() == 'Municípios Frase') { if ($municipioFrase1 != null) { if ($feature != $municipioFrase1) { $municipioFrase2 = $municipioFrase1; $municipioFrase1 = $feature; } } }
                if ($location->getDescription() == 'Municípios Foto Horizontal') { $municipioFotoHorizontal = $feature; }
                if ($location->getDescription() == 'Municípios Foto Grande') { $municipioFotoGrande = $feature; }
                if ($location->getDescription() == 'Municípios Frase Curta') { $municipioFraseCurta = $feature; }

            }    
        }    

        /*$coverFeatures = $em->getRepository('BackBundle:Article')->findBy(
            array('coverFeature' => true),array('id' => 'DESC'),null,null
        );*/

        $queryCoverFeatures = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.coverFeature = :coverFeature')
            ->setParameter(':coverFeature', true)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
        $coverFeatures = $queryCoverFeatures->getResult();

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelPrincipal = :principal')
            ->setParameter(':todos', true)
            ->setParameter(':principal', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $querySlide = $em->getRepository('BackBundle:Slide')
            ->createQueryBuilder('s')
            ->orderBy('s.ordem', 'ASC')
            ->getQuery();
        $slides = $querySlide->getResult();

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $groupsDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true),array('groupName' => 'ASC'),null,null
        );

        $cultures = $em->getRepository('BackBundle:Culture')->findBy(
            array('status' => true),array('date' => 'ASC'),null,null
        );         

        $editoriaPolitica = $em->getRepository('BackBundle:Editorial')->findOneByName('Política');
        $queryMaisLidaPolitica = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaPolitica)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasPolitica = $queryMaisLidaPolitica->getResult();
        $maisLidaPolitica = null;
        $relacionadasPolitica = [];
        if (count($maisLidasPolitica) > 0) {
            $maisLidaPolitica = $maisLidasPolitica[0];

            if (count($maisLidasPolitica) > 1) {
                array_push($relacionadasPolitica, $maisLidasPolitica[1]);
            }
            if (count($maisLidasPolitica) > 2) {
                array_push($relacionadasPolitica, $maisLidasPolitica[2]);
            }
        }

        $editoriaEntretenimento = $em->getRepository('BackBundle:Editorial')->findOneByName('Entretenimento');
        $queryMaisLidaEntretenimento = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaEntretenimento)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasEntretenimento = $queryMaisLidaEntretenimento->getResult();
        $maisLidaEntretenimento = null;
        $relacionadasEntretenimento = [];
        if (count($maisLidasEntretenimento) > 0) {
            $maisLidaEntretenimento = $maisLidasEntretenimento[0];

            if (count($maisLidasEntretenimento) > 1) {
                array_push($relacionadasEntretenimento, $maisLidasEntretenimento[1]);
            }
            if (count($maisLidasEntretenimento) > 2) {
                array_push($relacionadasEntretenimento, $maisLidasEntretenimento[2]);
            }
        }

        $editoriaEsporte = $em->getRepository('BackBundle:Editorial')->findOneByName('Esportes');
        $queryMaisLidaEsporte = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaEsporte)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasEsporte = $queryMaisLidaEsporte->getResult();
        $maisLidaEsporte = null;
        $relacionadasEsporte = [];
        if (count($maisLidasEsporte) > 0) {
            $maisLidaEsporte = $maisLidasEsporte[0];

            if (count($maisLidasEsporte) > 1) {
                array_push($relacionadasEsporte, $maisLidasEsporte[1]);
            }
            if (count($maisLidasEsporte) > 2) {
                array_push($relacionadasEsporte, $maisLidasEsporte[2]);
            }
        }

        $partners = $em->getRepository('BackBundle:Editorial')->findBy(
            array('partner' => true),array('name' => 'ASC'),null,null
        );
        $queryPartners = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial IN (:editorial)')
            ->setParameter(':editorial', $partners)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
        $partnerArticles = $queryPartners->getResult();

        $metrica = $em->getRepository('BackBundle:Metric')->findOneBy(
            array(),array('id' => 'ASC')
        );
        $acessowebsite = $metrica->getAcessowebsite();
        $acessowebsite = $acessowebsite + 1;
        $metrica->setAcessowebsite($acessowebsite);
        $em->persist($metrica);
        $em->flush();


        return $this->render('FrontBundle:Default:index.html.twig', array(
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'dayOfWeek' => $dayOfWeek,
            'time' => $time,

            'slides' => $slides,
            'banners' => $banners,
            'coverFeatures' => $coverFeatures,
            'colunas' => $colunas,
            'blogs' => $blogs,
            'groupsDestaque' => $groupsDestaque,
            'cultures' => $cultures,
            'partnerArticles' => $partnerArticles,

            'principal1' => $principal1,
            'principal2' => $principal2,
            'secundario1' => $secundario1,
            'secundario2' => $secundario2,
            'secundario3ComFoto' => $secundario3ComFoto,
            'featureComFoto1' => $featureComFoto1,
            'featureComFoto2' => $featureComFoto2,
            'featureSemFoto1' => $featureSemFoto1,
            'featureSemFoto2' => $featureSemFoto2,

            'politicaPrincipal' => $politicaPrincipal,
            'politicaSecundario' => $politicaSecundario,
            'politicaTerciario1' => $politicaTerciario1,
            'politicaTerciario2' => $politicaTerciario2,
            'politicaFoto1' => $politicaFoto1,
            'politicaFoto2' => $politicaFoto2,
            'policitaFrase1' => $policitaFrase1,
            'policitaFrase2' => $policitaFrase2,
            'maisLidaPolitica' => $maisLidaPolitica,
            'relacionadasPolitica' => $relacionadasPolitica,

            'entretePrincipal' => $entretePrincipal,
            'entreteSlide' => $entreteSlide,
            'entreteFotoPequena' => $entreteFotoPequena,
            'entreteFrase' => $entreteFrase,
            'entreteFoto1' => $entreteFoto1,
            'entreteFoto2' => $entreteFoto2,
            'entreteFoto3' => $entreteFoto3,
            'maisLidaEntretenimento' => $maisLidaEntretenimento,
            'relacionadasEntretenimento' => $relacionadasEntretenimento,

            'esportePrincipal' => $esportePrincipal,
            'esporteSecundarioFoto' => $esporteSecundarioFoto,
            'esporteFotoHorizontal1' => $esporteFotoHorizontal1,
            'esporteFrase' => $esporteFrase,
            'esporteFotoHorizontal2' => $esporteFotoHorizontal2,
            'esporteFotoGrande' => $esporteFotoGrande,
            'esporteFotoPequena' => $esporteFotoPequena,
            'maisLidaEsporte' => $maisLidaEsporte,
            'relacionadasEsporte' => $relacionadasEsporte,

            'municipioPrincipal' => $municipioPrincipal,
            'municipioFoto1' => $municipioFoto1,
            'municipioFoto2' => $municipioFoto2,
            'municipioFrase1' => $municipioFrase1,
            'municipioFrase2' => $municipioFrase2,
            'municipioFotoHorizontal' => $municipioFotoHorizontal,
            'municipioFotoGrande' => $municipioFotoGrande,
            'municipioFraseCurta' => $municipioFraseCurta,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5,

            'urgentes' => $urgentes
        ));
    }

    /**
     * @Route("/publicacao/{id}/", name="publicacao")
     * @Method({"GET"})
     */
    public function publicacaoAction(Article $article)
    {
        if ($article->getEditorial() != null) {
            $friendlyEditorial = $article->getEditorial()->getFriendlyEditorial();
        } else {
            $friendlyEditorial = '-';
        }

        $em = $this->getDoctrine()->getManager();

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->redirect($this->generateUrl('article01', array(
            'friendlyEditorial' => $friendlyEditorial,
            'friendly' => $article->getFriendly(),
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'id' => $article->getId(),
        )));
    }

    /**
     * @Route("/noticia/{friendlyEditorial}/{id}/{friendly}", name="article01")
    * @Method({"GET"})
     */
    public function articleAction(Article $article)
    {

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $article->setVisitorNumberWeb($article->getVisitorNumberWeb() + 1);
        $em->persist($article);
        $em->flush();

        date_default_timezone_set('America/Fortaleza');
        //$today = new \DateTime(date('Y-m-d H:i:s'));
        $today = new \DateTime($article->getDate()->format('Y-m-d H:i:s'));
        $dayOfWeek = $today->format('l');
        if ($dayOfWeek == 'Sunday') { $dayOfWeek = 'Domingo'; }
        if ($dayOfWeek == 'Monday') { $dayOfWeek = 'Segunda'; }
        if ($dayOfWeek == 'Tuesday') { $dayOfWeek = 'Terça'; }
        if ($dayOfWeek == 'Wednesday') { $dayOfWeek = 'Quarta'; }
        if ($dayOfWeek == 'Thursday') { $dayOfWeek = 'Quinta'; }
        if ($dayOfWeek == 'Friday') { $dayOfWeek = 'Sexta'; }
        if ($dayOfWeek == 'Saturday') { $dayOfWeek = 'Sábado'; }

        $day = $today->format('d');
        $year = $today->format('Y');

        $month = $today->format('M');
        if ($month == 'Jan') { $month = 'jan'; }
        if ($month == 'Feb') { $month = 'fev'; }
        if ($month == 'Mar') { $month = 'mar'; }
        if ($month == 'Apr') { $month = 'abr'; }
        if ($month == 'May') { $month = 'mai'; }
        if ($month == 'Jun') { $month = 'jun'; }
        if ($month == 'Jul') { $month = 'jul'; }
        if ($month == 'Ago') { $month = 'ago'; }
        if ($month == 'Sep') { $month = 'set'; }
        if ($month == 'Oct') { $month = 'out'; }
        if ($month == 'Nov') { $month = 'nov'; }
        if ($month == 'Dez') { $month = 'dez'; }

        $time = $today->format('G:i');

        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelArtigos = :visivel')
            ->setParameter(':todos', true)
            ->setParameter(':visivel', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerTopoEditoria1 = null;

        if (count($bannersTopoPagina) > 0) { 
            $i = rand(0, count($bannersTopoPagina) - 1);
            $bannerTopoPagina = $bannersTopoPagina[ $i ];
        }

        if (count($bannersDestaque) > 0) { 
            $i = rand(0, count($bannersDestaque) - 1);
            $bannerDestaque = $bannersDestaque[ $i ];
        }

        if (count($bannersTopoEditoria1) > 0) { 
            $i = rand(0, count($bannersTopoEditoria1) - 1);
            $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ];
        }

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        // Calculando o trigger do bloqueio de conteúdo
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $client = $this->get('security.token_storage')->getToken()->getUser();
            if($client instanceof Client) {
                $hasActiveSubscription = $client->hasActiveSubscription();
            }
        } else {
            $hasActiveSubscription = false;
        }

        $payWallTrigger = !$hasActiveSubscription && $article->getLocked();

        if ($payWallTrigger) {
            $content = $article->getContent();
            $article->setContent(substr($content, 0, 201));
        }

        return $this->render('FrontBundle:Default:article01.html.twig', array(
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'dayOfWeek' => $dayOfWeek,
            'time' => $time,
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'article' => $article,
            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,

            'payWallTrigger' => $payWallTrigger,
            'freeContent' => $article->getFreeContent()
        ));
    }


    /**
     * @Route("/manutencao", name="manutencao")
	* @Method({"GET"})
     */
    public function manutencaoAction()
    {
        return $this->render('FrontBundle:Default:manutencao.html.twig');
    }

     /**
     * @Route("/eleicao", name="eleicao")
    * @Method({"GET"})
     */
    public function eleicaoAction()
    {
        return $this->render('FrontBundle:Default:eleicao.html.twig');
    }

    /**
     * @Route("/contato", name="contato")
     * @Method({"GET"})
     */
    public function contatoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:contato.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
        ));
    }

    /**
     * @Route("/municipios", name="municipios")
     * @Method({"GET"})
     */
    public function municipiosAction(){

        $em = $this->getDoctrine()->getManager();
        $queryA = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryA->where('c.name LIKE :letra');
        $queryA->setParameter(':letra', 'a%');
        $queryA->orderBy('c.name', 'ASC');
        $query = $queryA->getQuery();
        $aCities = $query->getResult();

        $queryB = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryB->where('c.name LIKE :letra');
        $queryB->setParameter(':letra', 'b%');
        $queryB->orderBy('c.name', 'ASC');
        $query = $queryB->getQuery();
        $bCities = $query->getResult();

        $queryC = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryC->where('c.name LIKE :letra');
        $queryC->setParameter(':letra', 'c%');
        $queryC->orderBy('c.name', 'ASC');
        $query = $queryC->getQuery();
        $cCities = $query->getResult();

        $queryD = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryD->where('c.name LIKE :letra');
        $queryD->setParameter(':letra', 'd%');
        $queryD->orderBy('c.name', 'ASC');
        $query = $queryD->getQuery();
        $dCities = $query->getResult();

        $queryE = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryE->where('c.name LIKE :letra');
        $queryE->setParameter(':letra', 'e%');
        $queryE->orderBy('c.name', 'ASC');
        $query = $queryE->getQuery();
        $eCities = $query->getResult();

        $queryF = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryF->where('c.name LIKE :letra');
        $queryF->setParameter(':letra', 'f%');
        $queryF->orderBy('c.name', 'ASC');
        $query = $queryF->getQuery();
        $fCities = $query->getResult();

        $queryG = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryG->where('c.name LIKE :letra');
        $queryG->setParameter(':letra', 'g%');
        $queryG->orderBy('c.name', 'ASC');
        $query = $queryG->getQuery();
        $gCities = $query->getResult();

        $queryH = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryH->where('c.name LIKE :letra');
        $queryH->setParameter(':letra', 'h%');
        $queryH->orderBy('c.name', 'ASC');
        $query = $queryH->getQuery();
        $hCities = $query->getResult();

        $queryI = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryI->where('c.name LIKE :letra');
        $queryI->setParameter(':letra', 'i%');
        $queryI->orderBy('c.name', 'ASC');
        $query = $queryI->getQuery();
        $iCities = $query->getResult();

        $queryJ = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryJ->where('c.name LIKE :letra');
        $queryJ->setParameter(':letra', 'j%');
        $queryJ->orderBy('c.name', 'ASC');
        $query = $queryJ->getQuery();
        $jCities = $query->getResult();

        $queryK = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryK->where('c.name LIKE :letra');
        $queryK->setParameter(':letra', 'k%');
        $queryK->orderBy('c.name', 'ASC');
        $query = $queryK->getQuery();
        $kCities = $query->getResult();

        $queryL = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryL->where('c.name LIKE :letra');
        $queryL->setParameter(':letra', 'l%');
        $queryL->orderBy('c.name', 'ASC');
        $query = $queryL->getQuery();
        $lCities = $query->getResult();

        $queryM = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryM->where('c.name LIKE :letra');
        $queryM->setParameter(':letra', 'm%');
        $queryM->orderBy('c.name', 'ASC');
        $query = $queryM->getQuery();
        $mCities = $query->getResult();

        $queryN = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryN->where('c.name LIKE :letra');
        $queryN->setParameter(':letra', 'n%');
        $queryN->orderBy('c.name', 'ASC');
        $query = $queryN->getQuery();
        $nCities = $query->getResult();

        $queryO = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryO->where('c.name LIKE :letra');
        $queryO->setParameter(':letra', 'o%');
        $queryO->orderBy('c.name', 'ASC');
        $query = $queryO->getQuery();
        $oCities = $query->getResult();

        $queryP = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryP->where('c.name LIKE :letra');
        $queryP->setParameter(':letra', 'p%');
        $queryP->orderBy('c.name', 'ASC');
        $query = $queryP->getQuery();
        $pCities = $query->getResult();

        $queryQ = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryQ->where('c.name LIKE :letra');
        $queryQ->setParameter(':letra', 'q%');
        $queryQ->orderBy('c.name', 'ASC');
        $query = $queryQ->getQuery();
        $qCities = $query->getResult();

        $queryR = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryR->where('c.name LIKE :letra');
        $queryR->setParameter(':letra', 'r%');
        $queryR->orderBy('c.name', 'ASC');
        $query = $queryR->getQuery();
        $rCities = $query->getResult();

        $queryS = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryS->where('c.name LIKE :letra');
        $queryS->setParameter(':letra', 's%');
        $queryS->orderBy('c.name', 'ASC');
        $query = $queryS->getQuery();
        $sCities = $query->getResult();

        $queryT = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryT->where('c.name LIKE :letra');
        $queryT->setParameter(':letra', 't%');
        $queryT->orderBy('c.name', 'ASC');
        $query = $queryT->getQuery();
        $tCities = $query->getResult();

        $queryU = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryU->where('c.name LIKE :letra');
        $queryU->setParameter(':letra', 'u%');
        $queryU->orderBy('c.name', 'ASC');
        $query = $queryU->getQuery();
        $uCities = $query->getResult();

        $queryV = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryV->where('c.name LIKE :letra');
        $queryV->setParameter(':letra', 'v%');
        $queryV->orderBy('c.name', 'ASC');
        $query = $queryV->getQuery();
        $vCities = $query->getResult();

        $queryW = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryW->where('c.name LIKE :letra');
        $queryW->setParameter(':letra', 'w%');
        $queryW->orderBy('c.name', 'ASC');
        $query = $queryW->getQuery();
        $wCities = $query->getResult();

        $queryX = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryX->where('c.name LIKE :letra');
        $queryX->setParameter(':letra', 'x%');
        $queryX->orderBy('c.name', 'ASC');
        $query = $queryX->getQuery();
        $xCities = $query->getResult();

        $queryY = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryY->where('c.name LIKE :letra');
        $queryY->setParameter(':letra', 'y%');
        $queryY->orderBy('c.name', 'ASC');
        $query = $queryY->getQuery();
        $yCities = $query->getResult();

        $queryZ = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryZ->where('c.name LIKE :letra');
        $queryZ->setParameter(':letra', 'z%');
        $queryZ->orderBy('c.name', 'ASC');
        $query = $queryZ->getQuery();
        $zCities = $query->getResult();

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );
        return $this->render('FrontBundle:Default:cities.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'cidadesA' => $aCities,
            'cidadesB' => $bCities,
            'cidadesC' => $cCities,
            'cidadesD' => $dCities,
            'cidadesE' => $eCities,
            'cidadesF' => $fCities,
            'cidadesG' => $gCities,
            'cidadesH' => $hCities,
            'cidadesI' => $iCities,
            'cidadesJ' => $jCities,
            'cidadesK' => $kCities,
            'cidadesL' => $lCities,
            'cidadesM' => $mCities,
            'cidadesN' => $nCities,
            'cidadesO' => $oCities,
            'cidadesP' => $pCities,
            'cidadesQ' => $qCities,
            'cidadesR' => $rCities,
            'cidadesS' => $sCities,
            'cidadesT' => $tCities,
            'cidadesU' => $uCities,
            'cidadesV' => $vCities,
            'cidadesW' => $wCities,
            'cidadesX' => $xCities,
            'cidadesY' => $yCities,
            'cidadesZ' => $zCities
        ));
    }


    /**
     * @Route("/municipios/{id}/{friendlyCity}", name="municipios_feed")
     * @Method({"GET", "POST"})
     */
    public function municipiosFeedAction(Request $request, $id)
    {
        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $city = $em->getRepository('BackBundle:City')->findOneById($id);

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.city = :cidade');
        $queryArticle->setParameter(':cidade', $city);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $mostReads = $query->getResult();

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.city is not null');
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $lastOnes = $query->getResult();


        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.city = :cidade');
        $queryArticle->setParameter(':cidade', $city);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.date', 'DESC');
        $query = $queryArticle->getQuery();
        $articles = $query->getResult();
        $hits = (count($articles) / 10);
        $count = count($articles);
        $page = 1;

        if ($hits > 1) {
            $queryArticle->setFirstResult(10 * ($page - 1));
            $queryArticle->setMaxResults(10);
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
        }

        $filterform = $this->createForm('FrontBundle\Filter\FeedBlogType', NULL, array(
            'action' => $this->generateUrl('municipios_feed', array(
                'id' => $city->getId(),
                'friendlyCity' => $city->getFriendlyCity()
            )),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
            $queryArticle->where('a.city = :cidade');
            $queryArticle->setParameter(':cidade', $city);
            $queryArticle->andWhere('a.publishedAt <= :data');
            $queryArticle->setParameter(':data', $today);

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryArticle->orderBy('a.date', 'DESC');
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
            $hits = (count($articles) / 10);
            $count = count($articles);;

            if ($hits > 1) {
                $queryArticle->setFirstResult(10 * ($page - 1));
                $queryArticle->setMaxResults(10);
                $query = $queryArticle->getQuery();
                $articles = $query->getResult();
            }
        }

        $blogsDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true, 'kind' => 'Blog'),array('groupName' => 'ASC'), null, null
        );

        $colunasDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true, 'kind' => 'Coluna'),array('groupName' => 'ASC'), null, null
        );

        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelMunicipios = :visivel')
            ->setParameter(':todos', true)
            ->setParameter(':visivel', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }


        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:municipiosfeed.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,

            'city' => $city,
            'mostReads' => $mostReads,
            'lastOnes' => $lastOnes,
            'articles' => $articles,
            'blogsDestaque' => $blogsDestaque,
            'colunasDestaque' => $colunasDestaque,
            'filter_form' => $filterform->createView(),
            'hits' => ceil($hits),
            'count' => $count,
            'page' => $page,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5
        ));
    }

    /**
     * @Route("/municipios/{id}/{articleId}", name="municipios_publicacao")
     * @Method({"GET"})
     */
    public function municipiosPublicacaoArticleAction(Request $request, City $municipio, Article $article)
    {
        $em = $this->getDoctrine()->getManager();
        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->redirect($this->generateUrl('municipios_article', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'friendlyCity' => $municipio->getFriendlyCity(),
            'id' => $municipio->getId(),
            'articleId' => $article->getId(),
            'friendly' => $article->getFriendly()
        )));
    }

    /**
     * @Route("/municipios/{id}/{friendlyCity}/{articleId}/{friendly}", name="municipios_article")
     * @Method({"GET", "POST"})
     */
    public function municipiosArticleAction(Request $request, $id, $articleId)
    {

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $city = $em->getRepository('BackBundle:City')->findOneById($id);

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.city = :cidade');
        $queryArticle->setParameter(':cidade', $city);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $mostReads = $query->getResult();

        $article = $em->getRepository('BackBundle:Article')->findOneById($articleId);
        $article->setVisitorNumberWeb($article->getVisitorNumberWeb() + 1);
        $em->persist($article);
        $em->flush();

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.city = :cidade');
        $queryArticle->setParameter(':cidade', $city);
        $queryArticle->andWhere('a.id < :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(1);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $articlesNext = $query->getResult();
        $next = null;
        if (count($articlesNext) > 0){
            $next = $articlesNext[0];
        }

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.city = :cidade');
        $queryArticle->setParameter(':cidade', $city);
        $queryArticle->andWhere('a.id > :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(1);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $articlesPrev = $query->getResult();
        $prev = null;
        if (count($articlesPrev) > 0){
            $prev = $articlesPrev[0];
        }


        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.city = :cidade');
        $queryArticle->setParameter(':cidade', $city);
        $queryArticle->andWhere('a.id != :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(5);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $latestArticles = $query->getResult();


        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelArtigos = :visivel')
            ->setParameter(':todos', true)
            ->setParameter(':visivel', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }


        date_default_timezone_set('America/Fortaleza');
        //$today = new \DateTime(date('Y-m-d H:i:s'));
        $today = new \DateTime($article->getDate()->format('Y-m-d H:i:s'));
        $dayOfWeek = $today->format('l');
        if ($dayOfWeek == 'Sunday') { $dayOfWeek = 'Domingo'; }
        if ($dayOfWeek == 'Monday') { $dayOfWeek = 'Segunda'; }
        if ($dayOfWeek == 'Tuesday') { $dayOfWeek = 'Terça'; }
        if ($dayOfWeek == 'Wednesday') { $dayOfWeek = 'Quarta'; }
        if ($dayOfWeek == 'Thursday') { $dayOfWeek = 'Quinta'; }
        if ($dayOfWeek == 'Friday') { $dayOfWeek = 'Sexta'; }
        if ($dayOfWeek == 'Saturday') { $dayOfWeek = 'Sábado'; }

        $day = $today->format('d');
        $year = $today->format('Y');

        $month = $today->format('M');
        if ($month == 'Jan') { $month = 'jan'; }
        if ($month == 'Feb') { $month = 'fev'; }
        if ($month == 'Mar') { $month = 'mar'; }
        if ($month == 'Apr') { $month = 'abr'; }
        if ($month == 'May') { $month = 'mai'; }
        if ($month == 'Jun') { $month = 'jun'; }
        if ($month == 'Jul') { $month = 'jul'; }
        if ($month == 'Ago') { $month = 'ago'; }
        if ($month == 'Sep') { $month = 'set'; }
        if ($month == 'Oct') { $month = 'out'; }
        if ($month == 'Nov') { $month = 'nov'; }
        if ($month == 'Dez') { $month = 'dez'; }

        $time = $today->format('G:i');

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:municipioArticle.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,

            'city' => $city,
            'mostReads' => $mostReads,
            'article' => $article,
            'next' => $next,
            'prev' => $prev,
            'latestArticles' => $latestArticles,
            'day' => $day,
            'year' => $year,
            'month' => $month,
            'time' => $time,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5
        ));
    }

    /**
     * @Route("/anuncie", name="commercial")
     * @Method({"GET"})
     */
    public function commercialAction()
    {
        $em = $this->getDoctrine()->getManager();
        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );
        return $this->render('FrontBundle:Default:commercial.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
        ));
    }

    /**
     * @Route("/mapa", name="map")
     * @Method({"GET"})
     */
    public function mapAction()
    {
        $em = $this->getDoctrine()->getManager();
        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:map.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
        ));
    }

    /**
     * @Route("/editorias", name="editorials")
     * @Method({"GET"})
     */
    public function editorialsAction(){

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        $editorias = $em->getRepository('BackBundle:Editorial')->findBy(
            array(), array('name' => 'ASC'), null, null
        );

        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelEditorias = :visivel')
            ->setParameter(':todos', true)
            ->setParameter(':visivel', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }
        }

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        return $this->render('FrontBundle:Default:editoriais.html.twig', array(
            'editorials' => $editorials,
            'editorias' => $editorias,
            'blogs' => $blogs,
            'colunas' => $colunas,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5
        ));


    }

    /**
     * @Route("/editorias/{id}/{friendlyEditorial}", name="editorial_feed")
     * @Method({"GET", "POST"})
     */
    public function editorialFeedIndexAction(Request $request, $friendlyEditorial, $id){

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $edit = $em->getRepository('BackBundle:Editorial')->findOneById($id);

        $statsService = $this->get('stats_service');

        // Montando o objeto da notícia mais lida de politica nos ultimos 2 meses
        $date_begin = new \DateTime(date('Y-m-d H:i:s', strtotime('-220 day')));
        $mostReads = $statsService->listMostReadByEditorial($edit->getName(), $date_begin, $today, 5);

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.editorial is not null');
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $lastOnes = $query->getResult();


        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.editorial = :editoria');
        $queryArticle->setParameter(':editoria', $edit);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->andWhere('a.status = :status');
        $queryArticle->setParameter(':status', 'Aprovado');
        $queryArticle->orderBy('a.date', 'DESC');
        $query = $queryArticle->getQuery();
        $articles = $query->getResult();
        $hits = (count($articles) / 10);
        $count = count($articles);
        $page = 1;

        if ($hits > 1) {
            $queryArticle->setFirstResult(10 * ($page - 1));
            $queryArticle->setMaxResults(10);
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
        }

        $filterform = $this->createForm('FrontBundle\Filter\FeedBlogType', NULL, array(
            'action' => $this->generateUrl('editorial_feed', array(
                'id' => $edit->getId(),
                'friendlyEditorial' => $edit->getFriendlyEditorial()
            )),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
            $queryArticle->where('a.editorial = :editoria');
            $queryArticle->setParameter(':editoria', $edit);
            $queryArticle->andWhere('a.publishedAt <= :data');
            $queryArticle->setParameter(':data', $today);
            $queryArticle->andWhere('a.status = :status');
            $queryArticle->setParameter(':status', 'Aprovado');

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryArticle->orderBy('a.date', 'DESC');
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
            $hits = (count($articles) / 10);
            $count = count($articles);;

            if ($hits > 1) {
                $queryArticle->setFirstResult(10 * ($page - 1));
                $queryArticle->setMaxResults(10);
                $query = $queryArticle->getQuery();
                $articles = $query->getResult();
            }
        }

        $editorialDestaque = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR :editorial MEMBER OF b.editorials')
            ->setParameter(':todos', true)
            ->setParameter(':editorial', $edit)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }


        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );
        return $this->render('FrontBundle:Default:editorialFeed.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,

            'editorial' => $edit,
            'mostReads' => $mostReads,
            'lastOnes' => $lastOnes,
            'articles' => $articles,
            'blogsDestaque' => $editorialDestaque,
            'filter_form' => $filterform->createView(),
            'hits' => ceil($hits),
            'count' => $count,
            'page' => $page,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5
        ));

    }

    /**
     * @Route("/blogs", name="newsgroups_blogs")
     * @Method({"GET"})
     */
    public function blogsAction()
    {

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $kind = 'blogs';

        if ($kind == 'blogs') { $kindSearch = 'Blog'; }
        if ($kind == 'colunas') { $kindSearch = 'Coluna'; }

        $newsgroups = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => $kindSearch),array('groupName' => 'ASC'),null,null
        );

        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelBlogs = :visivel')
            ->setParameter(':todos', true)
            ->setParameter(':visivel', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:newsgroups.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'newsgroups' => $newsgroups,
            'kind' => $kind,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5
        ));


    }

    /**
     * @Route("/colunas", name="newsgroups_colunas")
     * @Method({"GET"})
     */
    public function colunasAction()
    {

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $kind = 'colunas';

        if ($kind == 'blogs') { $kindSearch = 'Blog'; }
        if ($kind == 'colunas') { $kindSearch = 'Coluna'; }

        $newsgroups = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => $kindSearch),array('groupName' => 'ASC'),null,null
        );

        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelColunas = :visivel')
            ->setParameter(':todos', true)
            ->setParameter(':visivel', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:newsgroups.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'newsgroups' => $newsgroups,
            'kind' => $kind,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5
        ));


    }


    /**
     * @Route("/blogs/{id}/{friendlyNewsgroup}", name="newsgroup_blogs_feed")
     * @Method({"GET", "POST"})
     */
    public function newsgroupBlogsFeedAction(Request $request, $id, $friendlyNewsgroup)
    {

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $kind = 'blogs';
        $newsgroup = $em->getRepository('BackBundle:NewsGroup')->findOneById($id);

        if (is_numeric($friendlyNewsgroup)){
            $article = $em->getRepository('BackBundle:Article')->findOneById($friendlyNewsgroup);
            if ($article != null) {
                return $this->redirect($this->generateUrl('newsgroup_blogs_article', array(
                    'friendlyNewsgroup' => $newsgroup->getFriendlyNewsgroup(),
                    'newsgroupId' => $newsgroup->getId(),
                    'articleId' => $article->getId(),
                    'friendly' => $article->getFriendly()
                )));
            } else {
                return $this->redirect($this->generateUrl('home'));
            }
        }

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :newsgroup');
        $queryArticle->setParameter(':newsgroup', $newsgroup);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $mostReads = $query->getResult();

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :tipo');
        $queryArticle->setParameter(':tipo', $newsgroup);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->andWhere('a.status = :status');
        $queryArticle->setParameter(':status', 'Aprovado');
        $queryArticle->orderBy('a.date', 'DESC');
        $query = $queryArticle->getQuery();
        $articles = $query->getResult();
        $hits = (count($articles) / 10);
        $count = count($articles);
        $page = 1;

        if ($hits > 1) {
            $queryArticle->setFirstResult(10 * ($page - 1));
            $queryArticle->setMaxResults(10);
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
        }

        $filterform = $this->createForm('FrontBundle\Filter\FeedBlogType', NULL, array(
            'action' => $this->generateUrl('newsgroup_blogs_feed', array(
                'id' => $newsgroup->getId(),
                'friendlyNewsgroup' => $newsgroup->getFriendlyNewsgroup()
            )),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
            $queryArticle->where('a.newsgroup = :tipo');
            $queryArticle->setParameter(':tipo', $newsgroup);
            $queryArticle->andWhere('a.publishedAt <= :data');
            $queryArticle->setParameter(':data', $today);
            $queryArticle->andWhere('a.status = :status');
            $queryArticle->setParameter(':status', 'Aprovado');

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryArticle->orderBy('a.date', 'DESC');
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
            $hits = (count($articles) / 10);
            $count = count($articles);;

            if ($hits > 1) {
                $queryArticle->setFirstResult(10 * ($page - 1));
                $queryArticle->setMaxResults(10);
                $query = $queryArticle->getQuery();
                $articles = $query->getResult();
            }
        }


        $blogsDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true, 'kind' => 'Blog'),array('groupName' => 'ASC'), null, null
        );

        $colunasDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true, 'kind' => 'Coluna'),array('groupName' => 'ASC'), null, null
        );


        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR :newsgroup MEMBER OF b.newsgroups')
            ->setParameter(':todos', true)
            ->setParameter(':newsgroup', $newsgroup)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:newsgroupfeed.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'newsgroup' => $newsgroup,
            'kind' => $kind,
            'mostReads' => $mostReads,
            'articles' => $articles,
            'blogsDestaque' => $blogsDestaque,
            'colunasDestaque' => $colunasDestaque,
            'filter_form' => $filterform->createView(),
            'hits' => ceil($hits),
            'count' => $count,
            'page' => $page,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5
        ));
    }

    /**
     * @Route("/colunas/{id}/{friendlyNewsgroup}", name="newsgroup_colunas_feed")
     * @Method({"GET", "POST"})
     */
    public function newsgroupColunasFeedAction(Request $request, $id, $friendlyNewsgroup)
    {

        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $kind = 'colunas';
        $newsgroup = $em->getRepository('BackBundle:NewsGroup')->findOneById($id);

        if (is_numeric($friendlyNewsgroup)){
            $article = $em->getRepository('BackBundle:Article')->findOneById($friendlyNewsgroup);
            if ($article != null) {
                return $this->redirect($this->generateUrl('newsgroup_colunas_article', array(
                    'friendlyNewsgroup' => $newsgroup->getFriendlyNewsgroup(),
                    'newsgroupId' => $newsgroup->getId(),
                    'articleId' => $article->getId(),
                    'friendly' => $article->getFriendly()
                )));
            } else {
                return $this->redirect($this->generateUrl('home'));
            }
        }

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :newsgroup');
        $queryArticle->setParameter(':newsgroup', $newsgroup);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $mostReads = $query->getResult();

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :tipo');
        $queryArticle->setParameter(':tipo', $newsgroup);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->andWhere('a.status = :status');
        $queryArticle->setParameter(':status', 'Aprovado');
        $queryArticle->orderBy('a.date', 'DESC');
        $query = $queryArticle->getQuery();
        $articles = $query->getResult();
        $hits = (count($articles) / 10);
        $count = count($articles);
        $page = 1;

        if ($hits > 1) {
            $queryArticle->setFirstResult(10 * ($page - 1));
            $queryArticle->setMaxResults(10);
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
        }

        $filterform = $this->createForm('FrontBundle\Filter\FeedBlogType', NULL, array(
            'action' => $this->generateUrl('newsgroup_colunas_feed', array(
                'id' => $newsgroup->getId(),
                'friendlyNewsgroup' => $newsgroup->getFriendlyNewsgroup()
            )),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
            $queryArticle->where('a.newsgroup = :tipo');
            $queryArticle->setParameter(':tipo', $newsgroup);
            $queryArticle->andWhere('a.publishedAt <= :data');
            $queryArticle->andWhere('a.status = :status');
            $queryArticle->setParameter(':status', 'Aprovado');
            $queryArticle->setParameter(':data', $today);

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryArticle->orderBy('a.date', 'DESC');
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
            $hits = (count($articles) / 10);
            $count = count($articles);;

            if ($hits > 1) {
                $queryArticle->setFirstResult(10 * ($page - 1));
                $queryArticle->setMaxResults(10);
                $query = $queryArticle->getQuery();
                $articles = $query->getResult();
            }
        }


        $blogsDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true, 'kind' => 'Blog'),array('groupName' => 'ASC'), null, null
        );

        $colunasDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true, 'kind' => 'Coluna'),array('groupName' => 'ASC'), null, null
        );


        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR :newsgroup MEMBER OF b.newsgroups')
            ->setParameter(':todos', true)
            ->setParameter(':newsgroup', $newsgroup)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:newsgroupfeed.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'newsgroup' => $newsgroup,
            'kind' => $kind,
            'mostReads' => $mostReads,
            'articles' => $articles,
            'blogsDestaque' => $blogsDestaque,
            'colunasDestaque' => $colunasDestaque,
            'filter_form' => $filterform->createView(),
            'hits' => ceil($hits),
            'count' => $count,
            'page' => $page,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5
        ));
    }

    /**
     * @Route("/blogs/{newsgroupId}/{friendlyNewsgroup}/{articleId}/{friendly}", name="newsgroup_blogs_article")
     * @Method({"GET", "POST"})
     */
    public function newsgroupBlogsArticleAction(Request $request, $newsgroupId, $articleId)
    {
        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $kind = 'blogs';
        $newsgroup = $em->getRepository('BackBundle:NewsGroup')->findOneById($newsgroupId);

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :newsgroup');
        $queryArticle->setParameter(':newsgroup', $newsgroup);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $mostReads = $query->getResult();

        $article = $em->getRepository('BackBundle:Article')->findOneById($articleId);
        $article->setVisitorNumberWeb($article->getVisitorNumberWeb() + 1);
        $em->persist($article);
        $em->flush();

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :tipo');
        $queryArticle->setParameter(':tipo', $newsgroup);
        $queryArticle->andWhere('a.id < :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(1);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $articlesNext = $query->getResult();
        $next = null;
        if (count($articlesNext) > 0){
            $next = $articlesNext[0];
        }

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :tipo');
        $queryArticle->setParameter(':tipo', $newsgroup);
        $queryArticle->andWhere('a.id > :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(1);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $articlesPrev = $query->getResult();
        $prev = null;
        if (count($articlesPrev) > 0){
            $prev = $articlesPrev[0];
        }

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :tipo');
        $queryArticle->setParameter(':tipo', $newsgroup);
        $queryArticle->andWhere('a.id != :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(5);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $lastestArticles = $query->getResult();


        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR :newsgroup MEMBER OF b.newsgroups')
            ->setParameter(':todos', true)
            ->setParameter(':newsgroup', $newsgroup)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }   


        date_default_timezone_set('America/Fortaleza');
        //$today = new \DateTime(date('Y-m-d H:i:s'));
        $today = new \DateTime($article->getDate()->format('Y-m-d H:i:s'));
        $dayOfWeek = $today->format('l');
        if ($dayOfWeek == 'Sunday') { $dayOfWeek = 'Domingo'; }
        if ($dayOfWeek == 'Monday') { $dayOfWeek = 'Segunda'; }
        if ($dayOfWeek == 'Tuesday') { $dayOfWeek = 'Terça'; }
        if ($dayOfWeek == 'Wednesday') { $dayOfWeek = 'Quarta'; }
        if ($dayOfWeek == 'Thursday') { $dayOfWeek = 'Quinta'; }
        if ($dayOfWeek == 'Friday') { $dayOfWeek = 'Sexta'; }
        if ($dayOfWeek == 'Saturday') { $dayOfWeek = 'Sábado'; }

        $day = $today->format('d');
        $year = $today->format('Y');

        $month = $today->format('M');
        if ($month == 'Jan') { $month = 'jan'; }
        if ($month == 'Feb') { $month = 'fev'; }
        if ($month == 'Mar') { $month = 'mar'; }
        if ($month == 'Apr') { $month = 'abr'; }
        if ($month == 'May') { $month = 'mai'; }
        if ($month == 'Jun') { $month = 'jun'; }
        if ($month == 'Jul') { $month = 'jul'; }
        if ($month == 'Ago') { $month = 'ago'; }
        if ($month == 'Sep') { $month = 'set'; }
        if ($month == 'Oct') { $month = 'out'; }
        if ($month == 'Nov') { $month = 'nov'; }
        if ($month == 'Dez') { $month = 'dez'; }

        $time = $today->format('G:i');

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );
        
         // Calculando o trigger do bloqueio de conteúdo
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $client = $this->get('security.token_storage')->getToken()->getUser();
            if($client instanceof Client) {
                $hasActiveSubscription = $client->hasActiveSubscription();
            }
        } else {
            $hasActiveSubscription = false;
        }

        $payWallTrigger = !$hasActiveSubscription && $article->getLocked();

        return $this->render('FrontBundle:Default:newsgrouparticle.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'newsgroup' => $newsgroup,
            'kind' => $kind,
            'mostReads' => $mostReads,
            'article' => $article,
            'next' => $next,
            'prev' => $prev,
            'lastestArticles' => $lastestArticles,
            'day' => $day,
            'year' => $year,
            'month' => $month,
            'time' => $time,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5,

            'payWallTrigger' => $payWallTrigger,
            'freeContent' => $article->getFreeContent()
        ));
    }


    /**
     * @Route("/colunas/{newsgroupId}/{friendlyNewsgroup}/{articleId}/{friendly}", name="newsgroup_colunas_article")
     * @Method({"GET", "POST"})
     */
    public function newsgroupColunasArticleAction(Request $request, $newsgroupId, $articleId)
    {
        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));

        $em = $this->getDoctrine()->getManager();
        $kind = 'colunas';
        $newsgroup = $em->getRepository('BackBundle:NewsGroup')->findOneById($newsgroupId);

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :newsgroup');
        $queryArticle->setParameter(':newsgroup', $newsgroup);
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->orderBy('a.visitorNumberWeb', 'DESC');
        $queryArticle->setMaxResults(5);
        $query = $queryArticle->getQuery();
        $mostReads = $query->getResult();

        $article = $em->getRepository('BackBundle:Article')->findOneById($articleId);
        $article->setVisitorNumberWeb($article->getVisitorNumberWeb() + 1);
        $em->persist($article);
        $em->flush();

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :tipo');
        $queryArticle->setParameter(':tipo', $newsgroup);
        $queryArticle->andWhere('a.id < :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(1);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $articlesNext = $query->getResult();
        $next = null;
        if (count($articlesNext) > 0){
            $next = $articlesNext[0];
        }

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :tipo');
        $queryArticle->setParameter(':tipo', $newsgroup);
        $queryArticle->andWhere('a.id > :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(1);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $articlesPrev = $query->getResult();
        $prev = null;
        if (count($articlesPrev) > 0){
            $prev = $articlesPrev[0];
        }

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.newsgroup = :tipo');
        $queryArticle->setParameter(':tipo', $newsgroup);
        $queryArticle->andWhere('a.id != :id');
        $queryArticle->setParameter(':id', $article->getId());
        $queryArticle->andWhere('a.publishedAt <= :data');
        $queryArticle->setParameter(':data', $today);
        $queryArticle->setMaxResults(5);
        $queryArticle->orderBy('a.id', 'DESC');
        $query = $queryArticle->getQuery();
        $lastestArticles = $query->getResult();


        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR :newsgroup MEMBER OF b.newsgroups')
            ->setParameter(':todos', true)
            ->setParameter(':newsgroup', $newsgroup)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        /*$queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->getQuery();
        $banners = $queryBanner->getResult();*/

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }   


        date_default_timezone_set('America/Fortaleza');
        //$today = new \DateTime(date('Y-m-d H:i:s'));
        $today = new \DateTime($article->getDate()->format('Y-m-d H:i:s'));
        $dayOfWeek = $today->format('l');
        if ($dayOfWeek == 'Sunday') { $dayOfWeek = 'Domingo'; }
        if ($dayOfWeek == 'Monday') { $dayOfWeek = 'Segunda'; }
        if ($dayOfWeek == 'Tuesday') { $dayOfWeek = 'Terça'; }
        if ($dayOfWeek == 'Wednesday') { $dayOfWeek = 'Quarta'; }
        if ($dayOfWeek == 'Thursday') { $dayOfWeek = 'Quinta'; }
        if ($dayOfWeek == 'Friday') { $dayOfWeek = 'Sexta'; }
        if ($dayOfWeek == 'Saturday') { $dayOfWeek = 'Sábado'; }

        $day = $today->format('d');
        $year = $today->format('Y');

        $month = $today->format('M');
        if ($month == 'Jan') { $month = 'jan'; }
        if ($month == 'Feb') { $month = 'fev'; }
        if ($month == 'Mar') { $month = 'mar'; }
        if ($month == 'Apr') { $month = 'abr'; }
        if ($month == 'May') { $month = 'mai'; }
        if ($month == 'Jun') { $month = 'jun'; }
        if ($month == 'Jul') { $month = 'jul'; }
        if ($month == 'Ago') { $month = 'ago'; }
        if ($month == 'Sep') { $month = 'set'; }
        if ($month == 'Oct') { $month = 'out'; }
        if ($month == 'Nov') { $month = 'nov'; }
        if ($month == 'Dez') { $month = 'dez'; }

        $time = $today->format('G:i');
        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        // Calculando o trigger do bloqueio de conteúdo
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $client = $this->get('security.token_storage')->getToken()->getUser();
            if($client instanceof Client) {
                $hasActiveSubscription = $client->hasActiveSubscription();
            }
        } else {
            $hasActiveSubscription = false;
        }

        $payWallTrigger = !$hasActiveSubscription && $article->getLocked();

        if ($payWallTrigger) {
            $content = $article->getContent();
            $article->setContent(substr($content, 0, 201));
        }

        // Resgatando possivel erro de autenticação
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('FrontBundle:Default:newsgrouparticle.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'newsgroup' => $newsgroup,
            'kind' => $kind,
            'mostReads' => $mostReads,
            'article' => $article,
            'next' => $next,
            'prev' => $prev,
            'lastestArticles' => $lastestArticles,
            'day' => $day,
            'year' => $year,
            'month' => $month,
            'time' => $time,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5,

            'payWallTrigger' => $payWallTrigger,
            'freeContent' => $article->getFreeContent(),

            'error' => $error
        ));
    }

    /**
     * @Route("/mobile/{articleId}", name="mobile_article")
     * @Method({"GET", "POST"})
     */
    public function mobileArticleAction(Request $request, $articleId)
    {
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('BackBundle:Article')->findOneById($articleId);
        $article->setVisitorNumberApp($article->getVisitorNumberApp() + 1);
        $em->persist($article);
        $em->flush();

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        return $this->render('FrontBundle:Default:mobile.html.twig', array(
            'editorials' => $editorials,
            'blogs' => $blogs,
            'colunas' => $colunas,
            'article' => $article,
        ));
    }
}
