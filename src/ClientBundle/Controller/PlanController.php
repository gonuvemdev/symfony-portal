<?php
namespace ClientBundle\Controller;

use ClientBundle\Entity\Plan;
use ClientBundle\Form\PlanType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Plan controller.
 *
 * @Route("/admin/plans")
 */
class PlanController extends Controller
{
    /**
     * @Route("/new_plan", name="new_plan")
     */
    public function newPlanAction(Request $request)
    {
        // Create a new blank user and process the form
        $plan = new Plan();
        $form = $this->createForm(PlanType::class, $plan);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $charge = $plan->getCharge();
            switch($charge) {
                case 'AUTO':
                    $result = createPlanPagseguro();
                    $plan->setPagSeguroCode($result->code);
                    break;
                case 'EDUZZ':
                    break;
            }
                      
            /** TROCAR POR REFERENCIA UNICA */
            $plan->setReference('#CODIGOUNICO');

            $em = $this->getDoctrine()->getManager();
            $em->persist($plan);
            $em->flush();

            return $this->redirectToRoute('plan_show', ['id' => $plan->getId()]);
        }

        return $this->render('BackBundle:Subscriptions:newPlan.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/plans", name="list_plans")
     */
    public function listPlansAction(Request $request)
    {
        // Instanciando o Doctrine para manipular o Banco
        $em = $this->getDoctrine()->getManager();

        // Recuprando todos os planos do banco
        $queryPlans = $em->getRepository('ClientBundle:Plan')->createQueryBuilder('a');
        $queryPlans->orderBy('a.id', 'DESC');
        $query = $queryPlans->getQuery();
        $plans = $query->getResult();
        $hits = (count($plans) / 25);
        $count = count($plans);
        $page = 1;

        // Paginando com o query parameters
        $queryPage = $request->query->get('page');
        if($queryPage !== null) {
            $page = $queryPage;
        }

        // Calculando para puxar apenas a página solicitada
        if ($hits > 1) {
            $queryPlans->setFirstResult(25 * ($page - 1));
            $queryPlans->setMaxResults(25);
            $query = $queryPlans->getQuery();
            $plans = $query->getResult();
        }

        return $this->render('BackBundle:Subscriptions:listPlans.html.twig', [
            'plans' => $plans,
            'hits' => ceil($hits),
            'page' => $page
        ]);
    }

     /**
     * @Route("/show/{id}", name="plan_show")
     */
    public function showPlanAction(Plan $plan)
    {
        return $this->render('BackBundle:Subscriptions:showPlan.html.twig', [
            'plan' => $plan
        ]);
    }

    /**
     * @Route("/{id}/edit", name="plan_edit")
     *  @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Plan $plan)
    {
        $editForm = $this->createForm(PlanType::class, $plan);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('plan_edit', array('id' => $plan->getId()));
        }

        return $this->render('BackBundle:Subscriptions:editPlan.html.twig', array(
            'plan' => $plan,
            'edit_form' => $editForm->createView(),
        ));
    }

    private function createPlanPagseguro(Plan $plan)
    {
        // Criando o plano no PagSeguro
        $pagseguroClient = $this->get('pagseguro_client');

        $result = $pagseguroClient->createPlan($plan);

        return $result;
    }
}