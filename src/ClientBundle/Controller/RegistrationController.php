<?php
namespace ClientBundle\Controller;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\ClientStore;
use ClientBundle\Entity\Address;
use ClientBundle\Entity\Subscription;
use ClientBundle\Form\Eduzz\ClientType;

use ClientBundle\Helper\SubscriptionHelper;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegistrationController extends Controller
{
    /**
     * @Route("/new_client", name="new_client")
     */
    public function newClientAction(Request $request)
    {
        // Create a new blank user and process the form
        $client = new Client();
        $form = $this->createForm(clientType::class, $client);
        $form->handleRequest($request);

        // Recuperando os planos cadastrado
        $plans = $this->get('plan_helper')->retrieveAll();

        return $this->render('ClientBundle:Eduzz:registerEduzz.html.twig', array(
            'form' => $form->createView(),
            'plans' => $plans,
        ));
    }


    public function newclient(Request $request)
    {
        // Instanciando um manager do Doctrine
        $em = $this->getDoctrine()->getManager();

        // Create a new blank user and process the form
        $client = new Client();
        $form = $this->createForm(clientType::class, $client);
        $form->handleRequest($request);

        try {
            // Requisitando o serviço do PagSeguro
            $pagseguroClient = $this->get('pagseguro_client');
            // Gerando uma ID de seção de pagamento
            $sessionCode =  $pagseguroClient->generateSessionId();
        } catch(\Exception $e) {
            dump($e->getMessage());
        }
    
        // Recuperando os planos cadastrado
        $plans = $this->get('plan_helper')->retrieveAll();

        // Pegando a variavel do ambiente
        $environment = $this->getParameter('pagseguro_environment');

        if($form->isSubmitted() && $form->isValid())
        {
            // Encode the new users password
            $encoder = $this->get('security.password_encoder');

            $password = $encoder->encodePassword($client, $client->getPlainPassword());
            $client->setPassword($password);

            // Set their role
            $client->setRole('ROLE_USER');

            // Setando as informações sobre o cartão atual
            $client->setLast4($request->request->get('cardLast4'));
            $client->setCardBrand($request->request->get('cardBrand'));
            $client->setCardHolderDOB($request->request->get('card_holders_dob'));
            $client->setCardHolderName($request->request->get('card_name'));
            $client->setCardHolderCPF($request->request->get('card_holders_cpf'));

            // Setando a cidade do cliente
            $client_data = $request->request->get('client');
            $address_data = new Address();
            $address_data->updateInfo($client_data['address']);
            $client->setAddress($address_data);

            // Setando o telefone 
            $client->setPhone($request->request->get('phoneNumber'));

            // Limpando o CPF do cliente;
            $clientCpf = $client->getCpf();
            $clientCpf = str_replace(".", "", $clientCpf);
            $clientCpf = str_replace("-", "", $clientCpf);
            $client->setCpf($clientCpf);

            try {   
               $this->checkout($request, $client);
            } catch(\Exception $e) {
                $error_decode = json_decode($e->getMessage());
                foreach($error_decode->errors as $key => $value){
                    $this->addFlash("error", $this->get('translator')->trans($key, array(), 'pagseguro'));
                }
                return $this->render('ClientBundle:Auth:register.html.twig', array(
                    'form' => $form->createView(),
                    'pagseguroSessionId' => $sessionCode->getResult(),
                    'plans' => $plans,
                    'environment' => $this->getParameter('pagseguro_environment')
                ));
            }

            // Buscando o plano correto para exibir no checkout
            $plan = $this->get('plan_helper')->findByCode($request->request->get('plan'));

            // Logando automaticamente o usuário
            $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess(
                    $client,
                    $request,
                    $this->get('app.form_login_authenticator'),
                    'main'
                );
            
            // Definindo a url de redirecionamento
            $redirectUrl = $request->query->get('redirectUrl');

            if($redirectUrl != null){
                $redirectUrl = $redirectUrl;
            } else {
                $redirectUrl = $this->generateUrl(
                    'newsgroup_colunas_feed',
                    ['id' => 20, 'friendlyNewsgroup' => 'arimateia-azevedo']
                );
            }

            return $this->render('ClientBundle:Auth:register.checkout.html.twig', [
                'pagseguroSessionId' => '',
                'plan' => $plan,
                'environment' => $this->getParameter('pagseguro_environment'),
                'redirectUrl' => $redirectUrl
            ]);
        }

        return $this->render('ClientBundle:Auth:register.html.twig', array(
            'form' => $form->createView(),
            'pagseguroSessionId' => $sessionCode->getResult(),
            'plans' => $plans,
            'environment' => $this->getParameter('pagseguro_environment')
        ));
    }

    private function checkout(Request $request, $client){
        $pagseguroClient = $this->get('pagseguro_client');
        try {   
            $subReference = $this->get('subscription_helper')->generateReference(10);
            $ip = $request->getClientIp();
            $response = $pagseguroClient->addClientToPlan($client, $request->request->get('pagseguroSHash'),
                $request->request->get('pagseguroCToken'), $request->request->get('plan'), $subReference, $ip);
            $subscription = $this->get('subscription_helper')->createNew($request->request->get('plan'), $response->code, $subReference);
            $client->addSubscription($subscription);
        } catch(\Exception $e) {
            throw $e;
        }

        $em = $this->getDoctrine()->getManager();

        // Salvando o cliente e a assinatura
        $em->persist($client);
        $em->persist($subscription);
        $em->flush();   

        // Enviando um E-mail de confirmação da conta
        try {
            $this->get('sendgrid_helper')->sendNewSubSingle('Nova Assinatura', $client);
        }catch(\Exception $e) {
            return new Response($e->getMessage());
        }  
    }

    /**
     * @Route("/subscribe", name="subscribe_plan")
     */
    public function subscribeToPlanAction(Request $request)
    {
        try {
            // Requisitando o serviço do PagSeguro
            $pagseguroClient = $this->get('pagseguro_client');
            // Gerando uma ID de seção de pagamento
            $sessionCode =  $pagseguroClient->generateSessionId();
        } catch(\Exception $e) {
            dump($e->getMessage());
        }

        // Recuperando os planos cadastrado
        $plans = $this->get('plan_helper')->retrieveAll();

        if($request->isMethod('post')){
            $client = $this->get('security.token_storage')->getToken()->getUser();
            if($client instanceof Client) {
                try {
                    if(!$client->hasFullyActiveSubscription()) {
                        $this->checkout($request, $client);    
                    } else {
                        return $this->render('ClientBundle:Plan:subscribe.html.twig', array(
                            'pagseguroSessionId' => $sessionCode->getResult(),
                            'plans' => $plans,
                            'error' => 'Você já possui uma assinatura ativa',
                            'environment' => $this->getParameter('pagseguro_environment')
                        ));
                    }  
                } catch(\Exception $e) {
                    $error_decode = json_decode($e->getMessage());
                    foreach($error_decode->errors as $key => $value){
                        $this->addFlash("error", $this->get('translator')->trans($key, array(), 'pagseguro'));
                    }
                    return $this->render('ClientBundle:Plan:subscribe.html.twig', array(
                        'pagseguroSessionId' => $sessionCode->getResult(),
                        'plans' => $plans,
                        'environment' => $this->getParameter('pagseguro_environment')
                    ));
                }
            }
            // Buscando o plano correto para exibir no checkout
            $plan = $this->get('plan_helper')->findByCode($request->request->get('plan'));

            // Definindo a url de redirecionamento
            $redirectUrl = $this->generateUrl('client_profile');

            return $this->render('ClientBundle:Auth:register.checkout.html.twig', [
                'pagseguroSessionId' => '',
                'plan' => $plan,
                'environment' => $this->getParameter('pagseguro_environment'),
                'redirectUrl' => $redirectUrl
            ]);
        }

        return $this->render('ClientBundle:Plan:subscribe.html.twig', array(
            'pagseguroSessionId' => $sessionCode->getResult(),
            'plans' => $plans,
            'environment' => $this->getParameter('pagseguro_environment'),
        ));
    }

    public function storingClient(Request $request){

        $em = $this->getDoctrine()->getManager();

        $client = new ClientStore();
        $clientTemp = new Client();

        $mail = $request->request->get('client_email');
        $password = $request->request->get('client_password');

        try{
            //Setando valores no cliente;
            $client->setEmail($mail);
            $clientTemp->setEmail($mail);

            //Criptografando a senha do assinante
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($clientTemp, $request->request->get('client_password'));
            $client->setPassword($password);

            //Salvando o cliente 
             $em->persist($client);
             $em->flush();
            
            return $this->json(
                ['message' => 'Cliente criado com sucesso'],
                $status = 200,
                $header = [],
                $context = []
            );
        }catch(\Exception $e) {
            return $this->json(
                ['message' => $e->getMessage()],
                $status = 404,
                $header = [],
                $context = []
            );
        }
    }

    /**
     * @Route("/verify_user", name="verify_user")
     */
    public function verifyUser(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $mail = $request->request->get('client_email');

        $client = $em->getRepository('ClientBundle:Client')->findOneBy([
            'email' => $mail
        ]);

        if($client == null){
            return $this->storingClient($request);
        }else{
            return $this->json(['resp' => 'Email cadastrado']
            , 500, [], ['groups' => ['']]);
        }
   
    }

    /**
     * @Route("/register_client", name="register_client")
     */
    public function registerClient(Request $request)
    {
        // Instanciando um manager do Doctrine
        $em = $this->getDoctrine()->getManager();

        // Criando novo cliente
        $client = new Client();

        try {
            // Setando valores no cliente;
            $client->setName($request->request->get('client_name'));
            $client->setEmail($request->request->get('client_email'));

            // Criptografando a senha do assinante
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($client, $request->request->get('client_password'));
            $client->setPassword($password);

            // Setando o seu Role
            $client->setRole('ROLE_USER');

            // Gerando referência unica para assinatura
            $subReference = $this->get('subscription_helper')->generateReference(10);

            // Criando a sua assinatura
            $subscription = $this->get('subscription_helper')->createNew(
                $request->request->get('plan'), 
                '', 
                $subReference, 
                Subscription::EDUZZ_TYPE
            );

            // Adicionando a assinatura ao cliente
            $client->addSubscription($subscription);

            // Salvando o cliente e a assinatura
            $em->persist($client);
            $em->persist($subscription);
            $em->flush();
            
            return $this->json(
                ['message' => 'Cliente criado com sucesso'],
                $status = 200,
                $header = [],
                $context = []
            );
        } catch(\Exception $e) {
            return $this->json(
                ['message' => $e->getMessage()],
                $status = 404,
                $header = [],
                $context = []
            );
        }
    }

    /**
     * @Route("/eduzz_checkout", name="eduzz_checkout")
     */
    public function eduzzCheckoutAction(Request $request)
    {
        // Recuperando os parâmetros importantes da requisição
        $nome_comprador = $request->query->get('nome_comprador');

        return $this->render('ClientBundle:Eduzz:registerEduzz.checkout.html.twig', array(
            'buyer' => $nome_comprador
        ));
    }

    /**
     * @Route("/eduzz_checkout_ticket", name="eduzz_checkout_ticket")
     */
    public function eduzzCheckoutTicketAction(Request $request)
    {
        return $this->render('ClientBundle:Eduzz:registerEduzzTicket.checkout.html.twig');
    }
}