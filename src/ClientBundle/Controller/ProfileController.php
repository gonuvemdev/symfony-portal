<?php

namespace ClientBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\Subscription;

class ProfileController extends Controller
{
    /**
     * @Route("/client_profile", name="client_profile")
     * @Security("has_role('ROLE_USER')")
     */
    public function clientProfileAction(Request $request)
    {
        $client = $this->get('security.token_storage')->getToken()->getUser();
        if($client instanceof Client) {
            $subscription = $client->getLastSubscription();
            // Buscando a assinatura do mesmo no PagSeguro para saber se a mesma está desativada remotamente
            try {
                switch ($subscription->getType()) {
                    case Subscription::EDUZZ_TYPE:
                        $data = $this->get('profileHelper')->mountProfileEduzz($subscription);
                        $template = 'ClientBundle:Auth:profile.html.twig';
                        break;
                    case Subscription::PAGSEGURO_TYPE:
                        $data = $this->get('profileHelper')->mountProfilePagSeguro($subscription);
                        $template = 'ClientBundle:Auth:profile.html.twig';
                        break;
                    default:
                        $data = $this->get('profileHelper')->mountProfilePagSeguro($subscription);
                        $template = 'ClientBundle:Auth:profile.html.twig';
                        break;
                }
                
            } catch(\Exception $e) {
                die($e->getMessage());
            }
        }
            
        return $this->render($template, $data);
    }

     /**
     * @Route("/update_profile", name="update_profile")
     */
    public function updateProfile(Request $request)
    {
        // Create a new blank user and process the form    

        if ($request->isMethod('POST')) {
            $client_data = $request->request->get('client');
            
            // Buscando o user atual sem usar o método helper
            $em = $this->getDoctrine()->getManager();
            $client = $em->getRepository(Client::class)
                    ->findOneBy(['email' => $client_data['email']]);
            
            if(strlen($client_data['plainPassword']) > 0){
                // Requerindo o serviço para codificar a senha
                $encoder = $this->get('security.password_encoder');

                // Encodificando a senha
                $client_data['password'] = $encoder->encodePassword($client, $client_data['plainPassword']);
            }

            $client->updateInfo($client_data);

            $em->persist($client);
            $em->flush($client);
        }

        return $this->redirectToRoute('client_profile');        
    }

    /**
     * @Route("/forgot_password", name="forgot_password")
     */
    public function forgotPasswordAction(Request $request)
    {
        // Caso a requisição seja POST, validar o processo de troca de senha
        if($request->isMethod('POST'))
        {
            // Recuperando instância do manager
            $em = $this->getDoctrine()->getManager();

            $client = $em->getRepository(Client::class)->findBy(array('email' => $request->request->get('_email')))[0];
            
            // Conferindo o código enviado com o código no Banco de Dados
            $clientCode = $client->getRecoverPassCode();
            $receivedCode = $request->request->get('_verify_code');

            if(strcmp($clientCode, $receivedCode) == 0)
            {
                // Criptografando a nova senha
                $encoder = $this->get('security.password_encoder');

                // Trocando a senha
                $password = $encoder->encodePassword($client, $request->request->get('_new_password'));
                $client->setPassword($password);

                $em->persist($client);
                $em->flush($client);

                // Após a senha ter sido trocada som sucesso, apagar o código
                $client->setRecoverPassCode('');
                $em->persist($client);
                $em->flush($client);

                return $this->render('ClientBundle:Auth:forgotPasswordSuccess.html.twig');
            }

            // Para caso o código esteja errado
            return $this->render('ClientBundle:Auth:forgotPassword.html.twig', array(
                'codeSended' => true,
                'error' => 'Código incorreto',
                '_client_email' => $request->request->get('_email')
            ));
        }

        return $this->render('ClientBundle:Auth:forgotPassword.html.twig');
    }

    /**
     * @Route("/send_recovery_code", name="send_recovery_code")
     */
    public function sendRecoveryCodeAction(Request $request)
    {
        if($request->isMethod('POST')) {

            // Recuperando a referência do manager
            $em = $this->getDoctrine()->getManager();

            // Recuperando o cliente pelo e-mail
            $client = $em->getRepository(Client::class)->findBy(array('email' => $request->request->get('_email')));

            if(!count($client)) {
                return $this->render('ClientBundle:Auth:forgotPassword.html.twig', array(
                    'error' => 'Nenhum usuário com este e-mail encontrado'
                ));
            }

            // Gerando código para associar ao password
            $recoverCode = $this->get('subscription_helper')->generateReference(5);

            // Setando o código de recuperação
            $client[0]->setRecoverPassCode($recoverCode);

            // Enviando o e-mail com o código
            try {
                $em->persist($client[0]);
                $em->flush($client[0]);
                $this->get('sendgrid_helper')->sendRecoverPass('Código de Recuperação - Portal AZ', $client[0], $recoverCode);
                return $this->render('ClientBundle:Auth:forgotPassword.html.twig', array(
                    'codeSended' => true,
                    '_client_email' => $request->request->get('_email')
                ));
            }catch(\Exception $e) {
                return $this->render('ClientBundle:Auth:forgotPassword.html.twig', array(
                    'error' => 'Ocorreu um erro inexperado na geração do código, por favor tente novamente.'
                ));
            }
        }
    }


    /**
     * @Route("/update_card", name="update_card")
     * @Security("has_role('ROLE_USER')")
     */
    public function updateCardAction(Request $request)
    {
        if($request->isMethod('POST')) {
            // Recuperando o cliente logado
            $client = $this->get('security.token_storage')->getToken()->getUser();
            // Instanciando o serviço do PagSeguro
            $pagSeguroClient = $this->get('pagseguro_client');

            // Montando um array com os dados para o método do PagSeguro
            $cardData = array(
                'cardToken' => $request->request->get('pagseguroCToken'),
                'sendersHash' => $request->request->get('pagseguroSHash'),
            );

            // Setando previamente informações no cliente que serão usadas no método
            $client->setCardHolderName($request->request->get('card_name'));
            $client->setCardHolderDOB($request->request->get('card_holders_dob'));
            $client->setCardHolderCPF($request->request->get('card_holders_cpf'));

            try {
                $pagSeguroClient->changePreApprovalPayment($client, $cardData);
            } catch(\Exception $e) {
                die($e->getMessage());
            }

            // Setando as ultimas informações no cliente
            $client->setLast4($request->request->get('cardLast4'));
            $client->setCardBrand($request->request->get('cardBrand'));

            // Instanciando um Entity Manager
            $em = $this->getDoctrine()->getManager();

            $em->persist($client);
            $em->flush();

            return $this->redirectToRoute('client_profile');
        }
    }

    /**
     * @Route("/cancel_subscription", name="cancel_subscription")
     */
    public function cancelSubscriptionAction(Request $request)
    {
        if($request->isMethod('POST'))
        {
            // Checando se o usuário é do tipo Client e se a assinatura dele está ativa no BD Local
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $client = $this->get('security.token_storage')->getToken()->getUser();
                if($client instanceof Client) {
                    $stillActive = $client->getLastSubscription()->stillActive();
                    // Buscando a assinatura do mesmo no PagSeguro para saber se a mesma está desativada remotamente
                    try {
                        $pagSeguroSubscription = $this->get('pagseguro_client')->searchPreApproval($client->getLastSubscription()->getPagSeguroSubCode());
                    } catch(\Exception $e) {
                        die($e->getMessage());
                    }
                } 
            } else {
                return $this->redirectToRoute('home');
            }

            if($pagSeguroSubscription->getStatus() == 'ACTIVE')
            {
                $this->get('subscription_helper')->cancel($client->getLastSubscription());
            } else {
                die('Assinatura já cancelada');
            }
        }

        return $this->redirectToRoute('client_profile');
    }

}