<?php

namespace ClientBundle\Controller\Eduzz;
use ClientBundle\Entity\Client;
use ClientBundle\Entity\ClientStore;
use ClientBundle\Entity\Address;
use ClientBundle\Entity\Subscription;
use ClientBundle\Form\Eduzz\ClientType;

use ClientBundle\Helper\SubscriptionHelper;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EduzzWebHookController extends Controller
{
    /**
     * @Route("/notifications/eduzz", name="notification_eduzz")
     */
    public function eduzzListenerAction(Request $request)
    {
        switch($request->headers->get('content-type'))
        {
            case 'application/x-www-form-urlencoded':
                $data = $request->request->all();
                break;
            case 'application/json':
                $data = json_decode($request->getContent(), true);
                break;
        }

        if(array_key_exists('edz_cli_apikey', $data)) {
            $this->createHook($data);
            return new Response(
                'Transação escutada',
                Response::HTTP_OK,
                ['content-type' => 'text/html']
            );
        }
            
        if(array_key_exists('api_key', $data)) {
            $this->transactionHook($data);
            return new Response(
                'Transação escutada',
                Response::HTTP_OK,
                ['content-type' => 'text/html']
            );
        }
        

        return new Response(
            'Requisição não veio da Eduzz',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }
    
    private function createHook($data)
    {
        $api_key = $this->getParameter('eduzz_api_key');
        if($api_key == $data['edz_cli_apikey'])
        {
            // Instanciando um manager do Doctrine
            $em = $this->getDoctrine()->getManager();

            // Checando se o usuário já possui conta, se sim, apenas atualiza sua assinatura mais recente
            $client_exist = $this->get('client_helper')->findClient($data['edz_cli_taxnumber'], $data['edz_cli_email']);
            
            if($client_exist)
            {
                $this->get('eduzz_subscription_helper')->updateSubscription($data['edz_cli_cod'], $data['edz_cli_taxnumber']);
                return;
            }

            $password = $this->get('subscription_helper')->generatePassword(5, $data['edz_cli_rsocial']);

            $client = new Client();

            switch ($data['edz_fat_status']) {
                case '3':
                    try {
                        // Setando valores no cliente;
                        $client->setName($data['edz_cli_rsocial']);
                        $client->setEmail($data['edz_cli_email']);
                        $client->setCpf($data['edz_cli_taxnumber']);
                        $client->setPhone($data['edz_cli_cel']);
            
                        // Criptografando a senha do assinante
                        $encoder = $this->get('security.password_encoder');
                        $password_cript = $encoder->encodePassword($client, $password);
                        $client->setPassword($password_cript);
            
                        // Setando o seu Role
                        $client->setRole('ROLE_USER');
            
                        // Gerando referência unica para assinatura
                        $subReference = $this->get('subscription_helper')->generateReference(10);
            
                        // Criando a sua assinatura
                        $subscription = $this->get('subscription_helper')->createNew(
                            $data['edz_cnt_cod'], 
                            '', 
                            $subReference, 
                            Subscription::EDUZZ_TYPE
                        );
            
                        // Adicionando a assinatura ao cliente
                        $client->addSubscription($subscription);
            
                        // Salvando o cliente e a assinatura
                        $em->persist($client);
                        $em->persist($subscription);
                        $em->flush();
                        
                    } catch(\Exception $e) {
                        return $this->json(
                            ['message' => $e->getMessage()],
                            $status = 404,
                            $header = [],
                            $context = []
                        );
                    }
                    $this->get('eduzz_subscription_helper')->updateEduzzParameters(
                        $data['edz_cli_email'],
                        $data['edz_fat_cod'],
                        $data['edz_cli_cod']
                    );

                    $this->get('sendgrid_helper')->sendNewSubSingle('Nova Assinatura', $client, $password);
                    $this->get('eduzz_subscription_helper')->updateSubscription($data['edz_cli_cod'], $data['edz_cli_taxnumber']);
                    break;
                default:
                    break;
            }
        }
    }

    private function transactionHook($data)
    {
        $api_key = $this->getParameter('eduzz_api_key');
        if($api_key == $data['api_key'])
        {
            switch ($data['trans_status']) {
                case '3':
                    $this->get('eduzz_subscription_helper')->updateSubscription($data['cus_cod'], $data['cus_taxnumber']);
                    break;
                default:
                    break;
            }
        }
    }
}