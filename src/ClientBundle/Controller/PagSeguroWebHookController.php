<?php

namespace ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PagSeguroWebHookController extends Controller
{
    /**
     * @Route("/notifications/transaction", name="notification_transaction")
     */
    public function transactionListenerAction(Request $request)
    {
        $pagSeguroClient = $this->get('pagseguro_client');

        try {
            $response = $pagSeguroClient->listenTransaction();
        } catch(\Exception $e) {
            die($e->getMessage());
        }

        switch($response->getStatus()) {
            case 3:
                $this->get('subscription_helper')->activate($response->getReference(), $response->getLastEventDate());
                break;
        }

        return new Response(
            'Transação escutada',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }
}