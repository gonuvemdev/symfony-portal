<?php
namespace ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @Route("/client_login", name="client_login")
     */
    public function clientLoginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        // // Caso o usuário já esteja logado redireciona para o profile
        // if(!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
        //     return $this->redirectToRoute('client_profile');
        // }

        return $this->render(
            'ClientBundle:Auth:login.html.twig',
            array(
                'last_username' => $authenticationUtils->getLastUsername(),
                'error' => $error,
            )
        );
    }

    /**
     * @Route("/client_login_check", name="client_security_login_check")
     */
    public function clientLoginCheckAction()
    {
        
    }

    /**
     * @Route("/client_logout", name="client_logout")
     */
    public function clientLogoutAction()
    {
        
    }
}
