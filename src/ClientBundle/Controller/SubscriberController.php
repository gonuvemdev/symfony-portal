<?php 
namespace ClientBundle\Controller;

use ClientBundle\Entity\Client;
use ClientBundle\Entity\Subscription;
use ClientBundle\Form\SubscriberType;
use ClientBundle\Filter\SubscriberFilterType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Subscribers Controller
 * 
 * @Route("/admin/subscribers")
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 */
class SubscriberController extends Controller
{
    /**
     * @Route("/list", name="list_subscribers")
     */
    public function listSubscribersAction(Request $request)
    {
        //Instanciando o Doctrine para manipular o Banco
        $em = $this->getDoctrine()->getManager();

        $filterForm = $this->createForm(SubscriberFilterType::class, NULL, array(
            'action' => $this->generateUrl('list_subscribers'),
            'method' => 'POST'
        ));

        // Recuperando todos os planos do banco
        $querySubscribers = $em->getRepository('ClientBundle:Client')->createQueryBuilder('a');
       
        $filterForm->handleRequest($request);

        if($filterForm->isSubmitted() || $filterForm->isValid())
        {
            $name = $filterForm['name']->getData();
            if($name != null)
            {
                $querySubscribers->andWhere('a.name LIKE :name');
                $querySubscribers->setParameter(':name', "%".$name."%");
            }

            $email = $filterForm['email']->getData();
            if($email != null)
            {
                $querySubscribers->andWhere('a.email LIKE :email');
                $querySubscribers->setParameter(':email', "%".$email."%");
            }
        }

        $querySubscribers->orderBy('a.id', 'DESC');
        $query = $querySubscribers->getQuery();
        $subscribers = $query->getResult();
        $hits = (count($subscribers) / 25);
        $count = count($subscribers);
        $page = 1;

        // Paginando com o query parameters
        $queryPage = $request->query->get('page');
        if($queryPage !== null) {
            $page = $queryPage;
        }

        // Calculando para puxar apenas a página solicitada
        if ($hits > 1) {
            $querySubscribers->setFirstResult(25 * ($page - 1));
            $querySubscribers->setMaxResults(25);
            $query = $querySubscribers->getQuery();
            $subscribers = $query->getResult();
        }

        return $this->render('BackBundle:Subscriptions:listSubscribers.html.twig', [
            'subscribers' => $subscribers,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterForm->createView()
        ]);
    }

    private function prepareDataPagSeguro(Client $subscriber){
        // Requisitando o serviço do PagSeguro
        try {
            // Requisitando o serviço do PagSeguro
            $pagseguroClient = $this->get('pagseguro_client');
        } catch(\Exception $e) {
            dump($e->getMessage());
        }

        // Pegando o código de referência da assinatura no PagSeguro
        $pagSeguroSubCode = $subscriber->getLastSubscription()->getPagSeguroSubCode();

        // Listando as ordens de pagamento do assinante, de todos os status
        $paymentOrders = array();
        for($i = 1; $i <= 6; $i++)
        {
            $orders = $pagseguroClient->listPaymentOrders($pagSeguroSubCode, $i)->paymentOrders;

            if(count(get_object_vars($orders)))
            {
                foreach($orders as $key => $value) 
                {
                    array_push($paymentOrders, $value);   
                }
            }
        }

        $schedules = array();
        // Montando o array com os schedules
        foreach($paymentOrders as $key => $row)
        {
            $schedules[$key] = $row->schedulingDate;
        }

        array_multisort($schedules, SORT_DESC, $paymentOrders);

        return $paymentOrders;
    }

    /**
     * @Route("/show/{id}", name="show_subscriber")
     */
    public function showSubscriberAction(Client $subscriber)
    {
        // Recuperando a assinatura ativa do assinante
        $subscription = $subscriber->getLastSubscription();

        $paymentOrders = [];
        switch($subscription->getType()) {
            case Subscription::EDUZZ_TYPE:
                break;
            case Subscription::PAGSEGURO_TYPE:
                $paymentOrders = $this->prepareDataPagSeguro($subscriber);
                break;
        }

        return $this->render('BackBundle:Subscriptions:showSubscriber.html.twig', [
            'subscriber' => $subscriber,
            'paymentOrders' => $paymentOrders
        ]);
    }

    /**
     * @Route("/{id}/edit", name="subscriber_edit")
     * @Method({"GET", "POST"})
     */
    public function editSubscriberAction(Request $request, Client $subscriber)
    {
        // Criando o form para edição
        $form = $this->createForm(SubscriberType::class, $subscriber);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            // Setando o next due date
            $subscriptions = $subscriber->getSubscriptions();
            foreach($subscriptions as $subscri) {
                $keyCompose = 'next_due_date' . $subscri->getReference();
                $nextDueDate = $request->request->get($keyCompose);
                if(strlen($nextDueDate) == 0){
                    $subscri->eraseNextDueDate();
                } else {
                    $nextDueDate = date('c', strtotime($nextDueDate));
                    $subscri->setNextDueDate($nextDueDate);
                }
            }
            // Tratando o recoverPassCode
            $subscriber->setRecoverPassCode($request->request->get('subscriber')['recoverPassCode']);

            // Tratando erro da cidade
            if(array_key_exists('city',$request->request->get('subscriber')['address']))
                $subscriber->getAddress()->setCity($request->request->get('subscriber')['address']['city']);
            else
                $subscriber->getAddress()->setCity('Default Value');
            
            // Tratando erro do numéro null
            if($subscriber->getPhone() == null)
                $subscriber->setPhone('99999999');

            // Tratando códigos da eduzz
            $subscription = $subscriber->getLastSubscription();
            if($subscription->getType() == Subscription::EDUZZ_TYPE) {
                $subscription->setEduzzCusCode($request->request->get('eduzzCusCode'));
                $subscription->setEduzzFatCode($request->request->get('eduzzFatCode'));
            }

            // Setando o plano
            $plan = $this->get('plan_helper')->retrieveById($request->request->get('plan'));

            $subscription->setPlan($plan);

            $this->getDoctrine()->getManager()->flush();
        }

        // Recuperando os planos cadastrado
        $plans = $this->get('plan_helper')->retrieveAll();

        return $this->render('BackBundle:Subscriptions:editSubscriber.html.twig', array(
            'edit_form' => $form->createView(),
            'subscriber' => $subscriber,
            'plans' => $plans
        ));
    }
}