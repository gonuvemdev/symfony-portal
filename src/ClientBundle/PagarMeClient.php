<?php

namespace ClientBundle;

use Symfony\Component\DependencyInjection\ContainerInterface;
use ClientBundle\Helper\CurlHelper;

use \Datetime;

class PagarMeClient
{
    public function __construct($container, CurlHelper $curlHelper)
    {
        $this->container = $container;
        $this->curlHelper = $curlHelper;
        try {
            $this->client = new \PagarMe\Client(
                $this->container->getParameter('pagarme_api_key')
            );
        } catch(\Exception $e) {
            throw $e;
        }
    }

    public function createPlan($plan)
    {
        try {
            $response = $this->client->plans()->create([
                'amount' => $plan->getAmmountPerPayment() * 100,
                'days' => $plan->getPeriodInDays(),
                'name' => $plan->getName(),
                'payment_methods' => ['credit_card'],
                'charges' => null
            ]);
            return $response;
        } catch(\Exception $e) {
            throw $e;
        }
    }
}
