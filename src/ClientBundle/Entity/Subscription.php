<?php
namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use ClientBundle\Entity\Client;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="id", message="This id is already at use")
 */
class Subscription
{

    const EDUZZ_TYPE = 'eduzz_type';
    const PAGSEGURO_TYPE = 'pagseguro_type';

    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $pagSeguroSubCode = null;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    protected $eduzzCusCode = null;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
     protected $eduzzFatCode = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $reference;

    /**
     * @ORM\ManyToOne(targetEntity="Plan")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected $plan;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="subscription")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected $client;

    /**
     * @ORM\Column(type="boolean", options={"default" : "0"})
     */
    protected $active = false;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default": "1970-01-01"})
     */
    protected $nextDueDate = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endsAt = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
     private $type = null;

    function setReference($reference)
    {
        $this->reference = $reference;
    }    

    function getReference()
    {
        return $this->reference;
    }

    function getId()
    {
        return $this->id;
    }

    function setActive($boolean)
    {
        $this->active = $boolean;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    function getActive()
    {
        return $this->active;
    }

    function setPagSeguroSubCode($pagSeguroSubCode)
    {
        $this->pagSeguroSubCode = $pagSeguroSubCode;
    } 

    function getPagSeguroSubCode()
    {
        return $this->pagSeguroSubCode;
    }

    function getEduzzCusCode()
    {
        return $this->eduzzCusCode;
    }

    function setEduzzCusCode($eduzzCusCode)
    {
        $this->eduzzCusCode = $eduzzCusCode;
    } 

    function getEduzzFatCode()
    {
        return $this->eduzzFatCode;
    }

    function setEduzzFatCode($eduzzFatCode)
    {
        $this->eduzzFatCode = $eduzzFatCode;
    } 

    function setNextDueDate($nextDueDate)
    {
        $this->nextDueDate = new \DateTime($nextDueDate);
    }

    function getNextDueDate()
    {
        return $this->nextDueDate;
    }

    function eraseNextDueDate()
    {
        $this->nextDueDate = null;
    }

    function setEndsAt($endsAt)
    {
        $this->endsAt = new \DateTime($endsAt);
    }

    function getEndsAt()
    {
        return $this->endsAt;
    }

    function setClient($client)
    {
        $this->client = $client;
    }

    function setPlan($plan)
    {
        $this->plan = $plan;
    }

    function getPlan()
    {
        return $this->plan;
    }

    function setType($type) 
    {
        if(!in_array($type, array(self::EDUZZ_TYPE, self::PAGSEGURO_TYPE))){
            throw new \InvalidArgumentException("Invalid type");
        }
        $this->type = $type;
    }

    function getType()
    {
        return $this->type;
    }

    function stillActive()
    {
        return $this->nextDueDate > new \DateTime();
    }
}