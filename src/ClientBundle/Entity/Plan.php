<?php
namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="id", message="This id is already at use")
 */
class Plan
{
    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100, options={"default": "AUTO"})
     */
    protected $charge = 'AUTO';

    /**
     * @ORM\Column(type="string", length=100, options={"default": "AUTO"})
     */
    protected $period = 'MONTHLY';

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    protected $ammountPerPayment;

    /**
     * @Assert\Range(
     *      min = 1,
     *      max = 1000000,
     *      minMessage = "Você precisa definir pelo menos {{ limit }}unidade para prosseguir",
     *      maxMessage = "Você não pode definir mais que {{ limit }}unidades"
     * )
     */
    protected $expirationValue;

    /**
     * @ORM\Column(type="string", options={"default": "MONTHS"})
     */
    protected $unit = 'MONTHS';

    /**
     * @ORM\Column(type="string")
     */
    protected $details;

    /**
     * @ORM\Column(type="string")
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $pagSeguroCode = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $eduzzCode = null;

     /**
     * @ORM\Column(type="boolean", options={"default" : "0"})
     */
    protected $recommended = false;

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setCharge($charge)
    {
        $this->charge = $charge;
    }

    public function getCharge()
    {
        return $this->charge;
    }

    public function setEduzzCode($eduzzCode)
    {
        $this->eduzzCode = $eduzzCode;
    }

    public function getEduzzCode()
    {
        return $this->eduzzCode;
    }

    public function setPeriod($period)
    {
        $this->period = $period;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function setAmmountPerPayment($ammountPerPayment)
    {
        $this->ammountPerPayment = $ammountPerPayment;
    }

    public function getAmmountPerPayment()
    {
        return $this->ammountPerPayment;
    }

    public function setExpirationValue($expirationValue)
    {
        $this->expirationValue = $expirationValue;
    }

    public function getExpirationValue()
    {
        return $this->expirationValue;
    }

    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function setDetails($details)
    {
        $this->details = $details;
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function setPagSeguroCode($pagSeguroCode)
    {
        $this->pagSeguroCode = $pagSeguroCode;
    }

    public function getPagSeguroCode()
    {
        return $this->pagSeguroCode;
    }

    public function setRecommended($recommended)
    {
        $this->recommended = $recommended;
    }

    public function getRecommended()
    {
        return $this->recommended;
    }

    public function getLabel()
    {
        $label = '';
        switch($this->period) {
            case 'MONTHLY':
                $label = 'MENSAL';
                break;
            case 'YEARLY':
                $label = 'ANUAL';
                break;
            case 'SEMIANNUALLY';
                $label = 'SEMESTRAL';
                break;
            default:
                $label = 'TRIMESTRAL';
        }
        return $label;
    }

    public function getPeriodicity()
    {
        $label = '';
        switch($this->period) {
            case 'MONTHLY':
                $label = 'mês';
                break;
            case 'YEARLY':
                $label = 'ano';
                break;
            case 'SEMIANNUALLY';
                $label = 'semestre';
                break;
            default:
                $label = 'trimestre';
        }
        return $label;
    }

    public function getChargeLabel()
    {
        $label = '';
        switch($this->charge) {
            case  'AUTO':
                $label = 'Automático pelo Pagseguro';
                break;
            case 'EDUZZ':
                $label = 'Automático pelo Eduzz';
                break;
            default:
                $label = 'Cobrança não definida';
                break;
        }
        return $label;
    }

    public function getUnitLabel()
    {
        $label = '';
        switch($this->unit) {
            case 'MONTHS':
                $label = 'meses';
                break;
            default:
                $label = 'unidade não definida';
                break;
        }
        return $label;
    }

    public function getPeriodInDays()
    {
        $days = 30;
        switch($this->period)
        {
            case 'YEARLY':
                $days = 365;
                break;
            case 'SEMIANNUALLY';
                $days = 180;
                break;
            default:
                $days = 90;
        }
        return $days;
    }

}