<?php
namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable */
class Address
{
    /** @ORM\Column(type = "string", options={"default": ""}) */
    private $street = "";

    /** @ORM\Column(type = "string", options={"default": ""}) */
    private $number = "";

    /** @ORM\Column(type = "string", options={"default": ""}) */
    private $state = "";

    /** @ORM\Column(type = "string", options={"default": ""}) */
    private $complement = "";

    /** @ORM\Column(type = "string", options={"default": ""}) */
    private $district = "";

    /** @ORM\Column(type = "string", options={"default": ""}) */
    private $postalCode = "";

    /** @ORM\Column(type = "string", options={"default": ""}) */
    private $city = "";

    /** @ORM\Column(type = "string", options={"default": ""}) */
    private $country = "BRA";

    function setStreet($street)
    {   
        $this->street = $street;
    } 

    function getStreet()
    {
        return $this->street;
    }

    function setNumber($number)
    {
        $this->number = $number;
    }

    function getNumber()
    {
        return $this->number;
    }

    function setState($state)
    {
        $this->state = $state;
    }

    function getState()
    {
        return $this->state;
    }

    function setComplement($complement)
    {
        $this->complement = $complement;
    }

    function getComplement()
    {
        return $this->complement;
    }

    function setDistrict($district)
    {
        $this->district = $district;
    }

    function getDistrict()
    {
        return $this->district;
    }

    function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    function getPostalCode()
    {
        return $this->postalCode;
    }

    function setCity($city)
    {
        $this->city = $city;
    }

    function getCity()
    {
        return $this->city;
    }

    function setCountry($country)
    {
        $this->country = $country;
    }

    function getCountry()
    {
        return $this->country;
    }

    function updateInfo($address = array()){
        foreach($address as $key => $value ){
            $this->$key = $value;
        }
    }
}