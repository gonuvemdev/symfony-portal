<?php
namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use ClientBundle\Entity\Subscription;
use ClientBundle\Entity\Address;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Este e-mail já possui uma conta")
 * @UniqueEntity(fields="cpf", message="Este cpf já possui uma conta")
 */
class Client implements UserInterface
{

    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
        $this->address = new Address();
    }

    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=24, options={"default": ""})
     */
    protected $phoneNumber = '';

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $role;

    /**
     * @Assert\Length(max=4096)
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $gender = 'm';

    /**
     * @var \Date
     * 
     * @ORM\Column(type="date", nullable=true, options={"default": "1970-01-01"})
     */
    protected $dob = null;

    /**
     * @ORM\Column(type="string", options={"default": "00000000000"})
     */
    protected $cpf = '00000000000';

    /**
     * @ORM\Column(type="string", options={"default": "0000"}, length=4)
     */
    protected $cardLast4 = '0000';

    /**
     * @ORM\Column(type="string", options={"default": ""})
     */
    protected $cardBrand = '';

    /**
     * @ORM\Column(type="string", options={"default": ""})
     */
    protected $cardHoldersName = '';

    /**
     * @ORM\Column(type="string", options={"default": ""})
     */
    protected $cardHoldersDob = '';

    /**
     * @ORM\Column(type="string", options={"default": "00000000000"})
     */
    protected $cardHoldersCpf = '00000000000';

    /** @ORM\Embedded(class = "Address", columnPrefix = "adr_") */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="Subscription", mappedBy="client")
     */
    private $subscriptions;

    /**
     * @ORM\Column(type="string", options={"default": ""})
     */
    protected $recoverPassCode = '';

    public function eraseCredentials()
    {
        return null;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role = null)
    {
        $this->role = $role;
    }

    public function getRoles()
    {
        return [$this->getRole()];
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAddress()
    {
        return $this->address;
    }

    function setAddress($address)
    {
        $this->address = $address;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPhone()
    {
        return $this->phoneNumber;
    }

    public function setPhone($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    public function getSalt()
    {
        return null;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getDob()
    {
        return $this->dob;
    }

    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    public function getCpf()
    {
        return $this->cpf;
    }

    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    public function getLast4()
    {
        return $this->cardLast4;
    }

    public function setLast4($last4)
    {
        $this->cardLast4 = $last4;
    }
    
    public function getCardBrand()
    {
        return $this->cardBrand;
    }

    public function setCardBrand($cardBrand)
    {
        $this->cardBrand = $cardBrand;
    }

    public function getCardHolderName()
    {
        return $this->cardHoldersName;
    }

    public function setCardHolderName($cardHolderName)
    {
        $this->cardHoldersName = $cardHolderName;
    }

    public function getCardHolderDOB()
    {
        return $this->cardHoldersDob;
    }

    public function setCardHolderDOB($cardHoldersDob)
    {
        $this->cardHoldersDob = $cardHoldersDob;
    }

    public function getCardHolderCPF()
    {
        return $this->cardHoldersCpf;
    }

    public function setCardHolderCPF($cardHoldersCPF)
    {
        $this->cardHoldersCpf = $cardHoldersCPF;
    }

    public function getRecoverPassCode()
    {
        return $this->recoverPassCode;
    }

    public function setRecoverPassCode($recoverPassCode)
    {
        $this->recoverPassCode = $recoverPassCode;
    }

    /**
     * @return Collection|Subscription[]
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @return Subscription
     */
    public function getLastSubscription()
    {
        return $this->subscriptions->last();
    }

    public function addSubscription($subscription)
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions[] = $subscription;
            $subscription->setClient($this);
        }
    }

    public function removeSubscription($subscription){

        if($this->subscriptions->contains($subscription)) {
            $this->subscriptions->removeElement($subscription);
            if($subscription->getClient() === $this) {
                $subscription->setClient(null);
            }
        }
    }

    public function updateInfo($new_data)
    {
        $this->email = $new_data['email'];
        $this->name = $new_data['name'];
        if(array_key_exists('password', $new_data)){
            $this->password = $new_data['password'];
        }
        if(array_key_exists('cpf', $new_data)){
            $this->cpf = $new_data['cpf'];
        }
    }

    public function hasActiveSubscription()
    {
        return $this->getLastSubscription()->getActive() || $this->getLastSubscription()->stillActive();
    }

    public function hasFullyActiveSubscription()
    {
        return $this->getLastSubscription()->getActive() && $this->getLastSubscription()->stillActive();
    }
}