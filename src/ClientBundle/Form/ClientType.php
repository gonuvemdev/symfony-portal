<?php
namespace ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use ClientBundle\Form\AddressType;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Senha'],
                'second_options' => ['label' => 'Confirme sua senha'],
                'invalid_message' => 'A senha difere da confirmação'
            ])
            ->add('phone', TextType::class,[
                'attr' => ['type' => 'tel']
            ])
            ->add('cpf', TextType::class)
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Masculino' => 'm',
                    'Feminino' => 'f',
                    'Outro' => 'o'
                ]
            ])
            ->add('address', AddressType::Class)
            ->add('dob', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'placeholder' => '_ _ / _ _ / _ _ _ _'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'ClientBundle\Entity\Client'
        ]);
    }
}