<?php
namespace ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('postalCode', TextType::class, array(
                'empty_data' => 'Default value'
            ))
            ->add('street', TextType::class, array(
                'empty_data' => 'Default value'
            ))
            ->add('number', TextType::class, array(
                'empty_data' => 'Default value'
            ))
            ->add('complement', TextType::class, array(
                'empty_data' => 'Default value'
            ))
            ->add('district', TextType::class, array(
                'empty_data' => 'Default value'
            ))
            ->add('state', ChoiceType::class, [
                'choices' => [
                    'Acre'=>'AC',
                    'Alagoas'=>'AL',
                    'Amapá'=>'AP',
                    'Amazonas'=>'AM',
                    'Bahia'=>'BA',
                    'Ceará'=>'CE',
                    'Distrito Federal'=>'DF',
                    'Espírito Santo'=>'ES',
                    'Goiás'=>'GO',
                    'Maranhão'=>'MA',
                    'Mato Grosso'=>'MT',
                    'Mato Grosso do Sul'=>'MS',
                    'Minas Gerais'=>'MG',
                    'Pará'=>'PA',
                    'Paraíba'=>'PB',
                    'Paraná'=>'PR',
                    'Pernambuco'=>'PE',
                    'Piauí'=>'PI',
                    'Rio de Janeiro'=>'RJ',
                    'Rio Grande do Norte'=>'RN',
                    'Rio Grande do Sul'=>'RS',
                    'Rondônia'=>'RO',
                    'Roraima'=>'RR',
                    'Santa Catarina'=>'SC',
                    'São Paulo'=>'SP',
                    'Sergipe'=>'SE',
                    'Tocantins'=>'TO'
                ]
            ])
            ->add('city', ChoiceType::class, array(
                'empty_data' => 'Default value'
            ));

        $builder->get('city')->addEventListener(
            FormEvents::POST_SUBMIT,
            function(FormEvent $event) {
                $form = $event->getForm();
                $this->setCityOptions(
                    $form->getParent(),
                    $form->getViewData()
                );  
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'ClientBundle\Entity\Address'
        ]);
    }

    private function setCityOptions(FormInterface $form, $location) {
        $form->remove('city');
        $form->add('city', ChoiceType::class, [
            'choices' => [$location],
            'data' => $location
        ]);
    }
}