<?php
namespace ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use ClientBundle\Form\AddressType;

class SubscriberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('phone', TextType::class, [
                'attr' => ['type' => 'tel'],
                'required' => false
            ])
            ->add('cpf', TextType::class)
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Masculino' => 'm',
                    'Feminino' => 'f',
                    'Outro' => 'o'
                ]
            ])
            ->add('address', AddressType::Class, array(
                'required' => false
            ))
            ->add('dob', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['data-mask' => '00/00/0000'],
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'placeholder' => '_ _ / _ _ / _ _ _ _'
                ],
                'required' => false
            ])
            ->add('recoverPassCode', TextType::class, [
                'required' => false,
                'data' => ''
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'ClientBundle\Entity\Client',
            'validation_groups' => false,
        ]);
    }
}