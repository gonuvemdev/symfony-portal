<?php
namespace ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PlanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('charge', ChoiceType::class, [
                'choices' => [
                    'Automático pelo Pagseguro' => 'AUTO',
                    'Automático pelo Eduzz' => 'EDUZZ' 
                ]
            ])
            ->add('eduzzCode', TextType::class, array(
                'required' => false
            ))
            ->add('period', ChoiceType::class, [
                'choices' => [
                    'Mensal' => 'MONTHLY',
                    'Trimestral' => 'TRIMONTHLY',
                    'Anual' => 'YEARLY',
                    'Semestral' => 'SEMIANNUALLY'
                ]
            ])
            ->add('ammountPerPayment', MoneyType::class, [
                'currency' => 'BRL'
            ])
            ->add('expirationValue', NumberType::class)
            ->add('unit', ChoiceType::class, [
                'choices' => [
                    'Anos' => 'YEARS',
                    'Meses' => 'MONTHS',
                    'Dias' => 'DAYS'
                ]
            ])
            ->add('recommended', ChoiceType::class, [
                'choices' => [
                    'Sim' => true,
                    'Não' => false,
                ]
            ])
            ->add('details', TextareaType::class);

        // $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
        //     $plan = $event->getData();
        //     $form = $event->getForm();

        //     // Checando se o campo charge está setado para Eduzz
        //     if($plan->getCharge() === 'EDUZZ') {
        //         $form->add('eduzzCode', TextType::class);
        //     }
        // });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'ClientBundle\Entity\Plan'
        ]);
    }
}