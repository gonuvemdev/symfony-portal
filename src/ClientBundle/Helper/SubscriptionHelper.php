<?php

namespace ClientBundle\Helper;

use Symfony\Component\HttpFoundation\Response;

use ClientBundle\Entity\Subscription;
use ClientBundle\Entity\Client;

class SubscriptionHelper
{
    /**
    * Entity Manager
    *
    * @var Doctrine\ORM\EntityManager
    */
    protected $em;

    protected $planHelper;

    protected $pagSeguroClient;

    protected $sendgridHelper;

    public function __construct(\Doctrine\ORM\EntityManager $em, PlanHelper $planHelper, \ClientBundle\PagSeguroClient $pagSeguroClient, SendgridHelper $sendgridHelper)
    {
        $this->em = $em;
        $this->planHelper = $planHelper;
        $this->pagSeguroClient = $pagSeguroClient;
        $this->sendgridHelper = $sendgridHelper;
    }

    public function createNew($planCode, $subscriptionCode, $subReference, $type)
    {
        // Montado uma nova assinatura
        $newSubscription = new Subscription();

        // Colocando o código dependendo se é PagSeguro ou Eduzz
        switch($type)
        {
            case Subscription::EDUZZ_TYPE:
                $newSubscription->setType($type);
                $newSubscription->setEduzzCusCode($subscriptionCode);
                break;
            case Subscription::PAGSEGURO_TYPE:
                $newSubscription->setType($type);
                $newSubscription->setPagSeguroSubCode($subscriptionCode);
                break;
        }
        
        // Setando token unico de referência
        $newSubscription->setReference($subReference);

        // Recuperando o plano correto
        $plan = $this->planHelper->findByCode($type, $planCode);
        $newSubscription->setPlan($plan);

        // Setando a data de termino da associação
        // Calculando 1 ano a partir de agora
        $futureDate = date('Y-m-d', strtotime('+1 year'));
        $newSubscription->setEndsAt($futureDate);

        // Setando uma data temporária para a próxima cobrança
        // $nextDueDate = date('Y-m-d', strtotime('+1 month'));
        // switch($plan->getPeriod()) {
        //     case 'MONTHLY':
        //         $nextDueDate = date('Y-m-d', strtotime('+1 month'));
        //         break;
        //     case 'YEARLY':
        //         $nextDueDate = date('Y-m-d', strtotime('+1 year'));
        //         break;
        //     case 'SEMIANNUALLY':
        //         $nextDueDate = date('Y-m-d', strtotime('+6 month'));
        //         break;
        //     default:
        //         $nextDueDate = date('Y-m-d', strtotime('+3 month'));
        //         break;
        // }
        // $newSubscription->setNextDueDate($nextDueDate);

        if($newSubscription){
            return $newSubscription;
        }

        return false;
    }

    function activate($reference, $lastEventDate)
    {
        // achando uma assinatura pela referência
        $subscription = $this->findByReference($reference);

        // Tornando ela ativa
        $subscription->setActive(true);

        // Salvando a alteração no banco
        $this->em->persist($subscription);
        $this->em->flush($subscription);

        // Atualizando a assinatura no banco local
        $this->updateSubscription($reference);

        // Enviando e-mail para o cliente
        try {
            $this->sendgridHelper->sendApprovedTransSingle(null, $subscription->getClient(), $lastEventDate);
        }catch(\Exception $e) {
            die($e->getMessage());
        }  
    }

    function cancel($subscription)
    {
        try {
            $pagSeguroSubCode = $subscription->getPagSeguroSubCode();
            // Chamando a função do PagSeguro para cancelar o PreApproval
            $response = $this->pagSeguroClient->cancelPreApproval($pagSeguroSubCode);

            // Setando o estado da assinatura para desativada
            $subscription->setActive(false);

            // Setando o endsAt com o valor do nextDueDate
            $subscription->setEndsAt($subscription->getNextDueDate()->format('Y-m-d H:i:s'));

            // Salvando as modificações no banco
            $this->em->persist($subscription);
            $this->em->flush($subscription);

            return true;
        } catch(\Exception $e) {
            die($e->getMessage());
        }
    }

    function updateSubscription($reference)
    {
        // Achando uma assinatura pela referência   
        $subscription = $this->findByReference($reference);

        // Buscando as ordens de pagamento agendadas desta assinatura
        $paymentOrders = $this->pagSeguroClient->listPaymentOrders($subscription->getPagSeguroSubCode(), 1);

        // Procurando a próxima data de vencimento da próxima parcela
        $nextPaymentOrder = end($paymentOrders->paymentOrders);
        
        if($nextPaymentOrder){
            $subscription->setNextDueDate($nextPaymentOrder->schedulingDate);
        } else {
            dump('Não foi possivel achar a próxima cobrança, ou é a ultima parcela do plano');
        }

        // Salvando a alteração no banco
        $this->em->persist($subscription);
        $this->em->flush($subscription);
    }

    function findByReference($reference)
    {
        $subscription = $this->em->getRepository('ClientBundle:Subscription')->findOneBy([
            'reference' => $reference
        ]);

        return $subscription;
    }

    function generateReference($length){
        $reference = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited
   
       for ($i=0; $i < $length; $i++) {
           $reference .= $codeAlphabet[random_int(0, $max-1)];
       }
   
       return $reference;
   }

   function generatePassword($length, $client_name){
        $password = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $password .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $password;
    }
}