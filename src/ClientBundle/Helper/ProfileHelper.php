<?php

namespace ClientBundle\Helper;

use Symfony\Component\HttpFoundation\Response;

use ClientBundle\Entity\Subscription;
use ClientBundle\Entity\Client;

class ProfileHelper
{
    /**
    * Entity Manager
    *
    * @var Doctrine\ORM\EntityManager
    */
    protected $em;

    protected $planHelper;

    protected $pagSeguroClient;

    protected $container;

    public function __construct(\Doctrine\ORM\EntityManager $em, PlanHelper $planHelper, \ClientBundle\PagSeguroClient $pagSeguroClient, $container)
    {
        $this->em = $em;
        $this->planHelper = $planHelper;
        $this->pagSeguroClient = $pagSeguroClient;
        $this->container = $container;
    }

    public function mountProfilePagSeguro(Subscription $subscription)
    {
        try {
            // Resgatando as ordens que já foram pagas
            $paid_orders = $this->pagSeguroClient->listPaymentOrders($subscription->getPagSeguroSubCode(), 5);
            // Regatando as ordens que foram negadas pelo PagSeguro
            $non_paid_orders = $this->pagSeguroClient->listPaymentOrders($subscription->getPagSeguroSubCode(), 6);
            // Inicializando array com as orders
            $orders = [];
            foreach($paid_orders->paymentOrders as $order) {
                array_push($orders, $order);
            }
            foreach($non_paid_orders->paymentOrders as $order) {
                array_push($orders, $order);
            }       
            // Ordenando eles por data
            usort($orders, function($a1, $a2) {
                $v1 = strtotime($a1->lastEventDate);
                $v2 = strtotime($a2->lastEventDate);
                return $v2 - $v1; // $v2 - $v1 to reverse direction
             });

             // Gerando a id de seção para poder trocar o pagamento
             $session_code = $this->pagSeguroClient->generateSessionId();

             // Gerando o array associativo
             $data = array(
                'orders' => $orders,
                'pagseguroSessionId' => $session_code->getResult(),
                'environment' => $this->container->getParameter('pagseguro_environment')
             );
        }catch(\Exception $e) {
            die($e->getMessage());
        }

        return $data;
    }

    public function mountProfileEduzz()
    {
        try {
            $data = array(
                'orders' => [],
            );
        }catch(\Exception $e) {
            die($e->getMessage());
        }

        return $data;
    }
}