<?php

namespace ClientBundle\Helper\Eduzz;

use Symfony\Component\HttpFoundation\Response;

use ClientBundle\Helper\PlanHelper;
use ClientBundle\Helper\SendgridHelper;

use ClientBundle\Entity\Subscription;
use ClientBundle\Entity\Client;

class SubscriptionHelper
{
     /**
    * Entity Manager
    *
    * @var Doctrine\ORM\EntityManager
    */
    protected $em;

    protected $planHelper;

    protected $sendgridHelper;

    public function __construct(\Doctrine\ORM\EntityManager $em, PlanHelper $planHelper,  SendgridHelper $sendgridHelper)
    {
        $this->em = $em;
        $this->planHelper = $planHelper;
        $this->sendgridHelper = $sendgridHelper;
    }

    public function updateEduzzParameters($clientEmail, $eduzzFatCode, $eduzzCusCode)
    {
        // Recuperando a inscrição pelo e-mail do cliente recebido
        $subscription = $this->findByEmail($clientEmail);

        // Setando os valores que estavam faltando
        $subscription->setEduzzCusCode($eduzzCusCode);
        $subscription->setEduzzFatCode($eduzzFatCode);

        // Salvando a alteração no banco
        $this->em->persist($subscription);
        $this->em->flush($subscription);
    }

    public function updateSubscription($eduzzCusCode, $clientCpf)
    {
        // Achando assinatura pelo código de consumidor do eduzz
        $subscription = $this->findByCpf($clientCpf);

        // Pegando o plano referente a essa assinatura
        try {
            if($subscription == null)
                return;
            $plan = $subscription->getPlan();

            // Setando uma data temporária para a próxima cobrança
            $nextDueDate = date('Y-m-d', strtotime('+1 month'));
            switch($plan->getPeriod()) {
                case 'MONTHLY':
                    $nextDueDate = date('Y-m-d', strtotime('+1 month'));
                    break;
                case 'YEARLY':
                    $nextDueDate = date('Y-m-d', strtotime('+1 year'));
                    break;
                case 'SEMIANNUALLY':
                    $nextDueDate = date('Y-m-d', strtotime('+6 month'));
                    break;
                default:
                    $nextDueDate = date('Y-m-d', strtotime('+3 month'));
                    break;
            }
            $subscription->setNextDueDate($nextDueDate);

            // Salvando a alteração no banco
            $this->em->persist($subscription);
            $this->em->flush($subscription);
        }catch(\Exception $e) {
            throw $e;
        }
        
    }

    private function findByEduzzCode($eduzzCusCode)
    {
        $subscription = $this->em->getRepository('ClientBundle:Subscription')->findOneBy([
            'eduzzCusCode' => $eduzzCusCode
        ]);

        return $subscription;
    }

    private function findByCpf($cpf)
    {
        $client = $this->em->getRepository('ClientBundle:Client')->findOneBy([
            'cpf' => $cpf
        ]);

        return $client->getLastSubscription();
    }

    private function findByEmail($email)
    {
        $client = $this->em->getRepository('ClientBundle:Client')->findOneBy([
            'email' => $email
        ]);

        try {
            return $client->getLastSubscription();
        } catch(\Exception $e) {
            throw $e;
        }
    }
}