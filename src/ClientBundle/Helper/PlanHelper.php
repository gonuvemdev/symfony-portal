<?php

namespace ClientBundle\Helper;

use ClientBundle\Entity\Subscription;
use ClientBundle\Entity\Client;
use ClientBundle\Entity\Plan;


class PlanHelper
{
    /**
    * Entity Manager
    *
    * @var Doctrine\ORM\EntityManager
    */
    protected $em;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    public function findByCode($type, $code)
    {
        switch($type)
        {
            case Subscription::EDUZZ_TYPE:
                $plan = $this->em->getRepository('ClientBundle:Plan')->findOneBy([
                        'eduzzCode' => $code
                    ]
                );
                break;
            case Subscription::PAGSEGURO_TYPE:
                $plan = $this->em->getRepository('ClientBundle:Plan')->findOneBy([
                        'pagSeguroCode' => $code
                    ]
                );
                break;
        }
        
        return $plan;
    }

    public function retrieveById($id)
    {
        $plan = $this->em->getRepository('ClientBundle:Plan')->findOneBy([
            'id' => $id
        ]);
        
        return $plan;
    }

    public function retrieveAll()
    {
        $plans = $this->em->getRepository('ClientBundle:Plan')->findBy(array());
        return $plans;
    }
}