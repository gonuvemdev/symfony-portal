<?php 

namespace ClientBundle\Helper;

use Symfony\Component\HttpFoundation\Response;

use ClientBundle\Entity\Client;

class ClientHelper
{
    /**
     * Entity Manager
     * 
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    public function findClient($client_cpf, $client_email)
    {
       $query = $this->em->getRepository('ClientBundle:Client')
        ->createQueryBuilder('u')
        ->where('u.cpf = :cpf or u.email = :email')
        ->setParameter('cpf', $client_cpf)
        ->setParameter('email', $client_email);

        $client = $query->getQuery()->getOneOrNullResult();

        return $client;
    }
}