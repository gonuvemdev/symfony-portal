<?php 

use \SendGrid\Mail\From as From;
use \SendGrid\Mail\To as To;
use \SendGrid\Mail\Subject as Subject;
use \SendGrid\Mail\PlainTextContent as PlainTextContent;
use \SendGrid\Mail\HtmlContent as HtmlContent;
use \SendGrid\Mail\Mail as Mail;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\TranslatorInterface;

namespace ClientBundle\Helper;

class SendgridHelper
{

    private $container;

    private $translator;

    public function __construct($container, $translator) 
    {
        $this->container = $container;
        $this->translator = $translator;
    }

    public function sendNewSubSingle($subject = null, $client, $password)
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($this->container->getParameter('mailer_contact_from'), $this->container->getParameter('mailer_contact_user'));
        if($subject !== null) {
            $email->setSubject($subject);
        }
        $email->addTo(
            $client->getEmail(),
            $client->getName(),
            [
                "client_name" => $client->getName(),
                "client_email" => $client->getEmail(),
                "client_password" => $password
            ]
        );
        $email->setTemplateId($this->container->getParameter('mailer_new_sub_template'));
        $sendgrid = new \SendGrid($this->container->getParameter('sendgrid_api_key'));
        try {
            $response = $sendgrid->send($email);
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function sendRecoverPass($subject = null, $client, $recoverPassCode)
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($this->container->getParameter('mailer_contact_from'), $this->container->getParameter('mailer_contact_user'));
        if($subject !== null) {
            $email->setSubject($subject);
        }
        $email->addTo(
            $client->getEmail(),
            $client->getName(),
            [
                "recup_code" => $recoverPassCode
            ]
        );
        $email->setTemplateId($this->container->getParameter('mailer_recover_password_template'));
        $sendgrid = new \SendGrid($this->container->getParameter('sendgrid_api_key'));
        try {
            $response = $sendgrid->send($email);
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function sendApprovedTransSingle($subject = null, $client, $lastEvent)
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($this->container->getParameter('mailer_contact_from'), $this->container->getParameter('mailer_contact_user'));
        $date_from_string = strtotime($lastEvent);
        $month = date('F', $date_from_string);
        $month_translate = $this->translator->trans($month, array(), 'mailer');
        if($subject !== null) {
            $email->setSubject($subject);
        }
        $email->addTo(
            $client->getEmail(),
            $client->getName(),
            [
                "client_name" => $client->getName(),
                "order_month" => $month_translate
            ]
        );
        $email->setTemplateId($this->container->getParameter('mailer_approved_trans_template'));
        $sendgrid = new \SendGrid($this->container->getParameter('sendgrid_api_key'));
        try {
            $response = $sendgrid->send($email);
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}