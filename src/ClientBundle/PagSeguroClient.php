<?php

namespace ClientBundle;

use Symfony\Component\DependencyInjection\ContainerInterface;

use \Datetime;

class PagSeguroClient
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
        try {
            \PagSeguro\Library::initialize();
            \PagSeguro\Configuration\Configure::setEnvironment($this->container->getParameter('pagseguro_environment'));
            \PagSeguro\Configuration\Configure::setAccountCredentials(
                $this->container->getParameter('pagseguro_api_email'),
                $this->container->getParameter('pagseguro_api_key')
            );
            \PagSeguro\Configuration\Configure::setCharset('UTF-8');
            /** TODO: ATUALIZAR ESSE DADOS */
            \PagSeguro\Configuration\Configure::setLog(false, '');
        } catch(\Exception $e) {
            die($e->getMessage());
        }
    }

    public function generateSessionId()
    {
        try {
            $sessionCode = \PagSeguro\Services\Session::create(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            return $sessionCode;
        } catch(\Exception $e) {
            die($e->getMessage());
        }
    }

    public function chargeCustomerCreditCard($creditCardToken, $sendersHash, $client)
    {
        // Instanciando um novo pagamento usando cartão de crédito
        $creditCard = new \PagSeguro\Domains\Requests\DirectPayment\CreditCard();

        // Setando código de referência para futuras notificações
        $creditCard->setReference("LIBPHP000001");

        // Setando a moeda utilizada
        $creditCard->setCurrency("BRL");

        // Adicionando um item a esse pagamento
        $creditCard->addItems()->withParameters(
            '0001',
            'Notebook prata',
            2,
            10.00
        );

        /** INFORMAÇÕES SOBRE O CLIENTE */

        // Setando informações sobre o cliente
        $creditCard->setSender()->setName('João Comprador');
        $creditCard->setSender()->setEmail('email@sandbox.pagseguro.com.br');

        // Setando o CPF do cliente
        $creditCard->setSender()->setDocument()->withParameters(
            'CPF',
            '05841919369'
        );

        // Setando a hash do cliente obtida via o javascript
        $creditCard->setSender()->setHash($sendersHash);

        // Setando o numéro de telefone do cliente
        $creditCard->setSender()->setPhone()->withParameters(
            11,
            56273440
        );

         // Setando o endereço de entrega
         $creditCard->setShipping()->setAddress()->withParameters(
            'Av. Brig. Faria Lima',
            '1384',
            'Jardim Paulistano',
            '01452002',
            'São Paulo',
            'SP',
            'BRA',
            'apto. 114'
        );

        // Setando as informações de cobrança para o cartão de crédito
        $creditCard->setBilling()->setAddress()->withParameters(
            'Av. Brig. Faria Lima',
            '1384',
            'Jardim Paulistano',
            '01452002',
            'São Paulo',
            'SP',
            'BRA',
            'apto. 114'
        );

        // Setando o token do cartão de crédito
        $creditCard->setToken($creditCardToken);

        // Setando o numero de parcelas e o valor de cada uma
        $creditCard->setInstallment()->withParameters(1, '20.00');

        /** INFORMAÇÕES SOBRE O DONO DO CARTÃO */

        // Setando as informações do dono do cartão de crédito
        $creditCard->setHolder()->setBirthdate('01/10/1979');
        $creditCard->setHolder()->setName('João Comprador'); // Equals in Credit Card

        // Setando o CPF do dono do cartão de crédito
        $creditCard->setHolder()->setDocument()->withParameters(
            'CPF',
            '05841919369'
        );

        // Setando o numéro do dono do cartão
        $creditCard->setHolder()->setPhone()->withParameters(
            11,
            56273440
        );

        // Setando o modo de pagamento para essa transação
        $creditCard->setMode('DEFAULT');

        try {
            //Setando 
            $result = $creditCard->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            return $result;
        } catch (Exception $e) {
            echo "</br> <strong>";
            die($e->getMessage());
        }
    }

    public function createPlan($plan)
    {
        // Criando o novo plano
        $PagSeguroPlan = new \PagSeguro\Domains\Requests\DirectPreApproval\Plan();

        // Setando as informações
        $PagSeguroPlan->setReference('#CODIGOUNICOAQUI');
        $PagSeguroPlan->setPreApproval()->setName($plan->getName());
        $PagSeguroPlan->setPreApproval()->setCharge($plan->getCharge());
        $PagSeguroPlan->setPreApproval()->setPeriod($plan->getPeriod());
        $PagSeguroPlan->setPreApproval()->setAmountPerPayment($plan->getAmmountPerPayment());
        $PagSeguroPlan->setPreApproval()->setDetails($plan->getDetails());

        try {
            $response = $PagSeguroPlan->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            return $response;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function addClientToPlan($client, $senderHash, $cardToken, $plan, $reference, $senderIp)
    {
        $preApproval = new \PagSeguro\Domains\Requests\DirectPreApproval\Accession();
        $preApproval->setPlan($plan);
        $preApproval->setReference($reference);

        $preApproval->setSender()->setName($client->getName());//assinante
        $preApproval->setSender()->setEmail($client->getEmail());//assinante
        $preApproval->setSender()->setHash($senderHash);
        $preApproval->setSender()->setIp($senderIp);
        $preApproval->setSender()->setAddress()->withParameters(
            $client->getAddress()->getStreet(), 
            $client->getAddress()->getNumber(),
            $client->getAddress()->getDistrict(), 
            $client->getAddress()->getPostalCode(),
            $client->getAddress()->getCity(),
            $client->getAddress()->getState(),
            $client->getAddress()->getCountry()
        );//assinante

        $document = new \PagSeguro\Domains\DirectPreApproval\Document();
        $document->withParameters('CPF', $client->getCPF()); //assinante
        $preApproval->setSender()->setDocuments($document);

        $preApproval->setSender()->setPhone()->withParameters(substr($client->getPhone(),0,2), substr($client->getPhone(),2)); //assinante
        $preApproval->setPaymentMethod()->setCreditCard()->setToken($cardToken); //token do cartão de crédito gerado via javascript

        /** INFORMAÇÕES DO HOLDER DO CARTÃO */
        $document = new \PagSeguro\Domains\DirectPreApproval\Document();
        $document->withParameters('CPF', $client->getCardHolderCPF()); //dono do cartão
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setDocuments($document);
        // Precisa também de um telefone só para o dono do cartão
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setPhone()->withParameters(substr($client->getPhone(),0,2), substr($client->getPhone(),2));
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setName($client->getCardHolderName()); //nome do titular do cartão de crédito
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setBirthDate($client->getCardHolderDOB()); //data de nascimento do titular do cartão de crédito
        // Setando o endereço de cobrança, mas vamos utilizar o mesmo do cliente
        $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setBillingAddress()->withParameters(
            $client->getAddress()->getStreet(),
            $client->getAddress()->getNumber(),
            $client->getAddress()->getDistrict(), 
            $client->getAddress()->getPostalCode(),
            $client->getAddress()->getCity(),
            $client->getAddress()->getState(),
            $client->getAddress()->getCountry()
        );
        
        try {
            $response = $preApproval->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials() // credencias do vendedor no pagseguro
            );
            return $response;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function listenTransaction()
    {
        try {
            if (\PagSeguro\Helpers\Xhr::hasPost()) {
                $response = \PagSeguro\Services\Transactions\Notification::check(
                    \PagSeguro\Configuration\Configure::getAccountCredentials()
                );
            } else {
                throw new \InvalidArgumentException($_POST);
            }
            return $response;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function searchPreApproval($pagSeguroSubCode)
    {
        try {
            $response = \PagSeguro\Services\PreApproval\Search\Code::search(
                \PagSeguro\Configuration\Configure::getAccountCredentials(),
                $pagSeguroSubCode
            );
            return $response;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function listPaymentOrders($pagSeguroSubCode, $status)
    {
        $queryPreApproval = new \PagSeguro\Domains\Requests\DirectPreApproval\QueryPaymentOrder($pagSeguroSubCode, $status);
        try {
            $response = $queryPreApproval->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            return $response;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function cancelPreApproval($pagSeguroSubCode)
    {
        try {
            $response = \PagSeguro\Services\PreApproval\Cancel::create(
                \PagSeguro\Configuration\Configure::getAccountCredentials(),
                $pagSeguroSubCode
            );
            return $response;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function changePreApprovalPayment($client, $newCard){
        $changePayment = new \PagSeguro\Domains\Requests\DirectPreApproval\ChangePayment();
        $changePayment->setPreApprovalCode($client->getSubscription()->getPagSeguroSubCode());
        $changePayment->setSender()->setHash($newCard['sendersHash']);
        $changePayment->setCreditCard()->setToken($newCard['cardToken']); //token do cartão de crédito gerado via javascript
        $changePayment->setCreditCard()->setHolder()->setName($client->getCardHolderName()); //nome do titular do cartão de crédito
        $changePayment->setCreditCard()->setHolder()->setBirthDate($client->getCardHolderDOB()); //data de nascimento do titular do cartão de crédito

        $document = new \PagSeguro\Domains\DirectPreApproval\Document();
        $document->withParameters('CPF', $client->getCardHolderCPF());  //cpf do titular do cartão de crédito
        $changePayment->setCreditCard()->setHolder()->setDocuments($document);

        $changePayment->setCreditCard()->setHolder()->setPhone()->withParameters(substr($client->getPhone(),0,2), substr($client->getPhone(),2)); //telefone do titular do cartão de crédito
        $changePayment->setCreditCard()->setHolder()->setBillingAddress()->withParameters(
            $client->getAddress()->getComplement(), 
            $client->getAddress()->getNumber(),
            $client->getAddress()->getDistrict(), 
            $client->getAddress()->getPostalCode(),
            $client->getAddress()->getCity(),
            $client->getAddress()->getState(),
            $client->getAddress()->getCountry()
        ); //endereço do titular do cartão de crédito

        try {
            $response = $changePayment->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            return $response;
        } catch (\Exception $e) {
            throw $e;
        }
    }

}