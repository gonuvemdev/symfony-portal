<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 26/09/2018
 * Time: 17:04
 */

namespace AppBundle\Controller;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


/**
 * Destaque controller.
 *
 * @Route("/api/newsgroup")
 */
class NewsGroupController extends Controller
{

    /**
     * @Route("/fetchblog", name="api_newsgroup_blog_fetch")
     */
    public function fetchBlogAction(Request $request)
    {
        return '';
    }

    /**
     * @Route("/fetchcoluna", name="api_newsgroup_coluna_fetch")
     */
    public function fetchColunaAction(Request $request)
    {
        return '';
    }

    /**
     * @Route("/mostreads", name="api_mostreads_mostreads")
     */
    public function mostReadsAction(Request $request)
    {
        return '';
    }

    /**
     * @Route("/feed", name="api_mostreads_feed")
     */
    public function feedAction(Request $request)
    {
        return '';
    }



}