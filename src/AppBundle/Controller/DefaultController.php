<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackBundle\Entity\Installation;

/**
 * Defaulit controller.
 *
 * @Route("/api")
 */
class DefaultController extends Controller
{

    /**
     * Creates a new Installation entity.
     *
     * @Route("/installation/new", name="api_installation_new")
     * @Method({"GET", "POST"})
     */
    public function newInstallationAction(Request $request)
    {
        return '';
    }


    /**
     * Creates a new Installation entity.
     *
     * @Route("/installation/update/{id}", name="api_installation_update")
     * @Method({"GET", "POST"})
     */
    public function updateInstallationAction(Request $request, $id)
    {
        return '';
    }


    /**
     * @Route("/token-authentication/{idInstalacao}", name="api_usuario_token_authentication")
     */
    public function tokenAuthenticationAction(Request $request, $idInstalacao)
    {
        return '';
    }


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return '';
    }


    /**
     * @Route("/secure-resource", name="secure_resource")
     */
    public function secureResource()
    {
        return '';
    }

    /**
     * @Route("/teste", name="teste_secure_resource")
     */
    public function testeResource()
    {   
        return '';
    }


    /**
     * Creates a new Installation entity.
     *
     * @Route("/contato", name="api_contato")
     * @Method({"GET", "POST"})
     */
    public function contato(Request $request)
    {
        return '';
    }

    /**
     * Creates a new Installation entity.
     *
     * @Route("/advertise", name="api_advertise")
     * @Method({"GET", "POST"})
     */
    public function advertise(Request $request)
    {
        return '';
    }
}
