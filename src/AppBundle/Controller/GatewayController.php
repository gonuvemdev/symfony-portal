<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use BG\BarcodeBundle\Util\Base1DBarcode as barCode;
use BG\BarcodeBundle\Util\Base2DBarcode as matrixCode;

use BackBundle\Entity\Venda;
use BackBundle\Entity\Item;
use BackBundle\Entity\Cupom;

use mPDF;

/**
 * Categoria controller.
 *
 * @Route("/api/gateway")
 */
class GatewayController extends Controller
{


    /**
     * @Route("/token", name="api_gateway_token")
     */
    public function tokenVenda(Request $request)
    {
       
        return '';
    }


    public function newFatura($usuario, $token)
    {

        return '';     
    }

    public function chargeCardVenda($token, $id)
    {
        return '';
    }


    /**
     * @Route("/cupom/draw/{id}/{destino}", name="api_draw_cupom")
     */
    public function drawCupomVenda(Request $request, $id, $destino)
    {
        return '';
    }


    public function drawSlipVenda($id)
    {

        return '';
    }


    /**
     * @Route("/chargeslip", name="api_gateway_slip")
     */
    public function chargeSlipVenda(Request $request)
    {        
        return '';
    }

}