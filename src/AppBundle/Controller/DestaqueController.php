<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 26/09/2018
 * Time: 17:04
 */

namespace AppBundle\Controller;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use BackBundle\Form\UsuarioType;
use BackBundle\Entity\User;

/**
 * Destaque controller.
 *
 * @Route("/api/destaque")
 */
class DestaqueController extends Controller
{

    /**
     * @Route("/", name="api_destaque")
     */
    public function indexAction()
    {
        return '';
    }

}