<?php

namespace AppBundle\Controller;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use BackBundle\Form\UsuarioType;
use BackBundle\Entity\User;


use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserInterface;

/**
 * Usuário controller.
 *
 * @Route("/api/usuario")
 */
class UsuarioController extends Controller
{

    /**
     * @Route("/login", name="api_usuario_login")
     */
    public function loginAction(Request $request)
    {
        return '';
    }

    /**
     * @Route("/new", name="api_usuario_new")
     */
    public function newAction(Request $request)
    {       
        return '';
    }

    /**
     * Request reset user password: submit form and send email
     *
     * @Route("/sendemail", name="api_usuario_send_email")
     */
    public function sendEmailAction(Request $request)
    {
        return '';
    }

    /**
     * @Route("/perfil/show", name="api_usuario_perfil_show")
     */
    public function showPerfilAction(Request $request)
    {
        return '';
    }

    /**
     * @Route("/perfil/update", name="api_usuario_perfil_update")
     */
    public function updatePerfilAction(Request $request)
    {
        return '';
    }
}