<?php

namespace UtilBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

/**
 * {@inheritDoc}
 */
class SecurityController extends BaseController
{
    /**
     * {@inheritDoc}
     */
    public function renderLogin(array $data)
    {

        //dump($this->container->get('request_stack')->getCurrentRequest()->getPathInfo());die();

        if ('/admin/login' === $this->container->get('request_stack')->getCurrentRequest()->getPathInfo()) {
            $template = sprintf('FOSUserBundle:Security:login-admin.html.twig');
        } elseif ('/super/' === $this->container->get('request_stack')->getCurrentRequest()->getPathInfo()) {
            $template = sprintf('FOSUserBundle:Security:login-admin.html.twig');
        } else {
            $template = sprintf('FOSUserBundle:Security:login.html.twig');
        }

        return $this->container->get('templating')->renderResponse($template, $data);
    }
}