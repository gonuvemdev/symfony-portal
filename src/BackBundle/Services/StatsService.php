<?php 

namespace BackBundle\Services;

class StatsService
{
     /**
    * Entity Manager
    *
    * @var Doctrine\ORM\EntityManager
    */
    protected $em;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    public function mostReadByDay($day) 
    {
        $bottom_date = new \DateTime($day->format('Y-m-d H:i:s'));
        $bottom_date->setTime(0,0,0);
        $upper_date = new \DateTime($day->format('Y-m-d H:i:s'));
        $upper_date->setTime(23,59,59);

        $mostReadQuery = $this->em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.publishedAt >= :bottom_date')
            ->setParameter(':bottom_date', $bottom_date)
            ->andWhere('a.publishedAt <= :upper_date')
            ->setParameter(':upper_date', $upper_date)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->setMaxResults(1)
            ->getQuery();
        
        $mostReadArticle = $mostReadQuery->getResult();

        return $mostReadArticle;
    }

    public function mostReadByEditorial($editoria, $date_begin, $date_end)
    {
        // Setando as horas
        $bottom_date = new \DateTime($date_begin->format('Y-m-d H:i:s'));
        $bottom_date->setTime(0,0,0);
        $upper_date = new \DateTime($date_end->format('Y-m-d H:i:s'));
        $upper_date->setTime(23,59,59);

        // Achando o objeto da editoria para entrar como parametro
        $editoria_id = $this->em->getRepository('BackBundle:Editorial')->findOneByName($editoria);
        $queryMaisLida = $this->em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoria_id)
            ->andWhere('a.publishedAt >= :data_begin')
            ->setParameter(':data_begin', $bottom_date)
            ->andWhere('a.publishedAt <= :data_end')
            ->setParameter(':data_end', $upper_date)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->setMaxResults(1)
            ->getQuery();
        $maisLidas = $queryMaisLida->getResult();
        $maisLida = null;

        if (count($maisLidas) > 0) {
            $maisLida = $maisLidas[0];
        }
        return $maisLida;
    }

    public function listMostReadByEditorial($editoria, $date_begin, $date_end, $numberOfResults)
    {
        // Setando as horas
        $bottom_date = new \DateTime($date_begin->format('Y-m-d H:i:s'));
        $bottom_date->setTime(0,0,0);
        $upper_date = new \DateTime($date_end->format('Y-m-d H:i:s'));
        $upper_date->setTime(23,59,59);

        // Achando o objeto da editoria para entrar como parametro
        $editoria_id = $this->em->getRepository('BackBundle:Editorial')->findOneByName($editoria);
        $queryMaisLida = $this->em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoria_id)
            ->andWhere('a.publishedAt >= :data_begin')
            ->setParameter(':data_begin', $bottom_date)
            ->andWhere('a.publishedAt <= :data_end')
            ->setParameter(':data_end', $upper_date)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->setMaxResults($numberOfResults)
            ->getQuery();
        $maisLidas = $queryMaisLida->getResult();

        return $maisLidas;
    }

    public function lastArticlesbyEditoria($editoria, $date_begin, $date_end, $quant)
    {
        // Achando o objeto da editoria para entrar como parametro
        $articles = $this->queryArticlesByEditorial($editoria, $date_begin, $date_end, $quant);

        // while(count($articles) < $quant) {
        //     // Diminuindo a data de inicio para criar um intervalo maior;
        //     $date_begin = new \DateTime(date('Y-m-d H:i:s', strtotime('-30 day', strtotime($date_begin->format('Y-m-d H:i:s')))));
        //     $more_articles = $this->queryArticlesByEditorial($editoria, $date_begin, $date_end, $quant);
        //     if(count($more_articles) > $quant)
        //     {
        //         array_push($articles, array_slice($more_articles, $quant - count($articles) - 1));
        //     }
        // }

        return $articles;
    }

    public function queryArticlesByEditorial($editoria, $date_begin, $date_end, $quant)
    {
        // Setando as horas
        $bottom_date = new \DateTime($date_begin->format('Y-m-d H:i:s'));
        $bottom_date->setTime(0,0,0);
        $upper_date = new \DateTime($date_end->format('Y-m-d H:i:s'));
        $upper_date->setTime(23,59,59);

        // Achando o objeto da editoria para entrar como parametro
        $editoria_id = $this->em->getRepository('BackBundle:Editorial')->findOneByName($editoria);
        $query = $this->em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoria_id)
            ->andWhere('a.publishedAt >= :data_begin')
            ->setParameter(':data_begin', $bottom_date)
            ->andWhere('a.publishedAt <= :data_end')
            ->setParameter(':data_end', $upper_date)
            ->setMaxResults($quant)
            ->getQuery();
        $result = $query->getResult();

        return $result;
    }

    public function mostReadOfAll($date_begin, $date_end, $numberOfResults)
    {
        // Setando as horas
        $bottom_date = new \DateTime($date_begin->format('Y-m-d H:i:s'));
        $bottom_date->setTime(0,0,0);
        $upper_date = new \DateTime($date_end->format('Y-m-d H:i:s'));
        $upper_date->setTime(23,59,59);

        $queryMaisLida = $this->em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->andWhere('a.publishedAt >= :data_begin')
            ->setParameter(':data_begin', $bottom_date)
            ->andWhere('a.publishedAt <= :data_end')
            ->setParameter(':data_end', $upper_date)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->setMaxResults($numberOfResults)
            ->getQuery();
        $maisLidas = $queryMaisLida->getResult();

        return $maisLidas;
    }
}