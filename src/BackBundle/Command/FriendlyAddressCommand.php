<?php


namespace BackBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BackBundle\Entity\NewsGroup;
use BackBundle\Entity\City;
use BackBundle\Entity\Editorial;

class FriendlyAddressCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:friendly-name-update')

            ->setDescription('Updates the URL address with friendly names')

            ->setHelp('This command allows you to update the url of the articles and posts that are already published...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Connecting with database');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->where('a.friendly is null');
        $query = $queryArticle->getQuery();
        $articles = $query->getResult();

        $output->writeln('Starting updating operation');
        foreach($articles as $article){
            $article->setFriendly($this->friendlyAdressAction($article->getTitle()));
            $em->persist($article);
            $output->writeln("Artigo " . $article->getId() . " atualizado.");
        }

        $queryNewsGroup = $em->getRepository('BackBundle:NewsGroup')->createQueryBuilder('n');
        $queryNewsGroup->where('n.friendlyNewsgroup is null');
        $query = $queryNewsGroup->getQuery();
        $newsgroups = $query->getResult();

        $output->writeln('Starting updating operation on newsgroups');
        foreach($newsgroups as $newsgroup){
            $newsgroup->setFriendlyNewsgroup($this->friendlyAdressAction($newsgroup->getGroupName()));
            $em->persist($newsgroup);
            $output->writeln("Newsgroup " . $newsgroup->getId() . " atualizado.");
        }

        $queryCity = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryCity->where('c.friendlyCity is null');
        $query = $queryCity->getQuery();
        $cities = $query->getResult();

        $output->writeln('Starting updating operation on cities');
        foreach($cities as $city){
            $city->setFriendlyCity($this->friendlyAdressAction($city->getName()));
            $em->persist($city);
            $output->writeln("City " . $city->getId() . " atualizado.");
        }

        $queryEditorial = $em->getRepository('BackBundle:Editorial')->createQueryBuilder('e');
        $queryEditorial->where('e.friendlyEditorial is null');
        $query = $queryEditorial->getQuery();
        $editorials = $query->getResult();

        $output->writeln('Starting updating operation on editorials');
        foreach($editorials as $editorial){
            $editorial->setFriendlyEditorial($this->friendlyAdressAction($editorial->getName()));
            $em->persist($editorial);
            $output->writeln("Editorial " . $editorial->getId() . " atualizado.");
        }



        $em->flush();
        $output->writeln('Update finished');
    }

    public function friendlyAdressAction($title){
        $title = trim($title);
        $title = str_replace('á', 'a', $title);
        $title = str_replace('à', 'a', $title);
        $title = str_replace('ã', 'a', $title);
        $title = str_replace('â', 'a', $title);
        $title = str_replace('ä', 'a', $title);
        $title = str_replace('é', 'e', $title);
        $title = str_replace('è', 'e', $title);
        $title = str_replace('ê', 'e', $title);
        $title = str_replace('ë', 'e', $title);
        $title = str_replace('ó', 'o', $title);
        $title = str_replace('ô', 'o', $title);
        $title = str_replace('ò', 'o', $title);
        $title = str_replace('õ', 'o', $title);
        $title = str_replace('ö', 'o', $title);
        $title = str_replace('í', 'i', $title);
        $title = str_replace('î', 'i', $title);
        $title = str_replace('ï', 'i', $title);
        $title = str_replace('ú', 'u', $title);
        $title = str_replace('ù', 'u', $title);
        $title = str_replace('û', 'u', $title);
        $title = str_replace('ü', 'u', $title);
        $title = str_replace('ç', 'c', $title);
        $title = str_replace('\'', '', $title);
        $title = str_replace(' "', '', $title);
        $title = str_replace('"', '', $title);
        $title = str_replace(':', '', $title);
        $title = str_replace('!', '', $title);
        $title = str_replace('?', '', $title);
        $title = str_replace(' ', '-', $title);
        $title = strtolower($title);
        return $title;
    }
}