<?php

namespace BackBundle\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, array(
                'required'=>false
            ))
            ->add('city', TextType::class, array(
                'required'=>false
            ))
            ->add('state', ChoiceType::class, array(
                    'required'=>false,
                    'choices' => array(
                        'Acre' => 'Acre',
                        'Alagoas' => 'Alagoas',
                        'Amapá' => 'Amapá',
                        'Amazonas' => 'Amazonas',
                        'Bahia' => 'Bahia',
                        'Ceará' => 'Ceará',
                        'Distrito Federal' => 'Distrito Federal',
                        'Espírito Santo' => 'Espírito Santo',
                        'Goiás' => 'Goiás',
                        'Maranhão' => 'Maranhão',
                        'Mato Grosso' => 'Mato Grosso',
                        'Mato Grosso do Sul' => 'Mato Grosso do Sul',
                        'Minas Gerais' => 'Minas Gerais',
                        'Pará' => 'Pará',
                        'Paraíba' => 'Paraíba',
                        'Paraná' => 'Paraná',
                        'Pernambuco' => 'Pernambuco',
                        'Piauí' => 'Piauí',
                        'Rio de Janeiro' => 'Rio de Janeiro',
                        'Rio Grande do Norte' => 'Rio Grande do Norte',
                        'Rio Grande do Sul' => 'Rio Grande do Sul',
                        'Rondônia' => 'Rondônia',
                        'Roraima' => 'Roraima',
                        'Santa Catarina' => 'Santa Catarina',
                        'São Paulo' => 'São Paulo',
                        'Sergipe' => 'Sergipe',
                        'Tocantins' => 'Tocantins',
                    ))
            )
            ->add('page')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //    'data_class' => 'BackBundle\Entity\City'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_company_filter';
    }


}
