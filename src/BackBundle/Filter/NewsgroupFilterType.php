<?php

namespace BackBundle\Filter;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsgroupFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('owner', EntityType::class, array(
                'required'=>false,
                'class'=>'BackBundle\Entity\User',
                'choice_label' => 'nome',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.tipousuario IN (:tipousuario)')
                        ->setParameter(':tipousuario', array('Comum','Editor','Blog','Coluna','Administrador', 'Super Administrador'))
                        ->orderBy('e.nome', 'ASC');
                }
            ))
            ->add('groupName', TextType::class, array(
                'required'=>false
            ))
            ->add('featured', ChoiceType::class, array(
                'required'=>false,
                'choices' => array(
                    'Todos' => 'Todos',
                    'Ativo' => 'Ativo',
                    'Inativo' => 'Inativo'
                )
            ))
            ->add('status', ChoiceType::class, array(
                'required'=>true,
                'choices' => array(
                    'Todos' => 'Todos',
                    'Ativo' => 'Ativo',
                    'Inativo' => 'Inativo'
                )
            ))
            ->add('canComment', ChoiceType::class, array(
                'required'=>true,
                'choices' => array(
                    'Todos' => 'Todos',
                    'Sim' => 'Sim',
                    'Não' => 'Não'
                )
            ))
            ->add('kind', ChoiceType::class, array(
                'required'=>false,
                'choices' => array(
                    'Blog' => 'Blog',
                    'Coluna' => 'Coluna',
                    'Canal' => 'Canal'
                )
            ))
            ->add('page')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //    'data_class' => 'BackBundle\Entity\City'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_newsgroup_filter';
    }


}
