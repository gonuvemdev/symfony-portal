<?php

namespace BackBundle\Filter;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\DateTime;

class ArticleFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startdate', DateTimeType::class, array(
                'required' => false
            ))
            ->add('enddate', DateTimeType::class, array(
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'required' => false
            ))
            ->add('subtitle', TextType::class, array(
                'required' => false
            ))
            ->add('hat', TextType::class, array(
                'required' => false
            ))
            ->add('author', TextType::class, array(
                'required' => false
            ))
            ->add('origin', TextType::class, array(
                'required' => false
            ))
            ->add('credit', TextType::class, array(
                'required' => false
            ))
            ->add('publishedAt1', DateTimeType::class, array(
                'required' => false
            ))
            ->add('publishedAt2', DateTimeType::class, array(
                'required' => false
            ))
            ->add('content', TextType::class, array(
                'required' => false
            ))
            ->add('keywords', TextType::class, array(
                'required' => false
            ))
            ->add('status', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'Todos' => 'Todos',
                    'Cadastrado' => 'Cadastrado',
                    'Aprovado' => 'Aprovado',
                    'Rejeitado' => 'Rejeitado'
                )
            ))
            ->add('coverFeature', ChoiceType::class, array(
                'required' => true,
                'choices' =>array(
                    'Todos' => "Todos",
                    'Sim' => 'Sim',
                    'Não' => 'Não'
                )
            ))
            ->add('newsgroup', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\NewsGroup'
            ))
            ->add('editorial', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Editorial'
            ))
            ->add('city', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\City'
            ))
            ->add('page')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //    'data_class' => 'BackBundle\Entity\City'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_articles_filter';
    }


}
