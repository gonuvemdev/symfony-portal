<?php

namespace BackBundle\Filter;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class BannerFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Company'
            ))
            ->add('customer', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Customer'
            ))
            ->add('marketing', TextType::class, array(
                'required' => false,
            ))
            ->add('type', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'Super banner' => 'Super banner',
                    'Full banner' => 'Full banner',
                    'Side banner' => 'Side banner',
                    'Half banner' => 'Half banner',
                    'Retângulo lateral' => 'Retângulo lateral',
                    'Arranha céu' => 'Arranha céu',
                    'Retângulo cartaz' => 'Retângulo cartaz'
                )
            ))
            ->add('page')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //    'data_class' => 'BackBundle\Entity\City'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_banner_filter';
    }


}
