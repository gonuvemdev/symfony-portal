<?php

namespace BackBundle\Filter;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\DateTime;

class FeatureFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('location', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Location'
            ))
            ->add('enabled', ChoiceType::class, array(
                'required'=>true,
                'choices'=>array(
                    'Todos' => 'Todos',
                    'Sim' => 'Sim',
                    'Não' => 'Não'
                )
            ))
            ->add('scheduled', ChoiceType::class, array(
                'required'=>true,
                'choices'=>array(
                    'Todos' => 'Todos',
                    'Sim' => 'Sim',
                    'Não' => 'Não'
                )
            ))
            ->add('startdate', DateTimeType::class, array(
                'required' => false
            ))
            ->add('enddate', DateTimeType::class, array(
                'required' => false
            ))
            ->add('article', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Article'
            ))
            ->add('title', TextType::class, array(
                'required' => false
            ))
            ->add('subtitle', TextType::class, array(
                'required' => false
            ))
            ->add('hat', TextType::class, array(
                'required' => false
            ))
            ->add('page')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //    'data_class' => 'BackBundle\Entity\City'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_feature_filter';
    }


}
