<?php

namespace BackBundle\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class DocumentFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, array(
                'required'=>false
            ))
            ->add('credit', TextType::class, array(
                'required'=>false
            ))
            ->add('status', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'Todos'=>'Todos',
                    'Sim' => 'Sim',
                    'Não' => 'Não'
                )
            ))
            ->add('page')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //    'data_class' => 'BackBundle\Entity\City'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_document_filter';
    }


}
