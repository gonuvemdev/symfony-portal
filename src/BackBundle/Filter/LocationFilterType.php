<?php

namespace BackBundle\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class LocationFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, array(
                'required'=>false
            ))
            ->add('type', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'Todos'=>'Todos',
                    'Banner' => 'Banner',
                    'Postagem' => 'Postagem'
                )
            ))
            ->add('newsgroup', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\NewsGroup'
            ))
            ->add('page')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //    'data_class' => 'BackBundle\Entity\City'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_location_filter';
    }


}
