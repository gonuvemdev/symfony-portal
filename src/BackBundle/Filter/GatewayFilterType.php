<?php

namespace BackBundle\Filter;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GatewayFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('page', TextType::class, array(
                'required' => false
            ))
            ->add('status', ChoiceType::class, array(
                    'choices' => array(
                        'Pago' => 'Pago',
                        'Pendente' => 'Pendente',
                        'Cancelado' => 'Cancelado',
                        'Pago Parcialmente' => 'Pago Parcialmente',
                        'Reembolsado' => 'Reembolsado',
                        'Expirado' => 'Expirado',
                        'Autorizado' => 'Autorizado',
                    ),
                    'required' => false,
                )
            )
            ->add('datavencimentoinicio', DateType::class, array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false,
            ))
            ->add('datavencimentofim', DateType::class, array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false,
            ))
            ->add('contrato', TextType::class, array(
                'required' => false,
            ))
            ->add('cliente', EntityType::class, array(
                'class' => 'BackBundle:User',
                'choice_label' => 'nome',
                'required' => false,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.nome', 'ASC');
                }
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => 'DEX\BackBundle\Entity\Conta'
        ));
    }
}
