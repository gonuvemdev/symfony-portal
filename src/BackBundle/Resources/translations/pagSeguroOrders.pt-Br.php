<?php 
return array(
    "1" => "Agendada",
    "2" => "Processando",
    "3" => "Não Processada",
    "4" => "Suspensa",
    "5" => "Paga",
    "6" => 'Não Paga',
);