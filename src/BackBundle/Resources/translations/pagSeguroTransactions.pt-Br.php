<?php 
return array(
    "1" => "Aguardando Pagamento",
    "2" => "Em Análise",
    "3" => "Paga",
    "4" => "Disponível",
    "5" => "Em Disputa",
    "6" => 'Devolvida',
    "7" => "Cancelada"
);