<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CultureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'required' => true,
            ))
            ->add('description', TextType::class, array(
                'required' => true,
            ))
            ->add('date', DateTimeType::class, array(
                'required' => true,
            ))
            ->add('place', TextType::class, array(
                'required' => true,
            ))
            ->add('link', TextType::class, array(
                'required' => false,
            ))
            ->add('phone', TextType::class, array(
                'required' => true,
            ))
            ->add('status', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('mailTo', TextType::class, array(
                'required' => false
            ))
            ->add('imageFile', FileType::class, array(
                'required' => false
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Culture'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_culture';
    }


}
