<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FeatureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('articleTitle', TextType::class, array(
                'required' => false
            ))
            ->add('articleSubtitle', TextType::class, array(
                'required' => false
            ))
            ->add('articleHat', TextType::class, array(
                'required' => false
            ))
            ->add('scheduled', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('scheduledTo', DateTimeType::class, array(
                'required' => false
            ))
            ->add('imageSubtitle', TextType::class, array(
                'required' => false
            ))
            ->add('imageCredit', TextType::class, array(
                'required' => false
            ))
            ->add('imageDescription', TextType::class, array(
                'required' => false
            ))
            ->add('imageFile', FileType::class, array(
                'required'=> false
            ))
            ->add('location', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Location'
            ))
            ->add('article', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Article'
            ))
            ->add('enabled', CheckboxType::class, array(
                'required' => false,
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Feature'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_feature';
    }


}
