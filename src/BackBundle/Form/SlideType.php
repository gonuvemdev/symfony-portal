<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SlideType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('link', TextType::class, array(
                'required' => false,
            ))
            ->add('ordem', TextType::class, array(
                'required' => false,
            ))
            ->add('imageFile', FileType::class, array(
                'required' => false,
            ))
            ->add('article', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Article'
            ))
            ->add('ariArticle', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\AriArticle'
            ))
            ->add('title', TextType::class, array(
                'required' => false,
            ))
            ->add('dateInitial', DateTimeType::class, array(
                'required' => true
            ))
            ->add('dateFinal', DateTimeType::class, array(
                'required' => false
            ))
            ->add('subtitle', TextType::class, array(
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Slide'
        ));
    }
}