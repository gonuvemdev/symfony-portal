<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class EditorialType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required'=>true
            ))
            ->add('title', TextType::class, array(
                'required'=>false
            ))
            ->add('description', TextType::class, array(
                'required'=>false
            ))
            ->add('canComment', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('featured', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('status', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('partner', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('imageFile', FileType::class, array(
                'required' => false,
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Editorial'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_editorial';
    }


}
