<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CompanyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description')
            ->add('address')
            ->add('number')
            ->add('complement')
            ->add('neighborhood')
            ->add('city')
            ->add('state', ChoiceType::class, array(
                    'choices' => array(
                        'Acre' => 'Acre',
                        'Alagoas' => 'Alagoas',
                        'Amapá' => 'Amapá',
                        'Amazonas' => 'Amazonas',
                        'Bahia' => 'Bahia',
                        'Ceará' => 'Ceará',
                        'Distrito Federal' => 'Distrito Federal',
                        'Espírito Santo' => 'Espírito Santo',
                        'Goiás' => 'Goiás',
                        'Maranhão' => 'Maranhão',
                        'Mato Grosso' => 'Mato Grosso',
                        'Mato Grosso do Sul' => 'Mato Grosso do Sul',
                        'Minas Gerais' => 'Minas Gerais',
                        'Pará' => 'Pará',
                        'Paraíba' => 'Paraíba',
                        'Paraná' => 'Paraná',
                        'Pernambuco' => 'Pernambuco',
                        'Piauí' => 'Piauí',
                        'Rio de Janeiro' => 'Rio de Janeiro',
                        'Rio Grande do Norte' => 'Rio Grande do Norte',
                        'Rio Grande do Sul' => 'Rio Grande do Sul',
                        'Rondônia' => 'Rondônia',
                        'Roraima' => 'Roraima',
                        'Santa Catarina' => 'Santa Catarina',
                        'São Paulo' => 'São Paulo',
                        'Sergipe' => 'Sergipe',
                        'Tocantins' => 'Tocantins',
                    ))
            )
            ->add('zip')
            ->add('email')
            ->add('phone1')
            ->add('phone2')
            ->add('phone3')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Company'
        ));
    }
}
