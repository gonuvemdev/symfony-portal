<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AriArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateTimeType::class, array(
                'required'=>true
            ))
            ->add('title', TextType::class, array(
                'required'=>true
            ))
            ->add('firstContainPartI', TextareaType::class, array(
                'required'=>false
            ))
            ->add('firstContainPartIi', TextareaType::class, array(
                'required'=>false
            ))
            ->add('secondContainPartI', TextareaType::class, array(
                'required'=>false
            ))
            ->add('secondContainPartIi', TextareaType::class, array(
                'required'=>false
            ))
            ->add('pingpongTitle', TextType::class, array(
                'required'=>false
            ))
            ->add('pingpongContain', TextareaType::class, array(
                'required'=>false
            ))
            ->add('expressContain', TextareaType::class, array(
                'required'=>false
            ))
            ->add('status', ChoiceType::class, array(
                'required'=>true,
                'choices' => array(
                    'Aprovado' => 'Aprovado',
                    'Registrado' => 'Registrado',
                    'Em Revisão' => 'Em Revisão',
                    'Cancelado' => 'Cancelado'
                )
            ))
            ->add('subtitle', TextType::class, array(
                'required'=>false
            ))
            ->add('hat', TextType::class, array(
                'required'=>false
            ))
            ->add('imageFile', FileType::class, array(
                'required'=> false
            ))
            ->add('subtitleImage', TextType::class, array(
                'required'=>false
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\AriArticle'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_ariarticle';
    }


}
