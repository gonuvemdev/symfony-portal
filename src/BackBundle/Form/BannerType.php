<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class BannerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('link', TextType::class, array(
                'required' => false,
            ))
            ->add('ordem', TextType::class, array(
                'required' => false,
            ))
            ->add('imageFile', FileType::class, array(
                'required' => false,
            ))
            ->add('customer', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Customer'
            ))
            ->add('company', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Company'
            ))
            ->add('location', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Location',
                'choice_label' => 'description',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('l')
                        ->where('l.type = :type')
                        ->setParameter(':type', "Banner")
                        ->orderBy('l.description', 'ASC');
                }
            ))
            ->add('marketing', TextType::class, array(
                'required' => false,
            ))
            ->add('dateInitial', DateTimeType::class, array(
                'required' => true
            ))
            ->add('dateFinal', DateTimeType::class, array(
                'required' => true
            ))
            ->add('type', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'Super banner' => 'Super banner',
                    'Full banner' => 'Full banner',
                    'Side banner' => 'Side banner',
                    'Half banner' => 'Half banner',
                    'Retângulo lateral' => 'Retângulo lateral',
                    'Arranha céu' => 'Arranha céu',
                    'Retângulo cartaz' => 'Retângulo cartaz'
                )
            ))
            ->add('editorials', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Editorial',
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
            ))
            ->add('newsgroups', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\NewsGroup',
                'choice_label' => 'groupName',
                'multiple' => true,
                'expanded' => true,
            ))
            ->add('visivelTodos', CheckBoxType::class, array(
                'required' => false,
            ))
            ->add('visivelPrincipal', CheckBoxType::class, array(
                'required' => false,
            ))
            ->add('visivelArtigos', CheckBoxType::class, array(
                'required' => false,
            ))
            ->add('visivelEditorias', CheckBoxType::class, array(
                'required' => false,
            ))
            ->add('visivelMunicipios', CheckBoxType::class, array(
                'required' => false,
            ))
            ->add('visivelBlogs', CheckBoxType::class, array(
                'required' => false,
            ))
            ->add('visivelColunas', CheckBoxType::class, array(
                'required' => false,
            ))
            ->add('visivelConsultas', CheckBoxType::class, array(
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Banner'
        ));
    }
}