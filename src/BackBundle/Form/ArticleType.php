<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateTimeType::class, array(
                'required' => true
            ))
            ->add('title', TextType::class, array(
                'required' => true
            ))
            ->add('subtitle', TextType::class, array(
                'required' => false
            ))
            ->add('hat', TextType::class, array(
                'required' => false
            ))
            ->add('author', TextType::class, array(
                'required' => false
            ))
            ->add('origin', TextType::class, array(
                'required' => false
            ))
            ->add('credit', TextType::class, array(
                'required' => false
            ))
            ->add('publishedAt', DateTimeType::class, array(
                'required' => true
            ))
            ->add('content', TextareaType::class, array(
                'required' => true
            ))
            ->add('keywords', TextType::class, array(
                'required' => false
            ))
            ->add('status', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'Cadastrado' => 'Cadastrado',
                    'Aprovado' => 'Aprovado',
                    'Rejeitado' => 'Rejeitado'
                )
            ))
            ->add('coverFeature', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('locked', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('freeContent', TextareaType::class, array(
                'required' => false
            ))
            ->add('newsgroup', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\NewsGroup'
            ))
            ->add('editorial', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Editorial'
            ))
            ->add('location', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\Location'
            ))
            ->add('city', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle\Entity\City'
            ))
            ->add('imageFile', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile2', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile3', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile4', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile5', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile6', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile7', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile8', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile9', FileType::class, array(
                'required'=> false
            ))
            ->add('imageFile10', FileType::class, array(
                'required'=> false
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_article';
    }


}
