<?php

namespace BackBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScheduleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('scheduledTo', DateType::class, array(
                'required'=>true
            ))
            ->add('status', ChoiceType::class, array(
                'required'=>true,
                'choices' => array(
                    'Agendado' => 'Agendado',
                    'Publicado' => 'Publicado',
                    'Cancelado' => 'Cancelado'
                )
            ))
            ->add('article', EntityType::class, array(
                'required'=>true,
                'class'=>'BackBundle\Entity\Article'
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Schedule'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_schedule';
    }


}
