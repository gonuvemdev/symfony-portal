<?php

namespace BackBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RegistroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, array(
                'attr' => array(
                    'label' => 'Nome',
                ),
                'required' => true
            ))
            ->add('username', TextType::class, array(
                'attr' => array(
                    'label' => 'Nome',
                ),
                'required' => true
            ))
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'label' => 'E-mail'
                ),
                'required' => true
            ))
            ->add('password', RepeatedType::class, array(
                'attr' => array(
                    'label' => 'Senha',
                ),
                'type' => PasswordType::class,
                'invalid_message' => 'As senhas não conferem',
                'required' => false,
                'first_options' => array('label' => 'Senha'),
                'second_options' => array('label' => 'Repita a senha'),
            ))
            ->add('cpf', TextType::class, array(
                'attr' => array(
                    'label' => 'CPF'
                ),
                'required' => true
            ))
            ->add('rg', TextType::class, array(
                'attr' => array(
                    'label' => 'RG'
                ),
                'required' => false
            ))
            ->add('orgaoemissor', TextType::class, array(
                'attr' => array(
                    'label' => 'Orgão Emissor'
                ),
                'required' => false
            ))
            ->add('telefone1', TextType::class, array(
                'attr' => array(
                    'label' => 'Telefone'
                ),
                'required' => true
            ))
            ->add('telefone2', TextType::class, array(
                'attr' => array(
                    'label' => 'Telefone'
                ),
                'required' => false
            ))
            ->add('telefone3', TextType::class, array(
                'attr' => array(
                    'label' => 'Telefone'
                ),
                'required' => false
            ))
            ->add('endereco', TextType::class, array(
                'attr' => array(
                    'label' => 'Endereço'
                ),
                'required' => true
            ))
            ->add('numero', TextType::class, array(
                'attr' => array(
                    'label' => 'Número'
                ),
                'required' => true
            ))
            ->add('bairro', TextType::class, array(
                'attr' => array(
                    'label' => 'Bairro'
                ),
                'required' => true
            ))
            ->add('cidade', TextType::class, array(
                'attr' => array(
                    'label' => 'Cidade'
                ),
                'required' => true
            ))
            ->add('estado', TextType::class, array(
                'attr' => array(
                    'label' => 'Estado'
                ),
                'required' => true
            ))
            ->add('cep', TextType::class, array(
                'attr' => array(
                    'label' => 'cep'
                ),
                'required' => true
            ))

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'backbundle_usuario_registro';
    }
}