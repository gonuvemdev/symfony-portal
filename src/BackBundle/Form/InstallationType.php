<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InstallationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tokenDispositivo')
            ->add('tipoDispositivo')
            ->add('versaoDispositivo')
            ->add('idInstalacao')
            ->add('timezone')
            ->add('identificadorApp')
            ->add('versaoApp')
            ->add('nomeApp')
            ->add('canal')
            ->add('criadoEm')
            ->add('atualizadoEm')
            ->add('usuario')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Installation'
        ));
    }
}
