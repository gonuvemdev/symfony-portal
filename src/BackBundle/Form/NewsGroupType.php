<?php

namespace BackBundle\Form;

use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsGroupType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('groupName', TextType::class, array(
                'required' => true,
            ))
            ->add('ownerName', TextType::class, array(
                'required' => true,
            ))
            ->add('description', TextType::class, array(
                'required' => false,
            ))
            ->add('canComment', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('status', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('featured', CheckboxType::class, array(
                'required' => false,
            ))
            ->add('mailTo', TextType::class, array(
                'required' => false
            ))
            ->add('kind', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'Blog' => 'Blog',
                    'Coluna' => 'Coluna',
                    'Canal' => 'Canal'
                )
            ))
            ->add('imageFile', FileType::class, array(
                'required' => false
            ))
            ->add('imageFile2', FileType::class, array(
                'required' => false
            ))
            ->add('owner', EntityType::class, array(
                'required' => true,
                'class' => 'BackBundle\Entity\User',
                'choice_label' => 'nome',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.tipousuario IN (:tipousuario)')
                        ->setParameter(':tipousuario', array('Comum','Editor','Blog','Coluna','Administrador', 'Super Administrador'))
                        ->orderBy('e.nome', 'ASC');
                }
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\NewsGroup'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_newsgroup';
    }


}
