<?php

namespace BackBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, array(
                'attr' => array(
                    'label' => 'Nome',
                )
            ))
            ->add('username', TextType::class, array(
                'attr' => array(
                    'label' => 'Nome',
                )
            ))
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'label' => 'E-mail'
                )
            ))
            ->add('password', RepeatedType::class, array(
                'attr' => array(
                    'label' => 'Senha',
                ),
                'type' => PasswordType::class,
                'invalid_message' => 'As senhas não conferem',
                'required' => false,
                'first_options' => array('label' => 'Senha'),
                'second_options' => array('label' => 'Repita a senha'),
            ))
            ->add('cpf', TextType::class, array(
                'attr' => array(
                    'label' => 'CPF'
                ),
                'required' => false
            ))
            ->add('rg', TextType::class, array(
                'attr' => array(
                    'label' => 'RG'
                ),
                'required' => false
            ))
            ->add('orgaoemissor', TextType::class, array(
                'attr' => array(
                    'label' => 'Orgão Emissor'
                ),
                'required' => false
            ))
            ->add('telefone1', TextType::class, array(
                'attr' => array(
                    'label' => 'Telefone'
                ),
                'required' => false
            ))
            ->add('telefone2', TextType::class, array(
                'attr' => array(
                    'label' => 'Telefone'
                ),
                'required' => false
            ))
            ->add('telefone3', TextType::class, array(
                'attr' => array(
                    'label' => 'Telefone'
                ),
                'required' => false
            ))
            ->add('company', EntityType::class, array(
                'required' => false,
                'class' => 'BackBundle:Company',
                'choice_label' => 'description',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.description', 'ASC');
                }
            ))
            ->add('tipousuario', ChoiceType::class, array(
                    'required' => true,
                    'choices' => array(
                        'Comum' => 'Comum',
                        'Editor' => 'Editor',
                        'Blog' => 'Blog',
                        'Coluna' => 'Coluna',
                        'Administrador' => 'Administrador',
                        'Super Administrador' => 'Super Administrador',
                    ),
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'backbundle_usuario';
    }
}