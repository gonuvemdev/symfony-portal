<?php

namespace BackBundle\Controller;

use BackBundle\Entity\City;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * City controller.
 *
 * @Route("/admin/city")
 */
class CityController extends Controller
{
    /**
     * Lists all city entities.
     *
     * @Route("/index/", name="city_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $queryCities = $em->getRepository('BackBundle:City')->createQueryBuilder('c');
        $queryCities->orderBy('c.name', 'ASC');
        $query = $queryCities->getQuery();
        $cities = $query->getResult();
        $hits = (count($cities) / 25);
        $count = count($cities);
        $page = 1;
        if ($hits > 1) {
            $queryCities->setFirstResult(25 * ($page - 1));
            $queryCities->setMaxResults(25);
            $query = $queryCities->getQuery();

            $cities = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\CityFilterType', NULL, array(
            'action' => $this->generateUrl('city_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryCities = $em->getRepository('BackBundle:City')->createQueryBuilder('c');

            $name = $filterform['name']->getData();
            $entrou = false;
            if ($name != null) {
                $queryCities->where('c.name LIKE :name');
                $queryCities->setParameter(':name', '%' . $name . '%');
                $entrou = true;
            }
            $status = $filterform['status']->getData();
            if ($status != null) {
                if ($entrou) {
                    $queryCities->andWhere('c.status = :status');
                } else {
                    $queryCities->where('c.status = :status');
                }
                $queryCities->setParameter(':status', $status);
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryCities->orderBy('c.name', 'ASC');
            $query = $queryCities->getQuery();

            $cities = $query->getResult();
            $hits = (count($cities) / 25);
            $count = count($cities);

            if ($hits > 1) {
                $queryCities->setFirstResult(25 * ($page - 1));
                $queryCities->setMaxResults(25);
                $query = $queryCities->getQuery();

                $cities = $query->getResult();
            }
        }

        return $this->render('BackBundle:City:index.html.twig', array(
            'cities' => $cities,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new city entity.
     *
     * @Route("/new", name="city_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $city = new City();
        $form = $this->createForm('BackBundle\Form\CityType', $city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $city->setFriendlyCity($this->friendlyAddressAction($city->getName()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($city);
            $em->flush();

            return $this->redirectToRoute('city_show', array('id' => $city->getId()));
        }

        return $this->render('BackBundle:City:new.html.twig', array(
            'city' => $city,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a city entity.
     *
     * @Route("/{id}", name="city_show")
     * @Method("GET")
     */
    public function showAction(City $city)
    {
        $deleteForm = $this->createDeleteForm($city);

        return $this->render('BackBundle:City:show.html.twig', array(
            'city' => $city,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing city entity.
     *
     * @Route("/{id}/edit", name="city_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, City $city)
    {
        $deleteForm = $this->createDeleteForm($city);
        $editForm = $this->createForm('BackBundle\Form\CityType', $city);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $city->setFriendlyCity($this->friendlyAddressAction($city->getName()));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('city_edit', array('id' => $city->getId()));
        }

        return $this->render('BackBundle:City:edit.html.twig', array(
            'city' => $city,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a city entity.
     *
     * @Route("/{id}", name="city_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, City $city)
    {
        $form = $this->createDeleteForm($city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($city);
            $em->flush();
        }

        return $this->redirectToRoute('city_index');
    }

    /**
     * Creates a form to delete a city entity.
     *
     * @param City $city The city entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(City $city)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('city_delete', array('id' => $city->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function friendlyAddressAction($title){
        $title = strtolower($title);
        $title = str_replace('ó', 'o', $title);
        $title = str_replace('ô', 'o', $title);
        $title = str_replace('ò', 'o', $title);
        $title = str_replace('õ', 'o', $title);
        $title = str_replace('ö', 'o', $title);
        $title = str_replace('á', 'a', $title);
        $title = str_replace('à', 'a', $title);
        $title = str_replace('ã', 'a', $title);
        $title = str_replace('â', 'a', $title);
        $title = str_replace('ä', 'a', $title);
        $title = str_replace('é', 'e', $title);
        $title = str_replace('è', 'e', $title);
        $title = str_replace('ê', 'e', $title);
        $title = str_replace('ë', 'e', $title);
        $title = str_replace('í', 'i', $title);
        $title = str_replace('î', 'i', $title);
        $title = str_replace('ï', 'i', $title);
        $title = str_replace('ú', 'u', $title);
        $title = str_replace('ù', 'u', $title);
        $title = str_replace('û', 'u', $title);
        $title = str_replace('ü', 'u', $title);
        $title = str_replace('ç', 'c', $title);
        $title = str_replace('\'', '', $title);
        $title = str_replace(' "', '', $title);
        $title = str_replace('"', '', $title);
        $title = str_replace(':', '', $title);
        $title = str_replace('!', '', $title);
        $title = str_replace('?', '', $title);
        $title = str_replace(' ', '-', $title);
        $title = strtolower($title);
        return $title;
    }
}
