<?php

namespace BackBundle\Controller;

use BackBundle\BackBundle;
use BackBundle\Entity\Editorial;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Editorial controller.
 *
 * @Route("/admin/editorial")
 */
class EditorialController extends Controller
{
    /**
     * Lists all editorial entities.
     *
     * @Route("/index/", name="editorial_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /*$editorials = $em->getRepository('BackBundle:Editorial')->findAll();
        $hits = (count($editorials)/25);
        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
          array(),array(), 25, 25 * ($page - 1)
        );*/

        $queryEditorials = $em->getRepository('BackBundle:Editorial')->createQueryBuilder('e');
        $queryEditorials->orderBy('e.name', 'ASC');
        $query = $queryEditorials->getQuery();
        $editorials = $query->getResult();
        $hits = (count($editorials) / 25);
        $count = count($editorials);
        $page = 1;
        if ($hits > 1) {
            $queryEditorials->setFirstResult(25 * ($page - 1));
            $queryEditorials->setMaxResults(25);
            $query = $queryEditorials->getQuery();

            $editorials = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\EditorialFilterType', NULL, array(
            'action' => $this->generateUrl('editorial_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryEditorials = $em->getRepository('BackBundle:Editorial')->createQueryBuilder('e');

            $name = $filterform['name']->getData();
            $entrou = false;
            if ($name != null) {
                $queryEditorials->where('e.name LIKE :name');
                $queryEditorials->setParameter(':name', '%' . $name . '%');
                $entrou = true;
            }
            $status = $filterform['status']->getData();
            if ($status != 'Todos') {
                if($status == 'Ativo'){
                    $status = true;
                } else {
                    $status = false;
                }
                if ($entrou) {
                    $queryEditorials->andWhere('e.status = :status');
                } else {
                    $queryEditorials->where('e.status = :status');
                }
                $queryEditorials->setParameter(':status', $status);
                $entrou = true;
            }

            $comentarios = $filterform['canComment']->getData();
            if ($comentarios != 'Todos') {
                if($comentarios == 'Sim'){
                    $comentarios = true;
                } else {
                    $comentarios = false;
                }
                if ($entrou) {
                    $queryEditorials->andWhere('e.canComment = :canComment');
                } else {
                    $queryEditorials->where('e.canComment = :canComment');
                }
                $queryEditorials->setParameter(':canComment', $comentarios);
                $entrou = true;
            }

            $destaques = $filterform['featured']->getData();
            if ($destaques != 'Todos') {
                if($destaques == 'Sim'){
                    $destaques = true;
                } else {
                    $destaques = false;
                }
                if ($entrou) {
                    $queryEditorials->andWhere('e.featured = :featured');
                } else {
                    $queryEditorials->where('e.featured = :featured');
                }
                $queryEditorials->setParameter(':featured', $destaques);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryEditorials->orderBy('e.name', 'ASC');
            $query = $queryEditorials->getQuery();

            $editorials = $query->getResult();
            $hits = (count($editorials) / 25);
            $count = count($editorials);

            if ($hits > 1) {
                $queryEditorials->setFirstResult(25 * ($page - 1));
                $queryEditorials->setMaxResults(25);
                $query = $queryEditorials->getQuery();

                $editorials = $query->getResult();
            }
        }

        return $this->render('BackBundle:Editorial:index.html.twig', array(
            'editorials' => $editorials,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new editorial entity.
     *
     * @Route("/new", name="editorial_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $editorial = new Editorial();
        $form = $this->createForm('BackBundle\Form\EditorialType', $editorial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $editorial->setFriendlyEditorial($this->friendlyAddressAction($editorial->getName()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($editorial);
            $em->flush();

            return $this->redirectToRoute('editorial_show', array('id' => $editorial->getId()));
        }

        return $this->render('BackBundle:Editorial:/new.html.twig', array(
            'editorial' => $editorial,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a editorial entity.
     *
     * @Route("/{id}", name="editorial_show")
     * @Method("GET")
     */
    public function showAction(Editorial $editorial)
    {
        $deleteForm = $this->createDeleteForm($editorial);

        return $this->render('BackBundle:Editorial:show.html.twig', array(
            'editorial' => $editorial,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing editorial entity.
     *
     * @Route("/{id}/edit", name="editorial_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Editorial $editorial)
    {
        $deleteForm = $this->createDeleteForm($editorial);
        $editForm = $this->createForm('BackBundle\Form\EditorialType', $editorial);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $editorial->setFriendlyEditorial($this->friendlyAddressAction(($editorial->getName())));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('editorial_edit', array('id' => $editorial->getId()));
        }

        return $this->render('BackBundle:Editorial:edit.html.twig', array(
            'editorial' => $editorial,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a editorial entity.
     *
     * @Route("/{id}", name="editorial_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Editorial $editorial)
    {
        $form = $this->createDeleteForm($editorial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($editorial);
            $em->flush();
        }

        return $this->redirectToRoute('editorial_index');
    }

    /**
     * Creates a form to delete a editorial entity.
     *
     * @param Editorial $editorial The editorial entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Editorial $editorial)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('editorial_delete', array('id' => $editorial->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function friendlyAddressAction($title){
        $title = strtolower($title);
        $title = str_replace('ó', 'o', $title);
        $title = str_replace('ô', 'o', $title);
        $title = str_replace('ò', 'o', $title);
        $title = str_replace('õ', 'o', $title);
        $title = str_replace('ö', 'o', $title);
        $title = str_replace('á', 'a', $title);
        $title = str_replace('à', 'a', $title);
        $title = str_replace('ã', 'a', $title);
        $title = str_replace('â', 'a', $title);
        $title = str_replace('ä', 'a', $title);
        $title = str_replace('é', 'e', $title);
        $title = str_replace('è', 'e', $title);
        $title = str_replace('ê', 'e', $title);
        $title = str_replace('ë', 'e', $title);
        $title = str_replace('í', 'i', $title);
        $title = str_replace('î', 'i', $title);
        $title = str_replace('ï', 'i', $title);
        $title = str_replace('ú', 'u', $title);
        $title = str_replace('ù', 'u', $title);
        $title = str_replace('û', 'u', $title);
        $title = str_replace('ü', 'u', $title);
        $title = str_replace('ç', 'c', $title);
        $title = str_replace('\'', '', $title);
        $title = str_replace(' "', '', $title);
        $title = str_replace('"', '', $title);
        $title = str_replace(':', '', $title);
        $title = str_replace('!', '', $title);
        $title = str_replace('?', '', $title);
        $title = str_replace(' ', '-', $title);
        $title = strtolower($title);
        return $title;
    }
}
