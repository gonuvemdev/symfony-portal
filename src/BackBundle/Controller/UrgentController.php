<?php
/**
 * Created by PhpStorm.
 * User: dex
 * Date: 21/09/2018
 * Time: 11:45
 */

namespace BackBundle\Controller;


use BackBundle\Entity\Urgent;
use BackBundle\Form\UrgentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * Urgent controller
 * @Route ("/admin/urgente")
 */
class UrgentController extends Controller
{

    /**
     * Lists all urgent entities.
     *
     * @Route("/index/", name="urgente_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $queryUrgent = $em->getRepository('BackBundle:Urgent')->createQueryBuilder('u');
        $queryUrgent->orderBy('u.title', 'ASC');
        $query = $queryUrgent->getQuery();
        $urgents = $query->getResult();
        $hits = (count($urgents) / 25);
        $count = count($urgents);
        $page = 1;

        if ($hits > 1) {
            $queryUrgent->setFirstResult(25 * ($page - 1));
            $queryUrgent->setMaxResults(25);
            $query = $queryUrgent->getQuery();
            $urgents = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\UrgentFilterType', NULL, array(
            'action' => $this->generateUrl('urgente_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryUrgent = $em->getRepository('BackBundle:Urgent')->createQueryBuilder('u');

            $name = $filterform['title']->getData();
            $entrou = false;
            if ($name != null) {
                $queryUrgent->where('u.title LIKE :title');
                $queryUrgent->setParameter(':title', '%'.$name.'%' );
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryUrgent->orderBy('u.title', 'ASC');
            $query = $queryUrgent->getQuery();

            $urgents = $query->getResult();
            $hits = (count($urgents) / 25);
            $count = count($urgents);

            if ($hits > 1) {
                $queryUrgent->setFirstResult(25 * ($page - 1));
                $queryUrgent->setMaxResults(25);
                $query = $queryUrgent->getQuery();
                $urgents = $query->getResult();
            }
        }

        return $this->render('BackBundle:Urgent:index.html.twig', array(
            'urgents' => $urgents,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new urgent entity.
     *
     * @Route("/new", name="urgente_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $urgent = new Urgent();
        $form = $this->createForm('BackBundle\Form\UrgentType', $urgent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($urgent);
            $em->flush();

            return $this->redirectToRoute('urgente_show', array('id' => $urgent->getId()));
        }

        return $this->render('BackBundle:Urgent:new.html.twig', array(
            'urgent' => $urgent,
            'form' => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing urgent entity.
     *
     * @Route("/{id}/edit", name="urgente_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Urgent $urgent)
    {
        $deleteForm = $this->createDeleteForm($urgent);
        $editForm = $this->createForm('BackBundle\Form\UrgentType', $urgent);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('urgente_edit', array('id' => $urgent->getId()));
        }

        return $this->render('BackBundle:Urgent:edit.html.twig', array(
            'urgent' => $urgent,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a urgent entity.
     *
     * @Route("/{id}", name="urgente_show")
     * @Method("GET")
     */
    public function showAction(Urgent $urgent)
    {
        $deleteForm = $this->createDeleteForm($urgent);

        return $this->render('BackBundle:Urgent:show.html.twig', array(
            'urgent' => $urgent,
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a urgent entity.
     *
     * @Route("/{id}", name="urgente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Urgent $urgent)
    {
        $form = $this->createDeleteForm($urgent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($urgent);
            $em->flush();
        }

        return $this->redirectToRoute('urgente_index');
    }

    /**
     * Creates a form to delete a urgent entity.
     *
     * @param Urgent $urgent The urgent entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Urgent $urgent)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('urgente_delete', array('id' => $urgent->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}