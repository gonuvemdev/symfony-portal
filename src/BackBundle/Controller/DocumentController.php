<?php

namespace BackBundle\Controller;

use BackBundle\BackBundle;
use BackBundle\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Document controller.
 *
 * @Route("/admin/document")
 */
class DocumentController extends Controller
{
    /**
     * Lists all document entities.
     *
     * @Route("/index/", name="document_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $queryDocument = $em->getRepository('BackBundle:Document')->createQueryBuilder('d');
        $queryDocument->orderBy('d.description', 'ASC');
        $query = $queryDocument->getQuery();
        $documents = $query->getResult();
        $hits = (count($documents) / 25);
        $count = count($documents);
        $page = 1;

        if ($hits > 1) {
            $queryDocument->setFirstResult(25 * ($page - 1));
            $queryDocument->setMaxResults(25);
            $query = $queryDocument->getQuery();
            $documents = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\DocumentFilterType', NULL, array(
            'action' => $this->generateUrl('document_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryDocument = $em->getRepository('BackBundle:Document')->createQueryBuilder('d');

            $descricao = $filterform['description']->getData();
            $entrou = false;
            if ($descricao != null) {
                $queryDocument->where('d.description = :description');
                $queryDocument->setParameter(':description', $descricao );
                $entrou = true;
            }

            $credito = $filterform['credit']->getData();
            if ($credito != null) {
                if ($entrou) {
                    $queryDocument->andWhere('d.credit LIKE :credit');
                } else {
                    $queryDocument->where('d.credit LIKE :credit');
                }
                $queryDocument->setParameter(':credit', '%'.$credito.'%');
                $entrou = true;
            }

            $status = $filterform['status']->getData();
            if ($status != 'Todos') {
                if($status == 'Sim'){
                    $status = true;
                } else {
                    $status = false;
                }
                if ($entrou) {
                    $queryDocument->andWhere('d.status = :status');
                } else {
                    $queryDocument->where('d.status = :status');
                }
                $queryDocument->setParameter(':status', $status);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryDocument->orderBy('d.description', 'ASC');
            $query = $queryDocument->getQuery();

            $documents = $query->getResult();
            $hits = (count($documents) / 25);
            $count = count($documents);

            if ($hits > 1) {
                $queryDocument->setFirstResult(25 * ($page - 1));
                $queryDocument->setMaxResults(25);
                $query = $queryDocument->getQuery();

                $documents = $query->getResult();
            }
        }

        return $this->render('BackBundle:Document:index.html.twig', array(
            'documents' => $documents,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

        /*$documents = $em->getRepository('BackBundle:Document')->findAll();
        $hits = (count($documents)/25);
        $documents = $em->getRepository('BackBundle:Document')->findBy(
          array(), array(), 25, 25 * ($page - 1)
        );*/

    /**
     * Creates a new document entity.
     *
     * @Route("/new", name="document_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $document = new Document();
        $form = $this->createForm('BackBundle\Form\DocumentType', $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();

            return $this->redirectToRoute('document_show', array('id' => $document->getId()));
        }

        return $this->render('BackBundle:Document:new.html.twig', array(
            'document' => $document,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a document entity.
     *
     * @Route("/{id}", name="document_show")
     * @Method("GET")
     */
    public function showAction(Document $document)
    {
        $deleteForm = $this->createDeleteForm($document);

        return $this->render('BackBundle:Document:show.html.twig', array(
            'document' => $document,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing document entity.
     *
     * @Route("/{id}/edit", name="document_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Document $document)
    {
        $deleteForm = $this->createDeleteForm($document);
        $editForm = $this->createForm('BackBundle\Form\DocumentType', $document);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('document_edit', array('dd' => $document->getId()));
        }

        return $this->render('BackBundle:Document:edit.html.twig', array(
            'document' => $document,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a document entity.
     *
     * @Route("/{id}", name="document_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Document $document)
    {
        $form = $this->createDeleteForm($document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($document);
            $em->flush();
        }

        return $this->redirectToRoute('document_index');
    }

    /**
     * Creates a form to delete a document entity.
     *
     * @param Document $document The document entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Document $document)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('document_delete', array('id' => $document->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
