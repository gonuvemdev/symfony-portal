<?php

namespace BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use BackBundle\Entity\Banner;
use BackBundle\Form\BannerType;

/**
 * Banner controller.
 *
 * @Route("/admin/banner")
 */
class BannerController extends Controller
{

    /**
     * Lists all banner entities.
     *
     * @Route("/index/", name="banner_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $queryBanner = $em->getRepository('BackBundle:Banner')->createQueryBuilder('b');
        $queryBanner->orderBy('b.id', 'DESC');
        $query = $queryBanner->getQuery();
        $banners = $query->getResult();
        $hits = (count($banners) / 25);
        $count = count($banners);
        $page = 1;

        if ($hits > 1) {
            $queryBanner->setFirstResult(25 * ($page - 1));
            $queryBanner->setMaxResults(25);
            $query = $queryBanner->getQuery();
            $banners = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\BannerFilterType', NULL, array(
            'action' => $this->generateUrl('banner_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryBanners = $em->getRepository('BackBundle:Banner')->createQueryBuilder('b');

            $company = $filterform['company']->getData();
            $entrou = false;
            if ($company != null) {
                $queryBanners->where('b.company = :company');
                $queryBanners->setParameter(':company', $company );
                $entrou = true;
            }

            $cliente = $filterform['customer']->getData();
            if ($cliente != null) {
                if ($entrou) {
                    $queryBanners->andWhere('b.customer = :customer');
                } else {
                    $queryBanners->where('b.customer = :customer');
                }
                $queryBanners->setParameter(':customer', '%'.$cliente.'%');
                $entrou = true;
            }

            $campanha = $filterform['marketing']->getData();
            if ($campanha != null) {
                if ($entrou) {
                    $queryBanners->andWhere('b.marketing LIKE :marketing');
                } else {
                    $queryBanners->where('b.marketing LIKE :marketing');
                }
                $queryBanners->setParameter(':marketing', '%'.$campanha.'%');
                $entrou = true;
            }

            $tipo = $filterform['type']->getData();
            if ($tipo != null) {
                if ($entrou) {
                    $queryBanners->andWhere('b.type = :type');
                } else {
                    $queryBanners->where('b.type = :type');
                }
                $queryBanners->setParameter(':type', $tipo);
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryBanner->orderBy('b.id', 'DESC');
            $query = $queryBanners->getQuery();

            $banners = $query->getResult();
            $hits = (count($banners) / 25);
            $count = count($banners);

            if ($hits > 1) {
                $queryBanners->setFirstResult(25 * ($page - 1));
                $queryBanners->setMaxResults(25);
                $query = $queryBanners->getQuery();

                $banners = $query->getResult();
            }
        }


        /*$banners = $em->getRepository('BackBundle:Banner')->findAll();
        $hits = (count($banners) / 25);
        $banners = $em->getRepository('BackBundle:Banner')->findBy(
            array(),array('ordem' => 'ASC'),25,25 * ($page - 1)
        );*/

        return $this->render('BackBundle:Banner:index.html.twig', array(
            'banners' => $banners,
            'hits' => ceil( $hits ),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }
    

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="banner_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $banner = new Banner();
        $banner->setDateInitial( new \DateTime(date('Y-m-d H:i:s')) );
        $banner->setDateFinal( new \DateTime(date('Y-m-d H:i:s')) );
        $form = $this->createForm('BackBundle\Form\BannerType', $banner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($banner);
            $em->flush();

            return $this->redirect( $this->generateUrl('banner_show', array('id' => $banner->getId()) ));
        }

        /* JUST FOR EXPANDED MODE FALSE (EDITORIALS)
         * $em = $this->getDoctrine()->getManager();
        $editorials = $em->getRepository('BackBundle:Editorial')->findAll();
        $countEditorials = count($editorials);
        if ($countEditorials == 0) { $countEditorials = 1; }*/

        return $this->render('BackBundle:Banner:new.html.twig', array(
            'banner' => $banner,
            'form'   => $form->createView(),
            //'countEditorials' => $countEditorials
        ));
    }


    /**
     * Finds and displays a Banner entity.
     *
     * @Route("/{id}", name="banner_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Banner $banner)
    {
        $deleteForm = $this->createDeleteForm($banner);

        return $this->render('BackBundle:Banner:show.html.twig', array(
            'banner' => $banner,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    
    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="banner_edit")
     * @Template()
     */
    public function editAction(Request $request, Banner $banner)
    {

        $deleteForm = $this->createDeleteForm($banner);
        $editForm = $this->createForm('BackBundle\Form\BannerType', $banner);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($banner);
            $em->flush();

            return $this->redirectToRoute('banner_edit', array('id' => $banner->getId()));
        }

        /* JUST FOR EXPANDED MODE FALSE (EDITORIALS)
         * $em = $this->getDoctrine()->getManager();
        $editorials = $em->getRepository('BackBundle:Editorial')->findAll();
        $countEditorials = count($editorials);
        if ($countEditorials == 0) { $countEditorials = 1; }*/

        return $this->render('BackBundle:Banner:edit.html.twig', array(
            'banner'      => $banner,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            //'countEditorials' => $countEditorials
        ));
    }


    /**
     * Deletes a Banner entity.
     *
     * @Route("/{id}", name="banner_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Banner $banner)
    {
        $form = $this->createDeleteForm($banner);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackBundle:Banner')->find($banner->getId());

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Banner entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute('banner_index', array(
            'page' => 1
        ));
    }

    /**
     * Creates a form to delete a Banner entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Banner $banner)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('banner_delete', array('id' => $banner->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}