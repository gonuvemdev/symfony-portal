<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Image controller.
 *
 * @Route("/admin/image")
 */
class ImageController extends Controller
{
    /**
     * Lists all image entities.
     *
     * @Route("/index/", name="image_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $queryImage = $em->getRepository('BackBundle:Image')->createQueryBuilder('i');
        $queryImage->orderBy('i.description', 'ASC');
        $query = $queryImage->getQuery();
        $images = $query->getResult();
        $hits = (count($images) / 25);
        $count = count($images);
        $page = 1;

        if ($hits > 1) {
            $queryImage->setFirstResult(25 * ($page - 1));
            $queryImage->setMaxResults(25);
            $query = $queryImage->getQuery();
            $images = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\ImageFilterType', NULL, array(
            'action' => $this->generateUrl('image_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryImage = $em->getRepository('BackBundle:Image')->createQueryBuilder('i');

            $descricao = $filterform['description']->getData();
            $entrou = false;
            if ($descricao != null) {
                $queryImage->where('i.description = :description');
                $queryImage->setParameter(':description', $descricao );
                $entrou = true;
            }

            $credito = $filterform['credit']->getData();
            if ($credito != null) {
                if ($entrou) {
                    $queryImage->andWhere('i.credit LIKE :credit');
                } else {
                    $queryImage->where('i.credit LIKE :credit');
                }
                $queryImage->setParameter(':credit', '%'.$credito.'%');
                $entrou = true;
            }

            $status = $filterform['status']->getData();
            if ($status != 'Todos') {
                if($status == 'Sim'){
                    $status = true;
                } else {
                    $status = false;
                }
                if ($entrou) {
                    $queryImage->andWhere('i.status = :status');
                } else {
                    $queryImage->where('i.status = :status');
                }
                $queryImage->setParameter(':status', $status);
                $entrou = true;
            }            

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryImage->orderBy('i.description', 'ASC');
            $query = $queryImage->getQuery();

            $images = $query->getResult();
            $hits = (count($images) / 25);
            $count = count($images);

            if ($hits > 1) {
                $queryImage->setFirstResult(25 * ($page - 1));
                $queryImage->setMaxResults(25);
                $query = $queryImage->getQuery();

                $images = $query->getResult();
            }
        }

        return $this->render('BackBundle:Image:index.html.twig', array(
            'images' => $images,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

        /*$images = $em->getRepository('BackBundle:Image')->findAll();
        $hits = (count($images)/25);
        $images = $em->getRepository('BackBundle:Image')->findBy(
            array(),array(), 25, 25 * ($page - 1)
        );*/

    /**
     * Creates a new image entity.
     *
     * @Route("/new", name="image_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $image = new Image();
        $form = $this->createForm('BackBundle\Form\ImageType', $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->flush();

            return $this->redirectToRoute('image_show', array('id' => $image->getId()));
        }

        return $this->render('BackBundle:Image:new.html.twig', array(
            'image' => $image,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a image entity.
     *
     * @Route("/{id}", name="image_show")
     * @Method("GET")
     */
    public function showAction(Image $image)
    {
        $deleteForm = $this->createDeleteForm($image);

        return $this->render('BackBundle:Image:show.html.twig', array(
            'image' => $image,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing image entity.
     *
     * @Route("/{id}/edit", name="image_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Image $image)
    {
        $deleteForm = $this->createDeleteForm($image);
        $editForm = $this->createForm('BackBundle\Form\ImageType', $image);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('image_edit', array('id' => $image->getId()));
        }

        return $this->render('BackBundle:Image:edit.html.twig', array(
            'image' => $image,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a image entity.
     *
     * @Route("/{id}", name="image_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Image $image)
    {
        $form = $this->createDeleteForm($image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($image);
            $em->flush();
        }

        return $this->redirectToRoute('image_index');
    }

    /**
     * Creates a form to delete a image entity.
     *
     * @param Image $image The image entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Image $image)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('image_delete', array('id' => $image->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
