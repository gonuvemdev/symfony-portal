<?php

namespace BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use BackBundle\Form\UsuarioType;
use BackBundle\Entity\User;

/**
 * Local controller.
 *
 * @Route("/admin/usuarioapp")
 */
class UsuarioappController extends Controller
{

    /**
     * Lists all Users entities.
     *
     * @Route("/index/{page}", name="usuario_index_app", defaults={"page" = 1})s
     * @Method("GET")
     * @Template()
     */
    public function indexAction($page)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $queryClientes = $em->getRepository('BackBundle:User')->createQueryBuilder('c');
        $queryClientes->where('c.empresa IS NULL');
        $queryClientes->andWhere('c.tipousuario = :tipousuario');
        $queryClientes->setParameter(':tipousuario', 'Comum');
        $queryClientes->orderBy('c.nome', 'ASC');
        $query = $queryClientes->getQuery();
        $entities = $query->getResult();

        $hits = (count($entities) / 25);
        if ($hits > 1) {
            $queryClientes->setFirstResult(25 * ($page - 1));
            $queryClientes->setMaxResults(25);
            $query = $queryClientes->getQuery();
            $entities = $query->getResult();
        }

        return $this->render('usuarioapp/index.html.twig', array(
            'entities' => $entities,
            'hits' => ceil( $hits ) ,
            'page' => $page
        ));

    }


    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="usuario_new_app")
     * @Template()
     */
    public function newAction(Request $request)
    {

        $usuario = new User();
        $form = $this->createForm('BackBundle\Form\UsuarioType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$form->bind($request);

            //if ($form->isValid()) {

            $userManager = $this->container->get('fos_user.user_manager');
            $newUser = $userManager->createUser();
            $newUser->setUsername($usuario->getUsername());
            $newUser->setUsernameCanonical($usuario->getUsername());
            $encoder_service = $this->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($newUser);
            $password = $encoder->encodePassword($usuario->getPassword(), $newUser->getSalt());
            $newUser->setPassword($password);
            $newUser->setEmail($usuario->getEmail());
            $newUser->setEnabled(true);
            $userManager->updateUser($newUser);

            $tipousuario = $usuario->getTipoUsuario();
            $nome = $usuario->getNome();
            $cpf = $usuario->getCpf();
            $rg = $usuario->getRg();
            $orgaoemissor = $usuario->getOrgaoEmissor();
            $telefone1 = $usuario->getTelefone1();
            $telefone2 = $usuario->getTelefone2();
            $telefone3 = $usuario->getTelefone3();
            $empresa = $usuario->getEmpresa();

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('BackBundle:User')->find($newUser->getId());
            $user->setTipoUsuario($tipousuario);
            $user->setNome($nome);
            $user->setCpf($cpf);
            $user->setRg($rg);
            $user->setOrgaoEmissor($orgaoemissor);
            $user->setTelefone1($telefone1);
            $user->setTelefone2($telefone2);
            $user->setTelefone3($telefone3);
            $user->setEmpresa($empresa);
            date_default_timezone_set('America/Fortaleza');
            $user->setCriado( new \DateTime(date('Y-m-d H:i:s')) );
            $em->persist($user);
            $em->flush();


            return $this->redirect($this->generateUrl('usuario_show_app', array('id' => $user->getId())));
            //}
        }

        return $this->render('usuarioapp/new.html.twig', array(
            'form'   => $form->createView()
        ));
    }




    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="usuario_show_app")
     * @Method("GET")
     * @Template()
     */
    public function showAction(User $usuario)
    {
        $deleteForm = $this->createDeleteForm($usuario);

        return $this->render('usuarioapp/show.html.twig', array(
            'usuario' => $usuario,
            'delete_form' => $deleteForm->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="usuario_edit_app")
     * @Template()
     */
    public function editAction(Request $request, User $usuario)
    {
        $userPass = $this->getDoctrine()->getRepository('BackBundle:User')->findOneByUsername($usuario->getUsername());
        $senha = $userPass->getPassword();

        $deleteForm = $this->createDeleteForm($usuario);
        $editForm = $this->createForm('BackBundle\Form\UsuarioType', $usuario);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $userManager = $this->container->get('fos_user.user_manager');
            $newUser = $userManager->findUserByUsername($usuario->getUsername());
            $igualSenha = strcmp($usuario->getPassword(), null);
            if ($igualSenha != 0) {
                $encoder_service = $this->get('security.encoder_factory');
                $encoder = $encoder_service->getEncoder($newUser);
                $password = $encoder->encodePassword($usuario->getPassword(), $newUser->getSalt());
                $newUser->setPassword($password);
            } else {
                $newUser->setPassword($senha);
            }
            $userManager->updateUser($newUser);
            //$user->eraseCredentials();

            $tipousuario = $usuario->getTipoUsuario();
            $nome = $usuario->getNome();
            $cpf = $usuario->getCpf();
            $rg = $usuario->getRg();
            $orgaoemissor = $usuario->getOrgaoEmissor();
            $telefone1 = $usuario->getTelefone1();
            $telefone2 = $usuario->getTelefone2();
            $telefone3 = $usuario->getTelefone3();
            $empresa = $usuario->getEmpresa();

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('BackBundle:User')->find($newUser->getId());
            $user->setTipoUsuario($tipousuario);
            $user->setNome($nome);
            $user->setCpf($cpf);
            $user->setRg($rg);
            $user->setOrgaoEmissor($orgaoemissor);
            $user->setTelefone1($telefone1);
            $user->setTelefone2($telefone2);
            $user->setTelefone3($telefone3);
            $user->setEmpresa($empresa);
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('usuario_edit_app', array('id' => $newUser->getId())));
            // }
        }

        return $this->render('usuarioapp/edit.html.twig', array(
            'edit_form' => $editForm->createView(),
            'usuario' => $usuario,
            'delete_form' => $deleteForm->createView()
        ));
    }



    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="usuario_delete_app")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $usuario)
    {
        $form = $this->createDeleteForm($usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($usuario);
            $em->flush();
        }
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($usuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuario_delete_app', array('id' => $usuario->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}