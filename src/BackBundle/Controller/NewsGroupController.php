<?php

namespace BackBundle\Controller;

use BackBundle\Entity\NewsGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Newsgroup controller.
 *
 * @Route("/admin/newsgroup")
 */
class NewsGroupController extends Controller
{
    /**
     * Lists all newsGroup entities.
     *
     * @Route("/index/", name="newsgroup_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /*$newsGroups = $em->getRepository('BackBundle:NewsGroup')->findAll();
        $hits = (count($newsGroups)/25);
        $newsGroups = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array(),array(), 25, 25 * ($page - 1)
        );*/

        $queryNewsgroup = $em->getRepository('BackBundle:Newsgroup')->createQueryBuilder('n');
        $queryNewsgroup->orderBy('n.groupName', 'ASC');
        $query = $queryNewsgroup->getQuery();
        $newsgroups = $query->getResult();
        $hits = (count($newsgroups) / 25);
        $count = count($newsgroups);
        $page = 1;

        if ($hits > 1) {
            $queryNewsgroup->setFirstResult(25 * ($page - 1));
            $queryNewsgroup->setMaxResults(25);
            $query = $queryNewsgroup->getQuery();
            $newsgroups = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\NewsgroupFilterType', NULL, array(
            'action' => $this->generateUrl('newsgroup_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryNewsgroup = $em->getRepository('BackBundle:NewsGroup')->createQueryBuilder('n');

            $owner = $filterform['owner']->getData();
            $entrou = false;
            if ($owner != null) {
                $queryNewsgroup->where('n.owner = :owner');
                $queryNewsgroup->setParameter(':owner', $owner );
                $entrou = true;
            }

            $groupName = $filterform['groupName']->getData();
            if ($groupName != null) {
                if ($entrou) {
                    $queryNewsgroup->andWhere('n.groupName LIKE :groupName');
                } else {
                    $queryNewsgroup->where('n.groupName LIKE :groupName');
                }
                $queryNewsgroup->setParameter(':groupName', '%'.$groupName.'%');
                $entrou = true;
            }

            $destaque = $filterform['featured']->getData();
            if ($destaque != 'Todos') {
                if ($destaque != null) {
                    if ($entrou) {
                        $queryNewsgroup->andWhere('n.featured LIKE :featured');
                    } else {
                        $queryNewsgroup->where('n.featured LIKE :featured');
                    }
                    $queryNewsgroup->setParameter(':featured', '%' . $destaque . '%');
                    $entrou = true;
                }
            }

            $comentarios = $filterform['canComment']->getData();
            if ($comentarios != 'Todos') {
                if($comentarios == 'Sim'){
                    $comentarios = true;
                } else {
                    $comentarios = false;
                }
                if ($entrou) {
                    $queryNewsgroup->andWhere('n.canComment = :canComment');
                } else {
                    $queryNewsgroup->where('n.canComment = :canComment');
                }
                $queryNewsgroup->setParameter(':canComment', $comentarios);
                $entrou = true;
            }

            $status = $filterform['status']->getData();
            if ($status != 'Todos') {
                if($status == 'Ativo'){
                    $status = true;
                } else {
                    $status = false;
                }
                if ($entrou) {
                    $queryNewsgroup->andWhere('n.status = :status');
                } else {
                    $queryNewsgroup->where('n.status = :status');
                }
                $queryNewsgroup->setParameter(':status', $status);
                $entrou = true;
            }

            $kind = $filterform['kind']->getData();
            if ($kind != null) {
                if ($entrou) {
                    $queryNewsgroup->andWhere('n.kind = :kind');
                } else {
                    $queryNewsgroup->where('n.kind = :kind');
                }
                $queryNewsgroup->setParameter(':kind', $kind);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryNewsgroup->orderBy('n.groupName', 'ASC');
            $query = $queryNewsgroup->getQuery();

            $newsgroups = $query->getResult();
            $hits = (count($newsgroups) / 25);
            $count = count($newsgroups);

            if ($hits > 1) {
                $queryNewsgroup->setFirstResult(25 * ($page - 1));
                $queryNewsgroup->setMaxResults(25);
                $query = $queryNewsgroup->getQuery();

                $newsgroups = $query->getResult();
            }
        }
        
        return $this->render('BackBundle:Newsgroup:index.html.twig', array(
            'newsgroups' => $newsgroups,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new newsGroup entity.
     *
     * @Route("/new", name="newsgroup_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $newsGroup = new Newsgroup();
        $form = $this->createForm('BackBundle\Form\NewsGroupType', $newsGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newsGroup->setFriendlyNewsgroup($this->friendlyAddressAction($newsGroup->getGroupName()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($newsGroup);
            $em->flush();

            return $this->redirectToRoute('newsgroup_show', array('id' => $newsGroup->getId()));
        }

        return $this->render('BackBundle:Newsgroup:new.html.twig', array(
            'newsGroup' => $newsGroup,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a newsGroup entity.
     *
     * @Route("/{id}", name="newsgroup_show")
     * @Method("GET")
     */
    public function showAction(NewsGroup $newsGroup)
    {
        $deleteForm = $this->createDeleteForm($newsGroup);

        return $this->render('BackBundle:Newsgroup:show.html.twig', array(
            'newsGroup' => $newsGroup,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing newsGroup entity.
     *
     * @Route("/{id}/edit", name="newsgroup_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, NewsGroup $newsGroup)
    {
        $deleteForm = $this->createDeleteForm($newsGroup);
        $editForm = $this->createForm('BackBundle\Form\NewsGroupType', $newsGroup);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $newsGroup->setFriendlyNewsgroup($this->friendlyAddressAction($newsGroup->getGroupName()));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('newsgroup_edit', array('id' => $newsGroup->getId()));
        }

        return $this->render('BackBundle:Newsgroup:edit.html.twig', array(
            'newsGroup' => $newsGroup,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a newsGroup entity.
     *
     * @Route("/{id}", name="newsgroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, NewsGroup $newsGroup)
    {
        $form = $this->createDeleteForm($newsGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($newsGroup);
            $em->flush();
        }

        return $this->redirectToRoute('newsgroup_index');
    }

    /**
     * Creates a form to delete a newsGroup entity.
     *
     * @param NewsGroup $newsGroup The newsGroup entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(NewsGroup $newsGroup)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('newsgroup_delete', array('id' => $newsGroup->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function friendlyAddressAction($title){
        $title = strtolower($title);
        $title = str_replace('ó', 'o', $title);
        $title = str_replace('ô', 'o', $title);
        $title = str_replace('ò', 'o', $title);
        $title = str_replace('õ', 'o', $title);
        $title = str_replace('ö', 'o', $title);
        $title = str_replace('á', 'a', $title);
        $title = str_replace('à', 'a', $title);
        $title = str_replace('ã', 'a', $title);
        $title = str_replace('â', 'a', $title);
        $title = str_replace('ä', 'a', $title);
        $title = str_replace('é', 'e', $title);
        $title = str_replace('è', 'e', $title);
        $title = str_replace('ê', 'e', $title);
        $title = str_replace('ë', 'e', $title);
        $title = str_replace('í', 'i', $title);
        $title = str_replace('î', 'i', $title);
        $title = str_replace('ï', 'i', $title);
        $title = str_replace('ú', 'u', $title);
        $title = str_replace('ù', 'u', $title);
        $title = str_replace('û', 'u', $title);
        $title = str_replace('ü', 'u', $title);
        $title = str_replace('ç', 'c', $title);
        $title = str_replace('\'', '', $title);
        $title = str_replace(' "', '', $title);
        $title = str_replace('"', '', $title);
        $title = str_replace(':', '', $title);
        $title = str_replace('!', '', $title);
        $title = str_replace('?', '', $title);
        $title = str_replace(' ', '-', $title);
        $title = strtolower($title);
        return $title;
    }
}
