<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Article;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Utility controller.
 *
 * @Route("/utility")
 */
class AdminUtilityController extends Controller
{
    /**
     * @Route("/articles", methods="GET", name="articles_utility_list")
     */
    public function getArticlesApi(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $order = $request->query->get('ord') ?: 'DESC';
        $fieldOrder = $request->query->get('field') ?: 'id';

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->orderBy('a.' . $fieldOrder, $order);
        $queryArticle->select('a.id, a.title');
        $query = $queryArticle->getQuery();
        $articles = $query->getArrayResult();

        return $this->json([
            'articles' => $articles
        ], 200, [], ['groups' => ['']]);
    }
}