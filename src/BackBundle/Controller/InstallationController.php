<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Installation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Installation controller.
 *
 * @Route("/admin/installation")
 */
class InstallationController extends Controller
{
    /**
     * Lists all installation entities.
     *
     * @Route("/index/{page}", name="installation_index", defaults={"page" = 1})
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $installations = $em->getRepository('BackBundle:Installation')->findAll();
        $hits = (count($installations) / 25);
        $installations = $em->getRepository('BackBundle:Installation')->findBy(
            array(),array('criadoEm' => 'DESC'),25,25 * ($page - 1)
        );

        return $this->render('BackBundle:Installation:index.html.twig', array(
            'installations' => $installations,
            'hits' => ceil($hits),
            'page' => $page
        ));
    }

    /**
     * Creates a new installation entity.
     *
     * @Route("/new", name="installation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        return $this->redirectToRoute('installation_index', array('page' => 1));
        /*$installation = new Installation();
        $form = $this->createForm('BackBundle\Form\InstallationType', $installation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($installation);
            $em->flush($installation);

            return $this->redirectToRoute('installation_show', array('id' => $installation->getId()));
        }

        return $this->render('installation/new.html.twig', array(
            'installation' => $installation,
            'form' => $form->createView(),
        ));*/
    }

    /**
     * Finds and displays a installation entity.
     *
     * @Route("/{id}", name="installation_show")
     * @Method("GET")
     */
    public function showAction(Installation $installation)
    {
        return $this->redirectToRoute('installation_index', array('page' => 1));
        /*$deleteForm = $this->createDeleteForm($installation);

        return $this->render('installation/show.html.twig', array(
            'installation' => $installation,
            'delete_form' => $deleteForm->createView(),
        ));*/
    }

    /**
     * Displays a form to edit an existing installation entity.
     *
     * @Route("/{id}/edit", name="installation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Installation $installation)
    {
        return $this->redirectToRoute('installation_index', array('page' => 1));
        /*$deleteForm = $this->createDeleteForm($installation);
        $editForm = $this->createForm('BackBundle\Form\InstallationType', $installation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('installation_edit', array('id' => $installation->getId()));
        }

        return $this->render('installation/edit.html.twig', array(
            'installation' => $installation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));*/
    }

    /**
     * Deletes a installation entity.
     *
     * @Route("/{id}", name="installation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Installation $installation)
    {
        return $this->redirectToRoute('installation_index', array('page' => 1));
        /*$form = $this->createDeleteForm($installation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($installation);
            $em->flush($installation);
        }

        return $this->redirectToRoute('installation_index');*/
    }

    /**
     * Creates a form to delete a installation entity.
     *
     * @param Installation $installation The installation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Installation $installation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('installation_delete', array('id' => $installation->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
