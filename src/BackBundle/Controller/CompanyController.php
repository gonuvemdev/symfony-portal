<?php

namespace BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BackBundle\Entity\Company;
use BackBundle\Form\CompanyType;

/**
 * Company controller.
 *
 * @Route("/admin/company")
 */
class CompanyController extends Controller
{
    /**
     * Lists all Company entities.
     *
     * @Route("/index/", name="company_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $queryCompany = $em->getRepository('BackBundle:Company')->createQueryBuilder('c');
        $queryCompany->orderBy('c.description', 'ASC');
        $query = $queryCompany->getQuery();
        $companies = $query->getResult();
        $hits = (count($companies) / 25);
        $count = count($companies);
        $page = 1;

        if ($hits > 1) {
            $queryCompany->setFirstResult(25 * ($page - 1));
            $queryCompany->setMaxResults(25);
            $query = $queryCompany->getQuery();
            $companies = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\CompanyFilterType', NULL, array(
            'action' => $this->generateUrl('company_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryCompany = $em->getRepository('BackBundle:Company')->createQueryBuilder('c');

            $nome = $filterform['description']->getData();
            $entrou = false;
            if ($nome != null) {
                $queryCompany->where('c.description LIKE :description');
                $queryCompany->setParameter(':description', "%".$nome."%" );
                $entrou = true;
            }

            $cidade = $filterform['city']->getData();
            if ($cidade != null) {
                if ($entrou) {
                    $queryCompany->andWhere('c.city LIKE :city');
                } else {
                    $queryCompany->where('c.city LIKE :city');
                }
                $queryCompany->setParameter(':city', '%'.$cidade.'%');
                $entrou = true;
            }

            $estado = $filterform['state']->getData();
            if ($estado != null) {
                if ($entrou) {
                    $queryCompany->andWhere('c.state = :state');
                } else {
                    $queryCompany->where('c.state = :state');
                }
                $queryCompany->setParameter(':state', $estado);
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryCompany->orderBy('c.description', 'ASC');
            $query = $queryCompany->getQuery();

            $companies = $query->getResult();
            $hits = (count($companies) / 25);
            $count = count($companies);

            if ($hits > 1) {
                $queryCompany->setFirstResult(25 * ($page - 1));
                $queryCompany->setMaxResults(25);
                $query = $queryCompany->getQuery();
                $companies = $query->getResult();
            }
        }
        
        
        /*$companys = $em->getRepository('BackBundle:Company')->findAll();
        $hits = (count($companys) / 25);
        $companys = $em->getRepository('BackBundle:Company')->findBy(
            array(),array('description' => 'ASC'),25,25 * ($page - 1)
        );*/

        return $this->render('BackBundle:Company:index.html.twig', array(
            'companies' => $companies,
            'hits' => ceil( $hits ),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new Company entity.
     *
     * @Route("/new", name="company_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $company = new Company();
        $form = $this->createForm('BackBundle\Form\CompanyType', $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('company_show', array('id' => $company->getId()));
        }

        return $this->render('BackBundle:Company:new.html.twig', array(
            'company' => $company,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Company entity.
     *
     * @Route("/{id}", name="company_show")
     * @Method("GET")
     */
    public function showAction(Company $company)
    {
        $deleteForm = $this->createDeleteForm($company);

        return $this->render('BackBundle:Company:show.html.twig', array(
            'company' => $company,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Company entity.
     *
     * @Route("/{id}/edit", name="company_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Company $company)
    {
        $deleteForm = $this->createDeleteForm($company);
        $editForm = $this->createForm('BackBundle\Form\CompanyType', $company);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('company_edit', array('id' => $company->getId()));
        }

        return $this->render('BackBundle:Company:edit.html.twig', array(
            'company' => $company,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Company entity.
     *
     * @Route("/{id}", name="company_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Company $company)
    {
        $form = $this->createDeleteForm($company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($company);
            $em->flush();
        }

        return $this->redirectToRoute('company_index');
    }

    /**
     * Creates a form to delete a Company entity.
     *
     * @param Company $company The Company entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Company $company)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('company_delete', array('id' => $company->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
