<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Metric;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Metric controller.
 *
 * @Route("/admin/metric")
 */
class MetricController extends Controller
{
    /**
     * Lists all metric entities.
     *
     * @Route("/index/{page}", name="metric_index", defaults={"page" = 1})
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $metrics = $em->getRepository('BackBundle:Metric')->findAll();

        $hits = (count($metrics)/25);
        $metrics = $em->getRepository('BackBundle:Metric')->findBy(
            array(),array(), 25, 25 * ($page - 1)
        );

        return $this->render('BackBundle:Metric:index.html.twig', array(
            'metrics' => $metrics,
            'hits' => ceil($hits),
            'page' => $page
        ));
    }

    /**
     * Creates a new metric entity.
     *
     * @Route("/new", name="metric_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $metric = new Metric();
        $form = $this->createForm('BackBundle\Form\MetricType', $metric);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($metric);
            $em->flush();

            return $this->redirectToRoute('metric_show', array('id' => $metric->getId()));
        }

        return $this->render('BackBundle:Metric:new.html.twig', array(
            'metric' => $metric,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a metric entity.
     *
     * @Route("/{id}", name="metric_show")
     * @Method("GET")
     */
    public function showAction(Metric $metric)
    {
        $deleteForm = $this->createDeleteForm($metric);

        return $this->render('metric/show.html.twig', array(
            'metric' => $metric,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing metric entity.
     *
     * @Route("/{id}/edit", name="metric_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Metric $metric)
    {
        $deleteForm = $this->createDeleteForm($metric);
        $editForm = $this->createForm('BackBundle\Form\MetricType', $metric);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('metric_edit', array('id' => $metric->getId()));
        }

        return $this->render('metric/edit.html.twig', array(
            'metric' => $metric,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a metric entity.
     *
     * @Route("/{id}", name="metric_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Metric $metric)
    {
        $form = $this->createDeleteForm($metric);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($metric);
            $em->flush();
        }

        return $this->redirectToRoute('metric_index');
    }

    /**
     * Creates a form to delete a metric entity.
     *
     * @param Metric $metric The metric entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Metric $metric)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('metric_delete', array('id' => $metric->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
