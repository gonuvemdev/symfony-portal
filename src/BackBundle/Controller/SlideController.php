<?php

namespace BackBundle\Controller;

use BackBundle\BackBundle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use BackBundle\Entity\Slide;
use BackBundle\Form\SlideType;

/**
 * Slide controller.
 *
 * @Route("/admin/slide")
 */
class SlideController extends Controller
{

    /**
     * Lists all Slide entities.
     *
     * @Route("/index/", name="slide_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

//        $slides = $em->getRepository('BackBundle:Slide')->findAll();
//        $hits = (count($slides) / 25);
//        $slides = $em->getRepository('BackBundle:Slide')->findBy(
//            array(),array('ordem' => 'ASC'),25,25 * ($page - 1)
//        );
        
        $querySlides = $em->getRepository('BackBundle:Slide')->createQueryBuilder('s');
        $querySlides->orderBy('s.title', 'ASC');
        $query = $querySlides->getQuery();
        $slides = $query->getResult();
        $hits = (count($slides) / 25);
        $count = count($slides);
        $page = 1;

        if ($hits > 1) {
            $querySlides->setFirstResult(25 * ($page - 1));
            $querySlides->setMaxResults(25);
            $query = $querySlides->getQuery();
            $slides = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\SlideFilterType', NULL, array(
            'action' => $this->generateUrl('slide_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $querySlides = $em->getRepository('BackBundle:Slide')->createQueryBuilder('s');

            $titulo = $filterform['title']->getData();
            $entrou = false;
            if ($titulo != null) {
                $querySlides->where('s.title LIKE :title');
                $querySlides->setParameter(':title', "%".$titulo."%" );
                $entrou = true;
            }

            $datainicial = $filterform['startdate1']->getData();
            if ($datainicial != null) {
                if ($entrou) {
                    $querySlides->andWhere('s.date >= :startdate1');
                } else {
                    $querySlides->where('s.date >= :startdate1');
                }
                $querySlides->setParameter(':startdate1', $datainicial);
                $entrou = true;
            }

            $datafinal = $filterform['enddate1']->getData();
            if ($datafinal != null) {
                if ($entrou) {
                    $querySlides->andWhere('s.date <= :enddate1');
                } else {
                    $querySlides->where('s.date <= :enddate1');
                }
                $querySlides->setParameter(':enddate1', $datafinal);
                $entrou = true;
            }

            $datainicial2 = $filterform['startdate2']->getData();
            if ($datainicial2 != null) {
                if ($entrou) {
                    $querySlides->andWhere('s.date >= :startdate2');
                } else {
                    $querySlides->where('s.date >= :startdate2');
                }
                $querySlides->setParameter(':startdate2', $datainicial2);
                $entrou = true;
            }

            $datafinal2 = $filterform['enddate2']->getData();
            if ($datafinal2 != null) {
                if ($entrou) {
                    $querySlides->andWhere('s.date <= :enddate2');
                } else {
                    $querySlides->where('s.date <= :enddate2');
                }
                $querySlides->setParameter(':enddate2', $datafinal2);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $querySlides->orderBy('s.title', 'ASC');
            $query = $querySlides->getQuery();

            $slides = $query->getResult();
            $hits = (count($slides) / 25);
            $count = count($slides);

            if ($hits > 1) {
                $querySlides->setFirstResult(25 * ($page - 1));
                $querySlides->setMaxResults(25);
                $query = $querySlides->getQuery();
                $slides = $query->getResult();
            }
        }
        
        return $this->render('BackBundle:Slide:index.html.twig', array(
            'slides' => $slides,
            'hits' => ceil( $hits ) ,
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }
    

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="slide_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $slide = new Slide();
        $slide->setDateInitial( new \DateTime(date('Y-m-d H:i:s')) );
        $slide->setDateFinal( new \DateTime(date('Y-m-d H:i:s')) );
        $form = $this->createForm('BackBundle\Form\SlideType', $slide);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($slide);
            $em->flush();

            return $this->redirect( $this->generateUrl('slide_show', array('id' => $slide->getId()) ));
        }

        return $this->render('BackBundle:Slide:new.html.twig', array(
            'slide' => $slide,
            'form'   => $form->createView(),
        ));
    }


    /**
     * Finds and displays a Slide entity.
     *
     * @Route("/{id}", name="slide_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Slide $slide)
    {
        $deleteForm = $this->createDeleteForm($slide);

        return $this->render('BackBundle:Slide:show.html.twig', array(
            'slide' => $slide,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    

    
    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="slide_edit")
     * @Template()
     */
    public function editAction(Request $request, Slide $slide)
    {

        $deleteForm = $this->createDeleteForm($slide);
        $editForm = $this->createForm('BackBundle\Form\SlideType', $slide);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($slide);
            $em->flush();

            return $this->redirectToRoute('slide_edit', array('id' => $slide->getId()));
        }

        return $this->render('BackBundle:Slide:edit.html.twig', array(
            'slide'      => $slide,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a Slide entity.
     *
     * @Route("/{id}", name="slide_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Slide $slide)
    {
        $form = $this->createDeleteForm($slide);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackBundle:Slide')->find($slide->getId());

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Slide entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute('slide_index', array(
            'page' => 1
        ));
    }

    /**
     * Creates a form to delete a Slide entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Slide $slide)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('slide_delete', array('id' => $slide->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}