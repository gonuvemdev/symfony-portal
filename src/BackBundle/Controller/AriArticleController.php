<?php

namespace BackBundle\Controller;

use BackBundle\Entity\AriArticle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Ariarticle controller.
 *
 * @Route("/admin/ariarticle")
 */
class AriArticleController extends Controller
{
    /**
     * Lists all ariArticle entities.
     *
     * @Route("/index/", name="ariarticle_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

//        $ariArticles = $em->getRepository('BackBundle:AriArticle')->findAll();
//        $hits = (count($ariArticles)/25);
//        $ariArticles = $em->getRepository('BackBundle:AriArticle')->findBy(
//            array(), array(),25, 25 * ($page - 1)
//        );

        $queryArimateia = $em->getRepository('BackBundle:AriArticle')->createQueryBuilder('a');
        $queryArimateia->orderBy('a.title', 'ASC');
        $query = $queryArimateia->getQuery();
        $arimateia = $query->getResult();
        $hits = (count($arimateia) / 25);
        $count = count($arimateia);
        $page = 1;

        if ($hits > 1) {
            $queryArimateia->setFirstResult(25 * ($page - 1));
            $queryArimateia->setMaxResults(25);
            $query = $queryArimateia->getQuery();
            $arimateia = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\AriArticleFilterType', NULL, array(
            'action' => $this->generateUrl('ariarticle_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryArimateia = $em->getRepository('BackBundle:AriArticle')->createQueryBuilder('a');

            $titulo = $filterform['title']->getData();
            $entrou = false;
            if ($titulo != null) {
                $queryArimateia->where('a.title LIKE :title');
                $queryArimateia->setParameter(':title', "%".$titulo."%" );
                $entrou = true;
            }

            $datainicial = $filterform['startdate']->getData();
            if ($datainicial != null) {
                if ($entrou) {
                    $queryArimateia->andWhere('a.date >= :startdate');
                } else {
                    $queryArimateia->where('a.date >= :startdate');
                }
                $queryArimateia->setParameter(':startdate', $datainicial);
                $entrou = true;
            }

            $datafinal = $filterform['enddate']->getData();
            if ($datafinal != null) {
                if ($entrou) {
                    $queryArimateia->andWhere('a.date <= :enddate');
                } else {
                    $queryArimateia->where('a.date <= :enddate');
                }
                $queryArimateia->setParameter(':enddate', $datafinal);
                $entrou = true;
            }

            $status = $filterform['status']->getData();
            if ($status != 'Todos') {
                if ($entrou) {
                    $queryArimateia->andWhere('a.status = :status');
                } else {
                    $queryArimateia->where('a.status = :status');
                }
                $queryArimateia->setParameter(':status', $status);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryArimateia->orderBy('a.title', 'ASC');
            $query = $queryArimateia->getQuery();

            $arimateia = $query->getResult();
            $hits = (count($arimateia) / 25);
            $count = count($arimateia);

            if ($hits > 1) {
                $queryArimateia->setFirstResult(25 * ($page - 1));
                $queryArimateia->setMaxResults(25);
                $query = $queryArimateia->getQuery();
                $arimateia = $query->getResult();
            }
        }
        
        return $this->render('BackBundle:Ariarticle:index.html.twig', array(
            'arimateia' => $arimateia,
            'hits' => $hits,
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new ariArticle entity.
     *
     * @Route("/new", name="ariarticle_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ariArticle = new Ariarticle();
        $form = $this->createForm('BackBundle\Form\AriArticleType', $ariArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ariArticle);
            $em->flush();

            return $this->redirectToRoute('ariarticle_show', array('id' => $ariArticle->getId()));
        }

        return $this->render('BackBundle:Ariarticle:new.html.twig', array(
            'ariArticle' => $ariArticle,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ariArticle entity.
     *
     * @Route("/{id}", name="ariarticle_show")
     * @Method("GET")
     */
    public function showAction(AriArticle $ariArticle)
    {
        $deleteForm = $this->createDeleteForm($ariArticle);

        return $this->render('BackBundle:Ariarticle:show.html.twig', array(
            'ariArticle' => $ariArticle,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ariArticle entity.
     *
     * @Route("/{id}/edit", name="ariarticle_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, AriArticle $ariArticle)
    {
        $deleteForm = $this->createDeleteForm($ariArticle);
        $editForm = $this->createForm('BackBundle\Form\AriArticleType', $ariArticle);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ariarticle_edit', array('id' => $ariArticle->getId()));
        }

        return $this->render('BackBundle:Ariarticle:edit.html.twig', array(
            'ariArticle' => $ariArticle,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ariArticle entity.
     *
     * @Route("/{id}", name="ariarticle_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, AriArticle $ariArticle)
    {
        $form = $this->createDeleteForm($ariArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ariArticle);
            $em->flush();
        }

        return $this->redirectToRoute('ariarticle_index');
    }

    /**
     * Creates a form to delete a ariArticle entity.
     *
     * @param AriArticle $ariArticle The ariArticle entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AriArticle $ariArticle)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ariarticle_delete', array('id' => $ariArticle->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
