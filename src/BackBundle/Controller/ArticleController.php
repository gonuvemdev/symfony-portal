<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Article controller.
 *
 * @Route("/admin/article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all article entities.
     *
     * @Route("/index", name="article_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

//        $articles = $em->getRepository('BackBundle:Article')->findAll();
//        $hits = (count($articles))/25;
//        $articles = $em->getRepository('BackBundle:Article')->findBy(
//          array(), array(), 25, 25 * ($page - 1)
//        );

        $order = $request->query->get('ord') ?: 'DESC';
        $fieldOrder = $request->query->get('field') ?: 'id';

        $queryArticle = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticle->orderBy('a.' . $fieldOrder, $order);
        $query = $queryArticle->getQuery();
        $articles = $query->getResult();
        $hits = (count($articles) / 25);
        $count = count($articles);
        $page = 1;

        if ($hits > 1) {
            $queryArticle->setFirstResult(25 * ($page - 1));
            $queryArticle->setMaxResults(25);
            $query = $queryArticle->getQuery();
            $articles = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\ArticleFilterType', NULL, array(
            'action' => $this->generateUrl('article_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryArticles = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');

            $titulo = $filterform['title']->getData();
            $entrou = false;
            if ($titulo != null) {
                $queryArticles->where('a.title LIKE :title');
                $queryArticles->setParameter(':title', "%".$titulo."%" );
                $entrou = true;
            }

            $datainicial = $filterform['startdate']->getData();
            if ($datainicial != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.date >= :startdate');
                } else {
                    $queryArticles->where('a.date >= :startdate');
                }
                $queryArticles->setParameter(':startdate', $datainicial);
                $entrou = true;
            }

            $datafinal = $filterform['enddate']->getData();
            if ($datafinal != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.date <= :enddate');
                } else {
                    $queryArticles->where('a.date <= :enddate');
                }
                $queryArticles->setParameter(':enddate', $datafinal);
                $entrou = true;
            }


            $sub = $filterform['subtitle']->getData();
            if ($sub != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.subtitle LIKE :subtitle');
                } else {
                    $queryArticles->where('a.subtitle LIKE :subtitle');
                }
                $queryArticles->setParameter(':subtitle', "%".$sub."%");
                $entrou = true;
            }


            $chapeu = $filterform['hat']->getData();
            if ($chapeu != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.hat LIKE :hat');
                } else {
                    $queryArticles->where('a.hat LIKE :hat');
                }
                $queryArticles->setParameter(':hat', "%".$chapeu."%");
                $entrou = true;
            }


            $autor = $filterform['author']->getData();
            if ($autor != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.author LIKE :author');
                } else {
                    $queryArticles->where('a.author LIKE :author');
                }
                $queryArticles->setParameter(':author', "%".$autor."%");
                $entrou = true;
            }


            $source = $filterform['origin']->getData();
            if ($source != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.origin LIKE :origin');
                } else {
                    $queryArticles->where('a.origin LIKE :origin');
                }
                $queryArticles->setParameter(':origin', "%".$source."%");
                $entrou = true;
            }


            $credito = $filterform['credit']->getData();
            if ($credito != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.credit LIKE :credit');
                } else {
                    $queryArticles->where('a.credit LIKE :credit');
                }
                $queryArticles->setParameter(':credit', "%".$credito."%");
                $entrou = true;
            }


            $datapublicacao1 = $filterform['publishedAt1']->getData();
            if ($datapublicacao1 != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.publishedAt >= :publishedAt1');
                } else {
                    $queryArticles->where('a.publishedAt >= :publishedAt1');
                }
                $queryArticles->setParameter(':publishedAt1', $datapublicacao1);
                $entrou = true;
            }

            $datapublicacao2 = $filterform['publishedAt2']->getData();
            if ($datapublicacao2 != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.publishedAt <= :publishedAt2');
                } else {
                    $queryArticles->where('a.publishedAt <= :publishedAt2');
                }
                $queryArticles->setParameter(':publishedAt2', $datapublicacao2);
                $entrou = true;
            }

            $conteudo = $filterform['content']->getData();
            if ($conteudo != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.content LIKE :content');
                } else {
                    $queryArticles->where('a.content LIKE :content');
                }
                $queryArticles->setParameter(':content', "%".$conteudo."%");
                $entrou = true;
            }


            $palavraschave = $filterform['keywords']->getData();
            if ($palavraschave != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.keywords LIKE :keywords');
                } else {
                    $queryArticles->where('a.keywords LIKE :keywords');
                }
                $queryArticles->setParameter(':keywords', "%".$palavraschave."%");
                $entrou = true;
            }

            $newsgroup = $filterform['newsgroup']->getData();
            if ($newsgroup != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.newsgroup = :newsgroup');
                } else {
                    $queryArticles->where('a.newsgroup = :newsgroup');
                }
                $queryArticles->setParameter(':newsgroup', $newsgroup);
                $entrou = true;
            }

            $editorial = $filterform['editorial']->getData();
            if ($editorial != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.editorial = :editorial');
                } else {
                    $queryArticles->where('a.editorial = :editorial');
                }
                $queryArticles->setParameter(':editorial', $editorial);
                $entrou = true;
            }

            $destaque = $filterform['coverFeature']->getData();
            if ($destaque != 'Todos') {
                if($destaque == 'Sim'){
                    $destaque = true;
                } else {
                    $destaque = false;
                }
                if ($entrou) {
                    $queryArticles->andWhere('a.coverFeature = :coverFeature');
                } else {
                    $queryArticles->where('a.coverFeature = :coverFeature');
                }
                $queryArticles->setParameter(':coverFeature', $destaque);
                $entrou = true;
            }

            $city = $filterform['city']->getData();
            if ($city != null) {
                if ($entrou) {
                    $queryArticles->andWhere('a.city = :city');
                } else {
                    $queryArticles->where('a.city = :city');
                }
                $queryArticles->setParameter(':city', $city);
                $entrou = true;
            }


            $status = $filterform['status']->getData();
            if ($status != 'Todos') {
                if ($entrou) {
                    $queryArticles->andWhere('a.status = :status');
                } else {
                    $queryArticles->where('a.status = :status');
                }
                $queryArticles->setParameter(':status', $status);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryArticles->orderBy('a.' . $fieldOrder, $order);
            $query = $queryArticles->getQuery();

            $articles = $query->getResult();
            $hits = (count($articles) / 25);
            $count = count($articles);

            if ($hits > 1) {
                $queryArticles->setFirstResult(25 * ($page - 1));
                $queryArticles->setMaxResults(25);
                $query = $queryArticles->getQuery();
                $articles = $query->getResult();
            }
        }

        return $this->render('BackBundle:Article:index.html.twig', array(
            'articles' => $articles,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView(),
            'order' => $order
        ));
    }

    /**
     * Creates a new article entity.
     *
     * @Route("/new", name="article_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $article->setDate( new \DateTime(date('Y-m-d H:i:s')) );
        $article->setPublishedAt( new \DateTime(date('Y-m-d H:i:s')) );

        $form = $this->createForm('BackBundle\Form\ArticleType', $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setFriendly($this->friendlyAdressAction($article->getTitle()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_show', array('id' => $article->getId()));
        }

        return $this->render('BackBundle:Article:new.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a article entity.
     *
     * @Route("/{id}", name="article_show")
     * @Method("GET")
     */
    public function showAction(Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);

        return $this->render('BackBundle:Article:show.html.twig', array(
            'article' => $article,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     * @Route("/{id}/edit", name="article_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('BackBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $article->setFriendly($this->friendlyAdressAction($article->getTitle()));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_edit', array('id' => $article->getId()));
        }

        return $this->render('BackBundle:Article:edit.html.twig', array(
            'article' => $article,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a article entity.
     *
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('article_index');
    }

    /**
     * Visualize a preview of the article
     * 
     * @Route("/preview", name="article_preview")
     */
    public function previewArticle(Request $request)
    {
        // Criando o objeto do tipo Article
        $article = new Article();
        $form = $this->createForm('BackBundle\Form\ArticleType', $article);
        $form->handleRequest($request);

        // MODULARIZAR ESSA FUNÇÃO E USAR SÓ O QUE PRECISA
        $em = $this->getDoctrine()->getManager();
        $today = new \DateTime($article->getDate()->format('Y-m-d H:i:s'));

        $dayOfWeek = $today->format('l');
        $dayOfWeek = $this->get('translator')->trans($dayOfWeek, array(), 'article');

        $day = $today->format('d');
        $year = $today->format('Y');

        $month = $today->format('M');
        $month = $this->get('translator')->trans($month, array(), 'article');

        $time = $today->format('G:i');

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelArtigos = :visivel')
            ->setParameter(':todos', true)
            ->setParameter(':visivel', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersTopoEditoria1 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerTopoEditoria1 = null;

        if (count($bannersTopoPagina) > 0) { 
            $i = rand(0, count($bannersTopoPagina) - 1);
            $bannerTopoPagina = $bannersTopoPagina[ $i ];
        }

        if (count($bannersDestaque) > 0) { 
            $i = rand(0, count($bannersDestaque) - 1);
            $bannerDestaque = $bannersDestaque[ $i ];
        }

        if (count($bannersTopoEditoria1) > 0) { 
            $i = rand(0, count($bannersTopoEditoria1) - 1);
            $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ];
        }

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $editorials = $em->getRepository('BackBundle:Editorial')->findBy(
            array('featured' => true), array('name' => 'ASC'), null, null
        );

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->render('BackBundle:Article:preview.html.twig', array(
                'article' => $article,
                'day' => $day,
                'month' => $month,
                'year' => $year,
                'dayOfWeek' => $dayOfWeek,
                'time' => $time,
                'editorials' => $editorials,
                'blogs' => $blogs,
                'colunas' => $colunas,
                'bannerTopoPagina' => $bannerTopoPagina,
                'bannerDestaque' => $bannerDestaque,
                'bannerTopoEditoria1' => $bannerTopoEditoria1,
            ));
        }
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('article_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function friendlyAdressAction($title){
        $title = strtolower($title);
        $title = str_replace('ó', 'o', $title);
        $title = str_replace('ô', 'o', $title);
        $title = str_replace('ò', 'o', $title);
        $title = str_replace('õ', 'o', $title);
        $title = str_replace('ö', 'o', $title);
        $title = str_replace('á', 'a', $title);
        $title = str_replace('à', 'a', $title);
        $title = str_replace('ã', 'a', $title);
        $title = str_replace('â', 'a', $title);
        $title = str_replace('ä', 'a', $title);
        $title = str_replace('é', 'e', $title);
        $title = str_replace('è', 'e', $title);
        $title = str_replace('ê', 'e', $title);
        $title = str_replace('ë', 'e', $title);
        $title = str_replace('í', 'i', $title);
        $title = str_replace('î', 'i', $title);
        $title = str_replace('ï', 'i', $title);
        $title = str_replace('ú', 'u', $title);
        $title = str_replace('ù', 'u', $title);
        $title = str_replace('û', 'u', $title);
        $title = str_replace('ü', 'u', $title);
        $title = str_replace('ç', 'c', $title);
        $title = str_replace('\'', '', $title);
        $title = str_replace(' "', '', $title);
        $title = str_replace('"', '', $title);
        $title = str_replace(':', '', $title);
        $title = str_replace('!', '', $title);
        $title = str_replace('?', '', $title);
        $title = str_replace(' ', '-', $title);
        $title = strtolower($title);
        return $title;
    }
}
