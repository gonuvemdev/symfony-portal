<?php

namespace BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BackBundle\Entity\Push;
use BackBundle\Form\PushType;

/**
 * Push controller.
 *
 * @Route("/admin/push")
 */
class PushController extends Controller
{
    /**
     * Lists all Push entities.
     *
     * @Route("/index/{page}", name="push_index", defaults={"page" = 1})
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();

        $pushes = $em->getRepository('BackBundle:Push')->findAll();
        $hits = (count($pushes) / 25);
        $pushes = $em->getRepository('BackBundle:Push')->findBy(
            array(),array('data' => 'DESC'),25,25 * ($page - 1)
        );

        return $this->render('BackBundle:Push:index.html.twig', array(
            'pushes' => $pushes,
            'hits' => ceil( $hits ),
            'page' => $page
        ));
    }

    /**
     * Creates a new Push entity.
     *
     * @Route("/new", name="push_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $push = new Push();
        date_default_timezone_set('America/Fortaleza');
        $push->setData( new \DateTime(date('Y-m-d')) );
        $form = $this->createForm('BackBundle\Form\PushType', $push);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($push);
            $em->flush();

            //CHAMADA PARA FUNÇÃO DE ENVIO DE PUSH
            $content = array(
                "en" => $push->getDescricao()
            );

            $fields = array(
                'app_id' => "7c4fe249-e0a3-4c33-9d82-0040d479ecf2",
                'included_segments' => array('All'),
                'data' => array("foo" => "bar"),
                'contents' => $content
            );

            $fields = json_encode($fields);
            //print("\nJSON sent:\n");
            //print($fields);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                'Authorization: Basic YTI3MzgwZDgtYWY2MS00ZTQyLTlhYzMtNWRmNTQ4MzliMWMx'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);
            //FIM CHAMADA PUSH

            return $this->redirectToRoute('push_show', array('id' => $push->getId()));
        }


        return $this->render('push/new.html.twig', array(
            'push' => $push,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Push entity.
     *
     * @Route("/{id}", name="push_show")
     * @Method("GET")
     */
    public function showAction(Push $push)
    {
        $deleteForm = $this->createDeleteForm($push);

        return $this->render('push/show.html.twig', array(
            'push' => $push,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Push entity.
     *
     * @Route("/{id}/edit", name="push_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Push $push)
    {
        /*$deleteForm = $this->createDeleteForm($push);
        $editForm = $this->createForm('BackBundle\Form\PushType', $push);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($push);
            $em->flush();

            return $this->redirectToRoute('push_edit', array('id' => $push->getId()));
        }

        return $this->render('push/edit.html.twig', array(
            'push' => $push,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));*/

        return $this->redirectToRoute('push_index', array('page' => 1));
    }

    /**
     * Deletes a Push entity.
     *
     * @Route("/{id}", name="push_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Push $push)
    {
        /*$form = $this->createDeleteForm($push);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($push);
            $em->flush();
        }

        return $this->redirectToRoute('push_index');*/
        return $this->redirectToRoute('push_index', array('page' => 1));
    }

    /**
     * Creates a form to delete a Push entity.
     *
     * @param Push $push The Push entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Push $push)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('push_delete', array('id' => $push->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
