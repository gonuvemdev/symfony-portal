<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Feature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Gedmo\Sluggable\Util\Urlizer;

/**
 * Feature controller.
 *
 * @Route("/admin/feature")
 */
class FeatureController extends Controller
{
    /**
     * Lists all feature entities.
     *
     * @Route("/index/", name="feature_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /*$features = $em->getRepository('BackBundle:Feature')->findAll();
        $hits = (count($features)/25);
        $features = $em->getRepository('BackBundle:Feature')->findBy(
          array(), array(), 25, 25 * ($page - 1)
        );*/

        $queryFeature = $em->getRepository('BackBundle:Feature')->createQueryBuilder('f');
        $queryFeature->orderBy('f.id', 'DESC');
        $query = $queryFeature->getQuery();
        $features = $query->getResult();
        $hits = (count($features) / 25);
        $count = count($features);
        $page = 1;

        if ($hits > 1) {
            $queryFeature->setFirstResult(25 * ($page - 1));
            $queryFeature->setMaxResults(25);
            $query = $queryFeature->getQuery();
            $features = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\FeatureFilterType', NULL, array(
            'action' => $this->generateUrl('feature_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryFeature = $em->getRepository('BackBundle:Feature')->createQueryBuilder('f');

            $titulo = $filterform['title']->getData();
            $entrou = false;
            if ($titulo != null) {
                $queryFeature->where('f.articleTitle LIKE :title');
                $queryFeature->setParameter(':title', "%".$titulo."%" );
                $entrou = true;
            }

            $datainicial = $filterform['startdate']->getData();
            if ($datainicial != null) {
                if ($entrou) {
                    $queryFeature->andWhere('f.scheduledTo >= :startdate');
                } else {
                    $queryFeature->where('f.scheduledTo >= :startdate');
                }
                $queryFeature->setParameter(':startdate', $datainicial);
                $entrou = true;
            }

            $datafinal = $filterform['enddate']->getData();
            if ($datafinal != null) {
                if ($entrou) {
                    $queryFeature->andWhere('f.scheduledTo <= :enddate');
                } else {
                    $queryFeature->where('f.scheduledTo <= :enddate');
                }
                $queryFeature->setParameter(':enddate', $datafinal);
                $entrou = true;
            }


            $sub = $filterform['subtitle']->getData();
            if ($sub != null) {
                if ($entrou) {
                    $queryFeature->andWhere('f.articleSubtitle LIKE :subtitle');
                } else {
                    $queryFeature->where('f.articleSubtitle LIKE :subtitle');
                }
                $queryFeature->setParameter(':subtitle', "%".$sub."%");
                $entrou = true;
            }


            $chapeu = $filterform['hat']->getData();
            if ($chapeu != null) {
                if ($entrou) {
                    $queryFeature->andWhere('f.articleHat LIKE :hat');
                } else {
                    $queryFeature->where('f.articleHat LIKE :hat');
                }
                $queryFeature->setParameter(':hat', "%".$chapeu."%");
                $entrou = true;
            }

            $location = $filterform['location']->getData();
            if ($location != null) {
                if ($entrou) {
                    $queryFeature->andWhere('f.location = :location');
                } else {
                    $queryFeature->where('f.location = :location');
                }
                $queryFeature->setParameter(':location', $location);
                $entrou = true;
            }

            $artigo = $filterform['article']->getData();
            if ($artigo != null) {
                if ($entrou) {
                    $queryFeature->andWhere('f.article = :article');
                } else {
                    $queryFeature->where('f.article = :article');
                }
                $queryFeature->setParameter(':article', $artigo);
                $entrou = true;
            }

            $ativo = $filterform['enabled']->getData();
            if ($ativo != 'Todos') {
                if($ativo == 'Sim'){
                    $ativo = true;
                } else {
                    $ativo = false;
                }
                if ($entrou) {
                    $queryFeature->andWhere('f.enabled = :enabled');
                } else {
                    $queryFeature->where('f.enabled = :enabled');
                }
                $queryFeature->setParameter(':enabled', $ativo);
                $entrou = true;
            }

            $scheduled = $filterform['scheduled']->getData();
            if ($scheduled != 'Todos') {
                if($scheduled == 'Sim'){
                    $scheduled = true;
                } else {
                    $scheduled = false;
                }
                if ($entrou) {
                    $queryFeature->andWhere('f.scheduled = :scheduled');
                } else {
                    $queryFeature->where('f.scheduled = :scheduled');
                }
                $queryFeature->setParameter(':scheduled', scheduled);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryFeature->orderBy('f.id', 'DESC');
            $query = $queryFeature->getQuery();

            $features = $query->getResult();
            $hits = (count($features) / 25);
            $count = count($features);

            if ($hits > 1) {
                $queryFeature->setFirstResult(25 * ($page - 1));
                $queryFeature->setMaxResults(25);
                $query = $queryFeature->getQuery();
                $features = $query->getResult();
            }
        }

        return $this->render('BackBundle:Feature:index.html.twig', array(
            'features' => $features,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView(),
        ));
    }

    /**
     * Creates a new feature entity.
     *
     * @Route("/new", name="feature_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $feature = new Feature();
        $form = $this->createForm('BackBundle\Form\FeatureType', $feature);
        $form->handleRequest($request);

        $order = $request->query->get('ord') ?: 'DESC';
        $fieldOrder = $request->query->get('field') ?: 'id';

        $em = $this->getDoctrine()->getManager();

        $queryArticleList = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticleList->orderBy('a.' . $fieldOrder, $order);
        $queryArticleList->select('a.id, a.title');
        $query = $queryArticleList->getQuery();
        $articlesList = $query->getArrayResult();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($feature);
            $em->flush();

            return $this->redirectToRoute('feature_show', array('id' => $feature->getId()));
        }

        return $this->render('BackBundle:Feature:new.html.twig', array(
            'feature' => $feature,
            'form' => $form->createView(),
            'articleList' => json_encode($articlesList)
        ));
    }

    /**
     * Finds and displays a feature entity.
     *
     * @Route("/{id}", name="feature_show")
     * @Method("GET")
     */
    public function showAction(Feature $feature)
    {
        $deleteForm = $this->createDeleteForm($feature);

        return $this->render('BackBundle:Feature:show.html.twig', array(
            'feature' => $feature,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing feature entity.
     *
     * @Route("/{id}/edit", name="feature_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Feature $feature)
    {
        $deleteForm = $this->createDeleteForm($feature);
        $editForm = $this->createForm('BackBundle\Form\FeatureType', $feature);
        $editForm->handleRequest($request);

        $order = $request->query->get('ord') ?: 'DESC';
        $fieldOrder = $request->query->get('field') ?: 'id';

        $em = $this->getDoctrine()->getManager();

        $queryArticleList = $em->getRepository('BackBundle:Article')->createQueryBuilder('a');
        $queryArticleList->orderBy('a.' . $fieldOrder, $order);
        $queryArticleList->select('a.id, a.title');
        $query = $queryArticleList->getQuery();
        $articlesList = $query->getArrayResult();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('feature_edit', array('id' => $feature->getId()));
        }

        return $this->render('BackBundle:Feature:edit.html.twig', array(
            'feature' => $feature,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'articleList' => json_encode($articlesList)
        ));
    }

    /**
     * Deletes a feature entity.
     *
     * @Route("/{id}", name="feature_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Feature $feature)
    {
        $form = $this->createDeleteForm($feature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($feature);
            $em->flush();
        }

        return $this->redirectToRoute('feature_index');
    }

    /**
     * Creates a form to delete a feature entity.
     *
     * @param Feature $feature The feature entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Feature $feature)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('feature_delete', array('id' => $feature->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function setFeatureEditing(Feature $feature){

    }

    /**
     * Visualize a preview of the article
     * 
     * @Route("/preview", name="feature_preview")
     */
    public function previewFeature(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        date_default_timezone_set('America/Fortaleza');
        $today = new \DateTime(date('Y-m-d H:i:s'));
        $dayOfWeek = $today->format('l');
        if ($dayOfWeek == 'Sunday') { $dayOfWeek = 'Domingo'; }
        if ($dayOfWeek == 'Monday') { $dayOfWeek = 'Segunda'; }
        if ($dayOfWeek == 'Tuesday') { $dayOfWeek = 'Terça'; }
        if ($dayOfWeek == 'Wednesday') { $dayOfWeek = 'Quarta'; }
        if ($dayOfWeek == 'Thursday') { $dayOfWeek = 'Quinta'; }
        if ($dayOfWeek == 'Friday') { $dayOfWeek = 'Sexta'; }
        if ($dayOfWeek == 'Saturday') { $dayOfWeek = 'Sábado'; }

        $day = $today->format('d');
        $year = $today->format('Y');
        $month = $today->format('M');
        if ($month == 'Jan') { $month = 'jan'; }
        if ($month == 'Feb') { $month = 'fev'; }
        if ($month == 'Mar') { $month = 'mar'; }
        if ($month == 'Apr') { $month = 'abr'; }
        if ($month == 'May') { $month = 'mai'; }
        if ($month == 'Jun') { $month = 'jun'; }
        if ($month == 'Jul') { $month = 'jul'; }
        if ($month == 'Ago') { $month = 'ago'; }
        if ($month == 'Sep') { $month = 'set'; }
        if ($month == 'Oct') { $month = 'out'; }
        if ($month == 'Nov') { $month = 'nov'; }
        if ($month == 'Dez') { $month = 'dez'; }

        $time = $today->format('G:i');

        /*$features = $em->getRepository('BackBundle:Feature')->findBy(
            array('enabled' => true),array('id' => 'DESC'),null,null
        );*/

        $queryUrgent = $em->getRepository('BackBundle:Urgent')
            ->createQueryBuilder('u')
            ->where('u.startDate <= :startDate')
            ->setParameter(':startDate', $today)
            ->andWhere('u.endDate >= :endDate')
            ->setParameter(':endDate', $today)
            ->getQuery();
        $urgentes = $queryUrgent->getResult();



        $principal1 = null;
        $principal2 = null;
        $secundario1 = null;
        $secundario2 = null;
        $secundario3ComFoto = null;
        $featureComFoto1 = null;
        $featureComFoto2 = null;
        $featureSemFoto1 = null;
        $featureSemFoto2 = null;

        $politicaPrincipal = null;
        $politicaSecundario = null;
        $politicaTerciario1 = null;
        $politicaTerciario2 = null;
        $politicaFoto1 = null;
        $politicaFoto2 = null;
        $policitaFrase1 = null;
        $policitaFrase2 = null;

        $entretePrincipal = null;
        $entreteSlide = null;
        $entreteFotoPequena = null;
        $entreteFrase = null;
        $entreteFoto1 = null;
        $entreteFoto2 = null;
        $entreteFoto3 = null;

        $esportePrincipal = null;
        $esporteSecundarioFoto = null;
        $esporteFotoHorizontal1 = null;
        $esporteFrase = null;
        $esporteFotoHorizontal2 = null;
        $esporteFotoGrande = null;
        $esporteFotoPequena = null;

        $municipioPrincipal = null;
        $municipioFoto1 = null;
        $municipioFoto2 = null;
        $municipioFrase1 = null;
        $municipioFrase2 = null;
        $municipioFotoHorizontal = null;
        $municipioFotoGrande = null;
        $municipioFraseCurta = null;  


        
        $queryLocation = $em->getRepository('BackBundle:Location')
        ->createQueryBuilder('l')
        ->orderBy('l.description', 'ASC')
        ->getQuery();
        $locations = $queryLocation->getResult();

        foreach ($locations as $location) {
            $queryFeature = $em->getRepository('BackBundle:Feature')
            ->createQueryBuilder('f')
            ->where('f.enabled = :enabled')
            ->setParameter(':enabled', true)
            //->andWhere('f.scheduledTo <= :data')
            //->setParameter(':data', $today)
            //->andWhere('f.scheduled = :scheduled')
            //->setParameter(':scheduled', true)
            ->andWhere('f.location = :location')
            ->setParameter(':location', $location)
            ->setMaxResults(2)
            ->orderBy('f.scheduledTo', 'ASC')
            ->addOrderBy('f.id', 'ASC')
            ->getQuery();
            $features = $queryFeature->getResult();  

            foreach ($features as $feature) {
                $location = $feature->getLocation();
                    
                if ($location->getDescription() == 'Principal 1') { $principal1 = $feature; }
                if ($location->getDescription() == 'Principal 2') { $principal2 = $feature; }
                if ($location->getDescription() == 'Secundário 1') { $secundario1 = $feature; }
                if ($location->getDescription() == 'Secundário 2') { $secundario2 = $feature; }
                if ($location->getDescription() == 'Secundário 3 Foto') { $secundario3ComFoto = $feature; }
                if ($location->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 == null) { $featureComFoto1 = $feature; } }
                if ($location->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 != null) { if ($feature != $featureComFoto1) { $featureComFoto2 = $featureComFoto1; $featureComFoto1 = $feature; } } }
                if ($location->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 == null) { $featureSemFoto1 = $feature; } }
                if ($location->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 != null) { if ($feature != $featureSemFoto1) { $featureSemFoto2 = $featureSemFoto1; $featureSemFoto1 = $feature; } } }

                if ($location->getDescription() == 'Política Principal') { $politicaPrincipal = $feature; }
                if ($location->getDescription() == 'Política Secundário') { $politicaSecundario = $feature; }
                if ($location->getDescription() == 'Política Terciário') { if ($politicaTerciario1 == null) { $politicaTerciario1 = $feature; } }
                if ($location->getDescription() == 'Política Terciário') { if ($politicaTerciario1 != null) { if ($feature != $politicaTerciario1) { $politicaTerciario2 = $politicaTerciario1; $politicaTerciario1 = $feature; } } }
                if ($location->getDescription() == 'Política Foto 1') { $politicaFoto1 = $feature; }
                if ($location->getDescription() == 'Política Foto 2') { $politicaFoto2 = $feature; }
                if ($location->getDescription() == 'Política Frase') { if ($policitaFrase1 == null) { $policitaFrase1 = $feature; } }
                if ($location->getDescription() == 'Política Frase') { if ($policitaFrase1 != null) { if ($feature != $policitaFrase1) { $policitaFrase2 = $policitaFrase1; $policitaFrase1 = $feature; } } }

                if ($location->getDescription() == 'Entretê Principal') { $entretePrincipal = $feature; }
                if ($location->getDescription() == 'Entretê Slide') { $entreteSlide = $feature; }
                if ($location->getDescription() == 'Entretê Foto Pequena') { $entreteFotoPequena = $feature; }
                if ($location->getDescription() == 'Entretê Frase') { $entreteFrase = $feature; }
                if ($location->getDescription() == 'Entretê Foto 1') { $entreteFoto1 = $feature; }
                if ($location->getDescription() == 'Entretê Foto 2') { $entreteFoto2 = $feature; }
                if ($location->getDescription() == 'Entretê Foto 3') { $entreteFoto3 = $feature; }
                
                if ($location->getDescription() == 'Esporte Principal') { $esportePrincipal = $feature; }
                if ($location->getDescription() == 'Esporte Secundário Foto') { $esporteSecundarioFoto = $feature; }
                if ($location->getDescription() == 'Esporte Foto Horizontal 1') { $esporteFotoHorizontal1 = $feature; }
                if ($location->getDescription() == 'Esporte Frase') { $esporteFrase = $feature; }
                if ($location->getDescription() == 'Esporte Foto Horizontal 2') { $esporteFotoHorizontal2 = $feature; }
                if ($location->getDescription() == 'Esporte Foto Grande') { $esporteFotoGrande = $feature; }
                if ($location->getDescription() == 'Esporte Foto Pequena') { $esporteFotoPequena = $feature; }

                if ($location->getDescription() == 'Municípios Principal') { $municipioPrincipal = $feature; }
                if ($location->getDescription() == 'Municípios Foto 1') { $municipioFoto1 = $feature; }
                if ($location->getDescription() == 'Municípios Foto 2') { $municipioFoto2 = $feature; }
                if ($location->getDescription() == 'Municípios Frase') { if ($municipioFrase1 == null) { $municipioFrase1 = $feature; } }
                if ($location->getDescription() == 'Municípios Frase') { if ($municipioFrase1 != null) { if ($feature != $municipioFrase1) { $municipioFrase2 = $municipioFrase1; $municipioFrase1 = $feature; } } }
                if ($location->getDescription() == 'Municípios Foto Horizontal') { $municipioFotoHorizontal = $feature; }
                if ($location->getDescription() == 'Municípios Foto Grande') { $municipioFotoGrande = $feature; }
                if ($location->getDescription() == 'Municípios Frase Curta') { $municipioFraseCurta = $feature; }

            }    
        }    

        // Criando o objeto do tipo Feature
        $feature = new Feature();
        $form = $this->createForm('BackBundle\Form\FeatureType', $feature);
        $form->handleRequest($request);

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $form['imageFile']->getData();
        if ($uploadedFile) {
            $destination = $this->getParameter('kernel_project_dir').'/../web/arquivos';
            $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
            $newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
            $uploadedFile->move(
                $destination,
                $newFilename
            );
            $feature->setImageName($newFilename);
        }

        // Verificando a localização do destaque que está sendo editado
        if ($feature->getLocation()->getDescription() == 'Principal 1') { $principal1 = $feature;}
        if ($feature->getLocation()->getDescription() == 'Principal 2') { $principal2 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Secundário 1') { $secundario1 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Secundário 2') { $secundario2 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Secundário 3 Foto') { $secundario3ComFoto = $feature; }
        if ($feature->getLocation()->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 == null) { $featureComFoto1 = $feature; } }
        if ($feature->getLocation()->getDescription() == 'Destaque 4 Foto') { if ($featureComFoto1 != null) { if ($feature != $featureComFoto1) { $featureComFoto2 = $featureComFoto1; $featureComFoto1 = $feature; } } }
        if ($feature->getLocation()->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 == null) { $featureSemFoto1 = $feature; } }
        if ($feature->getLocation()->getDescription() == 'Destaque 4 Sem Foto') { if ($featureSemFoto1 != null) { if ($feature != $featureSemFoto1) { $featureSemFoto2 = $featureSemFoto1; $featureSemFoto1 = $feature; } } }

        if ($feature->getLocation()->getDescription() == 'Política Principal') { $politicaPrincipal = $feature; }
        if ($feature->getLocation()->getDescription() == 'Política Secundário') { $politicaSecundario = $feature; }
        if ($feature->getLocation()->getDescription() == 'Política Terciário') { if ($politicaTerciario1 == null) { $politicaTerciario1 = $feature; } }
        if ($feature->getLocation()->getDescription() == 'Política Terciário') { if ($politicaTerciario1 != null) { if ($feature != $politicaTerciario1) { $politicaTerciario2 = $politicaTerciario1; $politicaTerciario1 = $feature; } } }
        if ($feature->getLocation()->getDescription() == 'Política Foto 1') { $politicaFoto1 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Política Foto 2') { $politicaFoto2 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Política Frase') { if ($policitaFrase1 == null) { $policitaFrase1 = $feature; } }
        if ($feature->getLocation()->getDescription() == 'Política Frase') { if ($policitaFrase1 != null) { if ($feature != $policitaFrase1) { $policitaFrase2 = $policitaFrase1; $policitaFrase1 = $feature; } } }

        if ($feature->getLocation()->getDescription() == 'Entretê Principal') { $entretePrincipal = $feature; }
        if ($feature->getLocation()->getDescription() == 'Entretê Slide') { $entreteSlide = $feature; }
        if ($feature->getLocation()->getDescription() == 'Entretê Foto Pequena') { $entreteFotoPequena = $feature; }
        if ($feature->getLocation()->getDescription() == 'Entretê Frase') { $entreteFrase = $feature; }
        if ($feature->getLocation()->getDescription() == 'Entretê Foto 1') { $entreteFoto1 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Entretê Foto 2') { $entreteFoto2 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Entretê Foto 3') { $entreteFoto3 = $feature; }
        
        if ($feature->getLocation()->getDescription() == 'Esporte Principal') { $esportePrincipal = $feature; }
        if ($feature->getLocation()->getDescription() == 'Esporte Secundário Foto') { $esporteSecundarioFoto = $feature; }
        if ($feature->getLocation()->getDescription() == 'Esporte Foto Horizontal 1') { $esporteFotoHorizontal1 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Esporte Frase') { $esporteFrase = $feature; }
        if ($feature->getLocation()->getDescription() == 'Esporte Foto Horizontal 2') { $esporteFotoHorizontal2 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Esporte Foto Grande') { $esporteFotoGrande = $feature; }
        if ($feature->getLocation()->getDescription() == 'Esporte Foto Pequena') { $esporteFotoPequena = $feature; }

        if ($feature->getLocation()->getDescription() == 'Municípios Principal') { $municipioPrincipal = $feature; }
        if ($feature->getLocation()->getDescription() == 'Municípios Foto 1') { $municipioFoto1 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Municípios Foto 2') { $municipioFoto2 = $feature; }
        if ($feature->getLocation()->getDescription() == 'Municípios Frase') { if ($municipioFrase1 == null) { $municipioFrase1 = $feature; } }
        if ($feature->getLocation()->getDescription() == 'Municípios Frase') { if ($municipioFrase1 != null) { if ($feature != $municipioFrase1) { $municipioFrase2 = $municipioFrase1; $municipioFrase1 = $feature; } } }
        if ($feature->getLocation()->getDescription() == 'Municípios Foto Horizontal') { $municipioFotoHorizontal = $feature; }
        if ($feature->getLocation()->getDescription() == 'Municípios Foto Grande') { $municipioFotoGrande = $feature; }
        if ($feature->getLocation()->getDescription() == 'Municípios Frase Curta') { $municipioFraseCurta = $feature; }


        /*$coverFeatures = $em->getRepository('BackBundle:Article')->findBy(
            array('coverFeature' => true),array('id' => 'DESC'),null,null
        );*/

        $queryCoverFeatures = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.coverFeature = :coverFeature')
            ->setParameter(':coverFeature', true)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
        $coverFeatures = $queryCoverFeatures->getResult();

        $queryBanner = $em->getRepository('BackBundle:Banner')
            ->createQueryBuilder('b')
            ->where('b.visivelTodos = :todos OR b.visivelPrincipal = :principal')
            ->setParameter(':todos', true)
            ->setParameter(':principal', true)
            ->andWhere('b.dateInitial <= :data')
            ->andWhere('b.dateFinal >= :data')
            ->setParameter(':data', $today)
            ->getQuery();
        $banners = $queryBanner->getResult();

        $bannersTopoPagina = [];
        $bannersDestaque = [];
        $bannersNossosDestaques = [];
        $bannersTopoEditoria1 = [];
        $bannersEditoria1 = [];
        $bannersTopoEditoria2 = [];
        $bannersEditoria2 = [];
        $bannersTopoEditoria3 = [];
        $bannersEditoria3 = [];
        $bannersTopoEditoria5 = [];
        $bannersEditoria5 = [];

        foreach ($banners as $banner) {
            $location = $banner->getLocation();

            if ($location != null) { if ($location->getDescription() == '1 Topo da página') { array_push($bannersTopoPagina, $banner); } }
            if ($location != null) { if ($location->getDescription() == '2 Destaques') { array_push($bannersDestaque, $banner); } }
            if ($location != null) { if ($location->getDescription() == '3 Nossos Destaques') { array_push($bannersNossosDestaques, $banner); } }
            if ($location != null) { if ($location->getDescription() == '4 Topo da 1ª Editoria') { array_push($bannersTopoEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '5 Na 1ª Editoria') { array_push($bannersEditoria1, $banner); } }
            if ($location != null) { if ($location->getDescription() == '6 Topo da 2ª Editoria') { array_push($bannersTopoEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '7 Na 2ª Editoria') { array_push($bannersEditoria2, $banner); } }
            if ($location != null) { if ($location->getDescription() == '8 Topo da 3ª Editoria') { array_push($bannersTopoEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '9 Na 3ª Editoria') { array_push($bannersEditoria3, $banner); } }
            if ($location != null) { if ($location->getDescription() == '10 Topo da 5ª Editoria') { array_push($bannersTopoEditoria5, $banner); } }
            if ($location != null) { if ($location->getDescription() == '11 Na 5ª Editoria') { array_push($bannersEditoria5, $banner); } }

        }    

        $bannerTopoPagina = null;
        $bannerDestaque = null;
        $bannerNossosDestaques = null;
        $bannerTopoEditoria1 = null;
        $bannerEditoria1 = null;
        $bannerTopoEditoria2 = null;
        $bannerEditoria2 = null;
        $bannerTopoEditoria3 = null;
        $bannerEditoria3 = null;
        $bannerTopoEditoria5 = null;
        $bannerEditoria5 = null;

        if (count($bannersTopoPagina) > 0) { $i = rand(0, count($bannersTopoPagina) - 1); $bannerTopoPagina = $bannersTopoPagina[ $i ]; }
        if (count($bannersDestaque) > 0) { $i = rand(0, count($bannersDestaque) - 1); $bannerDestaque = $bannersDestaque[ $i ]; }
        if (count($bannersNossosDestaques) > 0) { $i = rand(0, count($bannersNossosDestaques) - 1); $bannerNossosDestaques = $bannersNossosDestaques[ $i ]; }
        if (count($bannersTopoEditoria1) > 0) { $i = rand(0, count($bannersTopoEditoria1) - 1); $bannerTopoEditoria1 = $bannersTopoEditoria1[ $i ]; }
        if (count($bannersEditoria1) > 0) { $i = rand(0, count($bannersEditoria1) - 1); $bannerEditoria1 = $bannersEditoria1[ $i ]; }
        if (count($bannersTopoEditoria2) > 0) { $i = rand(0, count($bannersTopoEditoria2) - 1); $bannerTopoEditoria2 = $bannersTopoEditoria2[ $i ]; }
        if (count($bannersEditoria2) > 0) { $i = rand(0, count($bannersEditoria2) - 1); $bannerEditoria2 = $bannersEditoria2[ $i ]; }
        if (count($bannersTopoEditoria3) > 0) { $i = rand(0, count($bannersTopoEditoria3) - 1); $bannerTopoEditoria3 = $bannersTopoEditoria3[ $i ]; }
        if (count($bannersEditoria3) > 0) { $i = rand(0, count($bannersEditoria3) - 1); $bannerEditoria3 = $bannersEditoria3[ $i ]; }
        if (count($bannersTopoEditoria5) > 0) { $i = rand(0, count($bannersTopoEditoria5) - 1); $bannerTopoEditoria5 = $bannersTopoEditoria5[ $i ]; }
        if (count($bannersEditoria5) > 0) { $i = rand(0, count($bannersEditoria5) - 1); $bannerEditoria5 = $bannersEditoria5[ $i ]; }

        $querySlide = $em->getRepository('BackBundle:Slide')
            ->createQueryBuilder('s')
            ->orderBy('s.ordem', 'ASC')
            ->getQuery();
        $slides = $querySlide->getResult();

        $colunas = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Coluna'),array('groupName' => 'ASC'),null,null
        );

        $blogs = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('kind' => 'Blog'),array('groupName' => 'ASC'),null,null
        );

        $groupsDestaque = $em->getRepository('BackBundle:NewsGroup')->findBy(
            array('featured' => true),array('groupName' => 'ASC'),null,null
        );

        $cultures = $em->getRepository('BackBundle:Culture')->findBy(
            array('status' => true),array('date' => 'ASC'),null,null
        );         

        $editoriaPolitica = $em->getRepository('BackBundle:Editorial')->findOneByName('Política');
        $queryMaisLidaPolitica = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaPolitica)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasPolitica = $queryMaisLidaPolitica->getResult();
        $maisLidaPolitica = null;
        $relacionadasPolitica = [];
        if (count($maisLidasPolitica) > 0) {
            $maisLidaPolitica = $maisLidasPolitica[0];

            if (count($maisLidasPolitica) > 1) {
                array_push($relacionadasPolitica, $maisLidasPolitica[1]);
            }
            if (count($maisLidasPolitica) > 2) {
                array_push($relacionadasPolitica, $maisLidasPolitica[2]);
            }
        }

        $editoriaEntretenimento = $em->getRepository('BackBundle:Editorial')->findOneByName('Entretenimento');
        $queryMaisLidaEntretenimento = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaEntretenimento)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasEntretenimento = $queryMaisLidaEntretenimento->getResult();
        $maisLidaEntretenimento = null;
        $relacionadasEntretenimento = [];
        if (count($maisLidasEntretenimento) > 0) {
            $maisLidaEntretenimento = $maisLidasEntretenimento[0];

            if (count($maisLidasEntretenimento) > 1) {
                array_push($relacionadasEntretenimento, $maisLidasEntretenimento[1]);
            }
            if (count($maisLidasEntretenimento) > 2) {
                array_push($relacionadasEntretenimento, $maisLidasEntretenimento[2]);
            }
        }

        $editoriaEsporte = $em->getRepository('BackBundle:Editorial')->findOneByName('Esportes');
        $queryMaisLidaEsporte = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial = :editorial')
            ->setParameter(':editorial', $editoriaEsporte)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.visitorNumberWeb', 'DESC')
            ->getQuery();
        $maisLidasEsporte = $queryMaisLidaEsporte->getResult();
        $maisLidaEsporte = null;
        $relacionadasEsporte = [];
        if (count($maisLidasEsporte) > 0) {
            $maisLidaEsporte = $maisLidasEsporte[0];

            if (count($maisLidasEsporte) > 1) {
                array_push($relacionadasEsporte, $maisLidasEsporte[1]);
            }
            if (count($maisLidasEsporte) > 2) {
                array_push($relacionadasEsporte, $maisLidasEsporte[2]);
            }
        }

        $partners = $em->getRepository('BackBundle:Editorial')->findBy(
            array('partner' => true),array('name' => 'ASC'),null,null
        );
        $queryPartners = $em->getRepository('BackBundle:Article')
            ->createQueryBuilder('a')
            ->where('a.editorial IN (:editorial)')
            ->setParameter(':editorial', $partners)
            ->andWhere('a.publishedAt <= :data')
            ->setParameter(':data', $today)
            ->setFirstResult(0)
            ->setMaxResults(3)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
        $partnerArticles = $queryPartners->getResult();

        $metrica = $em->getRepository('BackBundle:Metric')->findOneBy(
            array(),array('id' => 'ASC')
        );
        $acessowebsite = $metrica->getAcessowebsite();
        $acessowebsite = $acessowebsite + 1;
        $metrica->setAcessowebsite($acessowebsite);
        $em->persist($metrica);
        $em->flush();        
        
        return $this->render('BackBundle:Feature:preview.html.twig', array(
            'day' => $day,
            'month' => $month,
            'year' => $year,
            'dayOfWeek' => $dayOfWeek,
            'time' => $time,

            'slides' => $slides,
            'banners' => $banners,
            'coverFeatures' => $coverFeatures,
            'colunas' => $colunas,
            'blogs' => $blogs,
            'groupsDestaque' => $groupsDestaque,
            'cultures' => $cultures,
            'partnerArticles' => $partnerArticles,

            'principal1' => $principal1,
            'principal2' => $principal2,
            'secundario1' => $secundario1,
            'secundario2' => $secundario2,
            'secundario3ComFoto' => $secundario3ComFoto,
            'featureComFoto1' => $featureComFoto1,
            'featureComFoto2' => $featureComFoto2,
            'featureSemFoto1' => $featureSemFoto1,
            'featureSemFoto2' => $featureSemFoto2,

            'politicaPrincipal' => $politicaPrincipal,
            'politicaSecundario' => $politicaSecundario,
            'politicaTerciario1' => $politicaTerciario1,
            'politicaTerciario2' => $politicaTerciario2,
            'politicaFoto1' => $politicaFoto1,
            'politicaFoto2' => $politicaFoto2,
            'policitaFrase1' => $policitaFrase1,
            'policitaFrase2' => $policitaFrase2,
            'maisLidaPolitica' => $maisLidaPolitica,
            'relacionadasPolitica' => $relacionadasPolitica,

            'entretePrincipal' => $entretePrincipal,
            'entreteSlide' => $entreteSlide,
            'entreteFotoPequena' => $entreteFotoPequena,
            'entreteFrase' => $entreteFrase,
            'entreteFoto1' => $entreteFoto1,
            'entreteFoto2' => $entreteFoto2,
            'entreteFoto3' => $entreteFoto3,
            'maisLidaEntretenimento' => $maisLidaEntretenimento,
            'relacionadasEntretenimento' => $relacionadasEntretenimento,

            'esportePrincipal' => $esportePrincipal,
            'esporteSecundarioFoto' => $esporteSecundarioFoto,
            'esporteFotoHorizontal1' => $esporteFotoHorizontal1,
            'esporteFrase' => $esporteFrase,
            'esporteFotoHorizontal2' => $esporteFotoHorizontal2,
            'esporteFotoGrande' => $esporteFotoGrande,
            'esporteFotoPequena' => $esporteFotoPequena,
            'maisLidaEsporte' => $maisLidaEsporte,
            'relacionadasEsporte' => $relacionadasEsporte,

            'municipioPrincipal' => $municipioPrincipal,
            'municipioFoto1' => $municipioFoto1,
            'municipioFoto2' => $municipioFoto2,
            'municipioFrase1' => $municipioFrase1,
            'municipioFrase2' => $municipioFrase2,
            'municipioFotoHorizontal' => $municipioFotoHorizontal,
            'municipioFotoGrande' => $municipioFotoGrande,
            'municipioFraseCurta' => $municipioFraseCurta,

            'bannersTopoPagina' => $bannersTopoPagina,
            'bannersDestaque' => $bannersDestaque,
            'bannersNossosDestaques' => $bannersNossosDestaques,
            'bannersTopoEditoria1' => $bannersTopoEditoria1,
            'bannersEditoria1' => $bannersEditoria1,
            'bannersTopoEditoria2' => $bannersTopoEditoria2,
            'bannersEditoria2' => $bannersEditoria2,
            'bannersTopoEditoria3' => $bannersTopoEditoria3,
            'bannersEditoria3' => $bannersEditoria3,
            'bannersTopoEditoria5' => $bannersTopoEditoria5,
            'bannersEditoria5' => $bannersEditoria5,

            'bannerTopoPagina' => $bannerTopoPagina,
            'bannerDestaque' => $bannerDestaque,
            'bannerNossosDestaques' => $bannerNossosDestaques,
            'bannerTopoEditoria1' => $bannerTopoEditoria1,
            'bannerEditoria1' => $bannerEditoria1,
            'bannerTopoEditoria2' => $bannerTopoEditoria2,
            'bannerEditoria2' => $bannerEditoria2,
            'bannerTopoEditoria3' => $bannerTopoEditoria3,
            'bannerEditoria3' => $bannerEditoria3,
            'bannerTopoEditoria5' => $bannerTopoEditoria5,
            'bannerEditoria5' => $bannerEditoria5,

            'urgentes' => $urgentes
        ));
    }

}
