<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Culture;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Culture controller.
 *
 * @Route("/admin/culture")
 */
class CultureController extends Controller
{
    /**
     * Lists all culture entities.
     *
     * @Route("/index/", name="culture_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /*$cultures = $em->getRepository('BackBundle:Culture')->findAll();
        $hits = (count($cultures)/25);
        $cultures = $em->getRepository('BackBundle:Culture')->findBy(
          array(), array(), 25, 25 * ($page - 1)
        );*/

        $queryCulture = $em->getRepository('BackBundle:Culture')->createQueryBuilder('c');
        $queryCulture->orderBy('c.title', 'ASC');
        $query = $queryCulture->getQuery();
        $cultures = $query->getResult();
        $hits = (count($cultures) / 25);
        $count = count($cultures);
        $page = 1;

        if ($hits > 1) {
            $queryCulture->setFirstResult(25 * ($page - 1));
            $queryCulture->setMaxResults(25);
            $query = $queryCulture->getQuery();
            $cultures = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\CultureFilterType', NULL, array(
            'action' => $this->generateUrl('culture_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryCulture = $em->getRepository('BackBundle:Culture')->createQueryBuilder('c');

            $nome = $filterform['title']->getData();
            $entrou = false;
            if ($nome != null) {
                $queryCulture->where('c.title LIKE :title');
                $queryCulture->setParameter(':title', "%".$nome."%" );
                $entrou = true;
            }

            $datainicial = $filterform['startdate']->getData();
            if ($datainicial != null) {
                if ($entrou) {
                    $queryCulture->andWhere('c.date >= :startdate');
                } else {
                    $queryCulture->where('c.date >= :startdate');
                }
                $queryCulture->setParameter(':startdate', $datainicial);
                $entrou = true;
            }

            $datafinal = $filterform['enddate']->getData();
            if ($datafinal != null) {
                if ($entrou) {
                    $queryCulture->andWhere('c.date <= :enddate');
                } else {
                    $queryCulture->where('c.date <= :enddate');
                }
                $queryCulture->setParameter(':enddate', $datafinal);
                $entrou = true;
            }

            $status = $filterform['status']->getData();
            if ($status != 'Todos') {
                if($status == 'Aprovado'){
                    $status = true;
                } else {
                    $status = false;
                }
                if ($entrou) {
                    $queryCulture->andWhere('c.status = :status');
                } else {
                    $queryCulture->where('c.status = :status');
                }
                $queryCulture->setParameter(':status', $status);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryCulture->orderBy('c.title', 'ASC');
            $query = $queryCulture->getQuery();

            $cultures = $query->getResult();
            $hits = (count($cultures) / 25);
            $count = count($cultures);

            if ($hits > 1) {
                $queryCulture->setFirstResult(25 * ($page - 1));
                $queryCulture->setMaxResults(25);
                $query = $queryCulture->getQuery();
                $cultures = $query->getResult();
            }
        }
        
        return $this->render('BackBundle:Culture:index.html.twig', array(
            'cultures' => $cultures,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new culture entity.
     *
     * @Route("/new", name="culture_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $culture = new Culture();
        $form = $this->createForm('BackBundle\Form\CultureType', $culture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($culture);
            $em->flush();

            return $this->redirectToRoute('culture_show', array('id' => $culture->getId()));
        }

        return $this->render('BackBundle:Culture:new.html.twig', array(
            'culture' => $culture,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a culture entity.
     *
     * @Route("/{id}", name="culture_show")
     * @Method("GET")
     */
    public function showAction(Culture $culture)
    {
        $deleteForm = $this->createDeleteForm($culture);

        return $this->render('BackBundle:Culture:show.html.twig', array(
            'culture' => $culture,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing culture entity.
     *
     * @Route("/{id}/edit", name="culture_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Culture $culture)
    {
        $deleteForm = $this->createDeleteForm($culture);
        $editForm = $this->createForm('BackBundle\Form\CultureType', $culture);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('culture_edit', array('id' => $culture->getId()));
        }

        return $this->render('BackBundle:Culture:edit.html.twig', array(
            'culture' => $culture,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a culture entity.
     *
     * @Route("/{id}", name="culture_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Culture $culture)
    {
        $form = $this->createDeleteForm($culture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($culture);
            $em->flush();
        }

        return $this->redirectToRoute('culture_index');
    }

    /**
     * Creates a form to delete a culture entity.
     *
     * @param Culture $culture The culture entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Culture $culture)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('culture_delete', array('id' => $culture->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
