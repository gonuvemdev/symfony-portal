<?php

namespace BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use BackBundle\Form\RegistroType;
use BackBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class DefaultController extends Controller
{    

    /**
     * @Route("/admin", name="bem_vindo_short")
     * @Method({"GET", "POST"})
     */
    public function adminshortAction(Request $request)
    {
        return $this->redirectToRoute('bem_vindo');
    }

    /**
     * @Route("/admin/bemvindo", name="bem_vindo")
     * @Method({"GET", "POST"})
     */
    public function adminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        date_default_timezone_set('America/Fortaleza');

        $metrica = $em->getRepository('BackBundle:Metric')->findOneBy(
            array(),array('id' => 'ASC')
        );
        $acessowebsite = $metrica->getAcessowebsite();
        $acessoapp = $metrica->getAcessoapp();
        $acessogeral = $acessowebsite + $acessoapp;

        /*$queryOfertas = $em->getRepository('BackBundle:Oferta')->createQueryBuilder('o');
        $queryOfertas->where('o.statusoferta >= :statusoferta');
        $queryOfertas->setParameter('statusoferta', true);
        $queryOfertas->orderBy('o.id', 'DESC');
        $query = $queryOfertas->getQuery();
        $ofertas = $query->getResult();*/
        $ofertas = null;
        $count = count($ofertas);

        $dados = array();
        $geral = array();
        $geral['acesso'] = 0;
        $geral['emitidas'] = 0;
        $geral['pagas'] = 0;
        $geral['total'] = 0;
        $geral['ativas'] = 0;
        if (count($ofertas) > 0) {
            foreach ($ofertas as $oferta) {
                $queryEmitidas = $em->getRepository('BackBundle:Item')->createQueryBuilder('i');
                $queryEmitidas->where('i.oferta = :oferta');
                $queryEmitidas->setParameter(':oferta', $oferta);
                $queryEmitidas->orderBy('i.id', 'ASC');
                $queryE = $queryEmitidas->getQuery();
                $emitidas = $queryE->getResult();
                $countE = count($emitidas);

                $dados[$oferta->getId()]['emitidas'] = $countE;

                $queryEmitidas = $em->getRepository('BackBundle:Item')->createQueryBuilder('i');
                $queryEmitidas->innerJoin('i.venda', 'v', 'WITH', 'v.status = :status');
                $queryEmitidas->where('i.oferta = :oferta');
                $queryEmitidas->setParameter(':oferta', $oferta);
                $queryEmitidas->setParameter(':status', 'PAGO');
                $queryEmitidas->orderBy('i.id', 'ASC');
                $queryP = $queryEmitidas->getQuery();
                $pagas = $queryP->getResult();
                $countP = count($pagas);

                $dados[$oferta->getId()]['pagas'] = $countP;


                $dados[$oferta->getId()]['lucro'] = $countP * floatval($oferta->getComissionamento()) * floatval($oferta->getValorpromocional()) / 100;
                $dados[$oferta->getId()]['anunciante'] = $countP * (100 - floatval($oferta->getComissionamento())) * floatval($oferta->getValorpromocional()) / 100;
                $dados[$oferta->getId()]['lucrounitario'] = floatval($oferta->getComissionamento()) * floatval($oferta->getValorpromocional()) / 100;
                $dados[$oferta->getId()]['anuncianteunitario'] = (100 - floatval($oferta->getComissionamento())) * floatval($oferta->getValorpromocional()) / 100;

                if ($oferta->getMaximocompradores() != 0) {
                    $dados[$oferta->getId()]['desempenho'] = (100 * $countP) / $oferta->getMaximocompradores();
                } else {
                    $dados[$oferta->getId()]['desempenho'] = 0;
                }

                $geral['acesso'] = $geral['acesso'] + $oferta->getAcesso();
                $geral['emitidas'] = $geral['emitidas'] + $countE;
                $geral['pagas'] = $geral['pagas'] + $countP;
                $geral['total'] = $geral['total'] + $dados[$oferta->getId()]['lucro'];
                if ($oferta->getStatusoferta() == 1) {
                    $geral['ativas'] = $geral['ativas'] + 1;
                }

            }
        }

        $pushes = $em->getRepository('BackBundle:Push')->findAll();
        $geral['pushes'] = count($pushes);
        $queryClientes = $em->getRepository('BackBundle:User')->createQueryBuilder('c');
        $queryClientes->where('c.company IS NOT NULL');
        $queryClientes->andWhere('c.tipousuario = :tipousuario');
        $queryClientes->setParameter(':tipousuario', 'Comum');
        $queryClientes->orderBy('c.nome', 'ASC');
        $query = $queryClientes->getQuery();
        $clientes = $query->getResult();
        $geral['clientes'] = count($clientes);
        $companys = $em->getRepository('BackBundle:Company')->findAll();
        $geral['companys'] = count($companys);
        $installs = $em->getRepository('BackBundle:Installation')->findAll();
        $geral['installs'] = count($installs);
        $queryClientes = $em->getRepository('BackBundle:User')->createQueryBuilder('c');
        $queryClientes->where('c.company IS NULL');
        $queryClientes->andWhere('c.tipousuario = :tipousuario');
        $queryClientes->setParameter(':tipousuario', 'Comum');
        $queryClientes->orderBy('c.nome', 'ASC');
        $query = $queryClientes->getQuery();
        $apps = $query->getResult();
        $geral['usersapp'] = count($apps);

        // Pegando a notícia mais lida do dia anterior
        $yesterday = new \DateTime(date('Y-m-d H:i:s', strtotime('-1 day')));
        $yesterdayMostReadArticle = $this->get('stats_service')->mostReadByDay($yesterday);

        if(count($yesterdayMostReadArticle)){
            $yesterdayMostReadArticle = $yesterdayMostReadArticle[0];
        } else {
            $yesterdayMostReadArticle = null;
        }

        return $this->render('BackBundle:Default:index.html.twig', array(
            'dados' => $dados,
            'count' => $count,
            'ofertas' => $ofertas,
            'geral' => $geral,
            'acessowebsite' => $acessowebsite,
            'acessoapp' => $acessoapp,
            'acessogeral' => $acessogeral,
            'yesterdayMostReadArticle' => $yesterdayMostReadArticle
        ));
    }


    /**
     * @Route("/esqueci", name="esqueci_senha")
     */
    public function esqueciAction(Request $request)
    {
        $session = $request->getSession();
        $mensagem = $session->get('esqueciMensagem', 'null');

        $session->remove('esqueciMensagem');

        return $this->render('front/esqueci.html.twig', array(
            'mensagem' => $mensagem
        ));
    }

    /**
     * Request reset user password: submit form and send email
     *
     * @Route("/esquecisenha", name="usuario_forgot")
     */
    public function esqueciSenhaAction(Request $request)
    {
        $username = $request->get('email');
        $session = $request->getSession();

        /**
         * @var $user UserInterface
         */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            /*return $this->render('FOSUserBundle:Resetting:request.html.twig', array(
                'invalid_username' => $username
            ));*/
            //return new JsonResponse(['tipo' => 'error', 'mensagem' => 'E-mail ou usuário inválido!']);
            $session->set('esqueciMensagem', 'E-mail ou usuário inválido!');
            return $this->redirect($this->generateUrl('esqueci_senha'));
        }

        /*if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->render('FOSUserBundle:Resetting:password_already_requested.html.twig');
        }

        if (null === $user->getConfirmationToken()) {*/
        /**
         * @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface
         */
        $tokenGenerator = $this->get('fos_user.util.token_generator');
        $user->setConfirmationToken($tokenGenerator->generateToken());
        /* }*/

        $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);

        //return new JsonResponse(['tipo' => 'success', 'mensagem' => 'Confira seu email']);
        $session->set('esqueciMensagem', 'Confira seu email!');
        return $this->redirect($this->generateUrl('esqueci_senha'));
    }


    /**
     * Creates a new User entity.
     *
     * @Route("/registro", name="registro")
     * @Method({"GET", "POST"})
     */
    public function registroAction(Request $request)
    {
        $usuario = new User();
        $form = $this->createForm('BackBundle\Form\RegistroType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $userFinder = $em->getRepository('BackBundle:User')->findBy(
                array('username' => $form->get('username')->getData())
            );

            if ($userFinder != null){
                $this->get('session')->getFlashBag()->set('registerError', null);
                $this->get('session')->getFlashBag()->set('registerError', '** Nome de usuário já cadastrado! Tente outro por favor.');
                return $this->render('front/registro.html.twig', array(
                    'form'   => $form->createView()
                ));
            }

            $userFinder = $em->getRepository('BackBundle:User')->findBy(
                array('email' => $form->get('email')->getData())
            );

            if ($userFinder != null){
                $this->get('session')->getFlashBag()->set('registerError', null);
                $this->get('session')->getFlashBag()->set('registerError', '** E-mail já cadastrado! Tente outro por favor.');
                return $this->render('front/registro.html.twig', array(
                    'form'   => $form->createView()
                ));
            }

            $userManager = $this->container->get('fos_user.user_manager');
            $newUser = $userManager->createUser();
            $newUser->setUsername($form->get('username')->getData());
            $newUser->setUsernameCanonical($form->get('username')->getData());
            $encoder_service = $this->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($newUser);
            $password = $encoder->encodePassword($form->get('password')->getData(), $newUser->getSalt());
            $newUser->setPassword($password);
            $newUser->setEmail($form->get('email')->getData());
            $newUser->setEnabled(true);
            $userManager->updateUser($newUser);

            $tipousuario = 'Usuário';
            $nome = $form->get('nome')->getData();
            $cpf = $form->get('cpf')->getData();
            $rg = $form->get('rg')->getData();
            $orgaoemissor = $form->get('orgaoemissor')->getData();
            $telefone1 = $form->get('telefone1')->getData();
            $telefone2 = $form->get('telefone2')->getData();
            $telefone3 = $form->get('telefone3')->getData();
            $endereco = $form->get('endereco')->getData();
            $numero = $form->get('numero')->getData();
            $bairro = $form->get('bairro')->getData();
            $cidade = $form->get('cidade')->getData();
            $estado = $form->get('estado')->getData();
            $cep = $form->get('cep')->getData();
            $pais = "Brasil";


            $user = $em->getRepository('BackBundle:User')->find($newUser->getId());
            $user->setTipoUsuario($tipousuario);
            $user->setNome($nome);
            $user->setCpf($cpf);
            $user->setRg($rg);
            $user->setOrgaoEmissor($orgaoemissor);
            $user->setTelefone1($telefone1);
            $user->setTelefone2($telefone2);
            $user->setTelefone3($telefone3);
            $user->setEndereco($endereco);
            $user->setNumero($numero);
            $user->setBairro($bairro);
            $user->setCidade($cidade);
            $user->setEstado($estado);
            $user->setPais($pais);
            $user->setCep($cep);
            $newUser->addRole("ROLE_USER");
            $em->persist($user);
            $em->flush();

            // Here, "public" is the name of the firewall in your security.yml
            $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
            /*$this->get("security.context")->setToken($token);*/
            $this->get('security.token_storage')->setToken($token);

            // Fire the login event
            // Logging the user in above the way we do it doesn't do this automatically
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

            return $this->redirect($this->generateUrl('minha_conta'));
        }

        return $this->render('front/registro.html.twig', array(
            'form'   => $form->createView()
        ));
    }

    /**
     * @Route("/user/minhaconta", name="minha_conta")
     * @Method({"GET", "POST"})
     */
    public function minhacontaAction()
    {
        return $this->redirect($this->generateUrl('minhas_compras'));
    }

    /**
     * @Route("/user/minhaconta/minhascompras", name="minhas_compras")
     * @Method({"GET", "POST"})
     */
    public function minhascomprasAction()
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $compras = $em->getRepository('BackBundle:Venda')->findBy(
            array('usuario' => $user), array('data' => 'DESC'), null, null
        );

        return $this->render('front/minhascompras.html.twig', array(
            'compras' => $compras
        ));
    }


    /**
     * @Route("/user/minhaconta/meuperfil", name="meu_perfil")
     * @Method({"GET", "POST"})
     */
    public function meuperfilAction(Request $request)
    {
        $usuario = $this->getUser();
        $form = $this->createForm('BackBundle\Form\PerfilType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $nome = $form->get('nome')->getData();
            $cpf = $form->get('cpf')->getData();
            $rg = $form->get('rg')->getData();
            $orgaoemissor = $form->get('orgaoemissor')->getData();
            $telefone1 = $form->get('telefone1')->getData();
            $telefone2 = $form->get('telefone2')->getData();
            $telefone3 = $form->get('telefone3')->getData();
            $endereco = $form->get('endereco')->getData();
            $numero = $form->get('numero')->getData();
            $bairro = $form->get('bairro')->getData();
            $cidade = $form->get('cidade')->getData();
            $estado = $form->get('estado')->getData();
            $cep = $form->get('cep')->getData();
            $pais = "Brasil";

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('BackBundle:User')->find($usuario->getId());
            $user->setNome($nome);
            $user->setCpf($cpf);
            $user->setRg($rg);
            $user->setOrgaoEmissor($orgaoemissor);
            $user->setTelefone1($telefone1);
            $user->setTelefone2($telefone2);
            $user->setTelefone3($telefone3);
            $user->setEndereco($endereco);
            $user->setNumero($numero);
            $user->setBairro($bairro);
            $user->setCidade($cidade);
            $user->setEstado($estado);
            $user->setPais($pais);
            $user->setCep($cep);
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('meu_perfil'));

        }

        return $this->render('front/meuperfil.html.twig', array(
            'form'   => $form->createView(),
            'usuario' => $usuario
        ));
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/user/minhaconta/senha", name="senha")
     * @Method({"GET", "POST"})
     */
    public function senhaAction(Request $request)
    {
        $usuario = $this->getUser();
        $form = $this->createForm('BackBundle\Form\SenhaType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userManager = $this->container->get('fos_user.user_manager');
            $newUser = $userManager->findUserByUsername($usuario->getUsername());
            if ($form->get('password')->getData() != '0') {
                $encoder_service = $this->get('security.encoder_factory');
                $encoder = $encoder_service->getEncoder($newUser);
                $password = $encoder->encodePassword($form->get('password')->getData(), $newUser->getSalt());
                $newUser->setPassword($password);
            }
            $userManager->updateUser($newUser);

            return $this->redirect($this->generateUrl('senha'));
        }

        return $this->render('front/senha.html.twig', array(
            'form'   => $form->createView(),
            'usuario' => $usuario
        ));
    }

    /**
     * @Route("/admin/info", name="phpinfo")
     * @Method({"GET", "POST"})
     */
    public function infoAction(Request $request)
    {
        ob_start();
        phpinfo();
        $phpinfo = ob_get_clean();

        var_dump($phpinfo);
    }

    /**
     * @Route("/buscacep", name="busca_cep")
     * @Method({"GET", "POST"})
     */
    public function busca_cep(Request $request)
    {

        return '';
    }

     /**
     * Crop NewsGroup images
     *
     * @Route("/admin/cortarimagem/{id}/{imagem}", name="corta_imagem_newsgroup")
     * @Method({"GET", "POST"})
     */
    public function cortaNewsGroupAction(Request $request, $id, $imagem)
    {
        $em = $this->getDoctrine()->getManager();
        $newsgroup = $em->getRepository('BackBundle:NewsGroup')->findOneById($id);

        if ($request->isMethod('POST')) {
            $targ_w = 600;
            $targ_h = 300;
            $jpeg_quality = 100;

            if ($imagem == 1) { $src = $this->get('kernel')->getRootDir().'/../web/arquivos/' . $newsgroup->getImageName(); }
            if ($imagem == 2) { 
                $src = $this->get('kernel')->getRootDir().'/../web/arquivos/' . $newsgroup->getImageName2();
                $targ_h = 600;
            }
            $img_r = $this->imagecreatefromfile($src);
            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

            imagecopyresampled($dst_r,$img_r,0,0,$_POST['cropx'],$_POST['cropy'],
                $targ_w,$targ_h,$_POST['cropw'],$_POST['croph']);


            if ($imagem == 1) {  $croppedfile = $this->get('kernel')->getRootDir().'/../web/arquivos/' .  $newsgroup->getImageName(); }
            if ($imagem == 2) {  $croppedfile = $this->get('kernel')->getRootDir().'/../web/arquivos/' .  $newsgroup->getImageName2(); }
            imagejpeg($dst_r, $croppedfile , $jpeg_quality);

            return $this->redirectToRoute('newsgroup_show', array('id' => $newsgroup->getId()));
        }

        return $this->render('BackBundle:Default:cortar.html.twig', array(
            'newsgroup' => $newsgroup,
            'imagem' => $imagem
        ));
    }

    function imagecreatefromfile( $filename ) {
        if (!file_exists($filename)) {
            throw new \InvalidArgumentException('File "'.$filename.'" not found.');
        }
        switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
            case 'jpeg':
            case 'jpeg-filtered':
            case 'jpg-filtered':
            case 'jpg':
                return imagecreatefromjpeg($filename);
                break;

            case 'png-filtered':
            case 'png':
                //return imagecreatefrompng($filename);
                return imagecreatefromstring(file_get_contents($filename));
                break;

            case 'gif-filtered':
            case 'gif':
                return imagecreatefromstring(file_get_contents($filename));
                break;

            default:
                throw new \InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
                break;
        }
    }

    function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
        // creating a cut resource
        $cut = imagecreatetruecolor($src_w, $src_h);
        // copying relevant section from background to the cut resource
        imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
        // copying relevant section from watermark to the cut resource
        imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);
        // insert cut resource to destination image
        imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
    }
}
