<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Location controller.
 *
 * @Route("/admin/location")
 */
class LocationController extends Controller
{
    /**
     * Lists all location entitiel.
     *
     * @Route("/index/", name="location_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /*$locations = $em->getRepository('BackBundle:Location')->findAll();
        $hits = (count($locations)/25);
        $locations = $em->getRepository('BackBundle:Location')->findBy(
            array(),array(), 25, 25 * ($page - 1)
        );*/

        $queryLocation = $em->getRepository('BackBundle:Location')->createQueryBuilder('l');
        $queryLocation->orderBy('l.description', 'ASC');
        $query = $queryLocation->getQuery();
        $locations = $query->getResult();
        $hits = (count($locations) / 25);
        $count = count($locations);
        $page = 1;

        if ($hits > 1) {
            $queryLocation->setFirstResult(25 * ($page - 1));
            $queryLocation->setMaxResults(25);
            $query = $queryLocation->getQuery();
            $locations = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\LocationFilterType', NULL, array(
            'action' => $this->generateUrl('location_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryLocation = $em->getRepository('BackBundle:Location')->createQueryBuilder('s');

            $descricao = $filterform['description']->getData();
            $entrou = false;
            if ($descricao != null) {
                $queryLocation->where('s.description LIKE :description');
                $queryLocation->setParameter(':description', '%'.$descricao.'%' );
                $entrou = true;
            }

            $tipo = $filterform['type']->getData();
            if($tipo != 'Todos'){
                if ($tipo != null) {
                    if($entrou){
                        $queryLocation->andWhere('s.type LIKE :type');
                    } else {
                        $queryLocation->where('s.type LIKE :type');
                    }
                    $queryLocation->setParameter(':type', '%'.$tipo.'%' );
                    $entrou = true;
                }
            }


            $newsgroup = $filterform['newsgroup']->getData();
            if ($newsgroup != null) {
                if($entrou){
                    $queryLocation->andWhere('s.newsgroup = :newsgroup');
                } else {
                    $queryLocation->where('s.newsgroup = :newsgroup');
                }
                $queryLocation->setParameter(':newsgroup', $newsgroup );
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryLocation->orderBy('s.description', 'ASC');
            $query = $queryLocation->getQuery();

            $locations = $query->getResult();
            $hits = (count($locations) / 25);
            $count = count($locations);

            if ($hits > 1) {
                $queryLocation->setFirstResult(25 * ($page - 1));
                $queryLocation->setMaxResults(25);
                $query = $queryLocation->getQuery();

                $locations = $query->getResult();
            }
        }
        
        return $this->render('BackBundle:Location:index.html.twig', array(
            'locations' => $locations,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new location entity.
     *
     * @Route("/new", name="location_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $location = new Location();
        $form = $this->createForm('BackBundle\Form\LocationType', $location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($location);
            $em->flush();

            return $this->redirectToRoute('location_show', array('id' => $location->getId()));
        }

        return $this->render('BackBundle:Location:new.html.twig', array(
            'location' => $location,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a location entity.
     *
     * @Route("/{id}", name="location_show")
     * @Method("GET")
     */
    public function showAction(Location $location)
    {
        $deleteForm = $this->createDeleteForm($location);

        return $this->render('BackBundle:Location:show.html.twig', array(
            'location' => $location,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing location entity.
     *
     * @Route("/{id}/edit", name="location_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Location $location)
    {
        $deleteForm = $this->createDeleteForm($location);
        $editForm = $this->createForm('BackBundle\Form\LocationType', $location);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('location_edit', array('id' => $location->getId()));
        }

        return $this->render('BackBundle:Location:edit.html.twig', array(
            'location' => $location,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a location entity.
     *
     * @Route("/{id}", name="location_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Location $location)
    {
        $form = $this->createDeleteForm($location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($location);
            $em->flush();
        }

        return $this->redirectToRoute('location_index');
    }

    /**
     * Creates a form to delete a location entity.
     *
     * @param Location $location The location entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Location $location)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('location_delete', array('id' => $location->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
