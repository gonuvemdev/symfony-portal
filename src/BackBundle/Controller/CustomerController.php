<?php

namespace BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BackBundle\Entity\Customer;
use BackBundle\Form\CustomerType;

/**
 * Customer controller.
 *
 * @Route("/admin/customer")
 */
class CustomerController extends Controller
{
    /**
     * Lists all Customer entities.
     *
     * @Route("/index/", name="customer_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /*$customers = $em->getRepository('BackBundle:Customer')->findAll();
        $hits = (count($customers) / 25);
        $customers = $em->getRepository('BackBundle:Customer')->findBy(
            array(),array('description' => 'ASC'),25,25 * ($page - 1)
        );*/

        $queryCustomer = $em->getRepository('BackBundle:Customer')->createQueryBuilder('c');
        $queryCustomer->orderBy('c.description', 'ASC');
        $query = $queryCustomer->getQuery();
        $customers = $query->getResult();
        $hits = (count($customers) / 25);
        $count = count($customers);
        $page = 1;

        if ($hits > 1) {
            $queryCustomer->setFirstResult(25 * ($page - 1));
            $queryCustomer->setMaxResults(25);
            $query = $queryCustomer->getQuery();
            $customers = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\CustomerFilterType', NULL, array(
            'action' => $this->generateUrl('customer_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryCustomer = $em->getRepository('BackBundle:Customer')->createQueryBuilder('c');

            $nome = $filterform['description']->getData();
            $entrou = false;
            if ($nome != null) {
                $queryCustomer->where('c.description LIKE :description');
                $queryCustomer->setParameter(':description', "%".$nome."%" );
                $entrou = true;
            }

            $cidade = $filterform['city']->getData();
            if ($cidade != null) {
                if ($entrou) {
                    $queryCustomer->andWhere('c.city LIKE :city');
                } else {
                    $queryCustomer->where('c.city LIKE :city');
                }
                $queryCustomer->setParameter(':city', '%'.$cidade.'%');
                $entrou = true;
            }

            $estado = $filterform['state']->getData();
            if ($estado != null) {
                if ($entrou) {
                    $queryCustomer->andWhere('c.state = :state');
                } else {
                    $queryCustomer->where('c.state = :state');
                }
                $queryCustomer->setParameter(':state', $estado);
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryCustomer->orderBy('c.description', 'ASC');
            $query = $queryCustomer->getQuery();

            $customers = $query->getResult();
            $hits = (count($customers) / 25);
            $count = count($customers);

            if ($hits > 1) {
                $queryCustomer->setFirstResult(25 * ($page - 1));
                $queryCustomer->setMaxResults(25);
                $query = $queryCustomer->getQuery();
                $customers = $query->getResult();
            }
        }

        return $this->render('BackBundle:Customer:index.html.twig', array(
            'customers' => $customers,
            'hits' => ceil( $hits ),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new Customer entity.
     *
     * @Route("/new", name="customer_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $customer = new Customer();
        $form = $this->createForm('BackBundle\Form\CustomerType', $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();

            return $this->redirectToRoute('customer_show', array('id' => $customer->getId()));
        }

        return $this->render('BackBundle:Customer:new.html.twig', array(
            'customer' => $customer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Customer entity.
     *
     * @Route("/{id}", name="customer_show")
     * @Method("GET")
     */
    public function showAction(Customer $customer)
    {
        $deleteForm = $this->createDeleteForm($customer);

        return $this->render('BackBundle:Customer:show.html.twig', array(
            'customer' => $customer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Customer entity.
     *
     * @Route("/{id}/edit", name="customer_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Customer $customer)
    {
        $deleteForm = $this->createDeleteForm($customer);
        $editForm = $this->createForm('BackBundle\Form\CustomerType', $customer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();

            return $this->redirectToRoute('customer_edit', array('id' => $customer->getId()));
        }

        return $this->render('BackBundle:Customer:edit.html.twig', array(
            'customer' => $customer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Customer entity.
     *
     * @Route("/{id}", name="customer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Customer $customer)
    {
        $form = $this->createDeleteForm($customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($customer);
            $em->flush();
        }

        return $this->redirectToRoute('customer_index');
    }

    /**
     * Creates a form to delete a Customer entity.
     *
     * @param Customer $customer The Customer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Customer $customer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('customer_delete', array('id' => $customer->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
