<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Advertising;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Advertising controller.
 *
 * @Route("/admin/veiculacao")
 */
class AdvertisingController extends Controller
{
    /**
     * @Route("/index/", name="advertising_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $queryAdvertising = $em->getRepository('BackBundle:Advertising')->createQueryBuilder('a');
        $queryAdvertising->orderBy('a.date', 'DESC');
        $query = $queryAdvertising->getQuery();
        $ads = $query->getResult();
        $hits = (count($ads) / 25);
        $count = count($ads);
        $page = 1;

        if ($hits > 1) {
            $queryAdvertising->setFirstResult(25 * ($page - 1));
            $queryAdvertising->setMaxResults(25);
            $query = $queryAdvertising->getQuery();
            $ads = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\AdvertisingFilterType', NULL, array(
            'action' => $this->generateUrl('advertising_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $queryAdvertising = $em->getRepository('BackBundle:Advertising')->createQueryBuilder('a');


            $datainicial = $filterform['startdate']->getData();
            $entrou = false;
            if ($datainicial != null) {
                if ($entrou) {
                    $queryAdvertising->andWhere('a.date >= :startdate');
                } else {
                    $queryAdvertising->where('a.date >= :startdate');
                }
                $queryAdvertising->setParameter(':startdate', $datainicial);
                $entrou = true;
            }

            $datafinal = $filterform['enddate']->getData();
            if ($datafinal != null) {
                if ($entrou) {
                    $queryAdvertising->andWhere('a.date <= :enddate');
                } else {
                    $queryAdvertising->where('a.date <= :enddate');
                }
                $queryAdvertising->setParameter(':enddate', $datafinal);
                $entrou = true;
            }

            $banner = $filterform['banner']->getData();
            if ($banner != null) {
                if ($entrou) {
                    $queryAdvertising->andWhere('a.banner = :banner');
                } else {
                    $queryAdvertising->where('a.banner = :banner');
                }
                $queryAdvertising->setParameter(':banner', $banner);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $queryAdvertising->orderBy('a.date', 'DESC');
            $query = $queryAdvertising->getQuery();

            $ads = $query->getResult();
            $hits = (count($ads) / 25);
            $count = count($ads);

            if ($hits > 1) {
                $queryAdvertising->setFirstResult(25 * ($page - 1));
                $queryAdvertising->setMaxResults(25);
                $query = $queryAdvertising->getQuery();
                $ads = $query->getResult();
            }
        }

        return $this->render('BackBundle:Advertising:index.html.twig', array(
            'ads' => $ads,
            'hits' => $hits,
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new ad entity.
     *
     * @Route("/new", name="advertising_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ads = new Advertising();
        $form = $this->createForm('BackBundle\Form\AdvertisingType', $ads);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ads);
            $em->flush();

            return $this->redirectToRoute('advertising_show', array('id' => $ads->getId()));
        }

        return $this->render('BackBundle:Advertising:new.html.twig', array(
            'ad' => $ads,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ad entity.
     *
     * @Route("/{id}", name="advertising_show")
     * @Method("GET")
     */
    public function showAction(Advertising $ads)
    {
        $deleteForm = $this->createDeleteForm($ads);

        return $this->render('BackBundle:Advertising:show.html.twig', array(
            'ad' => $ads,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing advertising entity.
     *
     * @Route("/{id}/edit", name="advertising_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Advertising $ads)
    {
        $deleteForm = $this->createDeleteForm($ads);
        $editForm = $this->createForm('BackBundle\Form\AdvertisingType', $ads);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('advertising_edit', array('id' => $ads->getId()));
        }

        return $this->render('BackBundle:Advertising:edit.html.twig', array(
            'ad' => $ads,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes an advertising entity.
     *
     * @Route("/{id}", name="advertising_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Advertising $ads)
    {
        $form = $this->createDeleteForm($ads);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ads);
            $em->flush();
        }

        return $this->redirectToRoute('advertising_index');
    }

    /**
     * Creates a form to delete an advertising entity.
     *
     * @param Advertising $ads The ad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Advertising $ads)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('advertising_delete', array('id' => $ads->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
