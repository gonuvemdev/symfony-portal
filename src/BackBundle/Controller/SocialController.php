<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Social;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Socialgroup controller.
 *
 * @Route("/admin/social")
 */
class SocialController extends Controller
{
    /**
     * Lists all social entities.
     *
     * @Route("/index/", name="social_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $querySocial = $em->getRepository('BackBundle:Social')->createQueryBuilder('s');
        $querySocial->orderBy('s.description', 'ASC');
        $query = $querySocial->getQuery();
        $socials = $query->getResult();
        $hits = (count($socials) / 25);
        $count = count($socials);
        $page = 1;

        if ($hits > 1) {
            $querySocial->setFirstResult(25 * ($page - 1));
            $querySocial->setMaxResults(25);
            $query = $querySocial->getQuery();
            $socials = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\SocialFilterType', NULL, array(
            'action' => $this->generateUrl('social_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $querySocial = $em->getRepository('BackBundle:Social')->createQueryBuilder('s');

            $name = $filterform['description']->getData();
            $entrou = false;
            if ($name != null) {
                $querySocial->where('s.description LIKE :description');
                $querySocial->setParameter(':description', '%'.$name.'%' );
                $entrou = true;
            }
            
            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $querySocial->orderBy('s.description', 'ASC');
            $query = $querySocial->getQuery();

            $socials = $query->getResult();
            $hits = (count($socials) / 25);
            $count = count($socials);

            if ($hits > 1) {
                $querySocial->setFirstResult(25 * ($page - 1));
                $querySocial->setMaxResults(25);
                $query = $querySocial->getQuery();

                $socials = $query->getResult();
            }
        }

        /*$socials = $em->getRepository('BackBundle:Social')->findAll();
        $hits = (count($socials)/25);
        $socials = $em->getRepository('BackBundle:Social')->findBy(
            array(),array(), 25, 25 * ($page - 1)
        );*/

        return $this->render('BackBundle:Social:index.html.twig', array(
            'socials' => $socials,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new social entity.
     *
     * @Route("/new", name="social_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $social = new Social();
        $form = $this->createForm('BackBundle\Form\SocialType', $social);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($social);
            $em->flush();

            return $this->redirectToRoute('social_show', array('id' => $social->getId()));
        }

        return $this->render('BackBundle:Social:new.html.twig', array(
            'social' => $social,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a social entity.
     *
     * @Route("/{id}", name="social_show")
     * @Method("GET")
     */
    public function showAction(Social $social)
    {
        $deleteForm = $this->createDeleteForm($social);

        return $this->render('BackBundle:Social:show.html.twig', array(
            'social' => $social,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing social entity.
     *
     * @Route("/{id}/edit", name="social_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Social $social)
    {
        $deleteForm = $this->createDeleteForm($social);
        $editForm = $this->createForm('BackBundle\Form\SocialType', $social);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('social_edit', array('id' => $social->getId()));
        }

        return $this->render('BackBundle:Social:edit.html.twig', array(
            'social' => $social,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a social entity.
     *
     * @Route("/{id}", name="social_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Social $social)
    {
        $form = $this->createDeleteForm($social);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($social);
            $em->flush();
        }

        return $this->redirectToRoute('social_index');
    }

    /**
     * Creates a form to delete a social entity.
     *
     * @param Social $social The social entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Social $social)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('social_delete', array('id' => $social->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
