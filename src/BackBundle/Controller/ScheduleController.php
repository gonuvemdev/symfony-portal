<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Schedule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Schedule controller.
 *
 * @Route("/admin/schedule")
 */
class ScheduleController extends Controller
{
    /**
     * Lists all schedule entities.
     *
     * @Route("/index/", name="schedule_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

//        $schedules = $em->getRepository('BackBundle:Schedule')->findAll();
//        $hits = (count($schedules)/25);
//        $schedules = $em->getRepository('BackBundle:Schedule')->findBy(
//            array(),array(), 25, 25 * ($page - 1)
//        );

        $querySchedule = $em->getRepository('BackBundle:Schedule')->createQueryBuilder('s');
        $querySchedule->orderBy('s.scheduledTo', 'ASC');
        $query = $querySchedule->getQuery();
        $schedules = $query->getResult();
        $hits = (count($schedules) / 25);
        $count = count($schedules);
        $page = 1;

        if ($hits > 1) {
            $querySchedule->setFirstResult(25 * ($page - 1));
            $querySchedule->setMaxResults(25);
            $query = $querySchedule->getQuery();
            $schedules = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\ScheduleFilterType', NULL, array(
            'action' => $this->generateUrl('schedule_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $querySchedule = $em->getRepository('BackBundle:Schedule')->createQueryBuilder('s');

            $status = $filterform['status']->getData();
            $entrou = false;
            if ($status != 'Todos') {
                $querySchedule->where('s.status = :status');
                $querySchedule->setParameter(':status', $status );
                $entrou = true;
            }

            $datainicial = $filterform['startdate']->getData();
            if ($datainicial != null) {
                if ($entrou) {
                    $querySchedule->andWhere('s.scheduledTo >= :startdate');
                } else {
                    $querySchedule->where('s.scheduledTo >= :startdate');
                }
                $querySchedule->setParameter(':startdate', $datainicial);
                $entrou = true;
            }

            $datafinal = $filterform['enddate']->getData();
            if ($datafinal != null) {
                if ($entrou) {
                    $querySchedule->andWhere('s.scheduledTo <= :enddate');
                } else {
                    $querySchedule->where('s.scheduledTo <= :enddate');
                }
                $querySchedule->setParameter(':enddate', $datafinal);
                $entrou = true;
            }

            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $querySchedule->orderBy('s.scheduledTo', 'ASC');
            $query = $querySchedule->getQuery();

            $schedules = $query->getResult();
            $hits = (count($schedules) / 25);
            $count = count($schedules);

            if ($hits > 1) {
                $querySchedule->setFirstResult(25 * ($page - 1));
                $querySchedule->setMaxResults(25);
                $query = $querySchedule->getQuery();
                $schedules = $query->getResult();
            }
        }
        
        return $this->render('BackBundle:Schedule:index.html.twig', array(
            'schedules' => $schedules,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));
    }

    /**
     * Creates a new schedule entity.
     *
     * @Route("/new", name="schedule_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $schedule = new Schedule();
        $form = $this->createForm('BackBundle\Form\ScheduleType', $schedule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($schedule);
            $em->flush();

            return $this->redirectToRoute('schedule_show', array('id' => $schedule->getId()));
        }

        return $this->render('BackBundle:Schedule:new.html.twig', array(
            'schedule' => $schedule,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a schedule entity.
     *
     * @Route("/{id}", name="schedule_show")
     * @Method("GET")
     */
    public function showAction(Schedule $schedule)
    {
        $deleteForm = $this->createDeleteForm($schedule);

        return $this->render('BackBundle:Schedule:show.html.twig', array(
            'schedule' => $schedule,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing schedule entity.
     *
     * @Route("/{id}/edit", name="schedule_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Schedule $schedule)
    {
        $deleteForm = $this->createDeleteForm($schedule);
        $editForm = $this->createForm('BackBundle\Form\ScheduleType', $schedule);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('schedule_edit', array('id' => $schedule->getId()));
        }

        return $this->render('BackBundle:Schedule:edit.html.twig', array(
            'schedule' => $schedule,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a schedule entity.
     *
     * @Route("/{id}", name="schedule_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Schedule $schedule)
    {
        $form = $this->createDeleteForm($schedule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($schedule);
            $em->flush();
        }

        return $this->redirectToRoute('schedule_index');
    }

    /**
     * Creates a form to delete a schedule entity.
     *
     * @param Schedule $schedule The schedule entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Schedule $schedule)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('schedule_delete', array('id' => $schedule->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
