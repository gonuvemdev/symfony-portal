<?php

namespace BackBundle\Controller;

use BackBundle\Entity\SocialGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Socialgroup controller.
 *
 * @Route("/admin/socialgroup")
 */
class SocialGroupController extends Controller
{
    /**
     * Lists all socialGroup entities.
     *
     * @Route("/index/", name="socialgroup_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $querySocialGroups = $em->getRepository('BackBundle:SocialGroup')->createQueryBuilder('s');
        $querySocialGroups->orderBy('s.newsgroup', 'ASC');
        $query = $querySocialGroups->getQuery();
        $socialgroups = $query->getResult();
        $hits = (count($socialgroups) / 25);
        $count = count($socialgroups);
        $page = 1;

        if ($hits > 1) {
            $querySocialGroups->setFirstResult(25 * ($page - 1));
            $querySocialGroups->setMaxResults(25);
            $query = $querySocialGroups->getQuery();
            $socialgroups = $query->getResult();
        }

        $filterform = $this->createForm('BackBundle\Filter\SocialGroupFilterType', NULL, array(
            'action' => $this->generateUrl('socialgroup_index'),
            'method' => 'POST'
        ));
        $filterform->handleRequest($request);

        if ($filterform->isSubmitted() && $filterform->isValid()) {

            $querySocialGroups = $em->getRepository('BackBundle:SocialGroup')->createQueryBuilder('s');

            $newsgroup = $filterform['newsgroup']->getData();
            $entrou = false;
            if ($newsgroup != null) {
                $querySocialGroups->where('s.newsgroup = :newsgroup');
                $querySocialGroups->setParameter(':newsgroup', $newsgroup );
                $entrou = true;
            }

            $social = $filterform['social']->getData();

            if ($social != null) {
                if($entrou){
                    $querySocialGroups->andWhere('s.social = :social');
                } else {
                    $querySocialGroups->where('s.social = :social');
                }
                $querySocialGroups->setParameter(':social', $social );
                $entrou = true;
            }
            
            $pagina = $filterform['page']->getData();
            if ($pagina != null) {
                $page = $pagina;
            }

            $querySocialGroups->orderBy('s.newsgroup', 'ASC');
            $query = $querySocialGroups->getQuery();

            $socialgroups = $query->getResult();
            $hits = (count($socialgroups) / 25);
            $count = count($socialgroups);

            if ($hits > 1) {
                $querySocialGroups->setFirstResult(25 * ($page - 1));
                $querySocialGroups->setMaxResults(25);
                $query = $querySocialGroups->getQuery();

                $socialgroups = $query->getResult();
            }
        }
        
        /*$socialGroups = $em->getRepository('BackBundle:SocialGroup')->findAll();
        $hits = (count($socialGroups)/25);
        $socialGroups = $em->getRepository('BackBundle:SocialGroup')->findBy(
            array(),array(), 25, 25 * ($page - 1)
        );*/

        return $this->render('BackBundle:Socialgroup:index.html.twig', array(
            'socialgroups' => $socialgroups,
            'hits' => ceil($hits),
            'page' => $page,
            'filter_form' => $filterform->createView()
        ));;
    }

    /**
     * Creates a new socialGroup entity.
     *
     * @Route("/new", name="socialgroup_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $socialGroup = new Socialgroup();
        $form = $this->createForm('BackBundle\Form\SocialGroupType', $socialGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($socialGroup);
            $em->flush();

            return $this->redirectToRoute('socialgroup_show', array('id' => $socialGroup->getId()));
        }

        return $this->render('BackBundle:Socialgroup:new.html.twig', array(
            'socialGroup' => $socialGroup,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a socialGroup entity.
     *
     * @Route("/{id}", name="socialgroup_show")
     * @Method("GET")
     */
    public function showAction(SocialGroup $socialGroup)
    {
        $deleteForm = $this->createDeleteForm($socialGroup);

        return $this->render('BackBundle:Socialgroup:show.html.twig', array(
            'socialGroup' => $socialGroup,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing socialGroup entity.
     *
     * @Route("/{id}/edit", name="socialgroup_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SocialGroup $socialGroup)
    {
        $deleteForm = $this->createDeleteForm($socialGroup);
        $editForm = $this->createForm('BackBundle\Form\SocialGroupType', $socialGroup);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('socialgroup_edit', array('id' => $socialGroup->getId()));
        }

        return $this->render('BackBundle:Socialgroup:edit.html.twig', array(
            'socialGroup' => $socialGroup,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a socialGroup entity.
     *
     * @Route("/{id}", name="socialgroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SocialGroup $socialGroup)
    {
        $form = $this->createDeleteForm($socialGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($socialGroup);
            $em->flush();
        }

        return $this->redirectToRoute('socialgroup_index');
    }

    /**
     * Creates a form to delete a socialGroup entity.
     *
     * @param SocialGroup $socialGroup The socialGroup entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SocialGroup $socialGroup)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('socialgroup_delete', array('id' => $socialGroup->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
