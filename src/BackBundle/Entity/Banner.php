<?php

namespace BackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Banner
 *
 * @ORM\Table(name="banner")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\BannerRepository")
 * @Vich\Uploadable
 */
class Banner
{

    /*
     * Constructor
     */
    public function __construct()
    {
        $this->ads = new ArrayCollection();
        $this->editorials = new ArrayCollection();
        $this->newsgroups = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="banners")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="banners")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="banners")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     */
    private $location;

    /**
     * @ORM\OneToMany(targetEntity="Advertising", mappedBy="banner", cascade={"persist", "remove"})
     */
    private $ads;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_initial", type="datetimetz", nullable=true)
     */
    private $dateInitial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_final", type="datetimetz", nullable=true)
     */
    private $dateFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="martketing", type="string", length=255, nullable=true)
     */
    private $marketing;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem", type="integer", nullable=true)
     */
    private $ordem;

    /**
     *
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * @var bool
     *
     * @ORM\Column(name="visivel_todos", type="boolean", nullable=true)
     */
    private $visivelTodos;

    /**
     * @var bool
     *
     * @ORM\Column(name="visivel_principal", type="boolean", nullable=true)
     */
    private $visivelPrincipal;

    /**
     * @var bool
     *
     * @ORM\Column(name="visivel_artigos", type="boolean", nullable=true)
     */
    private $visivelArtigos;


    /**
     * @var bool
     *
     * @ORM\Column(name="visivel_editorias", type="boolean", nullable=true)
     */
    private $visivelEditorias;

    /**
     * @var bool
     *
     * @ORM\Column(name="visivel_blogs", type="boolean", nullable=true)
     */
    private $visivelBlogs;

    /**
     * @var bool
     *
     * @ORM\Column(name="visivel_colunas", type="boolean", nullable=true)
     */
    private $visivelColunas;

    /**
     * @var bool
     *
     * @ORM\Column(name="visivel_municipios", type="boolean", nullable=true)
     */
    private $visivelMunicipios;

    /**
     * @var bool
     *
     * @ORM\Column(name="visivel_consultas", type="boolean", nullable=true)
     */
    private $visivelConsultas;

    /**
     * @ORM\ManyToMany(targetEntity="Editorial", inversedBy="banners")
     * @ORM\JoinTable(name="banners_editorials")
     */
    private $editorials;

    /**
     * @ORM\ManyToMany(targetEntity="NewsGroup", inversedBy="banners")
     * @ORM\JoinTable(name="banners_newsgroups")
     */
    private $newsgroups;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param \BackBundle\Entity\Company $company
     * @return Banner
     */
    public function setCompany(\BackBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \BackBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set customer
     *
     * @param \BackBundle\Entity\Customer $customer
     * @return Banner
     */
    public function setCustomer(\BackBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \BackBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set location
     *
     * @param \BackBundle\Entity\Location $location
     * @return Banner
     */
    public function setLocation(\BackBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \BackBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set dateInitial
     *
     * @param \DateTime $dateInitial
     *
     * @return Banner
     */
    public function setDateInitial($dateInitial)
    {
        $this->dateInitial = $dateInitial;

        return $this;
    }

    /**
     * Get dateInitial
     *
     * @return \DateTime
     */
    public function getDateInitial()
    {
        return $this->dateInitial;
    }

    /**
     * Set dateFinal
     *
     * @param \DateTime $dateFinal
     *
     * @return Article
     */
    public function setDateFinal($dateFinal)
    {
        $this->dateFinal = $dateFinal;

        return $this;
    }

    /**
     * Get dateFinal
     *
     * @return \DateTime
     */
    public function getDateFinal()
    {
        return $this->dateFinal;
    }

    /**
     * Set marketing
     *
     * @param string $marketing
     *
     * @return Banner
     */
    public function setMarketing($marketing)
    {
        $this->marketing = $marketing;

        return $this;
    }

    /**
     * Get marketing
     *
     * @return string
     */
    public function getMarketing()
    {
        return $this->marketing;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Banner
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Banner
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Get ordem
     *
     * @return integer
     */
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * Set ordem
     *
     * @param integer $ordem
     *
     * @return Banner
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * Add ad
     *
     * @param \BackBundle\Entity\Advertising $ad
     * @return Banner
     */
    public function addAd(\BackBundle\Entity\Advertising $ad)
    {
        $ad->setBanner($this);
        $this->ads[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \BackBundle\Entity\Advertising $ad
     */
    public function removeAd(\BackBundle\Entity\Advertising $ad)
    {
        $this->ads->removeElement($ad);
        $ad->setBanner(null);
    }

    /**
     * Get ads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAds()
    {
        return $this->ads;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set visivelTodos
     *
     * @param boolean $visivelTodos
     *
     * @return Banner
     */
    public function setVisivelTodos($visivelTodos)
    {
        $this->visivelTodos = $visivelTodos;

        return $this;
    }

    /**
     * Get visivelTodos
     *
     * @return bool
     */
    public function getVisivelTodos()
    {
        return $this->visivelTodos;
    }

    /**
     * Add editorial
     *
     * @param \BackBundle\Entity\Editorial $editorial
     * @return Banner
     */
    public function addEditorial(Editorial $editorial)
    {
        $editorial->addBanner($this);
        $this->editorials[] = $editorial;

        return $this;
    }

    /**
     * Remove editorial
     *
     * @param \BackBundle\Entity\Editorial $editorial
     */
    public function removeEditorial(Editorial $editorial)
    {
        $this->editorials->removeElement($editorial);
    }

    /**
     * Get $editorials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEditorials()
    {
        return $this->editorials;
    }

    /**
     * Add newsgroup
     *
     * @param \BackBundle\Entity\NewsGroup $newsgroup
     * @return Banner
     */
    public function addNewsGroup(NewsGroup $newsgroup)
    {
        $newsgroup->addBanner($this);
        $this->newsgroups[] = $newsgroup;

        return $this;
    }

    /**
     * Remove newsgroup
     *
     * @param \BackBundle\Entity\NewsGroup $newsgroup
     */
    public function removeNewsGroup(NewsGroup $newsgroup)
    {
        $this->newsgroups->removeElement($newsgroup);
    }

    /**
     * Get $newsgroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewsGroups()
    {
        return $this->newsgroups;
    }


    /**
     * Set visivelPrincipal
     *
     * @param boolean $visivelPrincipal
     *
     * @return Banner
     */
    public function setVisivelPrincipal($visivelPrincipal)
    {
        $this->visivelPrincipal = $visivelPrincipal;

        return $this;
    }

    /**
     * Get visivelPrincipal
     *
     * @return bool
     */
    public function getVisivelPrincipal()
    {
        return $this->visivelPrincipal;
    }

    /**
     * Set visivelArtigos
     *
     * @param boolean $visivelArtigos
     *
     * @return Banner
     */
    public function setVisivelArtigos($visivelArtigos)
    {
        $this->visivelArtigos = $visivelArtigos;

        return $this;
    }

    /**
     * Get visivelArtigos
     *
     * @return bool
     */
    public function getVisivelArtigos()
    {
        return $this->visivelArtigos;
    }
    
    /**
     * Set visivelEditorias
     *
     * @param boolean $visivelEditorias
     *
     * @return Banner
     */
    public function setVisivelEditorias($visivelEditorias)
    {
        $this->visivelEditorias = $visivelEditorias;

        return $this;
    }

    /**
     * Get visivelEditorias
     *
     * @return bool
     */
    public function getVisivelEditorias()
    {
        return $this->visivelEditorias;
    }

    /**
     * Set visivelBlogs
     *
     * @param boolean $visivelBlogs
     *
     * @return Banner
     */
    public function setVisivelBlogs($visivelBlogs)
    {
        $this->visivelBlogs = $visivelBlogs;

        return $this;
    }

    /**
     * Get visivelBlogs
     *
     * @return bool
     */
    public function getVisivelBlogs()
    {
        return $this->visivelBlogs;
    }

    /**
     * Set visivelColunas
     *
     * @param boolean $visivelColunas
     *
     * @return Banner
     */
    public function setVisivelColunas($visivelColunas)
    {
        $this->visivelColunas = $visivelColunas;

        return $this;
    }

    /**
     * Get visivelColunas
     *
     * @return bool
     */
    public function getVisivelColunas()
    {
        return $this->visivelColunas;
    }

    /**
     * Set visivelMunicipios
     *
     * @param boolean $visivelMunicipios
     *
     * @return Banner
     */
    public function setVisivelMunicipios($visivelMunicipios)
    {
        $this->visivelMunicipios = $visivelMunicipios;

        return $this;
    }

    /**
     * Get visivelMunicipios
     *
     * @return bool
     */
    public function getVisivelMunicipios()
    {
        return $this->visivelMunicipios;
    }

    /**
     * Set visivelConsultas
     *
     * @param boolean $visivelConsultas
     *
     * @return Banner
     */
    public function setVisivelConsultas($visivelConsultas)
    {
        $this->visivelConsultas = $visivelConsultas;

        return $this;
    }

    /**
     * Get visivelConsultas
     *
     * @return bool
     */
    public function getVisivelConsultas()
    {
        return $this->visivelConsultas;
    }    

    /**
     * @return string
     */
    public function __toString()
    {
        $valor = '';
        if($this->getCustomer() != null){
            $valor = $this->getCustomer()->getDescription();
        }

        if($this->getMarketing() != null) {
            if ($valor == '') {
                $valor = $this->getMarketing();
            } else {
                $valor = $valor . ' - ' . $this->getMarketing();
            }
        }
        return $valor;
    }
}
