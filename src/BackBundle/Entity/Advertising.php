<?php

namespace BackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Advertising
 *
 * @ORM\Table(name="advertising")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\AdvertisingRepository")
 * @Vich\Uploadable
 */
class Advertising
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    private $date;


    /**
     * @ORM\ManyToOne(targetEntity="Banner", inversedBy="ads")
     * @ORM\JoinColumn(name="banner_id", referencedColumnName="id")
     */
    private $banner;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Advertising
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set banner
     *
     * @param \BackBundle\Entity\Banner $banner
     * @return Advertising
     */
    public function setBanner(\BackBundle\Entity\Banner $banner = null)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return \BackBundle\Entity\Banner
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}