<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Slide
 *
 * @ORM\Table(name="slide")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\SlideRepository")
 * @Vich\Uploadable
 */
class Slide
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="slides")
     * @ORM\JoinColumn(name="Article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * @ORM\ManyToOne(targetEntity="AriArticle", inversedBy="slides")
     * @ORM\JoinColumn(name="ari_article_id", referencedColumnName="id")
     */
    private $ariArticle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_initial", type="datetimetz", nullable=true)
     */
    private $dateInitial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_final", type="datetimetz", nullable=true)
     */
    private $dateFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordem", type="integer", nullable=true)
     */
    private $ordem;

    /**
     *
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set article
     *
     * @param \BackBundle\Entity\Article $article
     * @return Slide
     */
    public function setArticle(\BackBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \BackBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set ariArticle
     *
     * @param \BackBundle\Entity\AriArticle $ariArticle
     * @return Slide
     */
    public function setAriArticle(\BackBundle\Entity\AriArticle $ariArticle = null)
    {
        $this->ariArticle = $ariArticle;

        return $this;
    }

    /**
     * Get ariArticle
     *
     * @return \BackBundle\Entity\AriArticle
     */
    public function getAriArticle()
    {
        return $this->ariArticle;
    }

    /**
     * Set dateInitial
     *
     * @param \DateTime $dateInitial
     *
     * @return Article
     */
    public function setDateInitial($dateInitial)
    {
        $this->dateInitial = $dateInitial;

        return $this;
    }

    /**
     * Get dateInitial
     *
     * @return \DateTime
     */
    public function getDateInitial()
    {
        return $this->dateInitial;
    }

    /**
     * Set dateFinal
     *
     * @param \DateTime $dateFinal
     *
     * @return Article
     */
    public function setDateFinal($dateFinal)
    {
        $this->dateFinal = $dateFinal;

        return $this;
    }

    /**
     * Get dateFinal
     *
     * @return \DateTime
     */
    public function getDateFinal()
    {
        return $this->dateFinal;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Slide
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return Slide
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Slide
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Get ordem
     *
     * @return integer
     */
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * Set ordem
     *
     * @param integer $ordem
     *
     * @return Slide
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }
}
