<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * AriArticle
 *
 * @ORM\Table(name="ari_article")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\AriArticleRepository")
 * @Vich\Uploadable
 */
class AriArticle
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->slides = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="first_contain_part_i", type="text", nullable=true)
     */
    private $firstContainPartI;

    /**
     * @var string
     *
     * @ORM\Column(name="first_contain_part_ii", type="text", nullable=true)
     */
    private $firstContainPartIi;

    /**
     * @var string
     *
     * @ORM\Column(name="second_contain_part_i", type="text", nullable=true)
     */
    private $secondContainPartI;

    /**
     * @var string
     *
     * @ORM\Column(name="second_contain_part_ii", type="text", nullable=true)
     */
    private $secondContainPartIi;

    /**
     * @var string
     *
     * @ORM\Column(name="pingpong_title", type="string", length=255, nullable=true)
     */
    private $pingpongTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="pingpong_contain", type="text", nullable=true)
     */
    private $pingpongContain;

    /**
     * @var string
     *
     * @ORM\Column(name="express_contain", type="text", nullable=true)
     */
    private $expressContain;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="hat", type="string", length=255, nullable=true)
     */
    private $hat;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle_image", type="string", length=255, nullable=true)
     */
    private $subtitleImage;

    /**
     * @ORM\OneToMany(targetEntity="Slide", mappedBy="ariArticle", cascade={"persist", "remove"})
     */
    private $slides;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return AriArticle
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return AriArticle
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstContainPartI
     *
     * @param string $firstContainPartI
     *
     * @return AriArticle
     */
    public function setFirstContainPartI($firstContainPartI)
    {
        $this->firstContainPartI = $firstContainPartI;

        return $this;
    }

    /**
     * Get firstContainPartI
     *
     * @return string
     */
    public function getFirstContainPartI()
    {
        return $this->firstContainPartI;
    }

    /**
     * Set firstContainPartIi
     *
     * @param string $firstContainPartIi
     *
     * @return AriArticle
     */
    public function setFirstContainPartIi($firstContainPartIi)
    {
        $this->firstContainPartIi = $firstContainPartIi;

        return $this;
    }

    /**
     * Get firstContainPartIi
     *
     * @return string
     */
    public function getFirstContainPartIi()
    {
        return $this->firstContainPartIi;
    }

    /**
     * Set secondContainPartI
     *
     * @param string $secondContainPartI
     *
     * @return AriArticle
     */
    public function setSecondContainPartI($secondContainPartI)
    {
        $this->secondContainPartI = $secondContainPartI;

        return $this;
    }

    /**
     * Get secondContainPartI
     *
     * @return string
     */
    public function getSecondContainPartI()
    {
        return $this->secondContainPartI;
    }

    /**
     * Set secondContainPartIi
     *
     * @param string $secondContainPartIi
     *
     * @return AriArticle
     */
    public function setSecondContainPartIi($secondContainPartIi)
    {
        $this->secondContainPartIi = $secondContainPartIi;

        return $this;
    }

    /**
     * Get secondContainPartIi
     *
     * @return string
     */
    public function getSecondContainPartIi()
    {
        return $this->secondContainPartIi;
    }

    /**
     * Set pingpongTitle
     *
     * @param string $pingpongTitle
     *
     * @return AriArticle
     */
    public function setPingpongTitle($pingpongTitle)
    {
        $this->pingpongTitle = $pingpongTitle;

        return $this;
    }

    /**
     * Get pingpongTitle
     *
     * @return string
     */
    public function getPingpongTitle()
    {
        return $this->pingpongTitle;
    }

    /**
     * Set pingpongContain
     *
     * @param string $pingpongContain
     *
     * @return AriArticle
     */
    public function setPingpongContain($pingpongContain)
    {
        $this->pingpongContain = $pingpongContain;

        return $this;
    }

    /**
     * Get pingpongContain
     *
     * @return string
     */
    public function getPingpongContain()
    {
        return $this->pingpongContain;
    }

    /**
     * Set expressContain
     *
     * @param string $expressContain
     *
     * @return AriArticle
     */
    public function setExpressContain($expressContain)
    {
        $this->expressContain = $expressContain;

        return $this;
    }

    /**
     * Get expressContain
     *
     * @return string
     */
    public function getExpressContain()
    {
        return $this->expressContain;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return AriArticle
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return AriArticle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set hat
     *
     * @param string $hat
     *
     * @return AriArticle
     */
    public function setHat($hat)
    {
        $this->hat = $hat;

        return $this;
    }

    /**
     * Get hat
     *
     * @return string
     */
    public function getHat()
    {
        return $this->hat;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set subtitleImage
     *
     * @param string $subtitleImage
     *
     * @return AriArticle
     */
    public function setSubtitleImage($subtitleImage)
    {
        $this->subtitleImage = $subtitleImage;

        return $this;
    }

    /**
     * Get subtitleImage
     *
     * @return string
     */
    public function getSubtitleImage()
    {
        return $this->subtitleImage;
    }

    /**
     * Add slide
     *
     * @param \BackBundle\Entity\Slide $slide
     * @return AriArticle
     */
    public function addSlide(\BackBundle\Entity\Slide $slide)
    {
        $slide->setAriArticle($this);
        $this->slides[] = $slide;

        return $this;
    }

    /**
     * Remove slide
     *
     * @param \BackBundle\Entity\Slide $slide
     */
    public function removeSlide(\BackBundle\Entity\Slide $slide)
    {
        $this->slides->removeElement($slide);
        $slide->setAriArticle(null);
    }

    /**
     * Get slides
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlides()
    {
        return $this->slides;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }
}

