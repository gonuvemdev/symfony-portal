<?php

namespace BackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuario")
 */
class User extends BaseUser
{
    public function __construct()
    {
        parent::__construct();
        $this->instalacaos = new ArrayCollection();
        $this->newsgroups = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipousuario", type="string", length=255, nullable=true)
     */
    private $tipousuario;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="users")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=true)
     */
    private $nome;


    /**
     * @var string
     *
     * @ORM\Column(name="cpf", type="string", length=255, nullable=true)
     */
    private $cpf;


    /**
     * @var string
     *
     * @ORM\Column(name="rg", type="string", length=255, nullable=true)
     */
    private $rg;


    /**
     * @var string
     *
     * @ORM\Column(name="orgaoemissor", type="string", length=255, nullable=true)
     */
    private $orgaoemissor;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone1", type="string", length=255, nullable=true)
     */
    private $telefone1;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone2", type="string", length=255, nullable=true)
     */
    private $telefone2;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone3", type="string", length=255, nullable=true)
     */
    private $telefone3;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255, nullable=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=255, nullable=true)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="pais", type="string", length=255, nullable=true)
     */
    private $pais;

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=255, nullable=true)
     */
    private $cep;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="criado", type="datetimetz", nullable=true)
     */
    private $criado;

    /**
     * @ORM\OneToMany(targetEntity="Installation", mappedBy="usuario", cascade={"persist", "remove"})
     */
    private $instalacaos;
    
    /**
     * @ORM\OneToMany(targetEntity="NewsGroup", mappedBy="owner", cascade={"persist", "remove"})
     */
    private $newsgroups;

    /**
     * Set tipousuario
     *
     * @param string $tipousuario
     * @return User
     */
    public function setTipoUsuario($tipousuario)
    {
        $this->tipousuario = $tipousuario;

        return $this;
    }

    /**
     * Get tipousuario
     *
     * @return string
     */
    public function getTipoUsuario()
    {
        return $this->tipousuario;
    }

    /**
     * Set company
     *
     * @param \BackBundle\Entity\Company $company
     * @return User
     */
    public function setCompany(\BackBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \BackBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set customer
     *
     * @param \BackBundle\Entity\Customer $customer
     * @return User
     */
    public function setCustomer(\BackBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \BackBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return User
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }


    /**
     * Set cpf
     *
     * @param string $cpf
     * @return User
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * Get cpf
     *
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }


    /**
     * Set rg
     *
     * @param string $rg
     * @return User
     */
    public function setRg($rg)
    {
        $this->rg = $rg;

        return $this;
    }

    /**
     * Get rg
     *
     * @return string
     */
    public function getRg()
    {
        return $this->rg;
    }


    /**
     * Set orgaoemissor
     *
     * @param string $orgaoemissor
     * @return User
     */
    public function setOrgaoEmissor($orgaoemissor)
    {
        $this->orgaoemissor = $orgaoemissor;

        return $this;
    }

    /**
     * Get orgaoemissor
     *
     * @return string
     */
    public function getOrgaoEmissor()
    {
        return $this->orgaoemissor;
    }

    /**
     * Set telefone1
     *
     * @param string $telefone1
     * @return User
     */
    public function setTelefone1($telefone1)
    {
        $this->telefone1 = $telefone1;

        return $this;
    }

    /**
     * Get telefone1
     *
     * @return string
     */
    public function getTelefone1()
    {
        return $this->telefone1;
    }


    /**
     * Set telefone2
     *
     * @param string $telefone2
     * @return User
     */
    public function setTelefone2($telefone2)
    {
        $this->telefone2 = $telefone2;

        return $this;
    }

    /**
     * Get telefone2
     *
     * @return string
     */
    public function getTelefone2()
    {
        return $this->telefone2;
    }

    /**
     * Set telefone3
     *
     * @param string $telefone3
     * @return User
     */
    public function setTelefone3($telefone3)
    {
        $this->telefone3 = $telefone3;

        return $this;
    }

    /**
     * Get telefone3
     *
     * @return string
     */
    public function getTelefone3()
    {
        return $this->telefone3;
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     * @return User
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get endereco
     *
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return User
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     * @return User
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     * @return User
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return User
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set pais
     *
     * @param string $pais
     * @return User
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string
     */
    public function getPais()
    {
        return $this->pais;
    }


    /**
     * Set cep
     *
     * @param string $cep
     * @return User
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set criado
     *
     * @param \DateTime $criado
     *
     * @return User
     */
    public function setCriado($criado)
    {
        $this->criado = $criado;

        return $this;
    }

    /**
     * Get criado
     *
     * @return \DateTime
     */
    public function getCriado()
    {
        return $this->criado;
    }

    /**
     * Add instalacao
     *
     * @param \BackBundle\Entity\Installation $instalacao
     * @return User
     */
    public function addInstalacao(\BackBundle\Entity\Installation $instalacao)
    {
        $instalacao->setUsuario($this);
        $this->instalacaos[] = $instalacao;

        return $this;
    }

    /**
     * Remove instalacao
     *
     * @param \BackBundle\Entity\Installation $instalacao
     */
    public function removeInstalacao(\BackBundle\Entity\Installation $instalacao)
    {
        $this->instalacaos->removeElement($instalacao);
        $instalacao->setUsuario(null);
    }

    /**
     * Get instalacaos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInstalacaos()
    {
        return $this->instalacaos;
    }

    /**
     * Add newsgroup
     *
     * @param \BackBundle\Entity\NewsGroup $newsgroup
     * @return User
     */
    public function addNewsgroup(\BackBundle\Entity\NewsGroup $newsgroup)
    {
        $newsgroup->setUsuario($this);
        $this->newsgroups[] = $newsgroup;

        return $this;
    }

    /**
     * Remove newsgroup
     *
     * @param \BackBundle\Entity\NewsGroup $newsgroup
     */
    public function removeNewsgroup(\BackBundle\Entity\NewsGroup $newsgroup)
    {
        $this->newsgroups->removeElement($newsgroup);
        $newsgroup->setUsuario(null);
    }

    /**
     * Get newsgroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewsgroups()
    {
        return $this->newsgroups;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUsername();
    }

}