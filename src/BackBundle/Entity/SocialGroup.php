<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SocialGroup
 *
 * @ORM\Table(name="social_group")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\SocialGroupRepository")
 */
class SocialGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="NewsGroup", inversedBy="socialgroups")
     * @ORM\JoinColumn(name="newsgroup_id", referencedColumnName="id")
     */
    private $newsgroup;

    /**
     * @ORM\ManyToOne(targetEntity="Social", inversedBy="socialgroups")
     * @ORM\JoinColumn(name="social_id", referencedColumnName="id")
     */
    private $social;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return SocialGroup
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set newsgroup
     *
     * @param \BackBundle\Entity\NewsGroup $newsgroup
     * @return SocialGroup
     */
    public function setNewsGroup(\BackBundle\Entity\NewsGroup $newsgroup = null)
    {
        $this->newsgroup = $newsgroup;

        return $this;
    }

    /**
     * Get newsgroup
     *
     * @return \BackBundle\Entity\NewsGroup
     */
    public function getNewsGroup()
    {
        return $this->newsgroup;
    }

    /**
     * Set social
     *
     * @param \BackBundle\Entity\Social $social
     * @return SocialGroup
     */
    public function setSocial(\BackBundle\Entity\Social $social = null)
    {
        $this->social = $social;

        return $this;
    }

    /**
     * Get social
     *
     * @return \BackBundle\Entity\Social
     */
    public function getSocial()
    {
        return $this->social;
    }

}

