<?php

namespace BackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * NewsGroup
 *
 * @ORM\Table(name="news_group")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\NewsGroupRepository")
 * @Vich\Uploadable
 */
class NewsGroup
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->socialgroups = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->locations = new ArrayCollection();
        $this->banners = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="group_name", type="string", length=255)
     */
    private $groupName;

    /**
     * @var string
     *
     * @ORM\Column(name="owner_name", type="string", length=255, nullable=true)
     */
    private $ownerName;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="newsgroups")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="can_comment", type="boolean", nullable=true)
     */
    private $canComment;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="featured", type="boolean", nullable=true)
     */
    private $featured;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_to", type="string", length=255, nullable=true)
     */
    private $mailTo;

    /**
     * @var int
     *
     * @ORM\Column(name="visitor_number_web", type="integer", nullable=true)
     */
    private $visitorNumberWeb;

    /**
     * @var int
     *
     * @ORM\Column(name="visitor_number_app", type="integer", nullable=true)
     */
    private $visitorNumberApp;

    /**
     * @var string
     *
     * @ORM\Column(name="kind", type="string", length=255)
     */
    private $kind;


    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName2")
     *
     * @var File
     */
    private $imageFile2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName2;

    /**
     * @ORM\OneToMany(targetEntity="SocialGroup", mappedBy="newsgroup", cascade={"persist", "remove"})
     */
    private $socialgroups;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="newsgroup", cascade={"persist", "remove"})
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity="Location", mappedBy="newsgroup", cascade={"persist", "remove"})
     */
    private $locations;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Many Groups have Many Users.
     * @ORM\ManyToMany(targetEntity="Banner", mappedBy="newsgroups")
     */
    private $banners;

    /**
     * @var string
     *
     * @ORM\Column(name="friendly_newsgroup", type="text", nullable=true)
     *
     */
    private $friendlyNewsgroup;

    /**
     * @return string
     */
    public function getFriendlyNewsgroup()
    {
        return $this->friendlyNewsgroup;
    }

    /**
     * @param string $friendlyNewsgroup
     *
     * @return NewsGroup
     */
    public function setFriendlyNewsgroup($friendlyNewsgroup)
    {
        $this->friendlyNewsgroup = $friendlyNewsgroup;

        return $friendlyNewsgroup;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set groupName
     *
     * @param string $groupName
     *
     * @return NewsGroup
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;

        return $this;
    }

    /**
     * Get groupName
     *
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * Set ownerName
     *
     * @param string $ownerName
     *
     * @return NewsGroup
     */
    public function setOwnerName($ownerName)
    {
        $this->ownerName = $ownerName;

        return $this;
    }

    /**
     * Get ownerName
     *
     * @return string
     */
    public function getOwnerName()
    {
        return $this->ownerName;
    }

    /**
     * Set owner
     *
     * @param \BackBundle\Entity\User $user
     * @return NewsGroup
     */
    public function setOwner(\BackBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \BackBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return NewsGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set canComment
     *
     * @param boolean $canComment
     *
     * @return NewsGroup
     */
    public function setCanComment($canComment)
    {
        $this->canComment = $canComment;

        return $this;
    }

    /**
     * Get canComment
     *
     * @return bool
     */
    public function getCanComment()
    {
        return $this->canComment;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return NewsGroup
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set featured
     *
     * @param boolean $featured
     *
     * @return NewsGroup
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured
     *
     * @return bool
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set mailTo
     *
     * @param string $mailTo
     *
     * @return NewsGroup
     */
    public function setMailTo($mailTo)
    {
        $this->mailTo = $mailTo;

        return $this;
    }

    /**
     * Get mailTo
     *
     * @return string
     */
    public function getMailTo()
    {
        return $this->mailTo;
    }

    /**
     * Set visitorNumberWeb
     *
     * @param integer $visitorNumberWeb
     *
     * @return NewsGroup
     */
    public function setVisitorNumberWeb($visitorNumberWeb)
    {
        $this->visitorNumberWeb = $visitorNumberWeb;

        return $this;
    }

    /**
     * Get visitorNumberWeb
     *
     * @return int
     */
    public function getVisitorNumberWeb()
    {
        return $this->visitorNumberWeb;
    }

    /**
     * Set visitorNumberApp
     *
     * @param integer $visitorNumberApp
     *
     * @return NewsGroup
     */
    public function setVisitorNumberApp($visitorNumberApp)
    {
        $this->visitorNumberApp = $visitorNumberApp;

        return $this;
    }

    /**
     * Get visitorNumberApp
     *
     * @return int
     */
    public function getVisitorNumberApp()
    {
        return $this->visitorNumberApp;
    }

    /**
     * Set kind
     *
     * @param string $kind
     *
     * @return NewsGroup
     */
    public function setKind($kind)
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * Get kind
     *
     * @return string
     */
    public function getKind()
    {
        return $this->kind;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile2(File $image = null)
    {
        $this->imageFile2 = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile2()
    {
        return $this->imageFile2;
    }

    /**
     * @param string $imageName2
     */
    public function setImageName2($imageName2)
    {
        $this->imageName2 = $imageName2;
    }

    /**
     * @return string
     */
    public function getImageName2()
    {
        return $this->imageName2;
    }

    /**
     * Add socialgroup
     *
     * @param \BackBundle\Entity\SocialGroup $socialgroup
     * @return NewsGroup
     */
    public function addSocialGroup(\BackBundle\Entity\SocialGroup $socialgroup)
    {
        $socialgroup->setNewsGroup($this);
        $this->socialgroups[] = $socialgroup;

        return $this;
    }

    /**
     * Remove socialgroup
     *
     * @param \BackBundle\Entity\SocialGroup $socialgroup
     */
    public function removeSocialGroup(\BackBundle\Entity\SocialGroup $socialgroup)
    {
        $this->socialgroups->removeElement($socialgroup);
        $socialgroup->setNewsGroup(null);
    }

    /**
     * Get socialgroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialGroups()
    {
        return $this->socialgroups;
    }

    /**
     * Add article
     *
     * @param \BackBundle\Entity\Article $article
     * @return NewsGroup
     */
    public function addArticle(\BackBundle\Entity\Article $article)
    {
        $article->setNewsGroup($this);
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \BackBundle\Entity\Article $article
     */
    public function removeArticle(\BackBundle\Entity\Article $article)
    {
        $this->articles->removeElement($article);
        $article->setNewsGroup(null);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add location
     *
     * @param \BackBundle\Entity\Location $location
     * @return NewsGroup
     */
    public function addLocation(\BackBundle\Entity\Location $location)
    {
        $location->setNewsGroup($this);
        $this->locations[] = $location;

        return $this;
    }

    /**
     * Remove location
     *
     * @param \BackBundle\Entity\Location $location
     */
    public function removeLocation(\BackBundle\Entity\Location $location)
    {
        $this->locations->removeElement($location);
        $location->setNewsGroup(null);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Add banner
     *
     * @param \BackBundle\Entity\Banner $banner
     * @return NewsGroup
     */
    public function addBanner(Banner $banner)
    {
        $banner->addEditorial($this);
        $this->banners[] = $banner;

        return $this;
    }

    /**
     * Remove banner
     *
     * @param \BackBundle\Entity\Banner $banner
     */
    public function removeBanner(Banner $banner)
    {
        $this->banners->removeElement($banner);
    }

    /**
     * Get $banners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBanners()
    {
        return $this->banners;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getGroupName();
    }
}

