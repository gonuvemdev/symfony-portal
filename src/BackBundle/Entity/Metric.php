<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Metric
 *
 * @ORM\Table(name="metric")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\MetricRepository")
 */
class Metric
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="acessowebsite", type="integer", nullable=true)
     */
    private $acessowebsite;

    /**
     * @var int
     *
     * @ORM\Column(name="acessoapp", type="integer", nullable=true)
     */
    private $acessoapp;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set acessowebsite
     *
     * @param integer $acessowebsite
     *
     * @return Metric
     */
    public function setAcessowebsite($acessowebsite)
    {
        $this->acessowebsite = $acessowebsite;

        return $this;
    }

    /**
     * Get acessowebsite
     *
     * @return int
     */
    public function getAcessowebsite()
    {
        return $this->acessowebsite;
    }

    /**
     * Set acessoapp
     *
     * @param integer $acessoapp
     *
     * @return Metric
     */
    public function setAcessoapp($acessoapp)
    {
        $this->acessoapp = $acessoapp;

        return $this;
    }

    /**
     * Get acessoapp
     *
     * @return int
     */
    public function getAcessoapp()
    {
        return $this->acessoapp;
    }
}