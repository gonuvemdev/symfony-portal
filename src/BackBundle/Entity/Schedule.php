<?php

namespace BackBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Schedule
 *
 * @ORM\Table(name="schedule")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\ScheduleRepository")
 */
class Schedule
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scheduled_to", type="datetimetz")
     */
    private $scheduledTo;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="schedules")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scheduledTo
     *
     * @param \DateTime $scheduledTo
     *
     * @return Schedule
     */
    public function setScheduledTo($scheduledTo)
    {
        $this->scheduledTo = $scheduledTo;

        return $this;
    }

    /**
     * Get scheduledTo
     *
     * @return \DateTime
     */
    public function getScheduledTo()
    {
        return $this->scheduledTo;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Schedule
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set article
     *
     * @param \BackBundle\Entity\Article $article
     * @return SocialGroup
     */
    public function setArticle(\BackBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \BackBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getScheduledTo()->format('d/m/Y');
    }
}

