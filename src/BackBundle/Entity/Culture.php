<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Culture
 *
 * @ORM\Table(name="culture")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\CultureRepository")
 * @Vich\Uploadable
 */
class Culture
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255, nullable=true)
     */
    private $place;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_to", type="string", length=255, nullable=true)
     */
    private $mailTo;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var int
     *
     * @ORM\Column(name="visitor_number_web", type="integer", nullable=true)
     */
    private $visitorNumberWeb;

    /**
     * @var int
     *
     * @ORM\Column(name="visitor_number_app", type="integer", nullable=true)
     */
    private $visitorNumberApp;


    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName2;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Culture
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Culture
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
    * Set date
     *
     * @param \DateTime $date
     *
     * @return Installation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Culture
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Culture
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Culture
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return NewsGroup
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set mailTo
     *
     * @param string $mailTo
     *
     * @return NewsGroup
     */
    public function setMailTo($mailTo)
    {
        $this->mailTo = $mailTo;

        return $this;
    }

    /**
     * Get mailTo
     *
     * @return string
     */
    public function getMailTo()
    {
        return $this->mailTo;
    }

    /**
     * Set visitorNumberWeb
     *
     * @param integer $visitorNumberWeb
     *
     * @return NewsGroup
     */
    public function setVisitorNumberWeb($visitorNumberWeb)
    {
        $this->visitorNumberWeb = $visitorNumberWeb;

        return $this;
    }

    /**
     * Get visitorNumberWeb
     *
     * @return int
     */
    public function getVisitorNumberWeb()
    {
        return $this->visitorNumberWeb;
    }

    /**
     * Set visitorNumberApp
     *
     * @param integer $visitorNumberApp
     *
     * @return NewsGroup
     */
    public function setVisitorNumberApp($visitorNumberApp)
    {
        $this->visitorNumberApp = $visitorNumberApp;

        return $this;
    }

    /**
     * Get visitorNumberApp
     *
     * @return int
     */
    public function getVisitorNumberApp()
    {
        return $this->visitorNumberApp;
    }

    
     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile2(File $image = null)
    {
        $this->imageFile2 = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile2()
    {
        return $this->imageFile2;
    }

    /**
     * @param string $imageName2
     */
    public function setImageName2($imageName2)
    {
        $this->imageName2 = $imageName2;
    }

    /**
     * @return string
     */
    public function getImageName2()
    {
        return $this->imageName2;
    }

    

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }
}

