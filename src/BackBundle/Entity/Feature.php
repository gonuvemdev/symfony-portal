<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Feature
 *
 * @ORM\Table(name="feature")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\FeatureRepository")
 * @Vich\Uploadable
 */
class Feature
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="features")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="features")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * @var string
     *
     * @ORM\Column(name="article_title", type="string", length=255, nullable=true)
     */
    private $articleTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="article_subtitle", type="string", length=255, nullable=true)
     */
    private $articleSubtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="article_hat", type="string", length=255, nullable=true)
     */
    private $articleHat;

    /**
     * @var bool
     *
     * @ORM\Column(name="scheduled", type="boolean", nullable=true)
     */
    private $scheduled;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scheduled_to", type="datetimetz", nullable=true)
     */
    private $scheduledTo;

    /**
     * @var string
     *
     * @ORM\Column(name="image_subtitle", type="string", length=255, nullable=true)
     */
    private $imageSubtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="image_credit", type="string", length=255, nullable=true)
     */
    private $imageCredit;

    /**
     * @var string
     *
     * @ORM\Column(name="image_description", type="text", nullable=true)
     */
    private $imageDescription;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Feature
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set location
     *
     * @param \BackBundle\Entity\Location $location
     * @return Feature
     */
    public function setLocation(\BackBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \BackBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set article
     *
     * @param \BackBundle\Entity\Article $article
     * @return Feature
     */
    public function setArticle(\BackBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \BackBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set articleTitle
     *
     * @param string $articleTitle
     *
     * @return Feature
     */
    public function setArticleTitle($articleTitle)
    {
        $this->articleTitle = $articleTitle;

        return $this;
    }

    /**
     * Get articleTitle
     *
     * @return string
     */
    public function getArticleTitle()
    {
        return $this->articleTitle;
    }

    /**
     * Set articleSubtitle
     *
     * @param string $articleSubtitle
     *
     * @return Feature
     */
    public function setArticleSubtitle($articleSubtitle)
    {
        $this->articleSubtitle = $articleSubtitle;

        return $this;
    }

    /**
     * Get articleSubtitle
     *
     * @return string
     */
    public function getArticleSubtitle()
    {
        return $this->articleSubtitle;
    }

    /**
     * Set articleHat
     *
     * @param string $articleHat
     *
     * @return Feature
     */
    public function setArticleHat($articleHat)
    {
        $this->articleHat = $articleHat;

        return $this;
    }

    /**
     * Get articleHat
     *
     * @return string
     */
    public function getArticleHat()
    {
        return $this->articleHat;
    }

    /**
     * Set scheduled
     *
     * @param boolean $scheduled
     *
     * @return Feature
     */
    public function setScheduled($scheduled)
    {
        $this->scheduled = $scheduled;

        return $this;
    }

    /**
     * Get scheduled
     *
     * @return bool
     */
    public function getScheduled()
    {
        return $this->scheduled;
    }

    /**
     * Set scheduledTo
     *
     * @param \DateTime $scheduledTo
     *
     * @return Feature
     */
    public function setScheduledTo($scheduledTo)
    {
        $this->scheduledTo = $scheduledTo;

        return $this;
    }

    /**
     * Get scheduledTo
     *
     * @return \DateTime
     */
    public function getScheduledTo()
    {
        return $this->scheduledTo;
    }

    /**
     * Set imageSubtitle
     *
     * @param string $imageSubtitle
     *
     * @return Feature
     */
    public function setImageSubtitle($imageSubtitle)
    {
        $this->imageSubtitle = $imageSubtitle;

        return $this;
    }

    /**
     * Get imageSubtitle
     *
     * @return string
     */
    public function getImageSubtitle()
    {
        return $this->imageSubtitle;
    }

    /**
     * Set imageCredit
     *
     * @param string $imageCredit
     *
     * @return Feature
     */
    public function setImageCredit($imageCredit)
    {
        $this->imageCredit = $imageCredit;

        return $this;
    }

    /**
     * Get imageCredit
     *
     * @return string
     */
    public function getImageCredit()
    {
        return $this->imageCredit;
    }

    /**
     * Set imageDescription
     *
     * @param string $imageDescription
     *
     * @return Feature
     */
    public function setImageDescription($imageDescription)
    {
        $this->imageDescription = $imageDescription;

        return $this;
    }

    /**
     * Get imageDescription
     *
     * @return string
     */
    public function getImageDescription()
    {
        return $this->imageDescription;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Feature
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}

