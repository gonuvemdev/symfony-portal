<?php

namespace BackBundle\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\ArticleRepository")
 * @Vich\Uploadable
 */
class Article
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->schedules = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->slides = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"main"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="NewsGroup", inversedBy="articles")
     * @ORM\JoinColumn(name="newsgroup_id", referencedColumnName="id")
     */
    private $newsgroup;

    /**
     * @ORM\ManyToOne(targetEntity="Editorial", inversedBy="articles")
     * @ORM\JoinColumn(name="editorial_id", referencedColumnName="id")
     */
    private $editorial;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="articles")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"main"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="hat", type="string", length=255, nullable=true)
     */
    private $hat;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="origin", type="string", length=255, nullable=true)
     */
    private $origin;

    /**
     * @var string
     *
     * @ORM\Column(name="credit", type="string", length=255, nullable=true)
     */
    private $credit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_at", type="datetime")
     */
    private $publishedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="friendly", type="text", nullable=true)
     */
    private $friendly;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="cover_feature", type="boolean", nullable=true)
     */
    private $coverFeature;

    /**
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean", nullable=true, options={"default" : "0"})
     */
    private $locked = false;

    /**
     * @var string
     *
     * @ORM\Column(name="free_content", type="text", nullable=true)
     */
    private $freeContent;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     */
    private $keywords;

    /**
     * @var int
     *
     * @ORM\Column(name="visitor_number_web", type="integer", nullable=true)
     */
    private $visitorNumberWeb;

    /**
     * @var int
     *
     * @ORM\Column(name="visitor_number_app", type="integer", nullable=true)
     */
    private $visitorNumberApp;

    /**
     * @ORM\OneToMany(targetEntity="Schedule", mappedBy="article", cascade={"persist", "remove"})
     */
    private $schedules;

    /**
     * @ORM\OneToMany(targetEntity="Feature", mappedBy="article", cascade={"persist", "remove"})
     */
    private $features;

    /**
     * @ORM\OneToMany(targetEntity="Slide", mappedBy="article", cascade={"persist", "remove"})
     */
    private $slides;

    /**
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="articles")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     */
    private $location;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName2")
     *
     * @var File
     */
    private $imageFile2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName2;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName3")
     *
     * @var File
     */
    private $imageFile3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName3;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName4")
     *
     * @var File
     */
    private $imageFile4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName4;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName5")
     *
     * @var File
     */
    private $imageFile5;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName5;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName6")
     *
     * @var File
     */
    private $imageFile6;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName6;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName7")
     *
     * @var File
     */
    private $imageFile7;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName7;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName8")
     *
     * @var File
     */
    private $imageFile8;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName8;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName9")
     *
     * @var File
     */
    private $imageFile9;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName9;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName10")
     *
     * @var File
     */
    private $imageFile10;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName10;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set newsgroup
     *
     * @param \BackBundle\Entity\NewsGroup $newsgroup
     * @return Article
     */
    public function setNewsGroup(\BackBundle\Entity\NewsGroup $newsgroup = null)
    {
        $this->newsgroup = $newsgroup;

        return $this;
    }

    /**
     * Get newsgroup
     *
     * @return \BackBundle\Entity\NewsGroup
     */
    public function getNewsGroup()
    {
        return $this->newsgroup;
    }

    /**
     * Set editorial
     *
     * @param \BackBundle\Entity\Editorial $editorial
     * @return Article
     */
    public function setEditorial(\BackBundle\Entity\Editorial $editorial = null)
    {
        $this->editorial = $editorial;

        return $this;
    }

    /**
     * Get editorial
     *
     * @return \BackBundle\Entity\Editorial
     */
    public function getEditorial()
    {
        return $this->editorial;
    }

    /**
     * Set city
     *
     * @param \BackBundle\Entity\City $city
     * @return Article
     */
    public function setCity(\BackBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \BackBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Article
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set friendly
     *
     * @param string $friendly
     *
     * @return Article
     */
    public function setFriendly($friendly)
    {
        $this->friendly = $friendly;

        return $this;
    }

    /**
     * Get friendly
     *
     * @return string
     */
    public function getFriendly()
    {
        return $this->friendly;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return Article
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set hat
     *
     * @param string $hat
     *
     * @return Article
     */
    public function setHat($hat)
    {
        $this->hat = $hat;

        return $this;
    }

    /**
     * Get hat
     *
     * @return string
     */
    public function getHat()
    {
        return $this->hat;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Article
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set origin
     *
     * @param string $origin
     *
     * @return Article
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set credit
     *
     * @param string $credit
     *
     * @return Article
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return string
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     *
     * @return Article
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Article
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return Article
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set coverFeature
     *
     * @param boolean $coverFeature
     *
     * @return Article
     */
    public function setCoverFeature($coverFeature)
    {
        $this->coverFeature = $coverFeature;

        return $this;
    }

    /**
     * Get coverFeature
     *
     * @return bool
     */
    public function getCoverFeature()
    {
        return $this->coverFeature;
    }

    
    /**
     * Get locked
     *
     * @return bool
     */
    public function getLocked()
    {
        return $this->locked;
    }

     /**
     * Set locked
     *
     * @param bool $locked
     *
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get freeContent
     *
     * @return string
     */
    public function getFreeContent()
    {
        return $this->freeContent;
    }

     /**
     * Set freeContent
     *
     * @param string $freeContent
     *
     */
    public function setFreeContent($freeContent)
    {
        $this->freeContent = $freeContent;

        return $this;
    }

    /**
     * Set visitorNumberWeb
     *
     * @param integer $visitorNumberWeb
     *
     * @return Article
     */
    public function setVisitorNumberWeb($visitorNumberWeb)
    {
        $this->visitorNumberWeb = $visitorNumberWeb;

        return $this;
    }

    /**
     * Get visitorNumberWeb
     *
     * @return int
     */
    public function getVisitorNumberWeb()
    {
        return $this->visitorNumberWeb;
    }

    /**
     * Set visitorNumberApp
     *
     * @param integer $visitorNumberApp
     *
     * @return Article
     */
    public function setVisitorNumberApp($visitorNumberApp)
    {
        $this->visitorNumberApp = $visitorNumberApp;

        return $this;
    }

    /**
     * Get visitorNumberApp
     *
     * @return int
     */
    public function getVisitorNumberApp()
    {
        return $this->visitorNumberApp;
    }

    /**
     * Add schedules
     *
     * @param \BackBundle\Entity\Schedule $schedule
     * @return Article
     */
    public function addSchedule(\BackBundle\Entity\Schedule $schedule)
    {
        $schedule->setArticle($this);
        $this->schedules[] = $schedule;

        return $this;
    }

    /**
     * Remove schedules
     *
     * @param \BackBundle\Entity\Schedule $schedule
     */
    public function removeSchedule(\BackBundle\Entity\Schedule $schedule)
    {
        $this->schedules->removeElement($schedule);
        $schedule->setArticle(null);
    }

    /**
     * Get schedules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchedules()
    {
        return $this->schedules;
    }

    /**
     * Add feature
     *
     * @param \BackBundle\Entity\Feature $feature
     * @return Article
     */
    public function addFeature(\BackBundle\Entity\Feature $feature)
    {
        $feature->setArticle($this);
        $this->features[] = $feature;

        return $this;
    }

    /**
     * Remove feature
     *
     * @param \BackBundle\Entity\Feature $feature
     */
    public function removeFeature(\BackBundle\Entity\Feature $feature)
    {
        $this->features->removeElement($feature);
        $feature->setArticle(null);
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Add slide
     *
     * @param \BackBundle\Entity\Slide $slide
     * @return Article
     */
    public function addSlide(\BackBundle\Entity\Slide $slide)
    {
        $slide->setArticle($this);
        $this->slides[] = $slide;

        return $this;
    }

    /**
     * Remove slide
     *
     * @param \BackBundle\Entity\Slide $slide
     */
    public function removeSlide(\BackBundle\Entity\Slide $slide)
    {
        $this->slides->removeElement($slide);
        $slide->setArticle(null);
    }

    /**
     * Get slides
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlides()
    {
        return $this->slides;
    }

    /**
     * Set location
     *
     * @param \BackBundle\Entity\Location $location
     * @return Article
     */
    public function setLocation(\BackBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \BackBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image2
     */
    public function setImageFile2(File $image2 = null)
    {
        $this->imageFile2 = $image2;

        if ($image2) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile2()
    {
        return $this->imageFile2;
    }

    /**
     * @param string $imageName2
     */
    public function setImageName2($imageName2)
    {
        $this->imageName2 = $imageName2;
    }

    /**
     * @return string
     */
    public function getImageName2()
    {
        return $this->imageName2;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image3
     */
    public function setImageFile3(File $image3 = null)
    {
        $this->imageFile3 = $image3;

        if ($image3) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile3()
    {
        return $this->imageFile3;
    }

    /**
     * @param string $imageName3
     */
    public function setImageName3($imageName3)
    {
        $this->imageName3 = $imageName3;
    }

    /**
     * @return string
     */
    public function getImageName3()
    {
        return $this->imageName3;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image4
     */
    public function setImageFile4(File $image4 = null)
    {
        $this->imageFile4 = $image4;

        if ($image4) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile4()
    {
        return $this->imageFile4;
    }

    /**
     * @param string $imageName4
     */
    public function setImageName4($imageName4)
    {
        $this->imageName4 = $imageName4;
    }

    /**
     * @return string
     */
    public function getImageName4()
    {
        return $this->imageName4;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image5
     */
    public function setImageFile5(File $image5 = null)
    {
        $this->imageFile5 = $image5;

        if ($image5) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile5()
    {
        return $this->imageFile5;
    }

    /**
     * @param string $imageName5
     */
    public function setImageName5($imageName5)
    {
        $this->imageName5 = $imageName5;
    }

    /**
     * @return string
     */
    public function getImageName5()
    {
        return $this->imageName5;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile6(File $image6 = null)
    {
        $this->imageFile6 = $image6;

        if ($image6) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile6()
    {
        return $this->imageFile6;
    }

    /**
     * @param string $imageName6
     */
    public function setImageName6($imageName6)
    {
        $this->imageName6 = $imageName6;
    }

    /**
     * @return string
     */
    public function getImageName6()
    {
        return $this->imageName6;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image7
     */
    public function setImageFile7(File $image7 = null)
    {
        $this->imageFile7 = $image7;

        if ($image7) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile7()
    {
        return $this->imageFile7;
    }

    /**
     * @param string $imageName7
     */
    public function setImageName7($imageName7)
    {
        $this->imageName7 = $imageName7;
    }

    /**
     * @return string
     */
    public function getImageName7()
    {
        return $this->imageName7;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image8
     */
    public function setImageFile8(File $image8 = null)
    {
        $this->imageFile8 = $image8;

        if ($image8) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile8()
    {
        return $this->imageFile8;
    }

    /**
     * @param string $imageName8
     */
    public function setImageName8($imageName8)
    {
        $this->imageName8 = $imageName8;
    }

    /**
     * @return string
     */
    public function getImageName8()
    {
        return $this->imageName8;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image9
     */
    public function setImageFile9(File $image9 = null)
    {
        $this->imageFile9 = $image9;

        if ($image9) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile9()
    {
        return $this->imageFile9;
    }

    /**
     * @param string $imageName9
     */
    public function setImageName9($imageName9)
    {
        $this->imageName9 = $imageName9;
    }

    /**
     * @return string
     */
    public function getImageName9()
    {
        return $this->imageName9;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image10
     */
    public function setImageFile10(File $image10 = null)
    {
        $this->imageFile10 = $image10;

        if ($image10) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile10()
    {
        return $this->imageFile10;
    }

    /**
     * @param string $imageName10
     */
    public function setImageName10($imageName10)
    {
        $this->imageName10 = $imageName10;
    }

    /**
     * @return string
     */
    public function getImageName10()
    {
        return $this->imageName10;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }
}

