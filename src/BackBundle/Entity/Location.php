<?php

namespace BackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\LocationRepository")
 */
class Location
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->features = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->banners = new ArrayCollection();
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="NewsGroup", inversedBy="locations")
     * @ORM\JoinColumn(name="newsgroup_id", referencedColumnName="id")
     */
    private $newsgroup;

    /**
     * @ORM\OneToMany(targetEntity="Feature", mappedBy="location", cascade={"persist", "remove"})
     */
    private $features;    

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="location", cascade={"persist", "remove"})
     */
    private $articles;    

    /**
     * @ORM\OneToMany(targetEntity="Banner", mappedBy="location", cascade={"persist", "remove"})
     */
    private $banners;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Location
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Location
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set newsgroup
     *
     * @param \BackBundle\Entity\NewsGroup $newsgroup
     * @return Location
     */
    public function setNewsGroup(\BackBundle\Entity\NewsGroup $newsgroup = null)
    {
        $this->newsgroup = $newsgroup;

        return $this;
    }

    /**
     * Get newsgroup
     *
     * @return \BackBundle\Entity\NewsGroup
     */
    public function getNewsGroup()
    {
        return $this->newsgroup;
    }

    /**
     * Add feature
     *
     * @param \BackBundle\Entity\Feature $feature
     * @return Location
     */
    public function addFeature(\BackBundle\Entity\Feature $feature)
    {
        $feature->setLocation($this);
        $this->features[] = $feature;

        return $this;
    }

    /**
     * Remove feature
     *
     * @param \BackBundle\Entity\Feature $feature
     */
    public function removeFeature(\BackBundle\Entity\Feature $feature)
    {
        $this->features->removeElement($feature);
        $feature->setLocation(null);
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatures()
    {
        return $this->features;
    }  

    /**
     * Add article
     *
     * @param \BackBundle\Entity\Article $article
     * @return Location
     */
    public function addArticle(\BackBundle\Entity\Article $article)
    {
        $article->setLocation($this);
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \BackBundle\Entity\Article $article
     */
    public function removeArticle(\BackBundle\Entity\Article $article)
    {
        $this->articles->removeElement($article);
        $article->setLocation(null);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }    

    /**
     * Add banner
     *
     * @param \BackBundle\Entity\Banner $banner
     * @return Location
     */
    public function addBanner(\BackBundle\Entity\Banner $banner)
    {
        $banner->setLocation($this);
        $this->banners[] = $banner;

        return $this;
    }

    /**
     * Remove banner
     *
     * @param \BackBundle\Entity\Banner $banner
     */
    public function removeBanner(\BackBundle\Entity\Banner $banner)
    {
        $this->banners->removeElement($banner);
        $banner->setLocation(null);
    }

    /**
     * Get banners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBanners()
    {
        return $this->banners;
    }  

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDescription();
    }
}

