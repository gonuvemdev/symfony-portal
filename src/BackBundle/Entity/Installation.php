<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Installation
 *
 * @ORM\Table(name="installation")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\InstallationRepository")
 */
class Installation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tokenDispositivo", type="text", nullable=true)
     */
    private $tokenDispositivo;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoDispositivo", type="string", length=255, nullable=true)
     */
    private $tipoDispositivo;

    /**
     * @var string
     *
     * @ORM\Column(name="versaoDispositivo", type="string", length=255, nullable=true)
     */
    private $versaoDispositivo;

    /**
     * @var string
     *
     * @ORM\Column(name="idInstalacao", type="string", length=255, nullable=true)
     */
    private $idInstalacao;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=255, nullable=true)
     */
    private $timezone;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=255, nullable=true)
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="identificadorApp", type="string", length=255, nullable=true)
     */
    private $identificadorApp;

    /**
     * @var string
     *
     * @ORM\Column(name="versaoApp", type="string", length=255, nullable=true)
     */
    private $versaoApp;

    /**
     * @var string
     *
     * @ORM\Column(name="nomeApp", type="string", length=255, nullable=true)
     */
    private $nomeApp;

    /**
     * @var string
     *
     * @ORM\Column(name="canal", type="string", length=255, nullable=true)
     */
    private $canal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="criadoEm", type="datetimetz", nullable=true)
     */
    private $criadoEm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atualizadoEm", type="datetimetz", nullable=true)
     */
    private $atualizadoEm;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="instalacaos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tokenDispositivo
     *
     * @param string $tokenDispositivo
     *
     * @return Installation
     */
    public function setTokenDispositivo($tokenDispositivo)
    {
        $this->tokenDispositivo = $tokenDispositivo;

        return $this;
    }

    /**
     * Get tokenDispositivo
     *
     * @return string
     */
    public function getTokenDispositivo()
    {
        return $this->tokenDispositivo;
    }

    /**
     * Set tipoDispositivo
     *
     * @param string $tipoDispositivo
     *
     * @return Installation
     */
    public function setTipoDispositivo($tipoDispositivo)
    {
        $this->tipoDispositivo = $tipoDispositivo;

        return $this;
    }

    /**
     * Get tipoDispositivo
     *
     * @return string
     */
    public function getTipoDispositivo()
    {
        return $this->tipoDispositivo;
    }

    /**
     * Set versaoDispositivo
     *
     * @param string $versaoDispositivo
     *
     * @return Installation
     */
    public function setVersaoDispositivo($versaoDispositivo)
    {
        $this->versaoDispositivo = $versaoDispositivo;

        return $this;
    }

    /**
     * Get versaoDispositivo
     *
     * @return string
     */
    public function getVersaoDispositivo()
    {
        return $this->versaoDispositivo;
    }

    /**
     * Set idInstalacao
     *
     * @param string $idInstalacao
     *
     * @return Installation
     */
    public function setIdInstalacao($idInstalacao)
    {
        $this->idInstalacao = $idInstalacao;

        return $this;
    }

    /**
     * Get idInstalacao
     *
     * @return string
     */
    public function getIdInstalacao()
    {
        return $this->idInstalacao;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     *
     * @return Installation
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Installation
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set identificadorApp
     *
     * @param string $identificadorApp
     *
     * @return Installation
     */
    public function setIdentificadorApp($identificadorApp)
    {
        $this->identificadorApp = $identificadorApp;

        return $this;
    }

    /**
     * Get identificadorApp
     *
     * @return string
     */
    public function getIdentificadorApp()
    {
        return $this->identificadorApp;
    }

    /**
     * Set versaoApp
     *
     * @param string $versaoApp
     *
     * @return Installation
     */
    public function setVersaoApp($versaoApp)
    {
        $this->versaoApp = $versaoApp;

        return $this;
    }

    /**
     * Get versaoApp
     *
     * @return string
     */
    public function getVersaoApp()
    {
        return $this->versaoApp;
    }

    /**
     * Set nomeApp
     *
     * @param string $nomeApp
     *
     * @return Installation
     */
    public function setNomeApp($nomeApp)
    {
        $this->nomeApp = $nomeApp;

        return $this;
    }

    /**
     * Get nomeApp
     *
     * @return string
     */
    public function getNomeApp()
    {
        return $this->nomeApp;
    }

    /**
     * Set canal
     *
     * @param string $canal
     *
     * @return Installation
     */
    public function setCanal($canal)
    {
        $this->canal = $canal;

        return $this;
    }

    /**
     * Get canal
     *
     * @return string
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set criadoEm
     *
     * @param \DateTime $criadoEm
     *
     * @return Installation
     */
    public function setCriadoEm($criadoEm)
    {
        $this->criadoEm = $criadoEm;

        return $this;
    }

    /**
     * Get criadoEm
     *
     * @return \DateTime
     */
    public function getCriadoEm()
    {
        return $this->criadoEm;
    }

    /**
     * Set atualizadoEm
     *
     * @param \DateTime $atualizadoEm
     *
     * @return Installation
     */
    public function setAtualizadoEm($atualizadoEm)
    {
        $this->atualizadoEm = $atualizadoEm;

        return $this;
    }

    /**
     * Get atualizadoEm
     *
     * @return \DateTime
     */
    public function getAtualizadoEm()
    {
        return $this->atualizadoEm;
    }

    /**
     * Set usuario
     *
     * @param \BackBundle\Entity\User $usuario
     * @return Installation
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \BackBundle\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}

