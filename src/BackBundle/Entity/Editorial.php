<?php

namespace BackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Editorial
 *
 * @ORM\Table(name="editorial")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\EditorialRepository")
 * @Vich\Uploadable
 */
class Editorial
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->banners = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="can_comment", type="boolean", nullable=true)
     */
    private $canComment;

    /**
     * @var bool
     *
     * @ORM\Column(name="featured", type="boolean", nullable=true)
     */
    private $featured;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="partner", type="boolean", nullable=true)
     */
    private $partner;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="editorial", cascade={"persist", "remove"})
     */
    private $articles;

    /**
     *
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Many Groups have Many Users.
     * @ORM\ManyToMany(targetEntity="Banner", mappedBy="editorials")
     */
    private $banners;


    /**
     * @var string
     *
     * @ORM\Column(name="friendly_editorial", type="text", nullable=true)
     *
     */
    private $friendlyEditorial;

    /**
     * @return string
     */
    public function getFriendlyEditorial()
    {
        return $this->friendlyEditorial;
    }

    /**
     * @param string $friendlyEditorial
     */
    public function setFriendlyEditorial($friendlyEditorial)
    {
        $this->friendlyEditorial = $friendlyEditorial;

        return $friendlyEditorial;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Editorial
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Editorial
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Editorial
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set canComment
     *
     * @param boolean $canComment
     *
     * @return Editorial
     */
    public function setCanComment($canComment)
    {
        $this->canComment = $canComment;

        return $this;
    }

    /**
     * Get canComment
     *
     * @return bool
     */
    public function getCanComment()
    {
        return $this->canComment;
    }

    /**
     * Set featured
     *
     * @param boolean $featured
     *
     * @return Editorial
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured
     *
     * @return bool
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Editorial
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set partner
     *
     * @param boolean $partner
     *
     * @return Editorial
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get partner
     *
     * @return bool
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Add article
     *
     * @param \BackBundle\Entity\Article $article
     * @return Editorial
     */
    public function addArticle(\BackBundle\Entity\Article $article)
    {
        $article->setEditorial($this);
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \BackBundle\Entity\Article $article
     */
    public function removeArticle(\BackBundle\Entity\Article $article)
    {
        $this->articles->removeElement($article);
        $article->setEditorial(null);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Add banner
     *
     * @param \BackBundle\Entity\Banner $banner
     * @return Editorial
     */
    public function addBanner(Banner $banner)
    {
        $banner->addEditorial($this);
        $this->banners[] = $banner;

        return $this;
    }

    /**
     * Remove banner
     *
     * @param \BackBundle\Entity\Banner $banner
     */
    public function removeBanner(Banner $banner)
    {
        $this->banners->removeElement($banner);
    }

    /**
     * Get $banners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBanners()
    {
        return $this->banners;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    public function friendlyAdressAction($title){
        $title = strtolower($title);
        $title = str_replace('ó', 'o', $title);
        $title = str_replace('ô', 'o', $title);
        $title = str_replace('ò', 'o', $title);
        $title = str_replace('õ', 'o', $title);
        $title = str_replace('ö', 'o', $title);
        $title = str_replace('á', 'a', $title);
        $title = str_replace('à', 'a', $title);
        $title = str_replace('ã', 'a', $title);
        $title = str_replace('â', 'a', $title);
        $title = str_replace('ä', 'a', $title);
        $title = str_replace('é', 'e', $title);
        $title = str_replace('è', 'e', $title);
        $title = str_replace('ê', 'e', $title);
        $title = str_replace('ë', 'e', $title);
        $title = str_replace('í', 'i', $title);
        $title = str_replace('î', 'i', $title);
        $title = str_replace('ï', 'i', $title);
        $title = str_replace('ú', 'u', $title);
        $title = str_replace('ù', 'u', $title);
        $title = str_replace('û', 'u', $title);
        $title = str_replace('ü', 'u', $title);
        $title = str_replace('ç', 'c', $title);
        $title = str_replace('\'', '', $title);
        $title = str_replace(' "', '', $title);
        $title = str_replace('"', '', $title);
        $title = str_replace(':', '', $title);
        $title = str_replace('!', '', $title);
        $title = str_replace('?', '', $title);
        $title = str_replace(' ', '-', $title);
        $title = strtolower($title);
        return $title;
    }
}

