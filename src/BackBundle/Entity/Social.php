<?php

namespace BackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Social
 *
 * @ORM\Table(name="social")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\SocialRepository")
 * @Vich\Uploadable
 */
class Social
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->socialgroups = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @Vich\UploadableField(mapping="arquivo_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\OneToMany(targetEntity="SocialGroup", mappedBy="social", cascade={"persist", "remove"})
     */
    private $socialgroups;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Social
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Add socialgroup
     *
     * @param \BackBundle\Entity\SocialGroup $socialgroup
     * @return Social
     */
    public function addSocialGroup(\BackBundle\Entity\SocialGroup $socialgroup)
    {
        $socialgroup->setSocial($this);
        $this->socialgroups[] = $socialgroup;

        return $this;
    }

    /**
     * Remove socialgroup
     *
     * @param \BackBundle\Entity\SocialGroup $socialgroup
     */
    public function removeSocialGroup(\BackBundle\Entity\SocialGroup $socialgroup)
    {
        $this->socialgroups->removeElement($socialgroup);
        $socialgroup->setSocial(null);
    }

    /**
     * Get socialgroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialGroups()
    {
        return $this->socialgroups;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDescription();
    }
}

