# Changelog
Todas as mudanças notaveis para este projeto serão documentadas neste arquivo

<br />

##  **2019-05-02**
---

### Alterado

* Visual da barra de pesquisa, agora contém o botão buscar para facilitar o uso.

### Adcionado

* Função de busca textual (FullText Search). A barra de busca está funcional, e o termo a ser pesquisado é procurado no título da notícia.

### Consertado

* O upload de imagens (Vich Uploader) estava utilizando um caminho absoluto, isso ocasionava que certas imagens não apareciam, pois estavam indo para pastas erradas.

<br />

##  **2019-04-27**
---

### Alterado
- Estrutura interna do servidor, agora o código está dividido por lançamento, o que facilita a alteração, inclusão de novas funcionalidades.

### Adcionado
- Botão de compartilhar para o Whatsapp.
- Integração de implantação automática.

### Consertado
- Os botões de compartilhamento que já estavam presente (Facebook, Twitter) agora estão funcionais.
