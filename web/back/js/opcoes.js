var $collectionHolder;

var $addItemLink = $('<a href="#" class="add_item_link">Adicionar Opções</a>');
var $newLinkLi = $('<div class="col-lg-12"></div>').append($addItemLink);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionHolder = $('div.collection');

    $collectionHolder.find('div.collectionOpcao').each(function() {
        addItemFormDeleteLink($(this));
    });

    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addItemLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see code block below)
        addItemForm($collectionHolder, $newLinkLi);
    });

});

function addItemForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '$$name$$' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<div class="opcao"></div>').append(newForm);

    $newLinkLi.before($newFormLi);

    addItemFormDeleteLink($newFormLi);

}


function addItemFormDeleteLink($itemFormLi) {
    var $removeFormA = $('<a href="#"><i class="fa fa-fw fa-trash-o"></i>Apagar Opção</a>');
    $itemFormLi.append($removeFormA);

    $removeFormA.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // remove the li for the tag form
        $itemFormLi.remove();
    });

    $itemFormLi.append('<br><br>');
}

