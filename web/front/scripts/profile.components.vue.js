// Esse arquivo deve ser chamado depois do vue.js


Vue.component('Button', {
    props: {
      isSmall: { type: Boolean, default: false },
      kind: { type: String, default: 'primary' },
      id: { type: String }
    },
  computed: {
      kindClass: function(){
        if(this.kind === 'secondary')
          return 'profile-button-secondary';
        if(this.kind === 'danger')
          return 'profile-button-danger';
        return ''
      },
      className: function() {
        var small = this.isSmall ? " profile-button-small " : "";
        return 'profile-button' + small + this.kindClass;
      }
    },
    template: "<button :class='className' id='id'> <slot /> </button>"
})

Vue.component('Subscribe', {
    props: {
      url: { type: String },
    },
    template: " \
        <div class='profile-subscribe'> \
            <img src='/front/ima ges/profile/subscribe.png' alt='Assine' /> \
            <p> \
                Você não possui nenhuma assinatura ainda! \
                Que tal ter acesso a conteúdos exclusivos? \
                Basta realizar a assinatura agora! \
            </p> \
            <a :href='url'><Button>Assinar!</Button></a> \
        </div> \
    "
})


Vue.component('Separator', {
    template: "<div class='profile-separator'></div>"
});


Vue.component('Input', {
    props: {
        type: { type: String, default: 'text' },
        value: { default: '' },
        name: String,
        label: String,
        isDisabled: { type: Boolean, default: false },
        placeholder: String,
        id: String
    },
    template: " \
        <div class='profile-login-input'> \
            <label :for='name'>{{ label }}</label> \
            <input :name='name' :type='type' :value='value' :disabled='isDisabled' :placeholder='placeholder' :id='id'> \
        </div> \
    "
});


Vue.component('InputCPF', {
    props: {
        type: { type: String, default: 'text' },
        value: { default: '' },
        name: String,
        label: String,
        isDisabled: { type: Boolean, default: false },
        id: String,
    },
    mounted: function(){
      function transformCPFValue(value){
        var separatorList = [[3, '.'], [3, '.'], [3, '-'], [2, '']];
        var clean = value.replace(/[^\d]/g, "").substr(0, 11);
        var value2 = "";
        var index = 0;
        for(var i = 0; i < separatorList.length; i++){
          var separator = separatorList[i][1];
          var len = separatorList[i][0];
          value2 += clean.substr(index, len);
          index += len;
          if(clean.length > index) value2 += separator;
          else break;
        }
        return value2;
      }

      function cpfMask(e){
        var value = e.target.value
        e.target.value = transformCPFValue(value);
      }

      var input = document.querySelector('input[name="'+this.name+'"]')
      input.placeholder="000.000.000-00"
      input.onkeyup = cpfMask
      input.value = transformCPFValue(this.value)
    },
    template: " \
        <Input :name='name' :label='label' :isDisabled='isDisabled' :id='id'/> \
    "
});


Vue.component('Title', {
    template: "<h5 class='profile-title'> <slot /> </h5>"
})


Vue.component('Link', {
    props: ['link'],
    template: "<a class='profile-link' :href='link'> <slot /> </a>"
})

