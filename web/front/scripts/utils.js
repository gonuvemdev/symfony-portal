$(document).ready(function () {
    $('#client_address_postalCode').change(searchByCep);
    // Inicializando o Dropdown de Cidades
    let dropdown = document.getElementById('client_address_city');
    dropdown.length = 0;

    let defaultOption = document.createElement('option');
    defaultOption.text = 'Escolha a cidade';
    dropdown.add(defaultOption);
    dropdown.selectedIndex = 0;

    // Setando as mascaras de DOB
    $('#client_dob').mask('00/00/0000');
    $('#card_holders_dob').mask('00/00/0000');

    // Setando a mascara de CEP
    $('#client_address_postalCode').mask('00000-000');

    // Se estiver com algum estado selecionado 
    getUFId($("#client_address_state").val(), 'value');
})

// Smooth scroll nos links dos passos
$(document).on('click', 'a[href^="#"]', function(event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 100
    }, 500);
})

var bindCardUtil = function () {
    document.getElementById('card_holders_dob').addEventListener('change', function() {
        mascara(this, mdata);
    });

    document.getElementById('card_holders_dob').addEventListener('keyup', function() {
        mascara(this, mdata);
    });
}

function showShareModal(uri) {
    window.open(
        uri,
        'social-share-dialog',
        'width=626, height=436'
    );
}

function phoneMask(o, f) {
    v_obj = o;
    v_fun = f;
    setTimeout("execmask()", 1);
}

function execmask(){
    v_obj.value = v_fun(v_obj.value)
}

function mtel(v){
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}

window.onload = function() {
    document.getElementById('client_phone').onkeyup = function() {
        var clean = this.value.replace(/\(|\)|\s|-/g, '')
        if(clean.length > 11){
            this.value = mtel(clean.substring(0,11));
            return;
        }
        phoneMask(this, mtel);
    }
}


// Mascará em Vanilla para Data
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function mdata(v){
    v=v.replace(/\D/g,"");
    v=v.replace(/(\d{2})(\d)/,"$1/$2");
    v=v.replace(/(\d{2})(\d)/,"$1/$2");

    v=v.replace(/(\d{2})(\d{2})$/,"$1$2");
    return v;
}

// Listener para recarregar as cidades quando trocar o UF
$('#client_address_state').change(function(){
    getUFId($('#client_address_state').val(), '')
})

/** ================ FUNÇÕES AUXILIARES PARA A PARTE DE ENDEREÇO ================== */

function searchByCep() {
    // Recuperando o input com o cep;
    var cepInput = $('#client_address_postalCode');

    //Expressão regular para validar o CEP.
    var validateCep = /[0-9]{5}-[\d]{3}/g;

    // Validando o CEP
    if(validateCep.test(cepInput.val())){
        //Preenche os campos com "..." enquanto consulta webservice.
        $("#client_address_district").val("...");
        $("#client_address_street").val("...");
        $("#client_address_state").val("...");
        $("#client_address_city").val("...");

        var cepVal = cepInput.val().replace('-', '');

        //Consulta o webservice viacep.com.br/
        $.getJSON("https://viacep.com.br/ws/"+ cepVal +"/json/?callback=?", function(dados) {
            if (!("erro" in dados)) {
                $("#client_address_district").val(dados.bairro);
                $("#client_address_street").val(dados.logradouro);
                $("#client_address_state").val(dados.uf);
                getUFId(dados.uf, dados.localidade);
            }
            else {
                //CEP pesquisado não foi encontrado.
                // limpa_formulário_cep();
                alert("CEP não encontrado.");
                $("#client_address_district").val("");
                $("#client_address_street").val("");
                $("#client_address_state").val("");
                $("#client_address_city").val("");
            }
        })
    }
}

function getUFId(UF, localidade){
    var url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados'

    const request = new XMLHttpRequest();
    request.open('GET', url, true);

    request.onload = function() {
    if (request.status === 200) {
        const data = JSON.parse(request.responseText);
        let option;
        for (let i = 0; i < data.length; i++) {
            if(data[i].sigla === UF) {
                populateCityDropdown(data[i].id, localidade);
                break;
            }
        }
    } else {
        // Reached the server, but it returned an error
    }   
    }

    request.onerror = function() {
        console.error('An error occurred fetching the JSON from ' + url);
    };

    request.send();
}

function populateCityDropdown(UFId, localidade) {
    let dropdown = document.getElementById('client_address_city');

    const url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados/' + UFId + '/municipios';

    const request = new XMLHttpRequest();
    request.open('GET', url, true);

    request.onload = function() {
    if (request.status === 200) {
        const data = JSON.parse(request.responseText);
        let option;
        $('#client_address_city').empty();
        for (let i = 0; i < data.length; i++) {
            option = document.createElement('option');
            option.text = data[i].nome;
            option.value = data[i].nome;
            dropdown.add(option);
        }
        $("#client_address_city").val(localidade);
    } else {
        // Reached the server, but it returned an error
    }   
    }

    request.onerror = function() {
        console.error('An error occurred fetching the JSON from ' + url);
    };

    request.send();
}