if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(getPosSuccess, getPosErr, {timeout:10000});
} else {
    console.error('Location not available');
}

function getPosSuccess(pos) {
    var geoLat = pos.coords.latitude.toFixed(5);
    var geoLng = pos.coords.longitude.toFixed(5);
    var geoAcc = pos.coords.accuracy.toFixed(1);
    let coords = {
        geoLat,
        geoLng,
        geoAcc
    }
    getCity(coords);
}

function getPosErr(err) {
    switch(err.code) {
        case err.PERMISSION_DENIED:
            console.error("User denied the request for GeoLocation");
            break;
        case err.POSITION_UNAVAILABLE:
            console.error("Location information is unavailable");
            break;
        case err.TIMEOUT:
            console.error("The request to get user location timed out.");
            break;
        default:
            console.error("An unknow error ocurred");
    }
}

function getCity(coords) {
    let city = "Teresina";
    var URLRequest = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${coords.geoLat}&lon=${coords.geoLng}`;
    $.get(URLRequest)
        .done(function(data) {
            let city = data.address.city;
            let state = data.address.state;
            let text = `${city}, ${state}`
            $('#city').text(text);
        })
        .fail(function() {
            return city;
        });
}