jQuery(document).ready(function ($) {

    $('.mobile_menu').click(function (event) {
        event.preventDefault();
        var _this = $('.paz-nav');
        if (_this.hasClass('inline-block')) {
            _this.removeClass('inline-block');
            $('.paz-overlay').removeClass('is-visible');
        } else {
            _this.addClass('inline-block');
        }
        
    });

    var MqL = 0;
    moveNavigation();
    $(window).on('resize', function () {
        (!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
    }
    );

    $('.paz-nav-trigger').on('click', function (event) {
        event.preventDefault();
        if ($('.paz-main-content').hasClass('nav-is-visible')) {
            closeNav();
            $('.paz-overlay').removeClass('is-visible');
        } else {
            $(this).addClass('nav-is-visible');
            $('.paz-primary-nav').addClass('nav-is-visible');
            $('.paz-main-header').addClass('nav-is-visible');
            $('.paz-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                $('body').addClass('overflow-hidden');
            }
            );
            toggleSearch('close');
            $('.paz-overlay').addClass('is-visible');
        }
    }
    );

    $('.paz-search-trigger').on('click', function (event) {
        event.preventDefault();
        toggleSearch();
        closeNav();
    }
    );

    $('.paz-overlay').on('swiperight', function () {
        if ($('.paz-primary-nav').hasClass('nav-is-visible')) {
            closeNav();
            $('.paz-overlay').removeClass('is-visible');
        }
    }
    );
    $('.nav-on-left .paz-overlay').on('swipeleft', function () {
        if ($('.paz-primary-nav').hasClass('nav-is-visible')) {
            closeNav();
            $('.paz-overlay').removeClass('is-visible');
        }
    }
    );
    $('.paz-overlay').on('click', function () {
        closeNav();
        toggleSearch('close')
        $('.paz-overlay').removeClass('is-visible');
    }
    );

    $('.paz-primary-nav').children('.has-children').children('a').on('click', function (event) {
        event.preventDefault();
    }
    );

    $('.has-children').children('a').on('click', function (event) {
        if (!checkWindowWidth())
            event.preventDefault();
        var selected = $(this);
        if (selected.next('ul').hasClass('is-hidden')) {

            selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
            selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
            $('.paz-overlay').addClass('is-visible');
        } else {
            selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
            $('.paz-overlay').removeClass('is-visible');
        }
        toggleSearch('close');
    }
    );

    $('.go-back').on('click', function () {
        $(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
    }
    );

    function closeNav() {
        $('.paz-nav-trigger').removeClass('nav-is-visible');
        $('.paz-main-header').removeClass('nav-is-visible');
        $('.paz-primary-nav').removeClass('nav-is-visible');
        $('.has-children ul').addClass('is-hidden');
        $('.has-children a').removeClass('selected');
        $('.moves-out').removeClass('moves-out');
        $('.paz-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
            $('body').removeClass('overflow-hidden');
        }
        );
    }

    function toggleSearch(type) {
        if (type == "close") {
            //close serach
            $('.paz-search').removeClass('is-visible');
            $('.paz-search-trigger').removeClass('search-is-visible');
            $('.paz-overlay').removeClass('search-is-visible');
        } else {
            //toggle search visibility
            $('.paz-search').toggleClass('is-visible');
            $('.paz-search-trigger').toggleClass('search-is-visible');
            $('.paz-overlay').toggleClass('search-is-visible');
            if ($(window).width() > MqL && $('.paz-search').hasClass('is-visible'))
                $('.paz-search').find('input[type="search"]');
            ($('.paz-search').hasClass('is-visible')) ? $('.paz-overlay').addClass('is-visible') : $('.paz-overlay').removeClass('is-visible');
        }
    }

    function checkWindowWidth() {
        var e = window
                ,
                a = 'inner';
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        if (e[a + 'Width'] >= MqL) {
            return true;
        } else {
            return false;
        }
    }

    function moveNavigation() {
        var navigation = $('.paz-nav');
        var desktop = checkWindowWidth();
        if (desktop) {
            navigation.detach();
            navigation.insertAfter('.paz-header-buttons');
        } else {
            navigation.detach();
            navigation.insertBefore('.paz-main-content');
        }
    }
}
);

// Slider internal
var sliderinternal = new PazSlider();

sliderinternal.control('arrows', {
    autohide: false,
    insertTo: '#pazslider--inter'
});
sliderinternal.control('bullets', {
    autohide: true,
    align: 'top',
    margin: 20,
    insertTo: '#pazslider--inter'
});
sliderinternal.control('timebar', {
    align: 'top',
    insertTo: '#pazslider--inter'
});

sliderinternal.setup('pazslider--inter', {
    width: 824,
    height: 540,
    space: 5,
    loop: true,
    swipe: true,
    preload: 3,
    autoplay: true,
    keyboard: true,
    view: 'parallaxMask',
    speed: 11,
    autoHeight: true,
    keyboard: true,
    filters: {opacity: 0.15}
});

// Our highlights
var ourhighlights = new PazSlider();

ourhighlights.control('arrows', {
    autohide: false,
    insertTo: '#ourhighlights'
});
ourhighlights.control('bullets', {
    autohide: false,
    align: 'bottom',
    margin: 25,
    insertTo: '#ourhighlights'
});

ourhighlights.setup('ourhighlights', {
    width: 565,
    height: 245,
    space: 302,
    loop: false,
    swipe: true,
    preload: 2,
    autoplay: true,
    view: 'wave',
    speed: 11,
    grabCursor: false
});

if (ourhighlights.api !== undefined) {
    ourhighlights.api.addEventListener(MSSliderEvent.CHANGE_START, function (event) {
        var api = event.target;
        var currentSlide = api.index();
        var allSlide = api.count();

        if (currentSlide == 0) {
            jQuery('.paz-nav-prev').addClass("paz-nav-disabled");
            // jQuery('.paz-nav-prev').removeClass( "paz-nav-prev" );
        } else {
            jQuery('.paz-nav-prev').removeClass("paz-nav-disabled");
        }

        if (allSlide == (currentSlide + 1)) {
            jQuery('.paz-nav-next').addClass("paz-nav-disabled");
            // jQuery('.paz-nav-next').removeClass( "paz-nav-next" );
        } else {
            jQuery('.paz-nav-next').removeClass("paz-nav-disabled");
        }

    });
}

var ourhighlights2 = new PazSlider();

ourhighlights2.control('arrows', {
    autohide: false,
    insertTo: '#ourhighlights2'
});
ourhighlights2.control('bullets', {
    autohide: false,
    align: 'bottom',
    margin: 25,
    insertTo: '#ourhighlights2'
});

ourhighlights2.setup('ourhighlights2', {
    width: 565,
    height: 445,
    space: 302,
    loop: false,
    swipe: true,
    preload: 2,
    autoplay: true,
    view: 'wave',
    speed: 11,
    grabCursor: false
});

if (ourhighlights2.api !== undefined) {
    ourhighlights2.api.addEventListener(MSSliderEvent.CHANGE_START, function (event) {
        var api = event.target;
        var currentSlide = api.index();
        var allSlide = api.count();

        if (currentSlide == 0) {
            jQuery('.paz-nav-prev').addClass("paz-nav-disabled");
            // jQuery('.paz-nav-prev').removeClass( "paz-nav-prev" );
        } else {
            jQuery('.paz-nav-prev').removeClass("paz-nav-disabled");
        }

        if (allSlide == (currentSlide + 1)) {
            jQuery('.paz-nav-next').addClass("paz-nav-disabled");
            // jQuery('.paz-nav-next').removeClass( "paz-nav-next" );
        } else {
            jQuery('.paz-nav-next').removeClass("paz-nav-disabled");
        }

    });
}

// Carousel vertical
var carousel = new PazSlider();

carousel.control('arrows', {
    autohide: false,
    insertTo: '#pazcarousel'
});
carousel.control('bullets', {
    autohide: false,
    align: 'bottom',
    margin: 0,
    insertTo: '#pazcarousel'
});

carousel.setup('pazcarousel', {
    width: 214,
    height: 76,
    space: 20,
    loop: true,
    swipe: false,
    preload: 1,
    autoplay: true,
    view: 'basic',
    speed: 11,
    wheel: true,
    dir: 'v',
    grabCursor: false,
    autoHeight: true
});

// Main slider
var slider = new PazSlider();

slider.control('arrows', {
    autohide: false,
    insertTo: '#pazslider'
});
slider.control('bullets', {
    autohide: true,
    align: 'top',
    margin: 20,
    insertTo: '#pazslider'
});
slider.control('timebar', {
    align: 'top',
    insertTo: '#pazslider'
});


/* allain */
var _out = $(window).outerWidth();
if (_out < 975) {
    var slider = new PazSlider();
    slider.setup('pazslider', {
        width: 345,
        height: 332,
        space: 5,
        loop: true,
        swipe: true,
        preload: 1,
        autoplay: true,
        keyboard: true,
        view: 'parallaxMask',
        speed: 11,
        filters: {
            opacity: 0.15
        }
    });
} else {
    slider.setup('pazslider', {
        width: 458,
        height: 332,
        space: 5,
        loop: true,
        swipe: true,
        preload: 1,
        autoplay: true,
        keyboard: true,
        view: 'parallaxMask',
        speed: 11,
        filters: {
            opacity: 0.15
        }
    });
}


$(function () {

});



// Carousel
$('.paz-cr--blog').portalazCarousel({
    dots: true,
    infinite: false,
    speed: 1000,
    autoplaySpeed: 10000,
    slidesToShow: 6,
    slidesToScroll: 6,
    pauseOnHover: true,
    cssEase: 'cubic-bezier(.42, 0, .58, 1)'
});

$('.paz-cr--column').portalazCarousel({
    dots: true,
    infinite: false,
    speed: 1000,
    autoplaySpeed: 10000,
    slidesToShow: 6,
    slidesToScroll: 6,
    pauseOnHover: true,
    cssEase: 'cubic-bezier(.42, 0, .58, 1)'
});

$('.paz-cr--cartaz').portalazCarousel({
    dots: true,
    infinite: false,
    speed: 1000,
    autoplaySpeed: 10000,
    slidesToShow: 4,
    slidesToScroll: 4,
    pauseOnHover: true,
    //  draggable: false,
    cssEase: 'cubic-bezier(.42, 0, .58, 1)'
});
