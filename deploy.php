<?php
namespace Deployer;

require 'recipe/symfony.php';
require 'scripts/rsync.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'https://pedro458@bitbucket.org/pedro458/linode-deploy-symfony.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false); 

// Set http_user
set('http_user', 'www-data');

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', ['web/arquivos']);

// Writable dirs by web server 
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Setting rsync variables
set('rsync_src', __DIR__);
set('rsync_dest','{{release_path}}');

set('rsync',[
    'exclude'      => [
        '.git',
        'deploy.php',
        'deploy-teste.php',
        '.docker',
        'docker-compose.yml'
    ],
    'exclude-file' => false,
    'include'      => [],
    'include-file' => false,
    'filter'       => [],
    'filter-file'  => false,
    'filter-perdir'=> false,
    'flags'        => 'rzc', // Recursive, with compress
    'options'      => ['delete'],
    'timeout'      => 3600,
]);


// Hosts

/** STAGING */
host('linode:staging')
    ->hostname('173.255.194.248')
    ->user('root')
    ->identityFile('~/.ssh/id_rsa')
    ->stage('staging')
    ->set('deploy_path', '/var/www')
    ->set('symfony_env', 'dev');   

/** PRODUCTION */
host('linode:production')
    ->hostname('45.56.120.38')
    ->user('root')
    ->identityFile('~/.ssh/id_rsa')
    ->stage('production')
    ->set('deploy_path', '/var/www/portalaz_go')
    ->set('symfony_env', 'prod');   
        

// Override bin/console directiory
set('bin_dir', 'bin');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// Overwriting tasks to work with project
task('deploy:vendors', function () {
    run('cd {{release_path}} && php composer.phar install -vvv');
});

// Skipped tasks to be equal to scrips in composer.json
task('deploy:cache:warmup', function() {
    writeln('Do nothing');
});

task('deploy:writable', function() {
    writeln('Do nothing');
});

task('deploy:update_code', function () {
    writeln('Do nothing');
});

// Task to give www-user permission
task('deploy:permissions:cache', function() {
    run('cd {{release_path}}/var && chown -R www-data:www-data cache/');
});

task('deploy:permissions:logs', function() {
    run('cd {{release_path}}/var && chown -R www-data:www-data logs/');
});

task('deploy:permissions:sessions', function() {
    run('cd {{release_path}}/var && chown -R www-data:www-data sessions/');
});

// Rewriting stack of tasks
after('deploy:update_code', 'rsync');
after('deploy:symlink', 'deploy:permissions:logs');
after('deploy:permissions:logs', 'deploy:permissions:cache');
after('deploy:permissions:cache', 'deploy:permissions:sessions');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

// before('deploy:symlink', 'database:migrate');

